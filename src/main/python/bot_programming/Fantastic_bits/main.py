import heapq
import math
import sys
import time
from dataclasses import dataclass, field
from enum import Enum, StrEnum, auto
from functools import wraps
from typing import Optional, List, Dict


# To debug: print("Debug messages...", file=sys.stderr, flush=True)

def timed(func=None, *, repeat=10):
    if func is None:
        return lambda f: timed(f, repeat=repeat)

    @wraps(func)
    def wrapper(*args, **kwargs):
        durations = []
        result = None
        for _ in range(repeat):
            start = time.perf_counter_ns()
            result = func(*args, **kwargs)
            end = time.perf_counter_ns()
            duration = (end - start) / 1_000_000  # Convert to milliseconds
            durations.append(duration)
        avg_duration = sum(durations) / repeat
        print(f"{func.__name__} took {avg_duration} milliseconds", file=sys.stderr, flush=True)
        return result

    return wrapper


def round_far_from_0(f: float):
    return int(f + math.copysign(0.5, f))


@dataclass(frozen=True)
class Vector:
    x: float
    y: float

    def __neg__(self) -> 'Vector':
        return Vector(-self.x, -self.y)

    def __add__(self, other: 'Vector') -> 'Vector':
        return Vector(self.x + other.x, self.y + other.y)

    def __sub__(self, other: 'Vector') -> 'Vector':
        return Vector(self.x - other.x, self.y - other.y)

    def __pow__(self, power: int, modulo=None) -> 'Vector':
        return Vector(math.pow(self.x, power), math.pow(self.y, power))

    def __mul__(self, scalar: float) -> 'Vector':
        return Vector(self.x * scalar, self.y * scalar)

    def __truediv__(self, factor: float) -> 'Vector':
        return Vector(self.x / factor, self.y / factor)

    def __round__(self, ndigits: int = None) -> 'Vector':
        return Vector(round(self.x, ndigits), round(self.y, ndigits))

    def round_far_from_0(self) -> 'Vector':
        return Vector(round_far_from_0(self.x), round_far_from_0(self.y))

    def __floor__(self) -> 'Vector':
        return Vector(math.floor(self.x), math.floor(self.y))

    def __trunc__(self) -> 'Vector':
        return Vector(math.trunc(self.x), math.trunc(self.y))

    def dot(self, other: 'Vector') -> float:
        return self.x * other.x + self.y * other.y

    def length_squared(self) -> float:
        return self.dot(self)

    def length(self) -> float:
        return math.sqrt(self.length_squared())

    def normalize(self, new_length: float = 1.0) -> 'Vector':
        length = self.length()
        return Vector(0, 0) if length == 0 else self * (new_length / length)

    def project(self, other: 'Vector') -> 'Vector':
        other_normalized = other.normalize()
        projection_length = self.dot(other_normalized)
        return other_normalized * projection_length

    def cross(self, other: 'Vector') -> float:
        return self.x * other.y - self.y * other.x

    def distance_squared(self, other) -> float:
        return (self - other).length_squared()

    def distance(self, other) -> float:
        return (self - other).length()

    def rotate(self, angle: float) -> 'Vector':
        sin_theta, cos_theta = math.sin(angle), math.cos(angle)
        return Vector(self.x * cos_theta - self.y * sin_theta,
                      self.x * sin_theta + self.y * cos_theta)

    def within_bounds(self, xmin: float, xmax: float, ymin: float, ymax: float) -> bool:
        return xmin <= self.x <= xmax and ymin <= self.y <= ymax

    def is_zero(self) -> bool:
        return self.x == 0 and self.y == 0

    @classmethod
    def from_angle(cls, angle):
        cos_a, sin_a = math.cos(angle), math.sin(angle)
        return cls(cos_a, sin_a)

    @classmethod
    def Zero(cls):
        return Vector(0, 0)

    def clamp(self, minx: float, miny: float, maxx: float, maxy: float):
        return Vector(max(minx, min(maxx, self.x)), max(miny, min(maxy, self.y)))


class CollisionType(Enum):
    NORMAL = "NORMAL"
    GRAB = "GRAB"
    IGNORE = "IGNORE"


class Physics:
    @classmethod
    def update_position_with_collisions(cls, entities: List['PhysicalEntity'], dt: float = 1.0) -> None:
        current_time = 0.0
        collisions = cls.get_all_collisions(entities, dt)

        while current_time < dt:
            print(f"colls {[(c.time, c.entity1.id, c.entity2.id) for c in collisions]}", file=sys.stderr, flush=True)
            collision_time = collisions[0].time if collisions else dt
            collisions_to_solve = []
            while collisions and collisions[0].time == collision_time:
                collisions_to_solve.append(heapq.heappop(collisions))

            for entity in entities:
                entity.update_position(collision_time - current_time)

            if not collisions_to_solve:
                break

            current_time = collision_time

            solved_entities: Dict[int, PhysicalEntity] = {}
            for collision in collisions_to_solve:
                cls.resolve_collision(collision.entity1, collision.entity2)
                if not solved_entities.__contains__(collision.entity1.id):
                    solved_entities[collision.entity1.id] = collision.entity1
                if not solved_entities.__contains__(collision.entity2.id):
                    solved_entities[collision.entity2.id] = collision.entity2

            pending_collisions = []
            for collision in collisions:
                if collision.entity1.id not in solved_entities and collision.entity2.id not in solved_entities:
                    heapq.heappush(pending_collisions, collision)
            collisions = pending_collisions

            for c in cls.get_new_collisions(list(solved_entities.values()), entities, dt - current_time):
                heapq.heappush(collisions, Collision(current_time + c.time, c.entity1, c.entity2))

    @classmethod
    def get_all_collisions(cls, entities: List['PhysicalEntity'], dt: float = 1.0) -> List['Collision']:
        collisions = []
        num_entities = len(entities)

        for i, entity1 in enumerate(entities):
            for j in range(i + 1, num_entities):
                entity2 = entities[j]
                collision = entity1.detect_collision(entity2, dt)
                if collision:
                    heapq.heappush(collisions, collision)

        return collisions

    @classmethod
    def resolve_collision(cls, entity1: 'PhysicalEntity', entity2: 'PhysicalEntity') -> None:
        collision_type_1 = entity1.collision_type(entity2)
        collision_type_2 = entity2.collision_type(entity1)

        if collision_type_1 == CollisionType.NORMAL or collision_type_2 == CollisionType.NORMAL:
            cls.resolve_normal_collision(entity1, entity2)
        elif collision_type_1 == CollisionType.GRAB and collision_type_2 == CollisionType.IGNORE:
            if entity1.can_grab(entity2):
                cls.grab_entity(entity1, entity2)
        elif collision_type_2 == CollisionType.GRAB and collision_type_1 == CollisionType.IGNORE:
            if entity2.can_grab(entity1):
                cls.grab_entity(entity2, entity1)
        elif collision_type_2 == CollisionType.IGNORE and collision_type_1 == CollisionType.IGNORE:
            pass
        else:
            raise Exception(f"Cannot resolve collision among {entity1} and {entity2}")

    @classmethod
    def resolve_normal_collision(cls, entity1: 'PhysicalEntity', entity2: 'PhysicalEntity') -> None:
        if isinstance(entity1, PhysicalCircle) and isinstance(entity2, PhysicalRectangle):
            reference_point = entity1.closest_point_to(entity2)
        elif isinstance(entity1, PhysicalRectangle) and isinstance(entity2, PhysicalCircle):
            reference_point = entity2.closest_point_to(entity1)
        else:
            reference_point = entity2.position

        direction = reference_point - entity1.position
        m1, m2 = cls.get_effective_masses(entity1, entity2)
        if m1 == math.inf:
            proj2 = entity2.speed.project(direction)
            entity2.speed -= proj2 * 2
        elif m2 == math.inf:
            proj1 = entity1.speed.project(direction)
            entity1.speed -= proj1 * 2
        elif m1 == m2:
            proj1 = entity1.speed.project(direction)
            proj2 = entity2.speed.project(direction)
            s_diff = proj2 - proj1

            entity1.speed += s_diff
            entity2.speed -= s_diff
        else:
            proj1 = entity1.speed.project(direction)
            proj2 = entity2.speed.project(direction)
            s_diff = proj2 - proj1
            avg_mass = (m1 + m2) / 2
            entity1.speed += s_diff * (m2 / avg_mass)
            entity2.speed -= s_diff * (m1 / avg_mass)

        entity1.collided(entity2)
        entity2.collided(entity1)
        if entity1.grabbed:
            cls.grab_entity(entity1, entity1.grabbed)

    @classmethod
    def get_effective_masses(cls, entity1: 'PhysicalEntity', entity2: 'PhysicalEntity'):
        m1 = entity1.mass if entity1.collision_type(entity2) != CollisionType.IGNORE else math.inf
        m2 = entity2.mass if entity2.collision_type(entity1) != CollisionType.IGNORE else math.inf
        return m1, m2

    @classmethod
    def grab_entity(cls, grabber: 'PhysicalEntity', grabbed: 'PhysicalEntity') -> None:
        grabbed.speed = grabber.speed
        grabbed.position = grabber.position
        grabber.collided(grabbed)
        grabbed.collided(grabber)

    @classmethod
    def get_new_collisions(cls, solved_collisions: List['PhysicalEntity'], entities: List['PhysicalEntity'],
                           dt: float = 1.0):
        new_collisions = []
        seen_pairs = set()
        for solved in solved_collisions:
            for entity in entities:
                pair = tuple(sorted([solved.id, entity.id]))
                if pair in seen_pairs:
                    continue  # skip this pair if it has been checked before
                seen_pairs.add(pair)
                collision = solved.detect_collision(entity, dt)
                if collision and collision.time > 0:
                    new_collisions.append(collision)
        return new_collisions

    @classmethod
    def circle_circle_collision(cls, circle1: 'PhysicalCircle', circle2: 'PhysicalCircle', dt: float = 1.0):
        collision_distance = cls.collision_distance(circle1, circle2)

        if circle1.position.distance_squared(circle2.position) <= collision_distance ** 2:
            return Collision(0, circle1, circle2)  # Already colliding

        rel_pos, rel_speed = circle2.position - circle1.position, circle2.speed - circle1.speed
        collision_time = cls.solve_collision_time(rel_pos, rel_speed, collision_distance, dt)
        return Collision(collision_time, circle1, circle2) if collision_time is not None else None

    @classmethod
    def circle_rect_collision(cls, circle: 'PhysicalCircle', rect: 'PhysicalRectangle', dt: float = 1.0):
        closest = circle.closest_point_to(rect)
        if circle.position.distance_squared(closest) <= circle.radius ** 2:
            return Collision(0, circle, rect)  # Already colliding

        rel_pos = circle.position - closest
        rel_speed = (circle.speed - rect.speed).project(rel_pos)
        collision_time = cls.solve_collision_time(rel_pos, rel_speed, circle.radius, dt)
        return Collision(collision_time, circle, rect) if collision_time is not None else None

    @staticmethod
    def collision_distance(entity1, entity2):
        type1, type2 = entity1.collision_type(entity2), entity2.collision_type(entity1)
        if type1 == CollisionType.GRAB and type2 == CollisionType.IGNORE:
            return entity1.radius - 1
        elif type1 == CollisionType.IGNORE and type2 == CollisionType.GRAB:
            return entity2.radius - 1
        else:
            return entity1.radius + entity2.radius

    @staticmethod
    def solve_collision_time(rel_pos, rel_speed, collision_distance, dt):
        a = rel_speed.length_squared()
        if a == 0:
            return None  # Not approaching

        b = 2 * rel_pos.dot(rel_speed)
        c = rel_pos.length_squared() - collision_distance ** 2
        discriminant = b ** 2 - 4 * a * c

        if discriminant < 0:
            return None  # No real roots, no collision

        sqrt_discriminant = math.sqrt(discriminant)
        t1 = (-b - sqrt_discriminant) / (2 * a)
        t2 = (-b + sqrt_discriminant) / (2 * a)

        collision_times = [t for t in (t1, t2) if 0 <= t <= dt]
        return min(collision_times) if collision_times else None


class EntityType(Enum):
    WIZARD = auto()
    OPPONENT_WIZARD = auto()
    SNAFFLE = auto()
    BLUDGER = auto()
    WALL = auto()
    GOAL = auto()


@dataclass(kw_only=True)
class Entity:
    id: int
    position: Vector = Vector.Zero()


@dataclass
class Collision:
    time: float
    entity1: 'PhysicalEntity'
    entity2: 'PhysicalEntity'

    def __lt__(self, other: 'Collision') -> bool:
        return self.time < other.time


@dataclass(kw_only=True)
class PhysicalEntity(Entity):
    speed: Vector = Vector.Zero()
    mass: float = field(default=1, init=False)
    friction: float = field(default=1, init=False)

    def update_position(self, dt: float) -> None:
        self.position += self.speed * dt

    def apply_friction(self) -> None:
        self.speed *= self.friction

    def apply_thrust(self, target: Vector, thrust: int):
        self.speed += (target - self.position).normalize(thrust / self.mass)

    def collision_type(self, other: 'PhysicalEntity') -> CollisionType:
        return CollisionType.NORMAL

    def can_grab(self, other: 'PhysicalEntity') -> bool:
        return False

    def detect_collision(self, other: 'PhysicalEntity', dt: float = 1.0) -> Optional[Collision]:
        collision_type_1 = self.collision_type(other)
        collision_type_2 = other.collision_type(self)

        could_collide = False
        if collision_type_1 == CollisionType.NORMAL or collision_type_2 == CollisionType.NORMAL:
            could_collide = self.is_close_to(other, dt)
        elif collision_type_1 == CollisionType.GRAB and collision_type_2 == CollisionType.IGNORE:
            if self.can_grab(other):
                could_collide = self.is_close_to(other, dt)
        elif collision_type_1 == CollisionType.IGNORE and collision_type_2 == CollisionType.GRAB:
            if other.can_grab(self):
                could_collide = self.is_close_to(other, dt)
        elif collision_type_1 == CollisionType.IGNORE and collision_type_2 == CollisionType.IGNORE:
            could_collide = False
        else:
            raise Exception(f"Cannot resolve collision among {self} and {other}")

        if could_collide:
            if isinstance(self, PhysicalCircle) and isinstance(other, PhysicalCircle):
                return Physics.circle_circle_collision(self, other, dt)
            elif isinstance(self, PhysicalCircle) and isinstance(other, PhysicalRectangle):
                return Physics.circle_rect_collision(self, other, dt)
            elif isinstance(self, PhysicalRectangle) and isinstance(other, PhysicalCircle):
                return Physics.circle_rect_collision(other, self, dt)
            else:
                raise Exception(f"Invalid PhysicalEntities {self.__class__} and {other.__class__}")
        else:
            return None

    def is_close_to(self, other: 'PhysicalEntity', dt: float = 1.0):
        minx1, maxx1, miny1, maxy1 = self.calculate_ranges(dt)
        minx2, maxx2, miny2, maxy2 = other.calculate_ranges(dt)

        return not (minx1 >= maxx2 or maxx1 <= minx2 or miny1 >= maxy2 or maxy1 <= miny2)

    def calculate_ranges(self, dt: float = 1.0) -> (float, float, float, float):
        pass

    def collided(self, other: 'PhysicalEntity'):
        pass


@dataclass(kw_only=True)
class PhysicalCircle(PhysicalEntity):
    radius: int = field(default=0.0, init=False)
    grabbed = None
    grabbed_by = None

    def calculate_ranges(self, dt: float = 1.0):
        future_pos = self.position + self.speed * dt

        return (min(self.position.x, future_pos.x) - self.radius, max(self.position.x, future_pos.x) + self.radius,
                min(self.position.y, future_pos.y) - self.radius, max(self.position.y, future_pos.y) + self.radius)

    def closest_point_to(self, rectangle: 'PhysicalRectangle'):
        half_width = rectangle.width / 2
        half_height = rectangle.height / 2
        minx, maxx = rectangle.position.x - half_width, rectangle.position.x + half_width
        miny, maxy = rectangle.position.y - half_height, rectangle.position.y + half_height

        return self.position.clamp(minx, miny, maxx, maxy)


@dataclass(kw_only=True)
class PhysicalRectangle(PhysicalEntity):
    width: float
    height: float

    def bounds(self):
        half_width = self.width / 2
        half_height = self.height / 2

        return (self.position.x - half_width, self.position.x + half_width,
                self.position.y - half_height, self.position.y + half_height)

    def calculate_ranges(self, dt: float = 1.0):
        if self.speed == Vector.Zero():
            return self.bounds()

        future_pos = self.position + self.speed * dt
        half_width = self.width / 2
        half_height = self.height / 2

        return (min(self.position.x, future_pos.x) - half_width, max(self.position.x, future_pos.x) + half_width,
                min(self.position.y, future_pos.y) - half_height, max(self.position.y, future_pos.y) + half_height)


@dataclass(kw_only=True)
class StatefulEntity(Entity):
    state: int


@dataclass
class Wizard(StatefulEntity, PhysicalCircle):
    team: int
    mass = 1
    radius = 400
    friction = 0.75
    recently_grabbed = 0

    def collision_type(self, other: 'PhysicalEntity'):
        if isinstance(other, Snaffle):
            return CollisionType.GRAB
        else:
            return CollisionType.NORMAL

    def can_grab(self, other: 'PhysicalEntity') -> bool:
        return self.recently_grabbed == 0

    def collided(self, other: 'PhysicalEntity'):
        if isinstance(other, Snaffle):
            self.state = 1
            self.grabbed = other
            self.recently_grabbed = 3


@dataclass
class Snaffle(StatefulEntity, PhysicalCircle):
    mass = 0.5
    radius = 150
    friction = 0.75
    is_active = True

    def collision_type(self, other: 'PhysicalEntity'):
        if isinstance(other, Snaffle) and (self.state == 1 or other.state == 1):
            return CollisionType.IGNORE
        elif isinstance(other, Wizard):
            return CollisionType.IGNORE
        elif isinstance(other, Goal):
            return CollisionType.IGNORE
        else:
            return CollisionType.NORMAL

    def collided(self, other: 'PhysicalEntity'):
        if isinstance(other, Wizard):
            self.state = 1


@dataclass
class Bludger(StatefulEntity, PhysicalCircle):
    mass = 8
    radius = 200
    friction = 0.9

    def collision_type(self, other: 'PhysicalEntity'):
        return CollisionType.NORMAL

    def collided(self, other: 'PhysicalEntity'):
        if isinstance(other, Wizard):
            self.state = other.id


@dataclass
class Goal(PhysicalRectangle):
    def collision_type(self, other: 'PhysicalEntity'):
        return CollisionType.IGNORE


@dataclass
class Wall(PhysicalRectangle):
    def collision_type(self, other: 'PhysicalEntity'):
        return CollisionType.IGNORE


@dataclass
class Pole(PhysicalCircle):
    radius = 300

    def collision_type(self, other: 'PhysicalEntity'):
        return CollisionType.IGNORE


class EntityFactory:
    team_ids = [[], []]

    def from_input(self, entity_input: str):
        [entity_id, entity_type, x, y, vx, vy, state] = entity_input.split()

        team_id = self.get_team_id(entity_id, x)
        if entity_type == EntityType.WIZARD.name:
            entity = Wizard(id=int(entity_id), position=Vector(int(x), int(y)), speed=Vector(int(vx), int(vy)),
                            state=int(state), team=team_id)
        elif entity_type == EntityType.OPPONENT_WIZARD.name:
            entity = Wizard(id=int(entity_id), position=Vector(int(x), int(y)), speed=Vector(int(vx), int(vy)),
                            state=int(state), team=team_id)
        elif entity_type == EntityType.SNAFFLE.name:
            entity = Snaffle(id=int(entity_id), position=Vector(int(x), int(y)), speed=Vector(int(vx), int(vy)),
                             state=int(state))
        elif entity_type == EntityType.BLUDGER.name:
            entity = Bludger(id=int(entity_id), position=Vector(int(x), int(y)), speed=Vector(int(vx), int(vy)),
                             state=int(state))
        else:
            raise ValueError(f"Unknown entity type: {entity_type}")

        return entity

    def get_team_id(self, entity_id, x):
        if self.team_ids[0].__contains__(entity_id):
            team = 0
        elif self.team_ids[1].__contains__(entity_id):
            team = 1
        else:
            team = 0 if int(x) < 16000 / 2 else 1
            self.team_ids[team].append(entity_id)
        return team


class ActionType(StrEnum):
    MOVE = "MOVE"
    THROW = "THROW"
    OBLIVIATE = "OBLIVIATE"
    PETRIFICUS = "PETRIFICUS"
    ACCIO = "ACCIO"
    FLIPENDO = "FLIPENDO"

    def is_spell(self):
        return self in [ActionType.OBLIVIATE, ActionType.PETRIFICUS, ActionType.ACCIO, ActionType.FLIPENDO]


@dataclass
class Action:
    action_type: ActionType
    target_id: int = None
    thrust: int = 0
    position: Vector = None
    message: str = None

    def __str__(self):
        if self.action_type.is_spell():
            return f"{self.action_type} {self.target_id}" + (f" {self.message}" if self.message else "")
        else:
            return f"{self.action_type} {int(self.position.x)} {int(self.position.y)} {self.thrust}" + (
                f" {self.message}" if self.message else "")


@dataclass(kw_only=True)
class Spell:
    wizard_id: int
    target_id: int
    active: int = field(default=0, init=False)
    type_: ActionType = field(init=False)
    order: int = field(init=False)
    cost: int = field(init=False)
    targets: list[EntityType] = field(default_factory=list, init=False)

    def is_active(self):
        return self.active > 0

    def activate(self, game):
        if self.active > 0:
            self.active -= 1

    @classmethod
    def build(cls, wizard_id, action: Action):
        if action.action_type == ActionType.PETRIFICUS:
            return Petrificus(wizard_id=wizard_id, target_id=action.target_id)
        elif action.action_type == ActionType.ACCIO:
            return Accio(wizard_id=wizard_id, target_id=action.target_id)
        elif action.action_type == ActionType.FLIPENDO:
            return Flippendo(wizard_id=wizard_id, target_id=action.target_id)
        elif action.action_type == ActionType.OBLIVIATE:
            return Obliviate(wizard_id=wizard_id, target_id=action.target_id)
        else:
            raise Exception(f"Unknown action type: {action.action_type}")


@dataclass(kw_only=True)
class Obliviate(Spell):
    type_ = ActionType.OBLIVIATE
    order = 3
    cost = 5
    active = 4
    targets = [EntityType.BLUDGER]

    def activate(self, game):
        super().activate(game)
        # TODO: implement this


@dataclass(kw_only=True)
class Petrificus(Spell):
    type_ = ActionType.PETRIFICUS
    order = 0
    cost = 10
    active = 1
    # TODO: should only target opponent wizards
    targets = [EntityType.BLUDGER, EntityType.SNAFFLE, EntityType.WIZARD]

    def activate(self, game):
        super().activate(game)
        game.entities[self.target_id].speed = Vector.Zero()


@dataclass(kw_only=True)
class Accio(Spell):
    type_ = ActionType.ACCIO
    order = 1
    cost = 15
    active = 6
    targets = [EntityType.BLUDGER, EntityType.SNAFFLE]

    def activate(self, game):
        super().activate(game)
        if not game.entities.__contains__(self.target_id):
            self.active = 0
            return

        wizard = game.entities[self.wizard_id]
        target = game.entities[self.target_id]

        distance_squared = wizard.position.distance_squared(target.position)
        if distance_squared == 0:
            self.active = 0
            return

        power = min(3000000000 / distance_squared, 1000)

        target.apply_thrust(target.position + (wizard.position - target.position), power)


@dataclass(kw_only=True)
class Flippendo(Spell):
    type_ = ActionType.FLIPENDO
    order = 2
    cost = 20
    active = 3
    # TODO: should only target opponent wizards
    targets = [EntityType.BLUDGER, EntityType.SNAFFLE, EntityType.WIZARD]

    def activate(self, game):
        super().activate(game)
        if not game.entities.__contains__(self.target_id):
            self.active = 0
            return

        wizard = game.entities[self.wizard_id]
        target = game.entities[self.target_id]

        power = min(6000000000 / wizard.position.distance_squared(target.position), 1000)

        target.apply_thrust(target.position + (target.position - wizard.position), power)


@dataclass
class Game:
    player1: 'Player'
    player2: 'Player'
    active_spells: list[Spell] = field(default_factory=list)
    turn: int = 0
    snaffles: list[int] = field(default_factory=list)
    bludgers: list[int] = field(default_factory=list)
    wizards: dict[int, list[int]] = field(default_factory=dict)
    entities: dict = field(default_factory=dict)

    def simulate(self, actions: dict[int, list[Action]]):
        active_spells = sorted([s for s in self.active_spells if s.is_active()], key=lambda x: x.order)

        for team_id in actions.keys():
            player_actions = actions[team_id]
            for idx, action in enumerate(player_actions):
                wizard_id = self.wizards[team_id][idx]
                if action.action_type.is_spell():
                    spell = Spell.build(wizard_id, action)
                    if self.player1.team_id == team_id:
                        self.player1.magic -= spell.cost
                    else:
                        self.player2.magic -= spell.cost
                    self.active_spells.append(spell)
                elif action.action_type == ActionType.MOVE:
                    wizard: Wizard = self.entities[wizard_id]
                    wizard.apply_thrust(action.position, action.thrust)
                elif action.action_type == ActionType.THROW:
                    wizard: Wizard = self.entities[wizard_id]
                    if wizard.state == 1:
                        snaffle: Snaffle = [self.entities[snaffle_id] for snaffle_id in self.snaffles if
                                            self.entities[snaffle_id].position == wizard.position][0]
                        snaffle.apply_thrust(action.position, action.thrust)
                        wizard.state = 0
                        snaffle.state = 0

        if self.player1.magic < 100:
            self.player1.magic += 1
        if self.player2.magic < 100:
            self.player2.magic += 1

        for bludger_id in self.bludgers:
            bludger = self.entities[bludger_id]
            closest_wizard = min(
                [self.entities[wizard_id] for wizard_id in self.wizards[0] + self.wizards[1] if
                 wizard_id != bludger.state],
                key=lambda wizard: bludger.position.distance(wizard.position)
            )
            bludger.apply_thrust(closest_wizard.position, 1000)

        for spell in active_spells:
            spell.activate(game)
            if not spell.is_active():
                self.active_spells.remove(spell)

        Physics.update_position_with_collisions(list(self.entities.values()))

        for entity in self.entities.values():
            entity.apply_friction()
            entity.speed = entity.speed.round_far_from_0()
            entity.position = entity.position.round_far_from_0()

            if isinstance(entity, Snaffle) and self.player1.team_id == 0 and entity.position.x >= 16000:
                self.player1.score += 1
            elif isinstance(entity, Snaffle) and self.player1.team_id == 1 and entity.position.x <= 0:
                self.player1.score += 1

            if isinstance(entity, Snaffle) and self.player2.team_id == 0 and entity.position.x >= 16000:
                self.player2.score += 1
            elif isinstance(entity, Snaffle) and self.player2.team_id == 1 and entity.position.x <= 0:
                self.player2.score += 1

    def add_walls(self, minx: float, miny: float, maxx: float, maxy: float):
        width = maxx - minx
        height = maxy - miny
        width_mid = width / 2
        height_mid = height / 2
        center_y = miny + height_mid
        wall_h = height_mid - 2000
        upper_wall_y = miny + wall_h / 2
        lower_wall_y = miny + height - wall_h / 2

        self.entities[-1] = Goal(id=-1, position=Vector(minx, center_y), width=0, height=4000)
        self.entities[-2] = Goal(id=-2, position=Vector(maxx, center_y), width=0, height=4000)
        self.entities[-3] = Wall(id=-3, position=Vector(minx, upper_wall_y), width=0, height=wall_h)
        self.entities[-4] = Wall(id=-4, position=Vector(minx, lower_wall_y), width=0, height=wall_h)
        self.entities[-5] = Wall(id=-5, position=Vector(maxx, upper_wall_y), width=0, height=wall_h)
        self.entities[-6] = Wall(id=-6, position=Vector(maxx, lower_wall_y), width=0, height=wall_h)
        self.entities[-7] = Wall(id=-7, position=Vector(width_mid, miny), width=width, height=0)
        self.entities[-8] = Wall(id=-8, position=Vector(width_mid, maxy), width=width, height=0)
        self.entities[-9] = Pole(id=-9, position=Vector(minx, miny + wall_h))
        self.entities[-10] = Pole(id=-10, position=Vector(minx, miny + height - wall_h))
        self.entities[-11] = Pole(id=-11, position=Vector(maxx, miny + wall_h))
        self.entities[-12] = Pole(id=-12, position=Vector(maxx, miny + height - wall_h))

    def update_turn(self, my_score, enemy_score, my_magic, enemy_magic):
        self.turn += 1
        game.update_scores(my_score, enemy_score)
        game.update_magic(my_magic, enemy_magic)

    def update_scores(self, player1_score, player2_score):
        if self.player1.score != player1_score:
            print(f"SCORE ERR! {self.player1.team_id} {self.player1.score} != {player1_score}", file=sys.stderr,
                  flush=True)
            self.player1.score = player1_score
        if self.player2.score != player2_score:
            print(f"SCORE ERR! {self.player2.team_id} {self.player2.score} != {player2_score}", file=sys.stderr,
                  flush=True)
            self.player2.score = player2_score

    def update_magic(self, player1_magic, player2_magic):
        if self.player1.magic != my_magic:
            print(f"MAGIC ERR!! {self.player1.magic} != {player1_magic}", file=sys.stderr, flush=True)
            self.player1.magic = player1_magic
        if self.player1.magic != my_magic:
            print(f"MAGIC ERR!! {self.player2.magic} != {player2_magic}", file=sys.stderr, flush=True)
            self.player2.magic = player2_magic

    def update_entity(self, entity: PhysicalEntity | StatefulEntity):
        if not self.entities.__contains__(entity.id):
            self.entities[entity.id] = entity
        else:
            if self.entities[entity.id].speed != entity.speed:
                print(f"{entity.id} SPEED ERR!! {self.entities[entity.id].speed} != {entity.speed}",
                      file=sys.stderr, flush=True)
                self.entities[entity.id].speed = entity.speed
            if self.entities[entity.id].position != entity.position:
                print(f"{entity.id} POSITION ERR!! {self.entities[entity.id].position} != {entity.position}",
                      file=sys.stderr, flush=True)
                self.entities[entity.id].position = entity.position
            if self.entities[entity.id].state != entity.state:
                print(f"{entity.id} STATUS ERR!! {self.entities[entity.id].state} != {entity.state}",
                      file=sys.stderr, flush=True)
                self.entities[entity.id].state = entity.state

        if isinstance(entity, Snaffle):
            self.entities[entity.id].is_active = True
            if entity.grabbed_by:
                entity.grabbed_by = None
        if isinstance(entity, Wizard):
            wizard = self.entities[entity.id]
            if wizard.recently_grabbed > 0:
                wizard.recently_grabbed -= 1
                wizard.grabbed = None

        if self.turn == 1:
            if isinstance(entity, Wizard):
                if self.wizards.__contains__(entity.team):
                    self.wizards[entity.team].append(entity.id)
                else:
                    self.wizards[entity.team] = [entity.id]
            elif isinstance(entity, Snaffle):
                self.snaffles.append(entity.id)
            elif isinstance(entity, Bludger):
                self.bludgers.append(entity.id)
            else:
                raise Exception("Unknown entity type")

    def update(self):
        snaffles_out = []
        for entity in self.entities.values():
            if isinstance(entity, Snaffle):
                if entity.is_active:
                    entity.is_active = False
                else:
                    snaffles_out.append(entity.id)
        for snaffle_id in snaffles_out:
            print(f"removing snaffle {snaffle_id}", file=sys.stderr, flush=True)

            self.entities.pop(snaffle_id)
            self.snaffles.remove(snaffle_id)


@dataclass
class Player:
    team_id: int
    score: int = 0
    magic: int = 0

    def decide(self, game: Game):
        objectives = []
        magic = False
        actions = []

        for idx, wizard_id in enumerate(game.wizards[self.team_id]):
            wizard = game.entities[wizard_id]
            other_wizard = game.entities[game.wizards[self.team_id][1]] if idx == 0 else game.entities[
                game.wizards[self.team_id][0]]
            my_goal = Vector(16000, 3750) if self.team_id == 1 else Vector(0, 3750)
            if wizard.state == 0:
                closest_snaffle = None
                closest_distance = math.inf
                saveable_snaffle = None
                speedy_snaffle = None

                for snaffle_id in game.snaffles:
                    snaffle = game.entities[snaffle_id]
                    next_position = snaffle.position + snaffle.speed if isinstance(snaffle, Snaffle) else None
                    if isinstance(snaffle, Snaffle) and (
                            len(game.snaffles) == 1 or (not objectives.__contains__(snaffle))):
                        distance = wizard.position.distance(snaffle.position)
                        other_distance = other_wizard.position.distance(snaffle.position)
                        if distance < closest_distance and distance < other_distance:
                            closest_distance = distance
                            closest_snaffle = snaffle
                    if (isinstance(snaffle, Snaffle) and
                            saveable_snaffle is None and
                            ((self.team_id == 0 and 0 < next_position.x <= 100) or
                             (self.team_id == 1 and 16000 > next_position.x >= 15900)) and
                            snaffle.speed.length() > 10):
                        saveable_snaffle = snaffle
                    if (isinstance(snaffle, Snaffle) and
                        speedy_snaffle is None and (
                                (self.team_id == 0 and snaffle.speed.x < 0 and next_position.x > 9000) or
                                (self.team_id == 1 and snaffle.speed.x > 0 and next_position.x < 7000)) and
                        snaffle.speed.length() > 1000) and (
                            next_position.x > 0
                    ):
                        speedy_snaffle = snaffle

                if closest_snaffle is None:
                    closest_snaffle = game.entities[game.snaffles[0]]
                    closest_distance = wizard.position.distance(closest_snaffle.position)

                if not magic and (saveable_snaffle is not None) and self.magic >= 10:
                    actions.append(Action(ActionType.PETRIFICUS, target_id=saveable_snaffle.id, message="SAVED!"))
                    magic = True
                elif not magic and (speedy_snaffle is not None) and self.magic >= 10:
                    actions.append(Action(ActionType.PETRIFICUS, target_id=speedy_snaffle.id, message="STOP THERE!"))
                    magic = True
                elif not magic and 1500 < closest_distance < 8000 and (
                        (self.team_id == 0 and closest_snaffle.position.x < wizard.position.x) or (
                        self.team_id == 1 and closest_snaffle.position.x > wizard.position.x)) and self.magic >= 15:
                    actions.append(Action(ActionType.ACCIO, target_id=closest_snaffle.id))
                    magic = True
                elif not magic and ((self.team_id == 0 and closest_snaffle.position.x > wizard.position.x + 1000) or (
                        self.team_id == 1 and closest_snaffle.position.x + 1000 < wizard.position.x)) and self.magic >= 20:
                    actions.append(Action(ActionType.FLIPENDO, target_id=closest_snaffle.id))
                    magic = True
                else:
                    position_x = closest_snaffle.position.x - 100 if self.team_id == 0 else closest_snaffle.position.x + 100
                    position_y = closest_snaffle.position.y
                    actions.append(Action(ActionType.MOVE, position=Vector(position_x, position_y), thrust=150))
                objectives.append(closest_snaffle)
            else:
                enemys_goal = Vector(16000, 3750) if self.team_id == 0 else Vector(0, 3750)
                wizard_distance = enemys_goal.distance(wizard.position)
                other_wizard_distance = enemys_goal.distance(other_wizard.position)
                if wizard_distance < other_wizard_distance + 3000:
                    actions.append(Action(ActionType.THROW, position=enemys_goal, thrust=500))
                else:
                    position_x = other_wizard.position.x + 100 if self.team_id == 0 else other_wizard.position.x - 100
                    actions.append(
                        Action(ActionType.THROW, position=Vector(position_x, other_wizard.position.y), thrust=500,
                               message="Go on! Mate!"))

        return actions


team_id_ = int(input())  # if 0 you need to score on the right of the map, if 1 you need to score on the left
other_team = 0 if team_id_ == 1 else 1
game = Game(Player(team_id_), Player(other_team))

game.add_walls(0, 0, 16000, 7500)
entity_factory = EntityFactory()
next_state = None
while True:
    my_score, my_magic = [int(i) for i in input().split()]
    enemy_score, enemy_magic = [int(i) for i in input().split()]
    number_of_entities = int(input())  # number of entities still in game
    game.update_turn(my_score, enemy_score, my_magic, enemy_magic)

    for i in range(number_of_entities):
        inp = input()
        game.update_entity(entity_factory.from_input(inp))
    game.update()

    actions1 = game.player1.decide(game)
    actions2 = game.player2.decide(game)

    game.simulate({
        game.player1.team_id: actions1,
        game.player2.team_id: actions2,
    })

    for action in actions1:
        print(action)
