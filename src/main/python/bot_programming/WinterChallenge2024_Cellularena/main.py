import sys
from dataclasses import dataclass, field, is_dataclass
from functools import lru_cache
from typing import List, Dict, Optional, Set


@dataclass
class Position:
    x: int
    y: int

    def __hash__(self):
        return hash((self.x, self.y))

    def __eq__(self, other):
        if not isinstance(other, Position):
            return False
        return self.x == other.x and self.y == other.y


@dataclass
class Entity:
    position: Position


@dataclass
class WallEntity(Entity):
    pass


@dataclass
class ProteinEntity(Entity):
    type: str


@dataclass
class Organ(Entity):
    organ_id: int
    direction: str
    parent_id: int
    root_id: int


@dataclass
class RootOrgan(Organ):
    pass


@dataclass
class SporerOrgan(Organ):
    pass


@dataclass
class TentacleOrgan(Organ):
    pass


@dataclass
class HarvesterOrgan(Organ):
    pass


@dataclass
class BasicOrgan(Organ):
    pass


@dataclass
class Organism:
    root_id: int
    organs: Dict[Position, Organ] = field(default_factory=dict)

    def add_organ(self, organ: Organ):
        self.organs[organ.position] = organ


class DirectionUtils:
    @staticmethod
    @lru_cache(maxsize=None)
    def direction_to(source: Position, target: Position):
        dx, dy = target.x - source.x, target.y - source.y
        if abs(dx) > abs(dy):
            return "E" if dx > 0 else "W"
        return "S" if dy > 0 else "N"

    @staticmethod
    @lru_cache(maxsize=None)
    def distance_to(source: Position, target: Position):
        return abs(target.x - source.x) + abs(target.y - source.y)

    @staticmethod
    @lru_cache(maxsize=None)
    def get_neighbors(position: Position, distance=1):
        return {Position(position.x + dx, position.y + dy) for dx in range(-distance, distance + 1)
                for dy in range(-distance + abs(dx), distance + 1 - abs(dx))}


class TargetFinder:
    @staticmethod
    def closest_target(position: Position, targets):
        if not targets:
            return None
        return min(targets, key=lambda t: DirectionUtils.distance_to(position, t))

    @staticmethod
    def find_shootable_target(state, position: Position, direction, min_distance=4):
        best_shot = (None, Position(-1, -1))
        for distance in range(1, max(state.width, state.height)):
            target = Position(
                position.x + ((direction == "E") - (direction == "W")) * distance,
                position.y + ((direction == "S") - (direction == "N")) * distance
            )
            if not (0 <= target.x < state.width and 0 <= target.y < state.height):
                break
            if target in state.occupied_positions or target in state.attacked_positions:
                break
            if target in state.proteins:
                continue
            if distance < min_distance:
                continue
            close_organs = DirectionUtils.get_neighbors(target, 4) & state.my_organs.keys()
            is_next_to_enemy = DirectionUtils.get_neighbors(target) & state.enemy_organs.keys()
            if is_next_to_enemy or close_organs:
                continue
            best_shot = (distance, target)

        return best_shot


@dataclass
class ResourceManager:
    resources: Dict[str, int]
    resource_costs = {
        "BASIC": ['A'],
        "TENTACLE": ['B', 'C'],
        "HARVESTER": ['C', 'D'],
        "SPORER": ['B', 'D'],
        "SHOOT": ['A', 'B', 'C', 'D'],
    }

    def has_resources(self, type_: str) -> bool:
        cost = self.resource_costs.get(type_)
        return all(self.resources.get(r, 0) > 0 for r in cost)

    def consume_resources(self, type_: str):
        cost = self.resource_costs.get(type_)
        for r in cost:
            self.resources[r] -= 1


@dataclass
class GameState:
    width: int
    height: int
    required_actions: Optional[int] = None
    my_organisms: Dict[int, Organism] = field(default_factory=dict)
    enemy_organisms: Dict[int, Organism] = field(default_factory=dict)
    proteins: Dict[Position, ProteinEntity] = field(default_factory=dict)
    my_organs: Dict[Position, Organ] = field(default_factory=dict)
    enemy_organs: Dict[Position, Organ] = field(default_factory=dict)
    walls: Dict[Position, WallEntity] = field(default_factory=dict)
    resources: Dict[str, int] = field(default_factory=dict)
    enemy_resources: Dict[str, int] = field(default_factory=dict)
    used_organisms: Set = field(default_factory=set)
    used_positions: Set = field(default_factory=set)

    def is_organism_used(self, root_id: int) -> bool:
        return root_id in self.used_organisms

    def is_position_used(self, pos: Position) -> bool:
        return pos in self.used_positions

    def mark_organism_as_used(self, root_id: int, pos: Position):
        self.used_organisms.add(root_id)
        self.used_positions.add(pos)

    @property
    def resource_manager(self) -> ResourceManager:
        return ResourceManager(self.resources)

    @property
    def attacked_positions(self) -> Set[Position]:
        return {
            Position(position.x + (organ.direction == "E") - (organ.direction == "W"),
                     position.y + (organ.direction == "S") - (organ.direction == "N"))
            for position, organ in self.enemy_organs.items()
            if isinstance(organ, TentacleOrgan)
        }

    @property
    def occupied_positions(self) -> Set[Position]:
        return self.walls.keys() | self.my_organs.keys() | self.enemy_organs.keys()

    @property
    def harvested_proteins(self) -> Dict[Position, ProteinEntity]:
        harvested_positions = {
            Position(position.x + (organ.direction == "E") - (organ.direction == "W"),
                     position.y + (organ.direction == "S") - (organ.direction == "N"))
            for position, organ in self.my_organs.items()
            if isinstance(organ, HarvesterOrgan)
        }

        return {pos: protein for pos, protein in self.proteins.items() if pos in harvested_positions}


class Command:
    priority: int = 0

    def handle_growth(self, organ: Organ, position: Position, direction: Optional[str], organ_type: str,
                      state: GameState, commands: List[str], ignore_proteins: bool = False) -> bool:
        if not state.resource_manager.has_resources(organ_type.upper()):
            return False

        if not (0 <= position.x < state.width and 0 <= position.y < state.height):
            return False

        if position in state.occupied_positions or position in state.attacked_positions:
            return False

        if not ignore_proteins and position in state.proteins:
            return False

        command = f"GROW {organ.organ_id} {position.x} {position.y} {organ_type.upper()}"
        if direction:
            command += f" {direction}"

        commands.append(command)
        state.resource_manager.consume_resources(organ_type.upper())
        state.mark_organism_as_used(organ.root_id or organ.organ_id, position)
        return True

    def execute(self, state: GameState, commands: List[str]):
        pass


class TentacleManager(Command):
    priority = 1

    def execute(self, state: GameState, commands: List[str]):
        for organism in state.my_organisms.values():
            if state.is_organism_used(organism.root_id):
                continue

            for organ in organism.organs.values():
                if state.is_organism_used(organism.root_id):
                    continue
                for neighbor_pos in DirectionUtils.get_neighbors(organ.position):
                    if state.is_position_used(neighbor_pos):
                        continue
                    nearby_enemies = DirectionUtils.get_neighbors(neighbor_pos) & state.enemy_organs.keys()
                    for enemy_pos in nearby_enemies:
                        if state.is_organism_used(organism.root_id):
                            continue
                        direction = DirectionUtils.direction_to(neighbor_pos, enemy_pos)
                        if self.handle_growth(organ, neighbor_pos, direction, "TENTACLE", state, commands, True):
                            break
                        if self.handle_growth(organ, neighbor_pos, None, "BASIC", state, commands, True):
                            break
                        if self.handle_growth(organ, neighbor_pos, direction, "HARVESTER", state, commands, True):
                            break
                        if self.handle_growth(organ, neighbor_pos, direction, "SPORER", state, commands, True):
                            break


class ShootSporerManager(Command):
    priority = 2

    def execute(self, state: GameState, commands: List[str]):
        for organism in state.my_organisms.values():
            if state.is_organism_used(organism.root_id):
                continue
            for organ in organism.organs.values():
                if isinstance(organ, SporerOrgan):
                    distance, target = TargetFinder.find_shootable_target(state, organ.position, organ.direction)
                    if distance and state.resource_manager.has_resources("SHOOT"):
                        if state.is_position_used(target):
                            continue
                        commands.append(f"SPORE {organ.organ_id} {target.x} {target.y}")
                        state.resource_manager.consume_resources("SHOOT")
                        state.mark_organism_as_used(organ.root_id, target)
                        break


class HarvesterManager(Command):
    priority = 3

    def execute(self, state: GameState, commands: List[str]):
        for organism in state.my_organisms.values():
            if state.is_organism_used(organism.root_id):
                continue

            for organ in organism.organs.values():
                if state.is_organism_used(organism.root_id):
                    continue
                for neigh_pos in DirectionUtils.get_neighbors(organ.position):
                    if state.is_position_used(neigh_pos):
                        continue
                    nearby_proteins = (DirectionUtils.get_neighbors(neigh_pos)
                                       & state.proteins.keys()) - state.harvested_proteins.keys()
                    if nearby_proteins:
                        target = min(nearby_proteins,
                                     key=lambda p: (state.resources.get(state.proteins.get(p).type) != 0 + 1) *
                                                   state.proteins.get(p).type[0])
                        direction = DirectionUtils.direction_to(neigh_pos, target)
                        if self.handle_growth(organ, neigh_pos, direction, "HARVESTER", state, commands):
                            break


class GrowSporerManager(Command):
    priority = 4

    def execute(self, state: GameState, commands: List[str]):
        directions = ["N", "S", "E", "W"]
        possible_directions = []
        for organism in state.my_organisms.values():
            if state.is_organism_used(organism.root_id):
                continue

            best_direction_o = None
            for organ in organism.organs.values():
                if state.is_organism_used(organism.root_id):
                    continue
                for neigh_pos in DirectionUtils.get_neighbors(organ.position):
                    if state.is_position_used(neigh_pos):
                        continue
                    if not (0 <= neigh_pos.x < state.width and 0 <= neigh_pos.y < state.height):
                        continue
                    if neigh_pos in state.occupied_positions or neigh_pos in state.attacked_positions:
                        continue
                    if neigh_pos in state.harvested_proteins.keys():
                        continue
                    for direction in directions:
                        distance, target = TargetFinder.find_shootable_target(state, neigh_pos, direction)
                        if distance and (not best_direction_o or best_direction_o[0] < distance):
                            best_direction_o = (distance, organ, direction, neigh_pos)

            if best_direction_o:
                possible_directions.append(best_direction_o)

        possible_directions.sort(key=lambda x: x[0])

        for _, organ, direction, position in possible_directions:
            if state.is_organism_used(organ.root_id or organ.organ_id):
                continue
            if state.is_position_used(position):
                continue
            if self.handle_growth(organ, position, direction, "SPORER", state, commands, True):
                continue


class ExpansionManager(Command):
    priority = 5

    def execute(self, state: GameState, commands: List[str]):
        potential_growths = []
        for organism in state.my_organisms.values():
            if state.is_organism_used(organism.root_id):
                continue
            best_growth_o = None
            for organ in organism.organs.values():
                target = TargetFinder.closest_target(organ.position,
                                                     state.proteins.keys() - state.harvested_proteins.keys())
                if not target:
                    target = TargetFinder.closest_target(organ.position, state.enemy_organs.keys())

                for neigh_pos in DirectionUtils.get_neighbors(organ.position):
                    if state.is_position_used(neigh_pos):
                        continue
                    if not (0 <= neigh_pos.x < state.width and 0 <= neigh_pos.y < state.height):
                        continue
                    if neigh_pos in state.occupied_positions or neigh_pos in state.attacked_positions:
                        continue
                    steps_on_harvested = neigh_pos in state.harvested_proteins.keys()
                    steps_on_protein = neigh_pos in state.proteins.keys()
                    distance = DirectionUtils.distance_to(neigh_pos, target)
                    score = distance + (2 if steps_on_protein else 100 if steps_on_harvested else 0)
                    if not best_growth_o or best_growth_o[4] > score:
                        best_growth_o = (distance, organ, steps_on_protein, steps_on_harvested, score, neigh_pos)

            if best_growth_o:
                potential_growths.append(best_growth_o)

        potential_growths.sort(key=lambda x: x[4])

        for _, organ, steps_on_protein, steps_on_harvested, score, position in potential_growths:
            if state.is_organism_used(organ.root_id or organ.organ_id):
                continue
            if state.is_position_used(position):
                continue
            if self.handle_growth(organ, position, None, "BASIC", state, commands, steps_on_protein):
                continue


class WaitManager(Command):
    priority = 6

    def execute(self, state: GameState, commands: List[str]):
        remaining_actions = state.required_actions - len(commands)
        commands.extend("WAIT" for _ in range(remaining_actions))


class CommandChain:
    def __init__(self, state: GameState):
        self.state = state
        self.commands = []
        self.managers = self._get_sorted_managers()

    def _get_sorted_managers(self):
        return sorted([cls() for cls in Command.__subclasses__()], key=lambda manager: manager.priority)

    def execute(self):
        for manager in self.managers:
            manager.execute(self.state, self.commands)
        return self.commands


def parse_input(width, height, entity_count: int):
    state = GameState(width, height)

    for _ in range(entity_count):
        x, y, type, o, organ_id, direction, parent_id, root_id = input().split()
        x, y, organ_id, parent_id, root_id = map(int, (x, y, organ_id, parent_id, root_id))

        position = Position(x, y)
        if type == "WALL":
            state.walls[position] = WallEntity(position)
        elif type in "ABCD":
            state.proteins.update({position: ProteinEntity(position, type)})
        else:
            if type == "ROOT":
                organ = RootOrgan(position, organ_id, direction, parent_id, root_id)
            elif type == "SPORER":
                organ = SporerOrgan(position, organ_id, direction, parent_id, root_id)
            elif type == "HARVESTER":
                organ = HarvesterOrgan(position, organ_id, direction, parent_id, root_id)
            elif type == "TENTACLE":
                organ = TentacleOrgan(position, organ_id, direction, parent_id, root_id)
            else:
                organ = BasicOrgan(position, organ_id, direction, parent_id, root_id)

            organisms = state.my_organisms if int(o) == 1 else state.enemy_organisms
            positions = state.my_organs if int(o) == 1 else state.enemy_organs
            if root_id not in organisms:
                organisms[root_id] = Organism(root_id)
            organisms[root_id].add_organ(organ)
            positions.update({organ.position: organ})
    return state


def serialize_entity(entity):
    return [getattr(entity, key) for key in entity.__dataclass_fields__] if is_dataclass(entity) else entity


def execute_turn(state: GameState, turn_number: int):
    input_snapshot = {
        "my_resources": [n for r, n in state.resources.items()],
        "enemy_resources": [n for r, n in state.enemy_resources.items()],
        "my_organs": [serialize_entity(o) for rid, org in state.my_organisms.items() for o in org.organs],
        "enemy_organs": [serialize_entity(o) for rid, org in state.enemy_organisms.items() for o in org.organs],
        "proteins": [serialize_entity(p) for p in state.proteins],
        "walls": [serialize_entity(w) for w in state.walls],
    }
    commands = CommandChain(state).execute()
    print(f"DEBUG TURN {turn_number}: INPUT={input_snapshot}, OUTPUT={commands}", file=sys.stderr)
    print(*commands, sep="\n")


width, height = map(int, input().split())
turn_number = 0

while True:
    turn_number += 1
    state = parse_input(width, height, int(input()))
    my_resources = list(map(int, input().split()))
    enemy_resources = list(map(int, input().split()))
    state.resources = dict(zip("ABCD", my_resources))
    state.enemy_resources = dict(zip("ABCD", enemy_resources))
    state.required_actions = int(input())
    execute_turn(state, turn_number)
