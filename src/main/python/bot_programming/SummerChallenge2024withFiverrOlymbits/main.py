from abc import ABC
from dataclasses import dataclass, field
from functools import wraps
from itertools import zip_longest
from random import randint
import copy
import itertools
import math
import re
import sys
import time
import numpy as np

POSSIBLE_MOVES = ['UP', 'DOWN', 'LEFT', 'RIGHT']


def timed(func=None, *, repeat=10):
    if func is None:
        return lambda f: timed(f, repeat=repeat)

    @wraps(func)
    def wrapper(*args, **kwargs):
        durations = []
        result = None
        for _ in range(repeat):
            start = time.perf_counter_ns()
            result = func(*args, **kwargs)
            end = time.perf_counter_ns()
            duration = (end - start) / 1_000_000  # Convert to milliseconds
            durations.append(duration)
        avg_duration = sum(durations) / repeat
        print(f"{func.__name__} took {avg_duration} milliseconds", file=sys.stderr, flush=True)
        return result

    return wrapper


class MiniGame(ABC):
    finished: bool

    def possible_options(self):
        return [self]

    def get_scores(self):
        pass

    def weights(self, is_lowest):
        pass

    def best_paths(self):
        pass

    def best_path(self):
        pass

    def best_path_len(self):
        pass

    def next_actions_score(self, action):
        pass

    def simulate(self, actions: np.array):
        pass

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            self_dict = vars(self)
            other_dict = vars(other)

            # compare the dictionaries
            if self_dict.keys() != other_dict.keys():
                return False

            for key, value in self_dict.items():
                other_value = other_dict[key]

                # if both values are numpy arrays
                if isinstance(value, np.ndarray) and isinstance(other_value, np.ndarray):
                    # use numpy's array_equal method to compare
                    if not np.array_equal(value, other_value):
                        return False

                # If values are not numpy arrays and not equal then instances are not equal
                elif value != other_value:
                    return False

            # If all checks passed, instances are equal
            return True

        return False


@dataclass(eq=False)
class HurdleRace(MiniGame):
    racetrack: str
    player_idx: int
    positions: np.array
    stuns: np.array
    finished: bool = False

    def __post_init__(self):
        if self.racetrack == "GAME_OVER":
            self.finished = True

    @staticmethod
    def from_input(turn_input: str, player_idx: int):
        [racetrack, pos_1, pos_2, pos_3, stun_1, stun_2, stun_3, unused] = turn_input.split()
        positions = np.array([int(pos_1), int(pos_2), int(pos_3)])
        stuns = np.array([int(stun_1), int(stun_2), int(stun_3)])
        return HurdleRace(racetrack, player_idx, positions, stuns)

    def weights(self, is_lowest):
        if self.racetrack == "GAME_OVER":
            return {'U': 1, 'D': 1, 'L': 1, 'R': 1}

        other_positions = np.hstack(
            (np.array(self.positions)[:self.player_idx], np.array(self.positions[self.player_idx + 1:])))
        other_stuns = np.hstack((np.array(self.stuns)[:self.player_idx], np.array(self.stuns[self.player_idx + 1:])))

        pos = self.positions[self.player_idx]
        stuned = self.stuns[self.player_idx] > 0
        relaxed = ((np.all(other_stuns != 0) and not stuned and np.all(pos > other_stuns)) or
                   (np.all((pos - other_positions) > 2)
                    and pos < len(self.racetrack) - 1 and not self.racetrack[pos:pos + 3].__contains__('#')))

        if stuned or relaxed:
            u = d = r = l = 1
            print("RELAXED race!", file=sys.stderr, flush=True)
        else:
            u = d = r = l = 0
            if pos < len(self.racetrack) - 1 and self.racetrack[pos + 1] == '#':
                u += 1
            elif pos < len(self.racetrack) - 2 and self.racetrack[pos + 2] == '#':
                l += 1
            elif pos < len(self.racetrack) - 3 and self.racetrack[pos + 3] == '#':
                if is_lowest:
                    d += 1
                    u += 1
                else:
                    d += 2
                    u += 2
                    l += 1
            else:
                if is_lowest:
                    r += 1
                else:
                    l += 1
                    d += 2
                    u += 2
                    r += 5

            total = d + l + r + u
            if total > 0:
                l /= total
                u /= total
                r /= total
                d /= total

        response = {'U': u, 'D': d, 'L': l, 'R': r}
        print(f"Race! {response}", file=sys.stderr, flush=True)
        return response

    def get_scores(self):
        ordered_indexes = np.array([sorted(-self.positions).index(x) for x in -self.positions])
        return ((ordered_indexes == 0) * self.finished * 3 +
                (ordered_indexes == 0) * (not self.finished) * 0.3 +
                (ordered_indexes == 1) * self.finished * 1 +
                (ordered_indexes == 1) * (not self.finished) * 0.1)

    def best_path(self):
        racetrack = f"{self.racetrack}."
        remaining_track = racetrack[self.positions[self.player_idx] + 1:].replace('#.', '#')
        steps = re.findall(r'(\.+|#)', remaining_track)
        action_values = ['*' * self.stuns[self.player_idx]] + [self.straight_values(s) for s in steps]
        return ''.join(action_values)

    def best_path_lens(self):
        racetrack = f"{self.racetrack}."
        remaining_tracks = (racetrack[pos + 1:].replace('#.', '#') for pos in self.positions)
        steps_per_player = (re.findall(r'(\.+|#)', track) for track in remaining_tracks)
        action_amounts = [sum(self.straight_steps(step) for step in steps) for steps in steps_per_player]
        return np.add(action_amounts, self.stuns)

    @staticmethod
    def straight_steps(group):
        spaces = len(group)
        rights, not_rights = divmod(spaces, 3)
        downs, lefts = divmod(not_rights, 2)
        return 1 if group == '#' else rights + downs + lefts

    @staticmethod
    def straight_values(group):
        spaces = len(group)
        rights, not_rights = divmod(spaces, 3)
        downs, lefts = divmod(not_rights, 2)
        return 'U' if group == '#' else 'R' * rights + 'D' * downs + 'L' * lefts

    def best_path_len(self):
        return self.best_path_lens()[self.player_idx]

    def best_path_len_starting_with(self, actions):
        racetrack = f"{self.racetrack}."
        position = self.positions[self.player_idx]
        stuns = self.stuns[self.player_idx]
        max_len = len(self.racetrack) - 1
        idx = 0
        for idx, action in enumerate(actions):
            hurdle_at_1 = racetrack[min(position + 1, max_len)] == '#'
            hurdle_at_2 = racetrack[min(position + 2, max_len)] == '#'
            hurdle_at_3 = racetrack[min(position + 3, max_len)] == '#'
            stuns = (stuns - (stuns != 0) * 1 + (stuns == 0) *
                     (((action == 'R') * ((hurdle_at_1 + hurdle_at_2 + hurdle_at_3))) +
                      ((action == 'D') * ((hurdle_at_1 + hurdle_at_2))) +
                      ((action == 'U') * (hurdle_at_2)) +
                      ((action == 'L') * (hurdle_at_1))) * 2)
            position = (position + (stuns == 0) *
                        (((action == 'R') * (
                                hurdle_at_1 * 1 + (not hurdle_at_1 * hurdle_at_2) * 2 + (
                                (not hurdle_at_1) * (not hurdle_at_2)) * 3)) +
                         ((action == 'D') * (hurdle_at_1 * 1 + (not hurdle_at_1) * 2)) +
                         ((action == 'U') * 2) +
                         ((action == 'L') * 1)))
            if position >= max_len:
                position = max_len
                break

        if position == max_len:
            path_length = idx + 1
        else:
            remaining_track = racetrack[position + 1:].replace('#.', '#')
            steps = sum(self.straight_steps(group) for group in re.findall(r'(\.+|#)', remaining_track))
            path_length = stuns + idx + 1 + steps

        return path_length

    def next_actions_score(self, actions):
        if self.racetrack == "GAME_OVER":
            return 1
        path_lens = self.best_path_lens()
        best_path_len = path_lens[self.player_idx]
        if len(actions) < best_path_len:
            raise Exception("More actions are expected")

        path_length = self.best_path_len_starting_with(actions)

        other_path_lens = np.hstack((np.array(path_lens)[:self.player_idx], np.array(path_lens[self.player_idx + 1:])))
        return sum(other_path_lens >= path_length) * 0.5

    def simulate(self, actions: np.array):
        if self.finished:
            raise Exception(f"{self} is already finished")

        racetrack = np.array(list(self.racetrack))
        max_len = len(racetrack) - 1
        hurdle_at_1 = racetrack[np.minimum(self.positions + 1, max_len)] == '#'
        hurdle_at_2 = racetrack[np.minimum(self.positions + 2, max_len)] == '#'
        hurdle_at_3 = racetrack[np.minimum(self.positions + 3, max_len)] == '#'
        positions = (self.positions + (self.stuns == 0) *
                     (((actions == 'R') * (
                             hurdle_at_1 * 1 + (~hurdle_at_1 * hurdle_at_2) * 2 + (~hurdle_at_1 * ~hurdle_at_2) * 3)) +
                      ((actions == 'D') * (hurdle_at_1 * 1 + (~hurdle_at_1) * 2)) +
                      ((actions == 'U') * 2) +
                      ((actions == 'L') * 1)))
        positions = np.minimum(positions, max_len)

        stuns = (self.stuns - (self.stuns != 0) * 1 + (self.stuns == 0) *
                 (((actions == 'R') * ((hurdle_at_1 + hurdle_at_2 + hurdle_at_3) * 2)) +
                  ((actions == 'D') * ((hurdle_at_1 + hurdle_at_2) * 2)) +
                  ((actions == 'U') * (hurdle_at_2 * 2)) +
                  ((actions == 'L') * (hurdle_at_1 * 2))))

        finished = np.any(positions == len(self.racetrack) - 1)

        return HurdleRace(self.racetrack, self.player_idx, positions, stuns, finished)


@dataclass(eq=False)
class Archery(MiniGame):
    wind: str
    player_idx: int
    positions: list
    finished: bool = False
    best_moves_cache = {}

    def __post_init__(self):
        if self.wind == "GAME_OVER":
            self.finished = True

    @staticmethod
    def from_input(turn_input: str, player_idx: int):
        [wind, x1, y1, x2, y2, x3, y3, unused] = turn_input.split()
        positions = np.array([[int(x1), int(y1)], [int(x2), int(y2)], [int(x3), int(y3)]])
        return Archery(wind, player_idx, positions)

    def weights(self, is_lowest):
        if self.wind == "GAME_OVER":
            return {'U': 1, 'D': 1, 'L': 1, 'R': 1}

        x, y = self.positions[self.player_idx]

        other_positions = np.delete(np.array(self.positions), self.player_idx, axis=0)
        relaxed = ((int(self.wind[0]) < 2 or (len(self.wind) > 1 and int(self.wind[0]) + int(self.wind[1]) < 4) or
                    (len(self.wind) > 2 and int(self.wind[0]) + int(self.wind[1]) + int(self.wind[2]) < 6)) and
                   np.all(abs(x) < abs(other_positions[:, 0]) + 1) and
                   np.all(abs(y) < abs(other_positions[:, 1]) + 1))

        if relaxed:
            l = r = u = d = 1
            print("RELAXED arrow!", file=sys.stderr, flush=True)
        else:
            l = r = u = d = 0
            if x > 0:
                if is_lowest:
                    l += 9 - abs(x - int(self.wind[0])) + 9 - abs(y)
                elif x - int(self.wind[0]) >= 0:
                    l += 2
                else:
                    l += 1
            elif x < 0:
                if is_lowest:
                    r += 9 - abs(x + int(self.wind[0])) + 9 - abs(y)
                elif x + int(self.wind[0]) <= 0:
                    r += 2
                else:
                    r += 1
            if y > 0:
                if is_lowest:
                    u += 9 - abs(y - int(self.wind[0])) + 9 - abs(x)
                elif y - int(self.wind[0]) >= 0:
                    u += 2
                else:
                    u += 1
            elif y < 0:
                if is_lowest:
                    d += 9 - abs(y + int(self.wind[0])) + 9 - abs(x)
                elif y + int(self.wind[0]) <= 0:
                    d += 2
                else:
                    d += 1

        total = d + l + r + u
        if total > 0:
            l /= total
            u /= total
            r /= total
            d /= total

        response = {'U': u, 'D': d, 'L': l, 'R': r}
        print(f"Arch ! D {response}", file=sys.stderr, flush=True)
        return response

    def best_distances(self):
        positions = np.array(self.positions)
        for wind in self.wind:
            positions[:, 0] = (positions[:, 0] + (
                    -int(wind) * (positions[:, 0] > 0) + int(wind) * (positions[:, 0] <= 0)) *
                               (abs(positions[:, 0]) > abs(positions[:, 1])))
            positions[:, 1] = (
                    positions[:, 1] + (- int(wind) * (positions[:, 1] > 0) + int(wind) * (positions[:, 1] <= 0)) *
                    (abs(positions[:, 1]) >= abs(positions[:, 0])))
        return np.linalg.norm(positions, axis=1)

    def score_starting_with(self, actions):
        pos = self.positions[self.player_idx]

        idx1 = -1
        for idx1, action in enumerate(actions):
            if action == 'U':
                pos[1] -= int(self.wind[idx1])
            elif action == 'D':
                pos[1] += int(self.wind[idx1])
            elif action == 'L':
                pos[0] -= int(self.wind[idx1])
            elif action == 'R':
                pos[0] += int(self.wind[idx1])

        position = np.array(pos)
        for idx2 in range(idx1 + 1, len(self.wind)):
            wind = self.wind[idx2]
            position[0] = (position[0] + (
                    -int(wind) * (position[0] > 0) + int(wind) * (position[0] <= 0)) * (
                                   abs(position[0]) > abs(position[1])))
            position[1] = (
                    position[1] + (- int(wind) * (position[1] > 0) + int(wind) * (position[1] <= 0)) *
                    (abs(position[1]) >= abs(position[0])))

        return np.linalg.norm(position)

    def best_paths_(self):
        return [self.get_best_paths(0, pos, "", math.inf, []) for pos in self.positions]

    def get_best_paths(self, wind_idx, pos, path, closest_dist, best_paths):
        if closest_dist == 0 or wind_idx > 4:
            return closest_dist, best_paths

        cache_key = (wind_idx, pos[0], pos[1], path, closest_dist, str(best_paths))
        if cache_key in self.best_moves_cache:
            return self.best_moves_cache[cache_key]

        actions = {"U": [0, -1], "D": [0, 1], "L": [-1, 0], "R": [1, 0]}

        new_closest = closest_dist
        new_bests = best_paths
        for action, move in actions.items():
            new_path = path + action

            if wind_idx == len(self.wind):
                new_dist = np.linalg.norm(pos)
                if new_dist < new_closest:
                    new_closest = new_dist
                    new_bests = [new_path]
                elif new_dist == new_closest:
                    new_bests.append(new_path)
            else:
                new_pos = np.array(pos) + np.array(move) * int(self.wind[wind_idx])

                new_closest, new_bests = self.get_best_paths(wind_idx + 1, new_pos, new_path, new_closest, new_bests)

        self.best_moves_cache[cache_key] = new_closest, new_bests

        return new_closest, new_bests

    def get_scores(self):
        distances = np.linalg.norm(self.positions, axis=1)
        ordered_indexes = np.array([sorted(distances).index(x) for x in distances])
        return ((ordered_indexes == 0) * self.finished * 3 +
                (ordered_indexes == 0) * (not self.finished) * 0.3 +
                (ordered_indexes == 1) * self.finished * 1 +
                (ordered_indexes == 1) * (not self.finished) * 0.1)

    def best_path_len(self):
        return len(self.wind)

    def next_actions_score(self, actions):
        if self.wind == "GAME_OVER":
            return 1
        final_position = self.positions[self.player_idx].copy()
        current_wind = self.wind
        for action in actions:
            if current_wind == "":
                break
            if action == 'U':
                final_position[1] -= int(current_wind[0])
            elif action == 'D':
                final_position[1] += int(current_wind[0])
            elif action == 'L':
                final_position[0] -= int(current_wind[0])
            elif action == 'R':
                final_position[0] += int(current_wind[0])
            current_wind = current_wind[1:]

        score = 1 - (np.linalg.norm(final_position) / math.sqrt(81 + 81))
        return 0 if score <= 0 else score

    def simulate(self, actions: np.array):
        if self.finished:
            raise Exception(f"{self} is already finished")

        positions = self.positions + np.column_stack(
            ((actions == 'L') * -int(self.wind[0]) + (actions == 'R') * int(self.wind[0]),
             (actions == 'U') * -int(self.wind[0]) + (actions == 'D') * int(self.wind[0])))

        wind = self.wind[1:]
        finished = len(wind) == 0
        return Archery(wind, self.player_idx, positions, finished)


@dataclass(eq=False)
class RollerSpeedSkating(MiniGame):
    risk_order: str
    player_idx: int
    spaces: np.array
    risks: np.array
    turns_left: int
    finished: bool = False

    def __post_init__(self):
        if self.risk_order == "GAME_OVER":
            self.finished = True

    @staticmethod
    def from_input(turn_input: str, player_idx: int):
        [risk_order, spaces1, spaces2, spaces3, risk1, risk2, risk3, turns_left] = turn_input.split()
        spaces = np.array([int(spaces1), int(spaces2), int(spaces3)])
        risks = np.array([int(risk1), int(risk2), int(risk3)])
        return RollerSpeedSkating(risk_order, player_idx, spaces, risks, int(turns_left))

    def weights(self, is_lowest):
        if self.risk_order == "GAME_OVER":
            return {'U': 1, 'D': 1, 'L': 1, 'R': 1}

        u = d = r = l = 0
        travel = 0
        risk = 0

        other_spaces = np.hstack((np.array(self.spaces)[:self.player_idx], np.array(self.spaces[self.player_idx + 1:])))
        other_risks = np.hstack((np.array(self.risks)[:self.player_idx], np.array(self.risks[self.player_idx + 1:])))

        space = self.spaces[self.player_idx]
        risk = self.risks[self.player_idx]
        relaxed = ((np.all(other_risks < 0) and (0 <= risk) and np.all(space > other_spaces)) or
                   (np.all(space - other_spaces > 2) and 0 <= risk < 4))

        if risk < 0 or relaxed:
            l = r = u = d = 1
            print("RELAXED roll!", file=sys.stderr, flush=True)
        else:
            l = r = u = d = 0
            if risk == 4:
                if self.risk_order[0] == "U" and not is_lowest:
                    u += 1
                elif self.risk_order[0] == "D" and not is_lowest:
                    d += 1
                elif self.risk_order[0] == "L" and not is_lowest:
                    l += 1
                elif self.risk_order[0] == "R" and not is_lowest:
                    r += 1
                if self.risk_order[1] == "U" and not is_lowest:
                    u += 2
                elif self.risk_order[1] == "D" and not is_lowest:
                    d += 2
                elif self.risk_order[1] == "L" and not is_lowest:
                    l += 2
                elif self.risk_order[1] == "R" and not is_lowest:
                    r += 2
                if self.risk_order[3] == "U":
                    u += 3
                elif self.risk_order[3] == "D":
                    d += 3
                elif self.risk_order[3] == "L":
                    l += 3
                elif self.risk_order[3] == "R":
                    r += 3
            elif risk == 3:
                if self.risk_order[0] == "U":
                    u += 2
                elif self.risk_order[0] == "D":
                    d += 2
                elif self.risk_order[0] == "L":
                    l += 2
                elif self.risk_order[0] == "R":
                    r += 2
                if self.risk_order[1] == "U" and not is_lowest:
                    u += 1
                elif self.risk_order[1] == "D" and not is_lowest:
                    d += 1
                elif self.risk_order[1] == "L" and not is_lowest:
                    l += 1
                elif self.risk_order[1] == "R" and not is_lowest:
                    r += 1
            elif risk >= 1:
                if self.risk_order[0] == "U" and not is_lowest:
                    u += 1
                elif self.risk_order[0] == "D" and not is_lowest:
                    d += 1
                elif self.risk_order[0] == "L" and not is_lowest:
                    l += 1
                elif self.risk_order[0] == "R" and not is_lowest:
                    r += 1
                if self.risk_order[1] == "U" and not is_lowest:
                    u += 2
                elif self.risk_order[1] == "D" and not is_lowest:
                    d += 2
                elif self.risk_order[1] == "L" and not is_lowest:
                    l += 2
                elif self.risk_order[1] == "R" and not is_lowest:
                    r += 2
                if self.risk_order[3] == "U":
                    u += 3
                elif self.risk_order[3] == "D":
                    d += 3
                elif self.risk_order[3] == "L":
                    l += 3
                elif self.risk_order[3] == "R":
                    r += 3
            else:
                if self.risk_order[0] == "U" and not is_lowest:
                    u += 1
                elif self.risk_order[0] == "D" and not is_lowest:
                    d += 1
                elif self.risk_order[0] == "L" and not is_lowest:
                    l += 1
                elif self.risk_order[0] == "R" and not is_lowest:
                    r += 1
                if self.risk_order[1] == "U" and not is_lowest:
                    u += 2
                elif self.risk_order[1] == "D" and not is_lowest:
                    d += 2
                elif self.risk_order[1] == "L" and not is_lowest:
                    l += 2
                elif self.risk_order[1] == "R" and not is_lowest:
                    r += 2
                if self.risk_order[3] == "U":
                    u += 3
                elif self.risk_order[3] == "D":
                    d += 3
                elif self.risk_order[3] == "L":
                    l += 3
                elif self.risk_order[3] == "R":
                    r += 3

        total = d + l + r + u
        if total > 0:
            l /= total
            u /= total
            r /= total
            d /= total
        response = {'U': u, 'D': d, 'L': l, 'R': r}
        print(f"Skate! D {response}", file=sys.stderr, flush=True)
        return response

    def score_starting_with(self, actions):
        space = (self.spaces[self.player_idx] +
                 ((self.risks[self.player_idx] >= 0) *
                  ((actions[0] == self.risk_order[0]) * 1 +
                   ((actions[0] == self.risk_order[1]) + (actions[0] == self.risk_order[2])) * 2 +
                   (actions[0] == self.risk_order[3]) * 3)))

        risk = (self.risks[self.player_idx] + ((self.risks[self.player_idx] < 0) * 1) +
                ((self.risks[self.player_idx] > 0) *
                 ((actions[0] == self.risk_order[0]) * -1)) +
                ((self.risks[self.player_idx] >= 0) *
                 ((actions[0] == self.risk_order[2]) * 2 +
                  (actions[0] == self.risk_order[3]) * 2)))
        risk = -2 if risk >= 5 else risk

        progress_remaining_turns = self.turns_left + risk * (risk < 0) - 1
        score = space + 2 * progress_remaining_turns - (risk // 2 * (risk > 0))
        return score

    def get_scores(self):
        ordered_indexes = np.array([sorted(-self.spaces).index(x) for x in -self.spaces])
        return ((ordered_indexes == 0) * self.finished * 3 +
                (ordered_indexes == 0) * (not self.finished) * 0.3 +
                (ordered_indexes == 1) * self.finished * 1 +
                (ordered_indexes == 1) * (not self.finished) * 0.1)

    def best_path_len(self):
        return self.turns_left

    def next_actions_score(self, actions):
        if self.risk_order == "GAME_OVER":
            return 1
        if actions[0] == self.risk_order[2]:
            score = 1
        elif actions[0] == self.risk_order[3] and self.risks[self.player_idx] <= 2:
            score = 0.75
        elif actions[0] == self.risk_order[0] and self.risks[self.player_idx] >= 3:
            score = 0.25
        else:
            score = 0
        return score

    def simulate(self, actions: np.array):
        if self.finished:
            raise Exception(f"{self} is already finished")
        if len(self.risk_order) == 0:
            possibilities = [''.join(np.char.ljust(n, 1)) for n in list(itertools.permutations(POSSIBLE_MOVES, 4))]
            idx = randint(0, len(possibilities) - 1)
            self.risk_order = possibilities[idx]

        spaces = (self.spaces +
                  ((self.risks >= 0) *
                   ((actions == self.risk_order[0]) * 1 +
                    ((actions == self.risk_order[1]) + (actions == self.risk_order[2])) * 2 +
                    (actions == self.risk_order[3]) * 3)))

        unique, counts = np.unique(spaces % 10, return_counts=True)
        duplicates = unique[counts > 1]

        risks = (self.risks + ((self.risks < 0) * 1) +
                 ((self.risks > 0) *
                  ((actions == self.risk_order[0]) * -1)) +
                 ((self.risks >= 0) *
                  ((actions == self.risk_order[2]) * 2 +
                   (actions == self.risk_order[3]) * 2)) +
                 ((self.risks >= 0) * (np.isin(spaces % 10, duplicates) * 2)))
        risks = np.where(risks >= 5, -2, risks)

        turns_left = self.turns_left - 1
        finished = turns_left == 0
        return RollerSpeedSkating("", self.player_idx, spaces, risks, turns_left, finished)


@dataclass(eq=False)
class Diving(MiniGame):
    diving_goal: str
    player_idx: int
    points: np.array
    combos: np.array
    finished: bool = False

    def __post_init__(self):
        if self.diving_goal == "GAME_OVER":
            self.finished = True

    @staticmethod
    def from_input(turn_input: str, player_idx: int):
        [diving_goal, points1, points2, points3, combo1, combo2, combo3, unused] = turn_input.split()
        points = np.array([int(points1), int(points2), int(points3)])
        combos = np.array([int(combo1), int(combo2), int(combo3)])
        return Diving(diving_goal, player_idx, points, combos)

    def weights(self, is_lowest):
        if self.diving_goal == "GAME_OVER":
            return {'U': 1, 'D': 1, 'L': 1, 'R': 1}

        points = 0
        combo = 0
        missing_turns = len(self.diving_goal)

        other_points = np.hstack((np.array(self.points)[:self.player_idx], np.array(self.points[self.player_idx + 1:])))
        other_combos = np.hstack((np.array(self.combos)[:self.player_idx], np.array(self.combos[self.player_idx + 1:])))
        points = self.points[self.player_idx]
        combo = self.combos[self.player_idx]
        relaxed = np.all((other_points + other_combos * missing_turns) < points)

        if relaxed:
            l = r = u = d = 1
            print("RELAXED dive!", file=sys.stderr, flush=True)
        else:
            l = r = u = d = 0
            if self.diving_goal[0] == "U":
                u = 1
            elif self.diving_goal[0] == "D":
                d = 1
            elif self.diving_goal[0] == "L":
                l = 1
            elif self.diving_goal[0] == "R":
                r = 1

        response = {'U': u, 'D': d, 'L': l, 'R': r}
        print(f"Arrow! D {response}", file=sys.stderr, flush=True)
        return response

    def score_starting_with(self, actions):
        best_path_len = len(self.diving_goal)
        score = self.points[self.player_idx]

        actions = [goal if action is None else action for action, goal in zip_longest(actions, self.diving_goal)]
        groups = [sum(1 for _ in group) if equal else 0 for equal, group in
                  itertools.groupby(actions[i] == self.diving_goal[i] for i in range(best_path_len))]

        score += self.combos[self.player_idx] * groups[0] + groups[0] * (groups[0] + 1) // 2
        for idx in range(len(groups)):
            score += groups[idx] * (groups[idx] + 1) // 2

        return score

    def get_scores(self):
        ordered_indexes = np.array([sorted(-self.points).index(x) for x in -self.points])
        return ((ordered_indexes == 0) * self.finished * 3 +
                (ordered_indexes == 0) * (not self.finished) * 0.3 +
                (ordered_indexes == 1) * self.finished * 1 +
                (ordered_indexes == 1) * (not self.finished) * 0.1)

    def best_path_len(self):
        return len(self.diving_goal)

    def next_actions_score(self, actions):
        if self.diving_goal == "GAME_OVER":
            return 1
        best_path_len = self.best_path_len()
        if len(actions) < best_path_len:
            raise Exception("More actions are expected")

        score = self.points[self.player_idx]
        groups = ''.join(str(sum(1 for _ in group)) if equal else '0' for equal, group in
                         itertools.groupby(actions[i] == self.diving_goal[i] for i in range(best_path_len)))

        score += self.combos[self.player_idx] * int(groups[0])
        for group in groups:
            score += int(group) * (int(group) + 1) // 2

        other_points = np.hstack((np.array(self.points)[:self.player_idx], np.array(self.points[self.player_idx + 1:])))
        other_combos = np.hstack((np.array(self.combos)[:self.player_idx], np.array(self.combos[self.player_idx + 1:])))
        other_scores = other_points + other_combos * best_path_len + best_path_len * (best_path_len + 1) // 2
        return sum(score >= other_scores) * 0.5

    def simulate(self, actions: np.array):
        if self.finished:
            raise Exception(f"{self} is already finished")

        combos = ((self.combos + 1) * ((actions == self.diving_goal[0]) * 1))
        combos = combos + ((self.combos > 0) * (combos == 0) * 1)

        points = self.points + combos

        diving_goal = self.diving_goal[1:]
        finished = len(diving_goal) == 0
        return Diving(diving_goal, self.player_idx, points, combos, finished)


@dataclass
class Game:
    player_idx: int
    turn: int = 0
    golds: list[list[int]] = field(default_factory=list)
    silvers: list[list[int]] = field(default_factory=list)
    bronzes: list[list[int]] = field(default_factory=list)
    mini_games: list[MiniGame] = field(default_factory=list)

    def get_scores(self):
        minigame_scores = np.array([g.get_scores() for g in self.mini_games]).T
        golds = np.array(self.golds)
        silvers = np.array(self.silvers)
        scores = ((golds +
                   (minigame_scores == 0.3) * 0.1) * 3 +
                  (silvers +
                   (minigame_scores == 0.1) * 0.1))
        scores = scores + (scores == 0) * 0.01

        return np.prod(scores, axis=1)

    def weights(self):
        golds = np.array(copy.deepcopy(self.golds))
        silvers = np.array(copy.deepcopy(self.silvers))
        game_scores = (golds[self.player_idx] * 3 + silvers[self.player_idx])
        max_game_score = max(game_scores)
        lowest_game_score = min(game_scores)
        game_scores_softmax = []
        if max_game_score != 0:
            e_x = np.exp(max_game_score - game_scores)
            game_scores_softmax = e_x / e_x.sum(axis=0)

        ups = downs = lefts = rights = 0
        lens = [game.best_path_len() for game in self.mini_games]
        max_len = max(lens)
        print(
            f"0{self.mini_games[0].racetrack} 1{self.mini_games[1].wind} 2{self.mini_games[2].risk_order} 3{self.mini_games[3].diving_goal}",
            file=sys.stderr, flush=True)

        for idx, game in enumerate(self.mini_games):
            is_lowest = lowest_game_score == game_scores[idx]
            weights = game.weights(is_lowest)
            score = 0 if max_game_score == 0 else game_scores_softmax[idx]
            u = weights['U'] * (1.001-lens[idx]/max_len)
            d = weights['D'] * (1.001-lens[idx]/max_len)
            l = weights['L'] * (1.001-lens[idx]/max_len)
            r = weights['R'] * (1.001-lens[idx]/max_len)

            print(f"{idx} --- U{u} D{d} L{l} R{r}", file=sys.stderr, flush=True)

            if is_lowest:
                ups += u * 2
                downs += d * 2
                lefts += l * 2
                rights += r * 2
            else:
                ups += u
                downs += d
                lefts += l
                rights += r
        return {'U': ups,
                'D': downs,
                'L': lefts,
                'R': rights}

    def next_actions_score(self, actions):
        golds = np.array(copy.deepcopy(self.golds))
        silvers = np.array(copy.deepcopy(self.silvers))
        score = 0
        game_scores = (golds[self.player_idx] * 3 + silvers[self.player_idx])
        max_game_score = max(game_scores)
        e_x = np.exp(max_game_score - game_scores)
        game_scores = e_x / e_x.sum(axis=0)
        for idx, game in enumerate(self.mini_games):
            actions_score = game.next_actions_score(actions)
            actions_score = actions_score if max_game_score == 0 else \
                (actions_score * (4 * game_scores[idx]))
            score += actions_score

        return score / 4

    def simulate(self, actions):
        mini_games = []
        golds = np.array(copy.deepcopy(self.golds))
        silvers = np.array(copy.deepcopy(self.silvers))
        for idx, game in enumerate(self.mini_games):
            if not game.finished:
                new_state = game.simulate(actions)
                mini_games.append(new_state)
                if new_state.finished:
                    golds[:, idx] = golds[:, idx] + (new_state.get_scores() == 3)
                    silvers[:, idx] = silvers[:, idx] + (new_state.get_scores() == 1)
            else:
                mini_games.append(game)

        return Game(self.player_idx, self.turn + 1, golds.tolist(), silvers.tolist(), self.bronzes, mini_games)

    def game_updates(self):
        if self.mini_games[2].risk_order == "":
            return [''.join(np.char.ljust(n, 1)) for n in list(itertools.permutations(POSSIBLE_MOVES, 4))]
        else:
            return []

    def best_path_len(self):
        return max([m.best_path_len() for m in self.mini_games])


def run():
    player_idx = int(input())
    nb_games = int(input())
    game = Game(player_idx)

    while True:
        game.turn += 1
        scores_info = np.array([input().split(' '), input().split(' '), input().split(' ')], int)

        game.golds = scores_info[:, [1, 4, 7, 10]].tolist()
        game.silvers = scores_info[:, [2, 5, 8, 11]].tolist()
        game.bronzes = scores_info[:, [3, 6, 9, 12]].tolist()

        game.mini_games = [HurdleRace.from_input(input(), game.player_idx),
                           Archery.from_input(input(), game.player_idx),
                           RollerSpeedSkating.from_input(input(), game.player_idx),
                           Diving.from_input(input(), game.player_idx)]

        weights = game.weights()
        ups = weights['U']
        downs = weights['D']
        lefts = weights['L']
        rights = weights['R']
        print(f"TOTAL! {downs}, {lefts}, {rights}, {ups}", file=sys.stderr, flush=True)

        if ups > lefts and ups > downs and ups > rights:
            print("UP")
        elif lefts > downs and lefts > rights:
            print("LEFT")
        elif downs > rights:
            print("DOWN")
        else:
            print("RIGHT")

# run()
