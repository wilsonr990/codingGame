import copy
import math, logging
from dataclasses import dataclass, field
from enum import Enum
from typing import Dict

WIDTH = 10000.0
HEIGHT = 10000.0
SIMPLE_SCANS = False
MONSTER_UPPER_Y_LIMIT = 2500
MONSTER_START_MIN_Y = 5000
MONSTER_EAT_RANGE = 300
MONSTER_AGGRESSIVE_SPEED = 540
MONSTER_NORMAL_SPEED = 270
DRONE_MAX_BATTERY = 30
DRONE_BATTERY_REGEN = 1
DRONE_LIGHT_BATTERY_COST = 5
DRONE_MOVE_SPEED = 600
DRONE_EMERGENCY_SPEED = 300
DRONE_MOVE_SPEED_LOSS_PER_SCAN = 0
DRONE_SINK_SPEED = 300
DRONE_HIT_RANGE = 200
DRONE_UPPER_Y_LIMIT = 0
DRONE_LIGHT_SCAN_RANGE = 2000
DRONE_DARK_SCAN_RANGE = 800
DRONE_MAX_SCANS = 10000
FISH_FRIGHTENED_SPEED = 400
FISH_WILL_FLEE = True
FISH_HEARING_RANGE = (DRONE_DARK_SCAN_RANGE + DRONE_LIGHT_SCAN_RANGE) / 2


@dataclass
class Collision:
    t: float
    a: object
    b: object

    def happened(self):
        return self.t >= 0


class Closest:

    def __init__(self, to, entities, distance):
        self.to = to
        self.list = entities
        self.distance = distance

    def is_empty(self):
        return len(self.list) == 0

    def get(self):
        return self.list[0]

    def has_one(self):
        return len(self.list) == 1

    def get_pos(self):
        return self.list[0].getRelativePos(self.to)

    def get_mean_pos(self):
        if len(self.list) == 0:
            return None
        if self.has_one():
            return self.get_pos()

        total_x, total_y = 0, 0

        for entity in self.list:
            total_x += entity.getRelativePos(self.to).x
            total_y += entity.getRelativePos(self.to).y

        return Vector(total_x / len(self.list), total_y / len(self.list))


class CreatureType(Enum):
    FISH = "FISH"
    MONSTER = "MONSTER"


@dataclass(frozen=True)
class Vector:
    x: float
    y: float

    def rotate(self, angle):
        cos_a, sin_a = math.cos(angle), math.sin(angle)
        nx = (self.x * cos_a) - (self.y * sin_a)
        ny = (self.x * sin_a) + (self.y * cos_a)
        return Vector(nx, ny)

    def round(self):
        return Vector(int(round(self.x)), int(round(self.y)))

    def truncate(self):
        return Vector(int(self.x), int(self.y))

    def distance(self, v):
        return math.sqrt((v.x - self.x) ** 2 + (v.y - self.y) ** 2)

    def in_range(self, v, range):
        return (v.x - self.x) ** 2 + (v.y - self.y) ** 2 <= range ** 2

    def add(self, v):
        return Vector(self.x + v.x, self.y + v.y)

    def mult(self, factor):
        return Vector(self.x * factor, self.y * factor)

    def sub(self, v):
        return Vector(self.x - v.x, self.y - v.y)

    def length(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)

    def length_squared(self):
        return self.x ** 2 + self.y ** 2

    def normalize(self, length=1):
        current_length = self.length()
        scale_factor = 0 if current_length == 0 else length / current_length
        normalized_vector = self.mult(scale_factor)
        return normalized_vector

    def dot(self, v):
        return self.x * v.x + self.y * v.y

    def angle(self):
        return math.atan2(self.y, self.x)

    def project(self, force):
        normalize = self.normalize()
        return normalize.mult(normalize.dot(force))

    def cross(self, s):
        return Vector(-s * self.y, s * self.x)

    def hsymmetric(self, center=None):
        if center is None:
            return Vector(-self.x, self.y)
        else:
            return Vector(2 * center - self.x, self.y)

    def vsymmetric(self, center=None):
        if center is None:
            return Vector(self.x, -self.y)
        return Vector(self.x, 2 * center - self.y)

    def symmetric(self, center=None):
        if center is None:
            center = Vector(0, 0)
        return Vector(center.x * 2 - self.x, center.y * 2 - self.y)

    def within_bounds(self, minx, miny, maxx, maxy):
        return minx <= self.x < maxx and miny <= self.y < maxy

    def is_zero(self):
        return self.x == 0 and self.y == 0

    def symmetric_truncate(self, origin=None):
        if origin is None:
            return Vector(
                math.floor(self.x) if self.x < 0 else math.ceil(self.x),
                math.floor(self.y) if self.y < 0 else math.ceil(self.y)
            )
        return self.sub(origin).truncate().add(origin)

    def sqr_euclidean_to(self, other):
        return (other.x - self.x) ** 2 + (other.y - self.y) ** 2

    def manhattan_to(self, other):
        return abs(other.x - self.x) + abs(other.y - self.y)

    def chebyshev_to(self, x, y):
        return max(abs(x - self.x), abs(y - self.y))

    def euclidean_to(self, pos):
        return math.sqrt(self.sqr_euclidean_to(pos))

    def epsilon_round(self):
        return Vector(
            round(self.x * 10000000.0) / 10000000.0,
            round(self.y * 10000000.0) / 10000000.0
        )

    @classmethod
    def from_angle(cls, angle):
        cos_a, sin_a = math.cos(angle), math.sin(angle)
        return cls(cos_a, sin_a)


@dataclass
class Entity:
    id: int


@dataclass
class Creature(Entity):
    type: CreatureType = None
    known: bool = False
    speed: Vector = Vector(0, 0)
    min_pos: Vector = Vector(0, 0)
    max_pos: Vector = Vector(WIDTH, HEIGHT)

    def get_average_pos(self):
        return self.min_pos.add(self.max_pos).mult(0.1)

    def getRelativePos(self, drone):
        drone_pos = drone.pos
        drone_target = drone.target
        light = drone.light_on
        exp_distance = (DRONE_LIGHT_SCAN_RANGE if light else DRONE_DARK_SCAN_RANGE) + (
            MONSTER_EAT_RANGE if self.type == CreatureType.MONSTER else FISH_HEARING_RANGE)

        if self.type == CreatureType.MONSTER:
            closest = self.get_closest_pos_from(drone_pos)

            if not self.known and closest.distance(drone_pos) < exp_distance:
                return self.get_average_pos()

            if not self.known and closest.distance(drone_pos) < exp_distance:
                target_blocked_pos = drone_pos.add(drone_target.sub(drone_pos).normalize(exp_distance))

                if self.min_pos.x <= target_blocked_pos.x and self.min_pos.y <= target_blocked_pos.y and self.max_pos.x >= target_blocked_pos.x and self.max_pos.y >= target_blocked_pos.y:
                    closest = target_blocked_pos
                elif self.min_pos.x >= drone_pos.x and self.min_pos.y >= drone_pos.y:
                    if self.max_pos.x < target_blocked_pos.x:
                        y = drone_pos.y + ((exp_distance ** 2 - (self.max_pos.x - drone_pos.x) ** 2) ** 0.5)
                        closest = Vector(self.max_pos.x, y)
                    elif self.max_pos.y < target_blocked_pos.y:
                        x = drone_pos.x + ((exp_distance ** 2 - (self.max_pos.y - drone_pos.y) ** 2) ** 0.5)
                        closest = Vector(x, self.max_pos.y)
                    elif self.min_pos.x > target_blocked_pos.x:
                        y = drone_pos.y + ((exp_distance ** 2 - (self.min_pos.x - drone_pos.x) ** 2) ** 0.5)
                        closest = Vector(self.min_pos.x, y)
                    elif self.min_pos.y > target_blocked_pos.y:
                        x = drone_pos.x + ((exp_distance ** 2 - (self.min_pos.y - drone_pos.y) ** 2) ** 0.5)
                        closest = Vector(x, self.min_pos.y)
                elif self.min_pos.x >= drone_pos.x and self.max_pos.y <= drone_pos.y:
                    if self.min_pos.y > target_blocked_pos.y:
                        x = drone_pos.x - ((exp_distance ** 2 - (self.min_pos.y - drone_pos.y) ** 2) ** 0.5)
                        closest = Vector(x, self.min_pos.y)
                    elif self.max_pos.x < target_blocked_pos.x:
                        y = drone_pos.y + ((exp_distance ** 2 - (self.max_pos.x - drone_pos.x) ** 2) ** 0.5)
                        closest = Vector(self.max_pos.x, y)
                    elif self.max_pos.y < target_blocked_pos.y:
                        x = drone_pos.x - ((exp_distance ** 2 - (self.max_pos.y - drone_pos.y) ** 2) ** 0.5)
                        closest = Vector(x, self.max_pos.y)
                    elif self.min_pos.x > target_blocked_pos.x:
                        y = drone_pos.y + ((exp_distance ** 2 - (self.min_pos.x - drone_pos.x) ** 2) ** 0.5)
                        closest = Vector(self.min_pos.x, y)
                elif self.max_pos.x <= drone_pos.x and self.max_pos.y <= drone_pos.y:
                    if self.min_pos.y > target_blocked_pos.y:
                        x = drone_pos.x - ((exp_distance ** 2 - (self.min_pos.y - drone_pos.y) ** 2) ** 0.5)
                        closest = Vector(x, self.min_pos.y)
                    elif self.min_pos.x > target_blocked_pos.x:
                        y = drone_pos.y - ((exp_distance ** 2 - (self.min_pos.x - drone_pos.x) ** 2) ** 0.5)
                        closest = Vector(self.min_pos.x, y)
                    elif self.max_pos.y < target_blocked_pos.y:
                        x = drone_pos.x - ((exp_distance ** 2 - (self.max_pos.y - drone_pos.y) ** 2) ** 0.5)
                        closest = Vector(x, self.max_pos.y)
                    elif self.max_pos.x < target_blocked_pos.x:
                        y = drone_pos.y - ((exp_distance ** 2 - (self.max_pos.x - drone_pos.x) ** 2) ** 0.5)
                        closest = Vector(self.max_pos.x, y)
                elif self.max_pos.x <= drone_pos.x and self.min_pos.y >= drone_pos.y:
                    if self.max_pos.y < target_blocked_pos.y:
                        x = drone_pos.x - ((exp_distance ** 2 - (self.max_pos.y - drone_pos.y) ** 2) ** 0.5)
                        closest = Vector(x, self.max_pos.y)
                    elif self.min_pos.x > target_blocked_pos.x:
                        y = drone_pos.y + ((exp_distance ** 2 - (self.min_pos.x - drone_pos.x) ** 2) ** 0.5)
                        closest = Vector(self.min_pos.x, y)
                    elif self.min_pos.y > target_blocked_pos.y:
                        x = drone_pos.x - ((exp_distance ** 2 - (self.min_pos.y - drone_pos.y) ** 2) ** 0.5)
                        closest = Vector(x, self.min_pos.y)
                    elif self.max_pos.x < target_blocked_pos.x:
                        y = drone_pos.y + ((exp_distance ** 2 - (self.max_pos.x - drone_pos.x) ** 2) ** 0.5)
                        closest = Vector(self.max_pos.x, y)
            return closest
        else:
            furthest = self.get_furthest_pos_from(drone_pos)

            return furthest

    def get_closest_pos_from(self, drone_pos):
        closest = self.min_pos
        if self.known:
            closest = self.min_pos
        elif self.min_pos.x >= drone_pos.x and self.min_pos.y >= drone_pos.y:
            closest = self.min_pos
        elif self.min_pos.x >= drone_pos.x and self.max_pos.y <= drone_pos.y:
            closest = Vector(self.min_pos.x, self.max_pos.y)
        elif self.max_pos.x <= drone_pos.x and self.max_pos.y <= drone_pos.y:
            closest = self.max_pos
        elif self.max_pos.x <= drone_pos.x and self.min_pos.y >= drone_pos.y:
            closest = Vector(self.max_pos.x, self.min_pos.y)
        return closest

    def get_furthest_pos_from(self, drone_pos):
        furthest = self.min_pos
        if self.known:
            furthest = self.max_pos
        elif self.max_pos.x >= drone_pos.x and self.max_pos.y >= drone_pos.y:
            furthest = self.max_pos
        elif self.max_pos.x >= drone_pos.x and self.min_pos.y <= drone_pos.y:
            furthest = Vector(self.max_pos.x, self.min_pos.y)
        elif self.min_pos.x <= drone_pos.x and self.min_pos.y <= drone_pos.y:
            furthest = self.min_pos
        elif self.min_pos.x <= drone_pos.x and self.max_pos.y >= drone_pos.y:
            furthest = Vector(self.min_pos.x, self.max_pos.y)
        return furthest


@dataclass
class Monster(Creature):
    type: CreatureType = CreatureType.MONSTER
    min_pos: Vector = Vector(0, MONSTER_START_MIN_Y)
    max_pos: Vector = Vector(WIDTH, HEIGHT)
    may_be_moving: bool = False

    def getYBounds(self):
        return 2500.0, 10000.0

    def getApproxSpeed(self, drone, target_pos):
        if self.known:
            return self.speed
        elif self.may_be_moving:
            return target_pos.sub(self.getRelativePos(drone)).normalize(MONSTER_NORMAL_SPEED)
        else:
            return Vector(0, 0)

    def update_pos(self):
        if self.known:
            self.min_pos = self.snap_to_monster_zone(self.min_pos.add(self.speed))
            self.max_pos = self.min_pos
        elif self.may_be_moving:
            # correct speed based on closer active drones
            new_min = self.min_pos.add(Vector(-MONSTER_AGGRESSIVE_SPEED, -MONSTER_AGGRESSIVE_SPEED))
            self.min_pos = self.snap_to_monster_zone(new_min)
            new_max = self.max_pos.add(Vector(MONSTER_AGGRESSIVE_SPEED, MONSTER_AGGRESSIVE_SPEED))
            self.max_pos = self.snap_to_monster_zone(new_max)

    @staticmethod
    def snap_to_monster_zone(pos):
        snapped_pos = pos
        if pos.y > HEIGHT - 1:
            snapped_pos = Vector(pos.x, HEIGHT - 1)
        elif pos.y < MONSTER_UPPER_Y_LIMIT:
            snapped_pos = Vector(pos.x, MONSTER_UPPER_Y_LIMIT)
        return snapped_pos


@dataclass
class Fish(Creature):
    type: CreatureType = CreatureType.FISH
    min_pos: Vector = Vector(0, 2500)
    max_pos: Vector = Vector(WIDTH, HEIGHT)
    color: int = -1
    _type: int = -1

    def getYBounds(self):
        if self._type == 0:
            return 2500.0, 5000.0
        elif self._type == 1:
            return 5000.0, 7500.0
        elif self._type == 2:
            return 7500.0, 10000.0

    def getScore(self):
        return self._type + 1

    def update_pos(self):
        if self.known:
            self.min_pos = self.snap_to_fish_zone(self.min_pos.add(self.speed), self.getYBounds())
            self.max_pos = self.min_pos
        else:
            # correct speed based on closer active drones
            new_min = self.min_pos.add(Vector(-FISH_FRIGHTENED_SPEED, -FISH_FRIGHTENED_SPEED))
            self.min_pos = self.snap_to_fish_zone(new_min, self.getYBounds())
            new_max = self.max_pos.add(Vector(FISH_FRIGHTENED_SPEED, FISH_FRIGHTENED_SPEED))
            self.max_pos = self.snap_to_fish_zone(new_max, self.getYBounds())

    @staticmethod
    def snap_to_fish_zone(pos, y_bounds):
        snapped_pos = pos
        low_y, high_y = y_bounds
        if pos.y > HEIGHT - 1:
            snapped_pos = Vector(pos.x, HEIGHT - 1)
        elif pos.y > high_y:
            snapped_pos = Vector(pos.x, high_y)
        elif pos.y < low_y:
            snapped_pos = Vector(pos.x, low_y)
        return snapped_pos

    def update_speed(self):
        # correct speed based on closer active drones and other close fishes
        pass


@dataclass
class Drone(Entity):
    pos: Vector = Vector(0, 0)
    target = Vector(0, 0)
    dead: bool = False
    battery: int = 30
    scans: list[int] = field(default_factory=list)
    light_on: bool = False
    speed: Vector = Vector(0, 0)

    def getPos(self):
        return self.pos

    def update_speed(self, wait, target):
        move_speed = int(
            DRONE_MOVE_SPEED - DRONE_MOVE_SPEED * DRONE_MOVE_SPEED_LOSS_PER_SCAN * len(self.scans))

        if self.dead:
            self.speed = Vector(0, -DRONE_EMERGENCY_SPEED)
        elif wait and self.pos.y < HEIGHT - 1:
            self.speed = Vector(0, DRONE_SINK_SPEED)
        elif self.pos.y < HEIGHT - 1:
            self.speed = target.sub(self.pos).normalize(move_speed).truncate()
        else:
            self.speed = Vector(0, 0)

    def update_light(self, light_switch):
        if self.dead:
            self.light_on = False
            return

        elif light_switch and self.battery >= DRONE_LIGHT_BATTERY_COST:
            self.light_on = True
        else:
            self.light_on = False

        if self.light_on:
            self.drain_battery()
        else:
            self.recharge_battery()

    def drain_battery(self):
        self.battery -= DRONE_LIGHT_BATTERY_COST

    def recharge_battery(self):
        if self.battery < DRONE_MAX_BATTERY:
            self.battery += DRONE_BATTERY_REGEN
        if self.battery >= DRONE_MAX_BATTERY:
            self.battery = DRONE_MAX_BATTERY

    def get_collision(self, monster: Monster, monster_target):
        monster_pos = monster.getRelativePos(self)
        monster_speed = monster.getApproxSpeed(self, monster_target)

        if monster_pos.in_range(self.pos, DRONE_HIT_RANGE + MONSTER_EAT_RANGE):
            return Collision(0.0, monster, self)

        if self.speed.is_zero() and monster_speed.is_zero():
            return Collision(-1, monster, self)

        x = monster_pos.x
        y = monster_pos.y

        ux = self.pos.x
        uy = self.pos.y

        x2 = x - ux
        y2 = y - uy
        r2 = MONSTER_EAT_RANGE + DRONE_HIT_RANGE
        vx2 = monster_speed.x - self.speed.x
        vy2 = monster_speed.y - self.speed.y

        a = vx2 * vx2 + vy2 * vy2

        if a <= 0.0:
            return Collision(-1, monster, self)

        b = 2.0 * (x2 * vx2 + y2 * vy2)
        c = x2 * x2 + y2 * y2 - r2 * r2
        delta = b * b - 4.0 * a * c

        if delta < 0.0:
            return Collision(-1, monster, self)

        t = (-b - math.sqrt(delta)) / (2.0 * a)

        if t <= 0.0 or t > 1.0:
            return Collision(-1, monster, self)

        return Collision(t, monster, self)

    def update_pos(self):
        self.pos = self.snap_to_drone_zone(self.pos.add(self.speed)).truncate()

    def snap_to_drone_zone(self, pos):
        newPos = pos
        if pos.y > HEIGHT - 1:
            newPos = Vector(pos.x, HEIGHT - 1)
        elif pos.y < DRONE_UPPER_Y_LIMIT:
            newPos = Vector(pos.x, DRONE_UPPER_Y_LIMIT)

        if pos.x < 0:
            newPos = Vector(0, pos.y)
        elif pos.x >= WIDTH:
            newPos = Vector(WIDTH - 1, pos.y)
        return newPos

    def get_closest_fish_competitive(self, fish, drones):
        closests = []
        min_dist = 0

        other_drone = [drone for drone in drones if drone.id != self.id][0]

        for fish in fish:
            dist = fish.getRelativePos(self).sqr_euclidean_to(self.pos)
            other_dist = fish.getRelativePos(other_drone).sqr_euclidean_to(other_drone.pos)
            if dist < other_dist:
                if not closests or dist < min_dist:
                    closests.clear()
                    closests.append(fish)
                    min_dist = dist
                elif dist == min_dist:
                    closests.append(fish)

        return Closest(self, closests, math.sqrt(min_dist))

    def get_closest_fish(self, fish):
        closests = []
        min_dist = 0

        for fish in fish:
            dist = fish.getRelativePos(self).sqr_euclidean_to(self.pos)
            if not closests or dist < min_dist:
                closests.clear()
                closests.append(fish)
                min_dist = dist
            elif dist == min_dist:
                closests.append(fish)

        return Closest(self, closests, math.sqrt(min_dist))

    def get_closest_target_fish_competitive(self, fish, drones):
        closests = []
        min_dist = 0
        cur_type = 0

        other_drone = [drone for drone in drones if drone.id != self.id][0]

        for fish in fish:
            dist = fish.getRelativePos(self).sqr_euclidean_to(self.pos)
            other_dist = fish.getRelativePos(other_drone).sqr_euclidean_to(other_drone.pos)
            if dist < other_dist:
                if not closests or fish._type > cur_type or (fish._type == cur_type and dist < min_dist):
                    closests.clear()
                    closests.append(fish)
                    min_dist = dist
                    cur_type = fish._type
                elif fish._type == cur_type and dist == min_dist:
                    closests.append(fish)

        return Closest(self, closests, math.sqrt(min_dist))

    def get_closest_target_fish(self, fishes):
        closests = []
        min_dist = 0
        cur_type = 0

        for fish in fishes:
            dist = fish.getRelativePos(self).sqr_euclidean_to(self.pos)
            if not closests or fish._type > cur_type or (fish._type == cur_type and dist < min_dist):
                closests.clear()
                closests.append(fish)
                min_dist = dist
                cur_type = fish._type
            elif fish._type == cur_type and dist == min_dist:
                closests.append(fish)

        return Closest(self, closests, math.sqrt(min_dist))

    def simulate(self, target, wait, light):
        self.target = target
        self.update_light(light)
        self.update_speed(wait, target)
        self.update_pos()

    def collides_with_monster(self, target, wait, monsters, drones):
        drone_ = copy.deepcopy(self)
        drone_.update_speed(wait, target)
        monsters_ = [copy.deepcopy(monster) for monster in monsters]
        collided_id = -1

        for monster in monsters_:
            collided_drone = [drone_.get_collision(monster, d.pos).happened()
                              for d in drones if
                              monster.getRelativePos(d).distance(d.pos) < MONSTER_EAT_RANGE + DRONE_HIT_RANGE or
                              d.id == self.id]
            newPos = drone_.pos.add(drone_.speed)
            if collided_drone.__contains__(True) or newPos.y > HEIGHT or newPos.x < 0 or newPos.x > WIDTH:
                collided_id = monster.id
                if monster.known:
                    break

        return collided_id


@dataclass
class Player:
    id: int
    drones: Dict[int, Drone] = field(default_factory=dict)
    scans: list[int] = field(default_factory=list)
    score = 0


@dataclass
class Game:
    drones: Dict[int, Drone] = field(default_factory=dict)
    fishes: Dict[int, Fish] = field(default_factory=dict)
    monsters: Dict[int, Monster] = field(default_factory=dict)
    game_turn = 0
    players: Dict[int, Player] = field(default_factory=dict)

    def __post_init__(self):
        self.players: dict[int, Player] = {0: Player(0), 1: Player(1)}

    def myDrones(self):
        return self.players[0].drones.values()

    def opDrones(self):
        return self.players[1].drones.values()

    def updatePlayerDrone(self, player_id, drone_id, drone_x, drone_y, dead, battery):
        drone = Drone(drone_id, Vector(drone_x, drone_y), dead == 1, battery)

        if self.players[0].drones.__contains__(drone_id):
            assert self.drones[drone_id].pos == drone.pos
            assert self.drones[drone_id].battery == drone.battery

        self.drones[drone_id] = drone
        self.players[player_id].drones[drone_id] = drone

    def updateVisibleCreature(self, c_id, c_x, c_y, c_vx, c_vy):
        if self.fishes.__contains__(c_id):
            creature = self.fishes[c_id]
        else:
            creature = self.monsters[c_id]

        new_pos = Vector(c_x, c_y)
        new_speed = Vector(c_vx, c_vy)

        if creature.known:
            logging.error(f"c {str(new_pos)} {str(creature)}")
            assert creature.min_pos == new_pos
            assert creature.max_pos == new_pos

        creature.known = True
        creature.min_pos = new_pos
        creature.max_pos = new_pos
        creature.speed = new_speed

    def updateCreatureWithBlip(self, d_id, c_id, dir):
        if self.fishes.__contains__(c_id):
            creature = self.fishes[c_id]
        else:
            creature = self.monsters[c_id]
        drone = self.drones[d_id]

        min_y_bound = creature.getYBounds()[0]
        max_y_bound = creature.getYBounds()[1]
        if dir == "BL":
            creature.min_pos = Vector(max(0.0, creature.min_pos.x),
                                      max(drone.pos.y, min_y_bound, creature.min_pos.y))
            creature.max_pos = Vector(min(drone.pos.x, creature.max_pos.x),
                                      min(max_y_bound, creature.max_pos.y))
        elif dir == "BR":
            creature.min_pos = Vector(max(drone.pos.x, creature.min_pos.x),
                                      max(drone.pos.y, min_y_bound, creature.min_pos.y))
            creature.max_pos = Vector(min(HEIGHT, creature.max_pos.x),
                                      min(max_y_bound, creature.max_pos.y))
        elif dir == "TL":
            creature.min_pos = Vector(max(0.0, creature.min_pos.x),
                                      max(min_y_bound, creature.min_pos.y))
            creature.max_pos = Vector(min(drone.pos.x, creature.max_pos.x),
                                      min(drone.pos.y, max_y_bound, creature.max_pos.y))
        elif dir == "TR":
            creature.min_pos = Vector(max(drone.pos.x, creature.min_pos.x),
                                      max(min_y_bound, creature.min_pos.y))
            creature.max_pos = Vector(min(HEIGHT, creature.max_pos.x),
                                      min(drone.pos.y, max_y_bound, creature.max_pos.y))

    def simulate(self, targets, waits, lights):
        for idx, drone in enumerate(self.myDrones()):
            target = targets[idx]
            wait = waits[idx]
            light = lights[idx]

            drone.simulate(target, wait, light)

        for fish in self.fishes.values():
            fish.update_pos()
            if fish.known:
                for drone in self.myDrones():
                    if drone.light_on and fish.getRelativePos(drone).distance(
                            drone.pos) > DRONE_LIGHT_SCAN_RANGE + FISH_HEARING_RANGE:
                        fish.known = False
                    elif not drone.light_on and fish.getRelativePos(drone).distance(
                            drone.pos) > DRONE_DARK_SCAN_RANGE + FISH_HEARING_RANGE:
                        fish.known = False

        for monster in self.monsters.values():
            monster.update_pos()
            if monster.known:
                for drone in self.myDrones():
                    if drone.light_on and monster.getRelativePos(drone).distance(
                            drone.pos) > DRONE_LIGHT_SCAN_RANGE + MONSTER_EAT_RANGE:
                        monster.known = False
                    elif not drone.light_on and monster.getRelativePos(drone).distance(
                            drone.pos) > DRONE_DARK_SCAN_RANGE + MONSTER_EAT_RANGE:
                        monster.known = False

    def parseActions(self):
        actions = []
        for idx, drone in enumerate(self.myDrones()):
            if drone.speed == DRONE_SINK_SPEED:
                actions.append(f"WAIT {int(drone.light_on)}")
            else:
                actions.append(f"MOVE {int(drone.pos.x)} {int(drone.pos.y)} {int(drone.light_on)}")
        return actions

    def identify_target_fish(self, idx, targets):
        target_idx = targets[idx]
        target = None
        for i, fish in enumerate(self.fishes.values()):
            if i == target_idx:
                target = fish
                break
        return target

    def get_best_solution(self):
        targets, saves, waits, lights = ([Vector(0, 0), Vector(0, 0)], [0, 0], [0, 0], [0, 0])

        my_scans = self.players[0].scans
        op_scans = self.players[1].scans
        my_drone_scans = [scan for drone in self.myDrones() for scan in drone.scans]
        opponent_drone_scans = [scan for drone in self.opDrones() for scan in drone.scans]
        my_non_scanned_fishes = [fish for fish in self.fishes.values()
                                 if not my_scans.__contains__(fish.id)
                                 and not my_drone_scans.__contains__(fish.id)]
        op_non_scanned_fishes = [fish for fish in self.fishes.values()
                                 if not op_scans.__contains__(fish.id)
                                 and not opponent_drone_scans.__contains__(fish.id)]
        opponent_is_saving = len(op_non_scanned_fishes) == 0 and len(opponent_drone_scans) > 0

        for monster in self.monsters.values():
            monster.may_be_moving = monster.known

        logging.error(f"fish {str([(fish.id, fish.min_pos, fish.max_pos) for fish in self.fishes.values()])}")
        logging.error(
            f"monster {str([(monster.id, monster.may_be_moving, monster.min_pos, monster.max_pos) for monster in self.monsters.values()])}")

        targeted_fishes = []
        for i, drone in enumerate(self.myDrones()):
            my_non_scanned_fishes = [fish for fish in my_non_scanned_fishes
                                     if not targeted_fishes.__contains__(fish)]
            closest_fish: Closest = drone.get_closest_fish_competitive(my_non_scanned_fishes,
                                                                       self.myDrones())
            closest_fish_chill: Closest = drone.get_closest_fish(my_non_scanned_fishes)

            closest_target: Closest = drone.get_closest_target_fish_competitive(my_non_scanned_fishes,
                                                                                self.myDrones())

            # closest_fish = closest_fish_competitive if not closest_fish_competitive.is_empty() else closest_fish_chill
            closest_target_chill: Closest = drone.get_closest_target_fish(my_non_scanned_fishes)

            # closest_target = closest_target_competitive if not closest_target_competitive.is_empty() else closest_target_chill

            if not opponent_is_saving and not closest_target.is_empty():
                targeted_fishes += closest_target.list

            targets[i] = closest_target.get_mean_pos()
            if (opponent_is_saving and len(drone.scans) > 0) or closest_target.is_empty():
                targets[i] = Vector(drone.pos.x, 0)
            drone.target = targets[i]

            if not closest_fish.is_empty() and closest_fish.get_mean_pos().distance(drone.pos) < DRONE_LIGHT_SCAN_RANGE:
                if drone.battery > 10 or closest_fish.get_mean_pos().x < 1000 or closest_fish.get_mean_pos().x > 9000 or closest_fish.get_mean_pos().y > 9000:
                    lights[i] = 1

            # correct target to prevent clashes
            collide_id = drone.collides_with_monster(targets[i], waits[i], self.monsters.values(),
                                                     self.drones.values())

            angle_increment = 0.0
            angle_rotation = 0.0
            rounds = 0
            drone.target = targets[i]

            possible_best_target = drone.target
            while collide_id > -1 and rounds < 5 and not drone.dead:
                if angle_increment == 0.0:
                    crashed_monster_pos = self.monsters[collide_id].getRelativePos(drone)

                    drone_angle = targets[i].sub(drone.pos).angle() * 180 / math.pi
                    monster_angle = crashed_monster_pos.sub(drone.pos).angle() * 180 / math.pi

                    logging.error(drone_angle)
                    logging.error(monster_angle)
                    drone_angle = drone_angle if drone_angle >= 0 else drone_angle + 360
                    monster_angle = monster_angle if monster_angle >= 0 else monster_angle + 360

                    diff = drone_angle - monster_angle
                    if diff > 0:
                        angle_increment = 0.1
                    else:
                        angle_increment = - 0.1
                    logging.error(f"DIFF {diff} then inc {angle_increment}")

                # if collide_id != -1 and collide_id != 1:
                # logging.error(f"monster_loc {str(self.monsters[collide_id].getRelativePos(drone))}")

                angle_rotation += angle_increment
                if angle_rotation > 2 * math.pi or angle_rotation < -2 * math.pi:
                    drone.target = possible_best_target
                    break

                direction = targets[i].sub(drone.pos).rotate(angle_rotation)

                drone.target = drone.pos.add(direction)

                if collide_id > 1 and not self.monsters[collide_id].known:
                    logging.error(f"pbt {collide_id}")

                    possible_best_target = drone.target

                if drone.id == 2:
                    logging.error(
                        f"drone {str(drone.id)} collides {str(collide_id)} angle {str(direction.angle() * 180 / math.pi)}")

                collide_id = drone.collides_with_monster(drone.target, waits[i], self.monsters.values(),
                                                         self.drones.values())

                collided = collide_id != -1
                if not collided and rounds == 0:
                    collide_id = 1
                    rounds = 1
                    angle_increment = -angle_increment / 2
                elif not collided and rounds == 1:
                    collide_id = 1
                elif collided and rounds == 1:
                    rounds = 2
                    angle_increment = -angle_increment / 2
                elif not collided and rounds == 2:
                    collide_id = 1
                    rounds = 3
                    angle_increment = -angle_increment / 2
                elif not collided and rounds == 3:
                    collide_id = 1
                elif collided and rounds == 3:
                    rounds = 4
                    angle_increment = -angle_increment / 2
                elif not collided and rounds == 4:
                    break

            targets[i] = drone.target
            logging.error(targets[i].sub(drone.pos).angle() * 180 / math.pi)
        return targets, waits, lights

    def remove_non_blipped_fishes(self, blipped_fishes):
        prev_fishes = self.fishes.copy()
        self.fishes = {}
        for fish_id in blipped_fishes:
            if prev_fishes.__contains__(fish_id):  # exclude monsters
                self.fishes[fish_id] = prev_fishes[fish_id]


# Start Here

game = Game()

fish_count = int(input())
for _ in range(fish_count):
    fish_id, color, _type = map(int, input().split())
    if color == -1:
        game.monsters[fish_id] = Monster(id=fish_id)
    else:
        game.fishes[fish_id] = Fish(id=fish_id, _type=_type)

# game loop
while True:
    game.game_turn += 1

    my_score = int(input())
    foe_score = int(input())

    scans = []
    my_scan_count = int(input())
    for _ in range(my_scan_count):
        scans.append(int(input()))
    game.players[0].scans = scans

    scans = []
    foe_scan_count = int(input())
    for _ in range(foe_scan_count):
        scans.append(int(input()))
    game.players[1].scans = scans

    my_drone_count = int(input())
    for _ in range(my_drone_count):
        drone_id, drone_x, drone_y, dead, battery = map(int, input().split())
        game.updatePlayerDrone(0, drone_id, drone_x, drone_y, dead, battery)

    foe_drone_count = int(input())
    for _ in range(foe_drone_count):
        drone_id, drone_x, drone_y, dead, battery = map(int, input().split())
        game.updatePlayerDrone(1, drone_id, drone_x, drone_y, dead, battery)

    drone_scan_count = int(input())
    for _ in range(drone_scan_count):
        drone_id, fish_id = map(int, input().split())
        game.drones[drone_id].scans.append(fish_id)

    visible_fish_count = int(input())
    for _ in range(visible_fish_count):
        fish_id, fish_x, fish_y, fish_vx, fish_vy = map(int, input().split())
        game.updateVisibleCreature(fish_id, fish_x, fish_y, fish_vx, fish_vy)

    my_radar_blip_count = int(input())
    blipped_fishes = set()
    for _ in range(my_radar_blip_count):
        drone_id, fish_id, dir = input().split()
        game.updateCreatureWithBlip(int(drone_id), int(fish_id), dir)
        blipped_fishes.add(int(fish_id))
    game.remove_non_blipped_fishes(blipped_fishes)

    targets, waits, lights = game.get_best_solution()

    game.simulate(targets, waits, lights)
    actions = game.parseActions()

    for action in actions:
        print(action)
