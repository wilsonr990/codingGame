package codeGolf.theLabyrinth
import scala.collection.mutable
import scala.io.StdIn._
case class A(a:Int,b:Int,c:Int,d:Int)
case class B(a:Int,b:Int){def C(c:Int=1)(implicit d:A)={val e=b+c
B(a,if(e>d.c)d.c else e)}
def D(c:Int=1)(implicit d:A)={val e=b- c
B(a,if(e<d.a)d.a else e)}
def E(c:Int=1)(implicit d:A)={val e=a- c
B(if(e<d.b)d.b else e,b)}
def F(c:Int=1)(implicit d:A)={val e=a+c
B(if(e>d.d)d.d else e,b)}}
sealed class G(val a:Boolean){var b:List[B]=List()
var c:Int=0}
case class H() extends G(false)
case class I() extends G(true)
case class J() extends G(true)
case class K()extends G(true)
case class L()extends G(false)
class M(a:Int,b:Int,c:Int){implicit var d:A=A(0,0,b,a)
var e:Int=c
var f:Map[B,G]=Map()
var g:B=_
var h:B=_
var i:B=_
var j:Char='C'
var k:List[B]=List()
var l:B=_
var m:Char='C'
def n(n:B,o:List[String],p:B,q:mutable.Queue[B])={if(!f(n).b.contains(p))
f=f.updatedWith(n){r=>
r.map((s:G)=>{s.b::=p
s})}
if(!f.contains(p)){if(o(p.a)(p.b)=='C'){val s=K()
s.c=f(n).c+1
f=f.updated(p,s)
i=p
q.enqueue(p)
if(j=='C'&&h!=null&&(s.c+f(h).c)<=e)g=p}else if(o(p.a)(p.b)=='T'){val s=I()
s.c=f(n).c+1
h=p
f=f.updated(p,s)
q.enqueue(p)
if(j=='S')g=p else if(j=='C'&&i!=null&&(s.c+f(i).c)<=e){g=i}}else if(o(p.a)(p.b)=='?'){val o=L()
f=f.updated(p,o)
if(!(g!=null&&((j=='S'&&f(g).isInstanceOf[I])||(j=='C'&&f(g).isInstanceOf[K])))&&(g==null||f(n).c<f(g).c))
g=n}else if(o(p.a)(p.b)=='#'){val s=H()
f=f.updated(p,s)}else{val s=J()
q.enqueue(p)
s.c=f(n).c+1
f=f.updated(p,s)}}}
def o(a:B):List[B]={val b=f(a)
if(b.c!=0){o(b.b.filter(l=>f(l).a).filter(l=>f(l).c==b.c- 1).head):::List(a)}else List(a)}
def p(a:B,b:List[String])={l=a
if(b(a.a)(a.b)=='C'){m='S'
e-=1}
g=null
h=null
i=null
k=List()
f=Map()
f=f.updated(l,J())
j=if(m=='C'){if(b.exists(s=>s.contains('C')))'C'else'U'}else if(m=='S')'S'else 'U'
val v:mutable.Queue[B]=mutable.Queue.empty
v.enqueue(l)
while (v.nonEmpty){val c:B=v.dequeue()
n(c,b,B(c.a,c.D().b),v)
n(c,b,B(c.F().a,c.b),v)
n(c,b,B(c.a,c.C().b),v)
n(c,b,B(c.E().a,c.b),v)}
k=o(g)}
def q() ={if(k(1).b>l.b)"RIGHT"else if(k(1).a>l.a)"DOWN"else if(k(1).b<l.b)"LEFT"else if(k(1).a<l.a)"UP"else ""}}
object Player extends App{val Array(z,x,y)=readLine split " " map (_.toInt)
val w=new M(z,x,y)
while (true){val Array(r,c)=readLine split " "
val m=(0 until z).map(_=>readLine).toList
w.p(B(r.toInt,c.toInt),m)
println(w.q())}}