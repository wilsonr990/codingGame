package optimization.codeOfZombies;

import java.util.*;

class Human {
    int x;
    int y;
    int target;
    int turnsToTarget;

    Human(int x, int y) {
        this.x = x;
        this.y = y;
    }

    int distanceTo(int ox, int oy) {
        return (int) Math.sqrt((ox - x) * (ox - x) + (oy - y) * (oy - y));
    }
}

class Zombie {
    int x;
    int y;
    int target;
    int turnsToTarget;

    Zombie(int x, int y) {
        this.x = x;
        this.y = y;
    }

    int distanceTo(int ox, int oy) {
        return (int) Math.sqrt((ox - x) * (ox - x) + (oy - y) * (oy - y));
    }
}

class Player {

    public void main(String[] args) {
        Scanner in = new Scanner(System.in);

        // game loop
        while (true) {
            int x = in.nextInt();
            int y = in.nextInt();
            int humanCount = in.nextInt();

            Map<Integer, Human> humans = new HashMap<>();
            for (int i = 0; i < humanCount; i++) {
                int humanId = in.nextInt();
                int humanX = in.nextInt();
                int humanY = in.nextInt();
                humans.put(humanId, new Human(humanX, humanY));
            }
            int zombieCount = in.nextInt();

            Map<Integer, Zombie> zombies = new HashMap<>();
            for (int i = 0; i < zombieCount; i++) {
                int zombieId = in.nextInt();
                int zombieX = in.nextInt();
                int zombieY = in.nextInt();
                int zombieXNext = in.nextInt();
                int zombieYNext = in.nextInt();

                Zombie zombie = new Zombie(zombieX, zombieY);
                Integer targetHuman = humans.entrySet().stream().reduce((e1, e2) -> {
                    if (e1.getValue().distanceTo(zombieX, zombieY) < e2.getValue().distanceTo(zombieX, zombieY))
                        return e1;
                    else return e2;
                }).get().getKey();
                zombie.target = targetHuman;
                zombie.turnsToTarget = humans.get(targetHuman).distanceTo(zombieX, zombieY) / 400;

                zombies.put(zombieId, zombie);
            }

            Integer targetZombie = zombies.entrySet().stream().reduce((e1, e2) -> {
                Zombie z1 = e1.getValue();
                Zombie z2 = e2.getValue();

                if (z1.target == z2.target) {
                    if (z1.turnsToTarget < z2.turnsToTarget) {
                        return e1;
                    } else {
                        return e2;
                    }
                } else if ((z1.turnsToTarget < z2.turnsToTarget && z1.turnsToTarget >= z1.distanceTo(x, y) / 1000 - 2) ||
                        (z2.turnsToTarget < z2.distanceTo(x, y) / 1000 - 2)) {
                    return e1;
                } else if ((z2.turnsToTarget < z1.turnsToTarget && z2.turnsToTarget >= z2.distanceTo(x, y) / 1000 - 2) ||
                        (z1.turnsToTarget < z1.distanceTo(x, y) / 1000 - 2)) {
                    return e2;
                } else {
                    if (z1.turnsToTarget < z2.turnsToTarget) {
                        return e1;
                    } else {
                        return e2;
                    }
                }

            }).get().getKey();
            System.err.println(" >> " + targetZombie);


            int destinationX = zombies.get(targetZombie).x;
            int destinationY = zombies.get(targetZombie).y;

            System.out.println(destinationX + " " + destinationY); // Your destination coordinates
        }
    }
}