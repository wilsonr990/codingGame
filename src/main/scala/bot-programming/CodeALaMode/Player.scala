package botProgramming.CodeALaMode

import botProgramming.CodeALaMode.Item.Item

import scala.io.StdIn
import scala.math.{pow, sqrt}

case class Point(x: Int, y: Int) { // `val` not needed
  def distance(other: Point): Double =
    sqrt(pow(x - other.x, 2) + pow(y - other.y, 2))
}

abstract sealed class Action {
  def print()
}

case class Move(p: Point) extends Action {
  override def print() {
    println(s"MOVE ${p.x} ${p.y}")
  }
}

case class Wait() extends Action {
  override def print() {
    println(s"WAIT")
  }
}

case class Use(p: Point) extends Action {
  override def print() {
    println(s"USE ${p.x} ${p.y}")
  }
}

object TableType extends Enumeration {
  type TableType = Value
  val BLUEBERRY, STRAWBERRY, ICE_CREAM, PLATES, EMPTY, WINDOW, CHOPPING_BOARD, DOUGH, OVEN, NONE = Value

  def get(c: Char): TableType = c match {
    case 'D' => PLATES
    case 'B' => BLUEBERRY
    case 'I' => ICE_CREAM
    case 'W' => WINDOW
    case 'S' => STRAWBERRY
    case '#' => EMPTY
    case 'C' => CHOPPING_BOARD
    case 'H' => DOUGH
    case 'O' => OVEN
    case _   => NONE
  }
}

object Item extends Enumeration {
  type Item = Value
  val DISH, ICE_CREAM, BLUEBERRIES, STRAWBERRIES, CHOPPED_STRAWBERRIES, DOUGH, CROISSANT, NONE = Value
}

case class Table(_type: TableType.TableType, location: Point)

class Kitchen {
  private var tables: List[Table]         = List.empty
  var ovenContent: Item.Value = Item.NONE

  def addTable(table: Table): Unit = {
    tables = table :: tables
  }

  def setOvenContent(ovenContents: String, ovenTimer: Int): Unit = {
    ovenContent = Item.withName(ovenContents)
  }

  def get(item: TableType.TableType, point: Point): Table = {
    tables.filter(_._type == item).minBy(_.location.distance(point))
  }
}

case class Order(item: List[Item], award: Int)

class OrderManager {
  var orders: Seq[Order] = _

  def readOrders() {
    val numCustomers = StdIn.readInt() // the number of customers currently waiting for food
    orders = (0 until numCustomers).map(_ => {
      val Array(customerItem, customerAward) = StdIn.readLine split " "
      val items                              = customerItem.split("-").map(Item.withName)
      Order(items.toList, customerAward.toInt)
    })
  }

  def getOrder: Order = {
    orders.maxBy(order => order.award)
  }
}

class ActualPlayer(kitchen: Kitchen) {
  var croissantLocation: Option[Point] = None
  var location: Point                  = Point(0, 0)
  var served                           = false
  var holdingItems: List[Item]         = List.empty[Item]
  var currentOrder: List[Item]         = List.empty[Item]

  def hasOrder: Boolean = currentOrder.nonEmpty

  def assignOrder(order: Order) {
    currentOrder = order.item
  }

  def setLocation(point: Point) {
    location = point
  }

  def setHoldingItems(items: String) {
    holdingItems = items.split("-").map(Item.withName).toList
  }

  def prepare(): Unit = {
    System.err.println(
      s"$currentOrder - $holdingItems - ${kitchen.ovenContent}"
    )

    if (!served) {
      if (currentOrder.contains(Item.CROISSANT) && croissantLocation.isEmpty) {
        if (!holdingItems.contains(Item.CROISSANT)) {
          if (kitchen.ovenContent.equals(Item.NONE)) {
            if (!holdingItems.contains(Item.DOUGH))
              Use(kitchen.get(TableType.DOUGH, location).location).print()
            else
              Use(kitchen.get(TableType.OVEN, location).location).print()
          } else {
            Use(kitchen.get(TableType.OVEN, location).location).print()
          }
        } else {
          val closerEmpty = kitchen.get(TableType.EMPTY, location)
          Use(closerEmpty.location).print()
          croissantLocation = Some(closerEmpty.location)
        }
      } else if (holdingItems.contains(Item.CROISSANT) && !holdingItems
                   .contains(Item.DISH)) {
        Use(croissantLocation.get).print()
      } else if (currentOrder.contains(Item.CHOPPED_STRAWBERRIES) && !holdingItems
                   .contains(Item.CHOPPED_STRAWBERRIES)) {
        if (!holdingItems.contains(Item.STRAWBERRIES))
          Use(kitchen.get(TableType.STRAWBERRY, location).location).print()
        else if (!holdingItems.contains(Item.CHOPPED_STRAWBERRIES))
          Use(kitchen.get(TableType.CHOPPING_BOARD, location).location).print()
        else
          System.err.println("SHOULD NOT BE HERE!!!!")
      } else if (currentOrder.contains(Item.BLUEBERRIES) && !holdingItems
                   .contains(Item.BLUEBERRIES)) {
        if (!holdingItems.contains(Item.DISH))
          Use(kitchen.get(TableType.PLATES, location).location).print()
        else if (croissantLocation.isDefined && !holdingItems.contains(
                   Item.CROISSANT
                 ))
          Use(croissantLocation.get).print()
        else
          Use(kitchen.get(TableType.BLUEBERRY, location).location).print()
      } else if (currentOrder.contains(Item.ICE_CREAM) && !holdingItems
                   .contains(Item.ICE_CREAM)) {
        if (!holdingItems.contains(Item.DISH))
          Use(kitchen.get(TableType.PLATES, location).location).print()
        else if (croissantLocation.isDefined && !holdingItems.contains(
                   Item.CROISSANT
                 ))
          Use(croissantLocation.get).print()
        else
          Use(kitchen.get(TableType.ICE_CREAM, location).location).print()
      } else {
        served = true
        prepare()
      }
    } else {
      if (holdingItems.contains(Item.DISH)) {
        Use(kitchen.get(TableType.WINDOW, location).location).print()
      } else {
        served = false
        holdingItems = List.empty[Item]
        currentOrder = List.empty[Item]
        croissantLocation = None
        Wait().print()
      }
    }
  }
}

object Player extends App {
  val gridMaxX           = 11
  val gridMaxY           = 7
  val maxGeneralTurns    = 200
  val maxIndividualTurns = 100
  val maxCustomers       = 3

  val numAllCustomers = StdIn.readInt()
  for (i <- 0 until numAllCustomers) {
    // customerItem: the food the customer is waiting for
    // customerAward: the number of points awarded for delivering the food
    val Array(customerItem, customerAward) = StdIn.readLine split " "
  }

  val kitchen = new Kitchen()
  for (i <- 0 until 7) {
    val kitchenLine = StdIn.readLine
    var j           = 0
    while (j < kitchenLine.length) {
      val c = kitchenLine.charAt(j)
      val t = TableType.get(c)
      kitchen.addTable(Table(t, Point(j, i)))
      j += 1
      j - 1
    }
  }

  val player       = new ActualPlayer(kitchen)
  val orderManager = new OrderManager()

  // game loop
  while (true) {
    val turnsRemaining                      = StdIn.readInt()
    val Array(playerX, playerY, playerItem) = StdIn.readLine split " "
    player.setHoldingItems(playerItem)
    player.setLocation(Point(playerX.toInt, playerY.toInt))
    val Array(_partnerx, _partnery, partnerItem) = StdIn.readLine split " "
    val partnerx                                 = _partnerx.toInt
    val partnery                                 = _partnery.toInt
    val numtableswithitems                       = StdIn.readInt() // the number of tables in the kitchen that currently hold an item
    for (i <- 0 until numtableswithitems) {
      val Array(tableX, tableY, item) = StdIn.readLine split " "
    }

    // ovenContents: ignore until wood 1 league
    val Array(ovenContents, _oventimer) = StdIn.readLine split " "
    val ovenTimer                       = _oventimer.toInt
    kitchen.setOvenContent(ovenContents, ovenTimer)

    orderManager.readOrders()

    if (!player.hasOrder) {
      player.assignOrder(orderManager.getOrder)
    }

    player.prepare()
  }
}
