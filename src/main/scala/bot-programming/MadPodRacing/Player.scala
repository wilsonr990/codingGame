package botProgramming.MadPodRacing

import java.lang.reflect.Field
import java.util.UUID
import Pod.{MovementProps, RaceProps, StartProps}

import scala.annotation.tailrec
import scala.io.StdIn

// utils
sealed class Status()
case class RawStatus(a: Any) extends Status()
sealed class CustomException(sender: Any, status: Status, intention: Any) extends Exception() {
  override def getMessage: String = {
    def beautify(obj: Any) =
      obj.getClass.getName
        .replaceFirst(".*\\.", "")
        .replaceAll("([A-Z])", " $1")
        .replaceAll("([$])", "")
        .trim
    s"${beautify(this)}:\n  Sender -> $sender\n  Status -> $status\n  Intention -> $intention"
  }
}

case class ID(id: UUID = UUID.randomUUID())
trait Builder[T] {
  def apply(): this.type = {
    reset()
    this
  }
  def build(id: ID): T
  def reset() {
    val fields: Array[Field] = this.getClass.getDeclaredFields
    fields.foreach(f => {
      f.setAccessible(true)
      f.get(this) match {
        case _: Option[Any]   => f.set(this, None)
        case _: List[Any]     => f.set(this, List.empty)
        case _: Set[Any]      => f.set(this, Set.empty)
        case _: Map[Any, Any] => f.set(this, Map.empty)
        case _: Integer       => f.set(this, 0)
        case _                =>
      }
    })
  }
}
object a {
  def apply[T](obj: Builder[T]): T = obj.build(ID())
}

/** ANGLE */
sealed case class Angle(a: Double) {
  def +(other: Angle): Angle    = Angle(a + other.a)
  def -(other: Angle): Angle    = Angle(a - other.a)
  def <=(other: Angle): Boolean = a <= other.a
  def >=(other: Angle): Boolean = a >= other.a
  def <(other: Angle): Boolean  = a < other.a
  def >(other: Angle): Boolean  = a > other.a
  def /(c: Double): Angle       = Angle(a / c)
  def *(c: Double): Angle       = Angle(a * c)

  def grades: Double = a * 180 / Math.PI

  lazy val sin: Double = Math.sin(a)
  lazy val cos: Double = Math.cos(a)
  lazy val tan: Double = Math.tan(a)
}
object Angle {
  def apply(a: Double): Angle = {
    new Angle(normalAngle(a))
  }

  def fromGrades(a: Double): Angle = {
    new Angle(normalAngle(a * Math.PI / 180))
  }

  val PI: Angle = Angle(Math.PI)

  @tailrec
  def normalAngle(a: Double): Double = {
    if (a < 0) normalAngle(a + 2 * Math.PI)
    else if (a >= 2 * Math.PI) normalAngle(a - 2 * Math.PI)
    else a
  }
}

/** VECTOR */
sealed case class Vector(x: Double, y: Double, a: Angle, d: Double) {
  def +(other: Vector): Vector         = Vector(x + other.x, y + other.y)
  def -(other: Vector): Vector         = Vector(x - other.x, y - other.y)
  def *(scalar: Double): Vector        = Vector.fromP(a, d * scalar)
  def projected(other: Vector): Vector = if (other.d != 0) Vector.fromP(other.a, d * (a - other.a).cos) else this

  def closestDistanceTo(other: Vector): Double = {
    if (a - other.a < Angle(Math.PI / 2) || a - other.a > Angle(3 * Math.PI / 2)) {
      val projection = other.projected(this)
      if (projection.d > d)
        this.distance(other)
      else
        projection.distance(other)
    } else
      other.d
  }

  // TODO: implement sqDistance to calculate squared distance and use it in algorithms where required (would improve perf)
  def distance(other: Vector): Double = {
    val dx = x - other.x
    val dy = y - other.y
    Math.sqrt(dx * dx + dy * dy)
  }
}
object Vector {
  def apply(x: Double, y: Double): Vector = {
    Vector(x, y, Angle(Math.atan2(y, x)), Math.sqrt(x * x + y * y))
  }
  def fromP(a: Angle, d: Double): Vector = {
    if (d > 0)
      Vector(d * a.cos, d * a.sin, a, d)
    else Vector(d * a.cos, d * a.sin, a + Angle.PI, -d)
  }
}

/** DIMENSION */
case class Dimension(x: Int, y: Int)

/** POD */
sealed case class Pod(id: ID, radius: Int) {
  var status: Status = Pod.WaitingTeam()

  def `validateCanBeInTeam`(team: Team): Boolean =
    status match {
      case Pod.WaitingTeam() => true
      case _                 => throw Pod.AlreadyHasATeam(this, status, team)
    }

  def `inTeam`(team: Team): Pod = {
    status match {
      case Pod.WaitingTeam() => status = Pod.WaitingRace(team)
      case _                 => throw Pod.AlreadyHasATeam(this, status, team)
    }
    this
  }

  def `inRace`(race: Race): Pod = {
    status match {
      case Pod.WaitingRace(team) => status = Pod.RegisteredInRace(team, race)
      case _                     => throw Pod.NotReadyForARace(this, status, race)
    }
    this
  }

  def startsIn(position: Vector): Pod = {
    status match {
      case Pod.RegisteredInRace(team, race) if race.`canPlacePodIn`(this, position) =>
        status = Pod.WaitingToStart(team, race, Pod.StartProps(Some(position), None, None))
      case s @ Pod.WaitingToStart(_, race, m @ Pod.StartProps(None, _, _)) if race.`canPlacePodIn`(this, position) =>
        status = s.copy(movementProps = m.copy(location = Some(position)))
        if (race.`readyToStart`) race.`startRace`
      case _: Pod.RegisteredInRace => throw Pod.CantStartIn(this, status, position)
      case _: Pod.WaitingToStart   => throw Pod.CantStartIn(this, status, position)
      case _                       => throw Pod.NotReadyForARace(this, status, "setting initial location")
    }
    this
  }

  def startsWithAngle(angle: Angle): Pod = {
    status match {
      case Pod.RegisteredInRace(team, race) =>
        status = Pod.WaitingToStart(team, race, Pod.StartProps(None, Some(angle), None))
      case s @ Pod.WaitingToStart(_, race, m @ Pod.StartProps(_, None, _)) =>
        status = s.copy(movementProps = m.copy(angle = Some(angle)))
        if (race.`readyToStart`) race.`startRace`
      case _ => throw Pod.NotReadyForARace(this, status, "setting initial angle")
    }
    this
  }

  def startsWithInitialSpeed(speed: Vector): Pod = {
    status match {
      case Pod.RegisteredInRace(team, race) =>
        status = Pod.WaitingToStart(team, race, Pod.StartProps(None, None, Some(speed)))
      case s @ Pod.WaitingToStart(_, race, m @ Pod.StartProps(_, _, None)) =>
        status = s.copy(movementProps = m.copy(speed = Some(speed)))
        if (race.`readyToStart`) race.`startRace`
      case _ => throw Pod.NotReadyForARace(this, status, "setting initial speed")
    }
    this
  }

  def startsWithInput(): Pod = {
    status match {
      case Pod.RegisteredInRace(_, _) =>
        val Array(x, y, vx, vy, a, _) = StdIn.readLine().split(" ").map(_.toInt)
        startsIn(Vector(x, y)).startsWithAngle(Angle(a)).startsWithInitialSpeed(Vector(vx, vy))
      case _ => throw Pod.NotReadyForARace(this, status, "starting With Input")
    }
    this
  }

  def `readyToStart`: Boolean = status match {
    case Pod.WaitingToStart(_, _, Pod.StartProps(_: Some[Vector], _: Some[Angle], _: Some[Vector])) => true
    case _                                                                                          => false
  }

  def `touchesPodIn`(pod: Pod, position: Vector): Boolean = {
    if (pod == this) return false
    status match {
      case Pod.WaitingToStart(_, _, Pod.StartProps(Some(d), _, _)) => position.distance(d) < this.radius + pod.radius
      case Pod.InRace(_, _, Pod.MovementProps(d, _, _, _), _)      => position.distance(d) < this.radius + pod.radius
      case _                                                       => false
    }
  }

  def `startRace`: Pod = {
    status match {
      case Pod.WaitingToStart(team, race, StartProps(Some(position), Some(angle), Some(speed))) =>
        status = Pod.InRace(team, race, MovementProps(position, angle, speed), RaceProps())
      case _ => throw Pod.NotReadyForARace(this, status, "starting Race")
    }
    this
  }

  def nextAction(destination: Vector, thrust: Int): Pod = {
    status match {
      case s: Pod.InRace if thrust >= 0 && thrust <= 100 =>
        status = Pod.WithActions(destination, thrust, s)
      case s: Pod.WithActions if thrust >= 0 && thrust <= 100 =>
        status = s.copy(destination, thrust)
      case _: Pod.InRace =>
        throw Pod.InvalidThrust(this, status, s"($destination,$thrust)")
      case _: Pod.WithActions =>
        throw Pod.InvalidThrust(this, status, s"($destination,$thrust)")
      case _ => throw Pod.NotInRace(this, status, s"($destination,$thrust)")
    }
    this
  }

  def runTurn: Pod = {
    status match {
      case _: Pod.InRace =>
      case Pod.WithActions(destination, thrust, inRace @ Pod.InRace(_, race, m @ MovementProps(location, angle, speed, _), _)) =>
        val desired: Vector = destination - location
        val correctionAngle = desired.a - angle
        val finalAng =
          if (race.turn == 0 ||
              correctionAngle <= Angle.PI / 10 ||
              correctionAngle >= Angle.PI * 2 - Angle.PI / 10)
            desired.a
          else if (correctionAngle < Angle.PI)
            angle + Angle.PI / 10
          else
            angle - Angle.PI / 10

        val acc          = Vector.fromP(finalAng, thrust)
        val updatedSpeed = speed + acc
        status = inRace.copy(movementProps = m.copy(angle = finalAng, speed = updatedSpeed))
      case _ => throw Pod.NotInRace(this, status, "updating")
    }
    this
  }

  def timeToCollide(otherPod: Pod, time: Double = 1): Double = {
    (status, otherPod.status) match {
      case (Pod.InRace(_, _, MovementProps(location, _, speed, _), _), Pod.InRace(_, _, MovementProps(oLocation, _, oSpeed, _), _))
          if this != otherPod =>
        val relativeSpeed    = speed * time - oSpeed * time
        val relativeLocation = oLocation - location
        val closestDistance  = relativeSpeed.closestDistanceTo(relativeLocation)

        if (closestDistance < otherPod.radius + radius) {
          val projection            = relativeLocation.projected(relativeSpeed)
          val perpendicularDistance = projection.distance(relativeLocation)

          val pointOfContactToProjection = Math.sqrt(
            (radius + otherPod.radius) * (radius + otherPod.radius) - perpendicularDistance *
              perpendicularDistance)
          val distanceToCollision = projection.d - pointOfContactToProjection

          val timeToCollide = distanceToCollision * time / relativeSpeed.d
          if (timeToCollide < 0)
            time
          else timeToCollide
        } else
          time
      case _ => throw Pod.NotInRace(this, status, otherPod)
    }
  }

  def timeToCollideNextCheckpoint(time: Double): Double = {
    status match {
      case Pod.InRace(_, race, MovementProps(location, _, speed, _), RaceProps(nextCheck, _)) =>
        val Checkpoint(cLocation, cRadius) = race.circuit.checkpoints(nextCheck)

        val relativeSpeed    = speed * time
        val relativeLocation = cLocation - location
        val closestDistance  = relativeSpeed.closestDistanceTo(relativeLocation)

        if (closestDistance < cRadius + radius) {
          val projection            = relativeLocation.projected(relativeSpeed)
          val perpendicularDistance = projection.distance(relativeLocation)

          val pointOfContactToProjection = Math.sqrt((radius + cRadius) * (radius + cRadius) - perpendicularDistance * perpendicularDistance)
          val distanceToCollision        = projection.d - pointOfContactToProjection

          val timeToCollide = distanceToCollision * time / relativeSpeed.d
          if (timeToCollide < 0)
            time
          else timeToCollide
        } else
          time
      case _ => throw Pod.NotInRace(this, status, "colliding checkpoint")
    }
  }

  def passNextCheckpoint: Pod = {
    status match {
      case s @ Pod.InRace(_, race, _, r @ RaceProps(nextCheck, lap)) =>
        val nextCheckpoint = if ((nextCheck + 1) == race.circuit.checkpoints.size) 0 else nextCheck + 1
        if (nextCheck == 0) {
          if (lap + 1 != race.laps)
            status = s.copy(raceProps = r.copy(lap = lap + 1, nextCheck = nextCheckpoint))
          else
            race.`completedBy`(this)
        } else {
          status = s.copy(raceProps = r.copy(nextCheck = nextCheckpoint))
        }
      case _ => throw Pod.NotInRace(this, status, "passingCheckPoint")
    }
    this
  }

  def raceCompleted(pod: Pod): Pod = {
    status match {
      case s @ Pod.InRace(team, race, _, _) =>
        status = Pod.CompletedRace(team, race, s)
      case _ => throw Pod.NotInRace(this, status, "passingCheckPoint")
    }
    this
  }

  def move(time: Double = 1): Pod = {
    status match {
      case s @ Pod.InRace(_, _, m @ MovementProps(location, _, speed, _), _) =>
        val finalLocation = location + speed * time
        status = s.copy(
          movementProps = m.copy(
            location = Vector(Math.round(finalLocation.x), Math.round(finalLocation.y))
          )
        )
      case _ => throw Pod.NotInRace(this, status, "updating")
    }
    this
  }

  def getImpulseFrom(otherPod: Pod): Vector = {
    (status, otherPod.status) match {
      case (Pod.InRace(_, _, MovementProps(_, _, speed, _), _), Pod.InRace(_, _, MovementProps(_, _, oSpeed, _), _)) =>
        val impulse = oSpeed.projected(speed)
        if (impulse.d < 120)
          Vector.fromP(impulse.a, 120)
        else
          impulse
      case _ => throw Pod.NotInRace(this, status, "updating")
    }
  }

  def applyImpulse(impulse: Vector): Pod = {
    status match {
      case s @ Pod.InRace(_, _, m @ MovementProps(_, _, speed, _), _) =>
        val newSpeed = speed + impulse - speed.projected(impulse)
        status = s.copy(
          movementProps = m.copy(
            speed = newSpeed
          )
        )
      case _ => throw Pod.NotInRace(this, status, "updating")
    }
    this
  }

  def friction: Pod = {
    status match {
      case s @ Pod.InRace(_, _, m @ MovementProps(location, angle, speed, _), _) =>
        val finalSpeed = speed * 0.85
        status = s.copy(
          movementProps = m.copy(
            location = Vector(Math.round(location.x), Math.round(location.y)),
            angle = Angle.fromGrades(Math.round(angle.grades)),
            speed = Vector(Math.round(finalSpeed.x), Math.round(finalSpeed.y))
          )
        )
      case _ => throw Pod.NotInRace(this, status, "updating")
    }
    this
  }

  def `touching`(checkpoint: Checkpoint): Boolean = {
    status match {
      case Pod.InRace(_, _, MovementProps(location, _, _, _), _) => checkpoint.location.distance(location) < checkpoint.radius
      case _                                                     => throw Pod.NotInRace(this, status, checkpoint)
    }
  }

  def getMovementProps: MovementProps = {
    status match {
      case Pod.InRace(_, _, movementProps, _)                        => movementProps
      case Pod.WithActions(_, _, Pod.InRace(_, _, movementProps, _)) => movementProps
      case _                                                         => throw Pod.NotInRace(this, status, "gettingMovementProperties")
    }
  }

  def getRaceProps: RaceProps = {
    status match {
      case Pod.InRace(_, _, _, raceProps) => raceProps
      case _                              => throw Pod.NotInRace(this, status, "updating")
    }
  }
}
object Pod extends Builder[Pod] {
  case class StartProps(location: Option[Vector], angle: Option[Angle], speed: Option[Vector])
  case class MovementProps(location: Vector, angle: Angle, speed: Vector, mass: Int = 1)
  case class RaceProps(nextCheck: Int = 0, lap: Int = -1)

  private final case class WaitingTeam()                                                                      extends Status()
  private final case class WaitingRace(team: Team)                                                            extends Status()
  private final case class RegisteredInRace(team: Team, race: Race)                                           extends Status()
  private final case class WaitingToStart(team: Team, race: Race, movementProps: StartProps)                  extends Status()
  private final case class InRace(team: Team, race: Race, movementProps: MovementProps, raceProps: RaceProps) extends Status()
  private final case class WithActions(destination: Vector, thrust: Int, status: InRace)                      extends Status()
  private final case class CompletedRace(team: Team, race: Race, lastState: InRace)                           extends Status()

  var radius: Option[Int] = None

  def withRadius(radius: Int): Pod.type = {
    this.radius = Some(radius)
    this
  }

  override def build(id: ID): Pod = {
    if (radius.isEmpty) throw NotReadyToBuildData()
    new Pod(id, radius.get)
  }

  case class NotReadyToBuildData()                                           extends CustomException(this, RawStatus(radius), "building")
  case class AlreadyHasATeam(sender: Pod, status: Status, intention: Any)    extends CustomException(sender, status, intention)
  case class InvalidThrust(sender: Pod, status: Status, intention: Any)      extends CustomException(sender, status, intention)
  case class UpdateError(sender: Pod, status: Status, intention: Any)        extends CustomException(sender, status, intention)
  case class NotReadyForARace(sender: Pod, status: Status, intention: Any)   extends CustomException(sender, status, intention)
  case class NotInRace(sender: Pod, status: Status, intention: Any)          extends CustomException(sender, status, intention)
  case class CantStartIn(sender: Pod, status: Status, intention: Any)        extends CustomException(sender, status, intention)
  case class CantPassCheckpoint(sender: Pod, status: Status, intention: Any) extends CustomException(sender, status, intention)
  case class NotNextCheckpoint(sender: Pod, status: Status, intention: Any)  extends CustomException(sender, status, intention)
  case class ThrustNotValid(sender: Pod, status: Status, intention: Any)     extends CustomException(sender, status, intention)
}

/** TEAM */
sealed case class Team(pods: List[Pod]) {
  var status: Status = Team.WaitingForController()

  def `controlledBy`(player: APlayer): Team = {
    status match {
      case Team.WaitingForController() => status = Team.WaitingRace(player)
      case _                           => throw Team.AlreadyControlledByAPlayer(this, status, player)
    }
    this
  }

  def `participatesIn`(race: Race): Team = {
    status match {
      case Team.WaitingRace(player) =>
        race.`withParticipant`(this)
        pods.foreach(_.`inRace`(race))
        status = Team.WaitingToStart(player, race)
      case _ => throw Team.CantParticipateInRace(this, status, race)
    }
    this
  }

  def `readyToStart`: Boolean = {
    status match {
      case Team.WaitingToStart(_, _) =>
        pods.forall(_.`readyToStart`)
      case _ => false
    }
  }

  def `canPlacePodIn`(pod: Pod, position: Vector): Boolean = {
    status match {
      case Team.WaitingToStart(_, _) => !pods.exists(_.`touchesPodIn`(pod, position))
      case _                         => false
    }
  }

  def `startRace`: Team = {
    status match {
      case Team.WaitingToStart(player, race) =>
        pods.foreach(_.`startRace`)
        status = Team.InRace(player, race)
      case _ => throw Team.NotInRace(this, status, "starting Race")
    }
    this
  }
}
object Team extends Builder[Team] {
  private final case class WaitingForController()                      extends Status()
  private final case class WaitingRace(player: APlayer)                extends Status()
  private final case class WaitingToStart(player: APlayer, race: Race) extends Status()
  private final case class InRace(player: APlayer, race: Race)         extends Status()

  var pods: List[Pod] = List.empty
  var team            = new Team(pods)

  def withPod(pod: Pod): Team.type = {
    if (pods.size == 2) throw AlreadyHaveTwoBotsException(pod)
    this.pods :::= List(pod)
    this
  }

  override def build(id: ID): Team = {
    if (pods.size < 2) throw NotEnoughPodsException()
    val team: Team = new Team(pods)
    pods.forall(_.`validateCanBeInTeam`(team))
    pods.map(_.`inTeam`(team))
    team
  }

  case class NotEnoughPodsException()                                                 extends CustomException(this, RawStatus((pods, team)), "building")
  case class AlreadyHaveTwoBotsException(intention: Any)                              extends CustomException(this, RawStatus((pods, team)), intention)
  case class AlreadyControlledByAPlayer(sender: Team, status: Status, intention: Any) extends CustomException(sender, status, intention)
  case class CantParticipateInRace(sender: Team, status: Status, intention: Any)      extends CustomException(sender, status, intention)
  case class NotInRace(sender: Team, status: Status, intention: Any)                  extends CustomException(sender, status, intention)
}

/** CHECKPOINT */
sealed case class Checkpoint(location: Vector, radius: Int) {
  var status: Status = Checkpoint.NoStatus()

  def `touching`(checkpoint: Checkpoint): Boolean = {
    location.distance(checkpoint.location) < (radius + checkpoint.radius)
  }
}
object Checkpoint extends Builder[Checkpoint] {
  private final case class NoStatus() extends Status()

  var position: Option[Vector] = None
  var radius: Option[Int]      = None

  def withRadius(radius: Int): Checkpoint.type = {
    this.radius = Some(radius)
    this
  }

  def withPosition(position: Vector): Checkpoint.type = {
    this.position = Some(position)
    this
  }

  def fromInput: Checkpoint.type = {
    val Array(x, y) = StdIn.readLine().split(" ").map(_.toInt)
    withRadius(400).withPosition(Vector(x, y))
  }

  override def build(id: ID): Checkpoint = {
    if (position.isEmpty || radius.isEmpty) throw NotReadyToBuildData()
    new Checkpoint(position.get, radius.get)
  }

  case class NotReadyToBuildData() extends CustomException(this, RawStatus((position, radius)), "building")
}

/** CIRCUIT */
sealed case class Circuit(id: ID, checkpoints: Map[Int, Checkpoint]) {
  var status: Status = Circuit.NoStatus()
}
object Circuit extends Builder[Circuit] {
  private final case class NoStatus() extends Status()

  var checkpoints = Map.empty[Int, Checkpoint]
  var index       = 0

  def withCheckpoint(checkpoint: Checkpoint): Circuit.type = {
    if (checkpoints.values.exists(_.`touching`(checkpoint)))
      throw CheckpointsShouldNotBeTouching(checkpoint)
    this.checkpoints += (index -> checkpoint)
    index += 1
    this
  }

  override def build(id: ID): Circuit = {
    if (checkpoints.size < 2) throw NotEnoughCheckpointsException()
    new Circuit(id, checkpoints)
  }

  case class NotEnoughCheckpointsException()                extends CustomException(this, RawStatus(checkpoints, index), "building")
  case class CheckpointsShouldNotBeTouching(intention: Any) extends CustomException(this, RawStatus(checkpoints, index), intention)
}

/** RACE */
sealed case class Race(id: ID, circuit: Circuit, laps: Int) {
  var status: Status = Race.WaitingForParticipant(List.empty)

  def `withParticipant`(team: Team): Race = {
    status match {
      case Race.WaitingForParticipant(teams) if teams.size < 2 =>
        status = Race.WaitingForParticipant(teams ::: List(team))
      case _ =>
        throw Race.NotWaitingForParticipant(this, status, team)
    }
    this
  }

  def `readyToStart`: Boolean = {
    status match {
      case Race.WaitingForParticipant(teams) if teams.size == 2 && teams.forall(_.`readyToStart`) => true
      case _                                                                                      => false
    }
  }

  def `startRace`: Race = {
    status match {
      case Race.WaitingForParticipant(teams) if teams.size == 2 =>
        teams.foreach(_.`startRace`)
        status = Race.InProgress(teams)
      case _ =>
        throw Race.NoReadyToStart(this, status, "starting Race")
    }
    this
  }

  def `canPlacePodIn`(pod: Pod, position: Vector): Boolean = {
    status match {
      case Race.WaitingForParticipant(teams) if teams.forall(_.`canPlacePodIn`(pod, position)) => true
      case _                                                                                   => false
    }
  }

  def updateTurn(time: Double = 1): Race = {
    status match {
      case s @ Race.InProgress(teams, turn) if time > 0 =>
        val pods = teams.flatten(_.pods.map(_.runTurn))
        val nextCheckpointCollisionTime = pods.map { pod =>
          pod.timeToCollideNextCheckpoint(time) -> pod
        }
        val podCollisionTimes = pods.combinations(2).toList.map {
          case collidingPods @ List(pod1, pod2) =>
            pod1.timeToCollide(pod2, time) -> collidingPods
        }
        val (nextCollisionTime, List(collidingPod1, collidingPod2)) = podCollisionTimes.minBy(_._1)
        pods.foreach(_.move(nextCollisionTime))
        if (nextCollisionTime < time) {
          val impulse1 = collidingPod1.getImpulseFrom(collidingPod2)
          val impulse2 = collidingPod2.getImpulseFrom(collidingPod1)
          collidingPod1.applyImpulse(impulse1)
          collidingPod2.applyImpulse(impulse2)
          updateTurn(time - nextCollisionTime)
        } else {
          pods.foreach(_.friction)
          status = s.copy(turn = turn + 1)
        }
        nextCheckpointCollisionTime.foreach {
          case (timeToCollide, pod) if timeToCollide < time =>
            pod.passNextCheckpoint
          case _ =>
        }
      case Race.InProgress(_, _) =>
      case _                     => throw Race.RaceNotInProgress(this, status, time)
    }
    this
  }

  def `completedBy`(pod: Pod): Race = {
    status match {
      case Race.InProgress(teams, _) if teams.exists(_.pods.contains(pod)) =>
        teams.flatten(_.pods).foreach(_.raceCompleted(pod))
        status = Race.Completed(teams.find(_.pods.contains(pod)).get)
      case Race.InProgress(_, _) => throw Race.PodNotInRace(this, status, pod)
      case _                     => throw Race.RaceNotInProgress(this, status, pod)
    }
    this
  }

  def lapsOf(pod: Pod): Int = {
    status match {
      case Race.InProgress(_, _) => if (pod.getRaceProps.lap > 0) pod.getRaceProps.lap else 0
      case _                     => throw Race.RaceNotInProgress(this, status, pod)
    }
  }

  def turn(): Int = {
    status match {
      case Race.InProgress(_, turn) => turn
      case _                        => throw Race.RaceNotInProgress(this, status, "gettingTurn")
    }
  }

  def ended: Boolean = {
    status match {
      case Race.Completed(_) => true
      case _                 => false
    }
  }

  def winner: Team = {
    status match {
      case Race.Completed(winner) => winner
      case _                      => throw Race.RaceNotCompleted(this, status, "winner")
    }
  }
}
object Race extends Builder[Race] {
  private final case class WaitingForParticipant(teams: List[Team])     extends Status()
  private final case class InProgress(teams: List[Team], turn: Int = 0) extends Status()
  private final case class Completed(winner: Team)                      extends Status()

  var circuit: Option[Circuit] = None
  var laps: Option[Int]        = None

  def withCircuit(circuit: Circuit): Race.type = {
    this.circuit = Some(circuit)
    this
  }

  def withLaps(laps: Int): Race.type = {
    if (laps <= 0) throw NumberOfLapsShouldBeGreaterThanZero(laps)
    this.laps = Some(laps)
    this
  }

  def fromInput: Race.type = {
    val laps    = StdIn.readInt()
    val circuit = Circuit()
    (0 until StdIn.readInt()).map(_ => {
      circuit.withCheckpoint(a(Checkpoint().fromInput))
    })
    withCircuit(a(circuit)).withLaps(laps)
  }

  override def build(id: ID): Race = {
    if (circuit.isEmpty || laps.isEmpty) throw NotReadyToBuildData()
    new Race(id, circuit.get, laps.get)
  }

  case class NotReadyToBuildData()                                                  extends CustomException(this, RawStatus((circuit, laps)), "building")
  case class NumberOfLapsShouldBeGreaterThanZero(intention: Any)                    extends CustomException(this, RawStatus((circuit, laps)), intention)
  case class RaceNotInProgress(sender: Race, status: Status, intention: Any)        extends CustomException(sender, status, intention)
  case class PodNotInRace(sender: Race, status: Status, intention: Any)             extends CustomException(sender, status, intention)
  case class RaceNotCompleted(sender: Race, status: Status, intention: Any)         extends CustomException(sender, status, intention)
  case class NotWaitingForParticipant(sender: Race, status: Status, intention: Any) extends CustomException(sender, status, intention)
  case class NoReadyToStart(sender: Race, status: Status, intention: Any)           extends CustomException(sender, status, intention)
}

/** PLAYER */
sealed case class APlayer(id: ID) {
  var status: Status = APlayer.WaitingForTeam()

  def controls(team: Team): APlayer = {
    status match {
      case APlayer.WaitingForTeam() =>
        status = APlayer.WaitingForRace(team.`controlledBy`(this))
      case _ => throw APlayer.NotReadyToControlTeam(this, status, team)
    }
    this
  }

  def playInRace(race: Race): APlayer = {
    status match {
      case APlayer.WaitingForRace(team) =>
        status = APlayer.inRace(team.`participatesIn`(race))
      case _ => throw APlayer.NotReadyToPlayInRace(this, status, race)
    }
    this
  }

  def playRandom(): APlayer = {
    status match {
      case APlayer.inRace(team) =>
        println("8000 4500 100")
        println("8000 4500 100")
        team.pods.head.nextAction(Vector(8000, 4500), 100)
        team.pods(1).nextAction(Vector(8000, 4500), 100)
        team.pods.head.move()
        team.pods(1).move()
      case _ => throw APlayer.NotReadyToPlayInRace(this, status, "randomMove")
    }
    this
  }
}
object APlayer extends Builder[APlayer] {
  private final case class WaitingForTeam()           extends Status()
  private final case class WaitingForRace(team: Team) extends Status()
  private final case class inRace(team: Team)         extends Status()

  override def build(id: ID): APlayer = {
    new APlayer(id)
  }
  case class NotReadyToControlTeam(sender: APlayer, status: Status, intention: Any) extends CustomException(sender, status, intention)
  case class NotReadyToPlayInRace(sender: APlayer, status: Status, intention: Any)  extends CustomException(sender, status, intention)
}

/** MAP */
sealed class aMap(id: ID, dimension: Dimension)
object aMap extends Builder[aMap] {
  var dimension: Option[Dimension] = None

  def withDimension(dimension: Dimension): aMap.type = {
    this.dimension = Some(dimension)
    this
  }

  override def build(id: ID): aMap = {
    if (dimension.isEmpty) throw NoDimensionException()
    new aMap(id, dimension.get)
  }

  case class NoDimensionException() extends CustomException(this, RawStatus(dimension), "building")
}

/** GAME */
abstract sealed class Game(id: ID, map: aMap)
sealed class CodersStrikeBackGame(id: ID, map: aMap) extends Game(id, map) {
  var status: Status = CodersStrikeBackGame.WaitingPlayers(List.empty)
  val pod1           = a(Pod().withRadius(400))
  val pod2           = a(Pod().withRadius(400))
  val pod3           = a(Pod().withRadius(400))
  val pod4           = a(Pod().withRadius(400))
  val team1          = a(Team().withPod(pod1).withPod(pod2))
  val team2          = a(Team().withPod(pod3).withPod(pod4))

  def setPlayer(player: APlayer): CodersStrikeBackGame = {
    status match {
      case CodersStrikeBackGame.WaitingPlayers(players) if players.isEmpty =>
        status = CodersStrikeBackGame.WaitingPlayers(List(player))
        player.controls(team1)
      case CodersStrikeBackGame.WaitingPlayers(players) if players.size == 1 =>
        status = CodersStrikeBackGame.WaitingRace(players ::: List(player))
        player.controls(team2)
      case _ => throw CodersStrikeBackGame.NotWaitingForPlayers(this, status, player)
    }
    this
  }

  def setRace(race: Race): CodersStrikeBackGame = {
    status match {
      case CodersStrikeBackGame.WaitingRace(players) =>
        players.foreach(_.playInRace(race))
        status = CodersStrikeBackGame.WaitingRaceStart(players, race)
      case _ => throw CodersStrikeBackGame.NotWaitingForRace(this, status, race)
    }
    this
  }

  def startFromInput(): CodersStrikeBackGame = {
    status match {
      case CodersStrikeBackGame.WaitingRaceStart(players, race) =>
        pod1.startsWithInput()
        pod2.startsWithInput()
        pod3.startsWithInput()
        pod4.startsWithInput()
        status = CodersStrikeBackGame.Playing(players, race)
      case _ => throw CodersStrikeBackGame.NotReadyToInitialize(this, status, "initializing")
    }
    this
  }
}
object CodersStrikeBackGame {
  private final case class WaitingPlayers(players: List[APlayer])               extends Status()
  private final case class WaitingRace(players: List[APlayer])                  extends Status()
  private final case class WaitingRaceStart(players: List[APlayer], race: Race) extends Status()
  private final case class Playing(players: List[APlayer], race: Race)          extends Status()

  case class NotWaitingForRace(sender: CodersStrikeBackGame, status: Status, intention: Any)    extends CustomException(sender, status, intention)
  case class NotWaitingForPlayers(sender: CodersStrikeBackGame, status: Status, intention: Any) extends CustomException(sender, status, intention)
  case class NotReadyToInitialize(sender: CodersStrikeBackGame, status: Status, intention: Any) extends CustomException(sender, status, intention)
}
object Game extends Builder[Game] {

  var map: Option[aMap] = None

  def withMap(map: aMap): Game.type = {
    this.map = Some(map)
    this
  }

  override def build(id: ID): Game = {
    if (map.isEmpty) throw NoMapException()
    new CodersStrikeBackGame(id, map.get)
  }

  case class NoMapException() extends CustomException(this, RawStatus(map), "building")
}

object Player extends App {
  val game: CodersStrikeBackGame = a(Game().withMap(a(aMap().withDimension(Dimension(16000, 9000)))))
    .asInstanceOf[CodersStrikeBackGame]
  private val player1 = a(APlayer())
  private val player2 = a(APlayer())
  game
    .setPlayer(player1)
    .setPlayer(player2)
    .setRace(a(Race().fromInput))
    .startFromInput()

  var turn = 0
  // game loop
  while (true) {
    // To debug: Console.err.println("Debug messages...")
    player1.playRandom()

    turn += 1
  }
}
