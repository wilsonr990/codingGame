package botProgramming.SpringChallenge2023

import Utils._

import scala.annotation.tailrec
import scala.io.StdIn._

object Utils {
  var timeId = 0;
  val debug  = false;

  def timed[T](n: String, f: () => T): T = {
    if (debug) {
      val start = System.nanoTime()
      val ret   = f()
      val end   = System.nanoTime()
      val us    = (end - start) / 1000
      if (us != 0) Console.err.println(s"${timeId} Time $n: $us us")
      ret
    } else
      f()
  }

  def turnString(inputStr: List[List[String]], action: String): String = {
    def listToString(list: List[Any]): String = if (list.isEmpty) "List()" else list.mkString("List(\"", "\", \"", "\")")

    def listOfListsToString(list: List[List[Any]]): String = if (list.isEmpty) "List()" else list.map(listToString).mkString("List(", ", ", ")")

    "testGameSituation(" + listOfListsToString(inputStr) + ", \"" + action + "\")"
  }
}

class Brain(val scientist: Scientist) {
  def nextState(world: World): String = {
    var minimumNeededAnts = 0

    val eggCellPrior = world
      .cellsWithEggs()
      .filter(c => {
        minimumNeededAnts += (c._2.baseDistances(c._2.closestBase) + 1) * 2
        minimumNeededAnts <= scientist.totalAnts
      })
      .map(c => "LINE " + c._2.closestBase + " " + c._1 + " " + 1)

    val value = world
      .cellsWithCrystals()
      .filter(c => {
        minimumNeededAnts += (c._2.baseDistances(c._2.closestBase) + 1) * 2
        minimumNeededAnts <= scientist.totalAnts
      })
    val crystalCellPrior =
      if (value.isEmpty) List()
      else
        value.tail
          .map(c => "LINE " + c._2.closestBase + " " + c._1 + " " + 1)

    val eggCell =
      if (eggCellPrior.isEmpty)
        world
          .cellsWithEggs()
          .filter(c => {
            minimumNeededAnts += c._2.baseDistances(c._2.closestBase) + 1
            minimumNeededAnts <= scientist.totalAnts
          })
          .map(c => "LINE " + c._2.closestBase + " " + c._1 + " " + 1)
      else List()

    val crystalCell =
      if (crystalCellPrior.isEmpty && eggCellPrior.isEmpty && eggCell.isEmpty)
        world
          .cellsWithCrystals()
          .filter(c => {
            minimumNeededAnts += c._2.baseDistances(c._2.closestBase) + 1
            minimumNeededAnts <= scientist.totalAnts
          })
          .map(c => "LINE " + c._2.closestBase + " " + c._1 + " " + 1)
      else List()

    val message =
      if (world.player.score > world.opponent.score) "BYE BYE!"
      else "WAIT!"

    (eggCellPrior ::: crystalCellPrior ::: eggCell ::: crystalCell ::: List("MESSAGE " + message + ""))
      .mkString(";")
  }
}

case class Cell(
    index: Int,
    _type: Int,
    initialResources: Int,
    neigh: List[Int],
) {
  var resources: Int               = 0
  var myAnts: Int                  = 0
  var oppAnts: Int                 = 0
  var closestBase: Int             = -1
  var baseDistances: Map[Int, Int] = Map()
}

case class Scientist() {
  var bases: List[Int] = List()
  var score: Int       = 0;
  var totalAnts: Int   = 0;
  val brain: Brain     = new Brain(this)

  def nextAction(world: World): String = {
    brain.nextState(world)
  }
}

class World() {
  var player: Scientist     = Scientist()
  var opponent: Scientist   = Scientist()
  var cells: Map[Int, Cell] = Map()
  var turns: Integer        = 0

  def cellsWithCrystals(): List[(Int, Cell)] =
    cells
      .filter(c => c._2._type == 2 && c._2.resources > 0)
      .toList
      .sortBy(c => c._2.baseDistances(c._2.closestBase))

  def cellsWithEggs(): List[(Int, Cell)] =
    cells
      .filter(c => c._2._type == 1 && c._2.resources > 0)
      .toList
      .sortBy(c => c._2.baseDistances(c._2.closestBase))

  def initializeCells(cellLines: List[String]): Unit = {
    cells = cellLines.zipWithIndex
      .map({
        case (c, i) =>
          val Array(_type, initialResources, _neigh0, _neigh1, _neigh2, _neigh3, _neigh4, _neigh5) = c.split(" ").filter(!_.equals(""))
          (i,
           Cell(
             i,
             _type.toInt,
             initialResources.toInt,
             List(_neigh0.toInt, _neigh1.toInt, _neigh2.toInt, _neigh3.toInt, _neigh4.toInt, _neigh5.toInt)
           ))
      })
      .toMap
  }

  def initializeBases(_friendlyBases: List[String], _opponentBases: List[String]): Unit = {
    player.bases = _friendlyBases.map(_.toInt)
    opponent.bases = _opponentBases.map(_.toInt)

    player.bases
      .foreach(b => {
        assignDistance(b, List(b), 0)
      })
  }

  @tailrec
  private def assignDistance(base: Int, cellList: List[Int], distance: Int): Unit = {
    val n = cellList
      .filter(!cells(_).baseDistances.contains(base))
      .flatMap(cell => {
        cells(cell).baseDistances = cells(cell).baseDistances + (base -> distance)
        if (cells(cell).closestBase == -1 || distance < cells(cell).baseDistances(cells(cell).closestBase))
          cells(cell).closestBase = base
        cells(cell).neigh
          .filter(n => n != -1 && !cells(n).baseDistances.contains(base))
      })
    if (n.nonEmpty)
      assignDistance(base, n, distance + 1)
  }

  def updateScores(scores: List[String]): Unit = {
    val List(myscore, oppScore) = scores
    player.score = myscore.toInt
    opponent.score = oppScore.toInt
  }

  def updateCells(updatedCellLines: List[String]): Unit = {
    var myAnts  = 0
    var oppAnts = 0
    updatedCellLines.zipWithIndex.foreach({
      case (c, i) =>
        val Array(_resources, _myAnts, _oppAnts) = c.split(" ").filter(!_.equals(""))
        cells
          .find(_._1 == i)
          .foreach(c => {
            c._2.resources = _resources.toInt
            c._2.myAnts = _myAnts.toInt
            c._2.oppAnts = _oppAnts.toInt
          })
        myAnts += _myAnts.toInt
        oppAnts += _oppAnts.toInt
    })
    player.totalAnts = myAnts
    opponent.totalAnts = oppAnts
  }
}

object Player extends App {
  val numberOfCells = readLine.toInt
  val cellLines     = (0 until numberOfCells).map(_ => readLine).toList
  val numberOfBases = readLine.toInt
  val friendlyBases = readLine.split(" ").filter(!_.equals("")).toList
  val opponentBases = readLine.split(" ").filter(!_.equals("")).toList

  val world: World = new World()
  world.initializeCells(cellLines)
  world.initializeBases(friendlyBases, opponentBases)

  while (true) {
    val scoreLine        = readLine.split(" ").filter(!_.equals("")).toList
    val updatedCellLines = (0 until numberOfCells).map(_ => readLine).toList
    val inputStr         = List(cellLines, friendlyBases, opponentBases, updatedCellLines)

    world.updateScores(scoreLine)
    world.updateCells(updatedCellLines)

    val action = world.player.nextAction(world)

    Console.err.println(turnString(inputStr, action))

    println(action)
  }
}
