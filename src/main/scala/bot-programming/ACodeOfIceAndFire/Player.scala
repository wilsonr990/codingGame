package botProgramming.ACodeOfIceAndFire

import scala.collection.immutable.Seq
import scala.io.StdIn
import scala.util.Random

// Exceptions
sealed class CustomException(message: String = "")                      extends Exception(message)
final case class InvalidCellException(message: String = "")             extends CustomException(message)
final case class InvalidTerritory(message: String = "")                 extends CustomException(message)
final case class InvalidBuildingException(message: String = "")         extends CustomException(message)
final case class MoveToOwnArmyException(message: String = "")           extends CustomException(message)
final case class MoveToMorePowerfulArmyException(message: String = "")  extends CustomException(message)
final case class MoveToFurtherException(message: String = "")           extends CustomException(message)
final case class MoveToOwnBuildingException(message: String = "")       extends CustomException(message)
final case class MoveToVoidAreaException(message: String = "")          extends CustomException(message)
final case class MoveToOutOfTheMap(message: String = "")                extends CustomException(message)
final case class TrainInFurtherTerritoryException(message: String = "") extends CustomException(message)
final case class TrainInBuildingException(message: String = "")         extends CustomException(message)
final case class TrainInUnitException(message: String = "")             extends CustomException(message)
final case class TrainInVoidAreaException(message: String = "")         extends CustomException(message)
final case class TrainInOutOfTheMap(message: String = "")               extends CustomException(message)

// utils
case class Dimension(x: Int, y: Int)
case class Coordinate(x: Int, y: Int) {
  def in(dimension: Dimension): Boolean =
    x >= 0 && x < dimension.x && y >= 0 && y < dimension.y

  def distance(other: Coordinate): Double = {
    val dx = x - other.x
    val dy = y - other.y
    Math.sqrt(dx * dx + dy * dy)
  }
  def isNextTo(other: Coordinate): Boolean = distance(other).equals(1.0)

  def anyValidDirection: Coordinate = {
    val options = Set(left, right, up, down)
    options.toList(Random.nextInt(options.size))
  }
  def left: Coordinate  = this.copy(x = if (x > 0) x - 1 else x)
  def right: Coordinate = this.copy(x = if (x < 11) x + 1 else x)
  def up: Coordinate    = this.copy(y = if (y > 0) y - 1 else y)
  def down: Coordinate  = this.copy(y = if (y < 11) y + 1 else y)

  def andSurrounds: List[Coordinate] = this :: surrounds
  def surrounds: List[Coordinate] =
    List(
      this.copy(x = x + 1),
      this.copy(x = x - 1),
      this.copy(y = y + 1),
      this.copy(y = y - 1)
    )
}

// Game
sealed class Cell(val coordinate: Coordinate)
case class VoidCell(override val coordinate: Coordinate)                     extends Cell(coordinate)
case class NeutralCell(override val coordinate: Coordinate)                  extends Cell(coordinate)
case class ActiveCell(owner: Faction, override val coordinate: Coordinate)   extends Cell(coordinate)
case class InactiveCell(owner: Faction, override val coordinate: Coordinate) extends Cell(coordinate)
object Cell {
  def apply(ident: Char)(implicit coordinate: Coordinate): Cell =
    ident match {
      case '.' => NeutralCell(coordinate)
      case '#' => VoidCell(coordinate)
      case 'O' => ActiveCell(Faction.o, coordinate)
      case 'o' => InactiveCell(Faction.o, coordinate)
      case 'X' => ActiveCell(Faction.x, coordinate)
      case 'x' => InactiveCell(Faction.x, coordinate)
      case _   => throw InvalidCellException(s"$ident not found")
    }
}
case class Territory(cells: Set[ActiveCell])(
    implicit validate: Boolean = false
) {
  if (validate && !cellsAreConsecutive()) throw InvalidTerritory()

  val size: Int = cells.size

  def contains(location: Coordinate): Boolean =
    cells.exists(_.coordinate == location)

  private def cellsAreConsecutive(): Boolean = {
    cells
      .foldRight(List.empty[List[Coordinate]]) { (cell, groups) =>
        val groupsNotCloseToTheNewCell =
          groups.filter(!_.exists(_.isNextTo(cell.coordinate)))
        val mergedGroupWithTheNewCell = cell.coordinate :: groups
          .filter(_.exists(_.isNextTo(cell.coordinate)))
          .flatten

        mergedGroupWithTheNewCell :: groupsNotCloseToTheNewCell
      }
      .size == 1
  }
}

case class Faction(id: Char)
object Faction {
  def apply(id: Int): Faction  = Faction(if (id == 0) 'o' else 'x')
  def apply(id: Char): Faction = new Faction(id.toLower)
  def o: Faction               = Faction('o')
  def x: Faction               = Faction('x')
}
case class ArmyId(id: Int)

case class MineSpot(location: Coordinate)

sealed class Building(val owner: Faction, val location: Coordinate)
case class Headquarter(override val owner: Faction, override val location: Coordinate) extends Building(owner, location)
object Building {
  def from(owner: Faction, coordinate: Coordinate, ident: Int): Building = {
    ident match {
      case 0 => Headquarter(owner, coordinate)
      case _ => throw InvalidBuildingException(s"$ident not found")
    }
  }
}

case class Unit(owner: Faction, id: ArmyId, level: Int, location: Coordinate) {
  val cost: Int = level match {
    case 1 => 10
    case 2 => 20
    case 3 => 30
    case _ => 100
  }
  val unkeep: Int = level match {
    case 1 => 1
    case 2 => 4
    case 3 => 20
    case _ => 10
  }
}

case class GameMap(
    cells: Set[Cell],
    buildings: Set[Building],
    units: Set[Unit],
    dimension: Dimension = Dimension(12, 12)
)(implicit val validate: Boolean = false) {
  if (validate && cells.exists(!_.coordinate.in(dimension)))
    throw InvalidCellException()

  private val territoriesPerFaction: Map[Faction, Territory] =
    cells
      .collect { case c @ ActiveCell(_, _) => c }
      .groupBy(_.owner)
      .map { case (owner, c) => (owner, Territory(c)) }

  def territoryOf(faction: Faction): Territory =
    territoriesPerFaction.getOrElse(faction, Territory(Set.empty))

  private val unitsPerFaction: Map[Faction, Set[Unit]] = units.groupBy(_.owner)

  def unitsOf(faction: Faction): Set[Unit] =
    unitsPerFaction.getOrElse(faction, Set.empty)

  private val headquartersPerFaction: Map[Faction, Headquarter] =
    buildings
      .collect { case h @ Headquarter(_, _) => h }
      .groupBy(_.owner)
      .map { case (owner, h) => (owner, h.head) }

  def headquarterOf(faction: Faction): Headquarter =
    headquartersPerFaction(faction)

  val factions: Set[Faction] = headquartersPerFaction.keySet

  def unitWith(unitId: ArmyId): Unit = {
    units.collect {
      case unit @ Unit(_, `unitId`, _, _) => unit
    }.head
  }

  def itemIn(location: Coordinate): Any = {
    val b: List[Any] =
      buildings.filter(_.location.equals(location)).asInstanceOf[List[Any]]
    val u: List[Any] =
      units.filter(_.location.equals(location)).asInstanceOf[List[Any]]
    (b ::: u).head
  }
}

case class GamePlayer(id: Faction, gold: Long = 0)

sealed class Action(verb: String, params: List[Int]) {
  def printable: String = s"$verb ${params.mkString(" ")}"
}
case class Move(id: ArmyId, location: Coordinate)  extends Action("MOVE", List(id.id, location.x, location.y))
case class Train(level: Int, location: Coordinate) extends Action("TRAIN", List(level, location.x, location.y))
case class Wait()                                  extends Action("WAIT", List.empty)

case class GameStatus(map: GameMap, players: List[GamePlayer]) {
  implicit val dimension: Dimension = Dimension(12, 12)

  def getRandomMove(faction: Faction): List[Action] = {
    val moves = map.unitsOf(faction).map {
      case Unit(_, unitId, _, coordinate) =>
        val horizontal = Random.nextBoolean()
        if (map.headquarterOf(faction).location.equals(Coordinate(0, 0))) {
          if (horizontal) Move(unitId, coordinate.right)
          else Move(unitId, coordinate.down)
        } else {
          if (horizontal) Move(unitId, coordinate.left)
          else Move(unitId, coordinate.up)
        }
    }
    val trainings =
      if (map.unitsOf(faction).size < 5)
        map.territoryOf(faction).cells.map { cell =>
          Train(Random.nextInt(3) + 1, cell.coordinate.anyValidDirection)
        } else Seq.empty
    moves.toList ::: trainings.toList
  }

  def isVoidCellAt(location: Coordinate): Boolean =
    map.itemIn(location).isInstanceOf[VoidCell]

  def locationContainsUnitWithGreaterLevel(level: Int, location: Coordinate): Boolean =
    map.itemIn(location).isInstanceOf[Unit] && map
      .itemIn(location)
      .asInstanceOf[Unit]
      .level >= level

  def locationContainsUnitOfPlayer(faction: Faction, location: Coordinate): Boolean =
    map.itemIn(location).isInstanceOf[Unit] && map
      .itemIn(location)
      .asInstanceOf[Unit]
      .owner == faction

  def locationContainsUnit(location: Coordinate): Boolean =
    map.itemIn(location).isInstanceOf[Unit]

  def enemyOf(faction: Faction): Faction =
    players.collectFirst { case GamePlayer(id, _) if id != faction => id }.get

  def locationContainsBuildingOfPlayer(faction: Faction, location: Coordinate): Boolean =
    map.itemIn(location).isInstanceOf[Building] && map
      .itemIn(location)
      .asInstanceOf[Building]
      .owner == faction

  def locationContainsBuilding(location: Coordinate): Boolean =
    map.itemIn(location).isInstanceOf[Building]

  def locationIsInOrNextToPlayerTerritory(location: Coordinate, faction: Faction): Boolean =
    location.andSurrounds.exists {
      map.territoryOf(faction).cells.map(_.coordinate).contains
    }

  def oneLevelArmiesCountFor(faction: Faction): Int =
    map.unitsOf(faction).count(_.level == 1)

  def unitsOnActiveCellsOfMap: Set[Unit] =
    map.unitsOf(Faction.o).filter { unit =>
      map.territoryOf(unit.owner).contains(unit.location)
    }

  def buildingsAfterAttackedLocation(location: Coordinate): List[Headquarter] =
    map.buildings.asInstanceOf[List[Headquarter]].filter(_.location != location)

  def unitsAfterAttackedLocation(location: Coordinate, faction: Faction, unitId: ArmyId): List[Unit] =
    map.units.asInstanceOf[List[Unit]].collect {
      case unit @ Unit(_, `unitId`, _, _)           => unit.copy(location = location)
      case unit @ Unit(_, _, _, l) if l != location => unit
    }

  def unitsAfterTrainedLocation(location: Coordinate, faction: Faction, level: Int): List[Unit] = {
    Unit(faction, ArmyId(location.x * 100 + location.y), level, location) :: map.units.toList
  }
}

case class Game(status: GameStatus) {

  def playersWithUpdatedIncome: List[GamePlayer] =
    status.players.map(player => {
      player.copy(gold = player.gold + incomeFor(player.id))
    })

  def incomeFor(faction: Faction): Int =
    status.map.territoryOf(faction).size - status.oneLevelArmiesCountFor(
      faction
    )

  def runTurn(actions: List[Action])(implicit validate: Boolean = false) {
    Some(
      status.copy(
        map = status.map.copy(units = status.unitsOnActiveCellsOfMap),
        players = playersWithUpdatedIncome
      )
    ).map(
      newStatus =>
        actions.foldRight(newStatus)(
          (action: Action, status: GameStatus) =>
            action match {
              case Move(unitId, newLocation) =>
                val Unit(faction, _, level, location) =
                  status.map.unitWith(unitId)
                if (!newLocation.in(Dimension(11, 11)))
                  throw MoveToVoidAreaException()
                if (!location.surrounds.contains(newLocation))
                  throw MoveToFurtherException()
                if (status
                      .locationContainsBuildingOfPlayer(faction, newLocation))
                  throw MoveToOwnBuildingException()
                if (status.locationContainsUnitOfPlayer(faction, newLocation))
                  throw MoveToOwnArmyException()
                if (status
                      .locationContainsUnitWithGreaterLevel(level, newLocation))
                  throw MoveToMorePowerfulArmyException()
                if (status.isVoidCellAt(newLocation))
                  throw MoveToVoidAreaException()
                status
              //          status.copy(
              //            buildings = status.buildingsAfterAttackedLocation(newLocation, faction),
              //            units = status.unitsAfterAttackedLocation(newLocation, faction, unitId)
              //          )
              case Train(level, newLocation) =>
                val faction = Faction.o
                if (!newLocation.in(Dimension(11, 11)))
                  throw TrainInOutOfTheMap()
                if (!status.locationIsInOrNextToPlayerTerritory(
                      newLocation,
                      faction
                    )) throw TrainInFurtherTerritoryException()
                if (status.locationContainsBuilding(newLocation))
                  throw TrainInBuildingException()
                if (status.locationContainsUnit(newLocation))
                  throw TrainInUnitException()
                if (status.isVoidCellAt(newLocation))
                  throw TrainInVoidAreaException()
                status
              //          status.copy(
              //            units = status.unitsAfterTrainedLocation(newLocation, faction, level)
              //          )
              case _ =>
                status
          }
      )
    )
  }

  def getValidMove(id: Faction): List[Action] = {
    var solved             = false
    var move: List[Action] = status.getRandomMove(id)
    //    do {
    //      try {
    //        runTurn(move)
    //        solved = true
    //      } catch {
    //        case e: CustomException =>
    //          move = status.getRandomMove(id)
    //          solved = false
    //      }
    //    } while (!solved)
    move
  }
}

/** The Goal
  * Build armies to defeat your opponent by destroying their headquarters.
  * */
object Player extends App {
  val numberMineSpots = StdIn.readInt()
  val mineSpots: Seq[MineSpot] = (0 until numberMineSpots)
    .map(_ => {
      val Array(x, y) = StdIn.readLine().split(" ")
      MineSpot(Coordinate(x.toInt, y.toInt))
    })

  // game loop
  while (true) {
    val gold           = StdIn.readInt()
    val income         = StdIn.readInt()
    val opponentGold   = StdIn.readInt()
    val opponentIncome = StdIn.readInt()
    val map: List[Cell] = (0 until 12)
      .flatMap((y: Int) => {
        val values = StdIn.readLine()
        (0 until values.length).map(x => {
          Cell(values(x))(Coordinate(x, y))
        })
      })
      .toList
    val buildingCount = StdIn.readInt()
    val buildings: Set[Building] = (0 until buildingCount)
      .map(_ => {
        val Array(owner, buildingType, x, y) = StdIn.readLine().split(" ")
        Building.from(
          Faction(owner.toInt),
          Coordinate(x.toInt, y.toInt),
          buildingType.toInt
        )
      })
      .toSet

    val unitCount = StdIn.readInt()
    val units: Set[Unit] = (0 until unitCount)
      .map(_ => {
        val Array(owner, unitId, level, x, y) = StdIn.readLine().split(" ")
        Unit(
          Faction(owner.toInt),
          ArmyId(unitId.toInt),
          level.toInt,
          Coordinate(x.toInt, y.toInt)
        )
      })
      .toSet

    val player1 = GamePlayer(Faction.o, gold)
    val player2 = GamePlayer(Faction.x, opponentGold)

    val game = Game(
      GameStatus(GameMap(map.toSet, buildings, units), List(player1, player2))
    )

    val folded = game.getValidMove(Faction.o).foldRight("") { (action, string) =>
      string.concat(action.printable).concat(";")
    }
    println(folded)
    // Write an action using println
    // To debug: Console.err.println("Debug messages...")

    //      println ("WAIT")
  }
}
