package botProgramming.Code4Life

import Utils._

import scala.annotation.tailrec
import scala.io.StdIn._

object Utils {
  var timeId = 0;
  val debug = false;

  def createMoleculesMap(strings: String*): Map[Char, Int] = {
    strings.map(_.toInt)
      .zipWithIndex.map({ case (v, i) => (('A' + i).toChar, v) }).toMap
  }

  def timed[T](n: String, f: () => T): T = {
    if (debug) {
      val start = System.nanoTime()
      val ret = f()
      val end = System.nanoTime()
      val us = (end - start) / 1000
      if (us != 0) Console.err.println(s"${timeId} Time $n: $us us")
      ret
    }
    else
      f()
  }

  def turnString(inputStr: List[List[String]], action: String): String = {
    def listToString(list: List[Any]): String = if (list.isEmpty) "List()" else list.mkString("List(\"", "\", \"", "\")")

    def listOfListsToString(list: List[List[Any]]): String = if (list.isEmpty) "List()" else list.map(listToString).mkString("List(", ", ", ")")

    "testGameSituation(" + listOfListsToString(inputStr) + ", \"" + action + "\")"
  }
}

object Locations extends Enumeration {
  type Location = Value

  val START_POS, SAMPLES, DIAGNOSIS, MOLECULES, LABORATORY = Value
}

abstract sealed class State(val location: Locations.Value) {
  def calculateAction(robot: Robot): String
}

case class Collect() extends State(Locations.SAMPLES) {
  def calculateAction(robot: Robot): String = {
    robot match {
      case _ if robot.actionBeforeGameEnd.isDefined => robot.actionBeforeGameEnd.get
      case _ if robot.toDiagnoseSamples.exists(_.health >= 30) || robot.toDiagnoseSamples.length >= 2 => "GOTO DIAGNOSIS"
      case _ if robot.toChooseMol.isDefined && robot.target != Locations.SAMPLES => "GOTO MOLECULES"
      case _ if robot.toChooseRank.isDefined => "CONNECT " + robot.toChooseRank.get
      case _ => "GOTO DIAGNOSIS"
    }
  }
}

case class Analyze() extends State(Locations.DIAGNOSIS) {
  override def calculateAction(robot: Robot): String = {
    robot match {
      case _ if robot.actionBeforeGameEnd.isDefined => robot.actionBeforeGameEnd.get
      case _ if robot.samples.length < 3 && robot.toDiagnoseSamples.nonEmpty =>
        "CONNECT " + robot.toDiagnoseSamples.head.id
      case _ if !robot.opponentIsInMolecules && robot.world.opponent.chooseMolecule().isDefined && robot.toChooseMol.isDefined =>
        "GOTO MOLECULES"
      case _ if robot.toAnalyzeSample.isDefined => "CONNECT " + robot.toAnalyzeSample.head.id
      case _ if robot.toTrashSample.isDefined && !robot.canCompleteSample && robot.opponentIsInLaboratory && robot.target == Locations.DIAGNOSIS => "WAIT"
      case _ if robot.toTrashSample.isDefined => "CONNECT " + robot.toTrashSample.head.id
      case _ if robot.samples.length == 1 => "GOTO SAMPLES"
      case _ => "GOTO MOLECULES"
    }
  }
}

case class Gather() extends State(Locations.MOLECULES) {
  override def calculateAction(robot: Robot): String = {
    robot match {
      case _ if robot.actionBeforeGameEnd.isDefined => robot.actionBeforeGameEnd.get
      case _ if robot.toChooseMol.isDefined => "CONNECT " + robot.toChooseMol.get
      case _ if robot.samples.length > 0 && !robot.canCompleteSample && robot.opponentIsInLaboratory => "WAIT"
      case _ => "GOTO LABORATORY"
    }
  }
}

case class Produce() extends State(Locations.LABORATORY) {
  override def calculateAction(robot: Robot): String = {
    robot match {
      case _ if robot.actionBeforeGameEnd.isDefined => robot.actionBeforeGameEnd.get
      case _ if robot.world.opponent.samples.exists(s => s.isAnalysed && s.health >= 20) && !robot.opponentCanCompleteSample() && robot.score > robot.world.opponent.score =>
        if (robot.samples.length > 1 && robot.canCompleteSample && robot.toChooseMol.nonEmpty) "GOTO MOLECULES"
        else if (robot.toAnalyzeSample.isDefined) "GOTO DIAGNOSIS"
        else if (robot.samples.length == 3) "WAIT"
        "GOTO SAMPLES"
      case _ if robot.toProduceSample.isDefined => "CONNECT " + robot.toProduceSample.head.id
      case _ if robot.samples.length > 1 && robot.canCompleteSample && robot.toChooseMol.nonEmpty => "GOTO MOLECULES"
      case _ => "GOTO SAMPLES"
    }
  }
}

class RobotBrain(val robot: Robot) {
  var state: State = Collect()

  def nextState(): String = {
    if (robot.target != state.location)
      "GOTO " + state.location
    else if (robot.eta != 0)
      if (robot.score > robot.world.opponent.score) if (robot.eta == 2) "CATCH ME!" else "IF YOU CAN!"
      else {
        if (robot.eta == 2) "COMING" else "FOR YOU!"
      }
    else {
      val action = state.calculateAction(robot)
      val newState = calculateNewState(List(state), action)
      if (newState != state) {
        state = newState
        "GOTO " + state.location
      }
      else if (action.contains("GOTO ") && newState == state)
        "WAIT"
      else action
    }
  }

  @tailrec
  private def calculateNewState(visitedStates: List[State], action: String): State = {
    val goto = """GOTO (.*)""".r
    action match {
      case goto(location) if visitedStates.exists(s => s.location.toString.equals(location)) =>
        Locations.withName(location) match {
          case Locations.SAMPLES => Collect()
          case Locations.DIAGNOSIS => Analyze()
          case Locations.MOLECULES => Gather()
          case Locations.LABORATORY => Produce()
        }
      case goto(location) =>
        Locations.withName(location) match {
          case Locations.SAMPLES => calculateNewState(Collect() :: visitedStates, Collect().calculateAction(robot))
          case Locations.DIAGNOSIS => calculateNewState(Analyze() :: visitedStates, Analyze().calculateAction(robot))
          case Locations.MOLECULES => calculateNewState(Gather() :: visitedStates, Gather().calculateAction(robot))
          case Locations.LABORATORY => calculateNewState(Produce() :: visitedStates, Produce().calculateAction(robot))
        }
      case _ => visitedStates.head
    }
  }
}


class Robot(val world: World) {
  var target: Locations.Value = _
  var eta: Int = 0
  var score: Int = 0
  var storage: Map[Char, Int] = createMoleculesMap("0", "0", "0", "0", "0")
  var expertise: Map[Char, Int] = createMoleculesMap("0", "0", "0", "0", "0")
  var samples: List[Sample] = List()
  var toDiagnoseSamples: List[Sample] = _
  var toChooseRank: Option[Int] = _
  var toAnalyzeSample: Option[Sample] = _
  var toTrashSample: Option[Sample] = _
  var toChooseMol: Option[Char] = _
  var toProduceSample: Option[Sample] = _
  var storageSum: Int = _
  var expertiseSum: Int = _
  var actionBeforeGameEnd: Option[String] = Option.empty

  val robotBrain: RobotBrain = new RobotBrain(this)

  def nextAction(): String = {
    samples = completeAndSortSamples(samples)
    world.samples = completeAndSortSamples(world.samples)
    world.opponent.samples = completeAndSortSamples(world.opponent.samples)

    toDiagnoseSamples = diagnosedSamples()
    toChooseRank = chooseSample()
    toAnalyzeSample = sampleToAnalyze()
    toTrashSample = sampleToTrash()
    toChooseMol = chooseMolecule()
    toProduceSample = sampleToProduce()
    actionBeforeGameEnd = getActionBeforeGameEnd

    robotBrain.nextState()
  }

  def chooseSample(): Option[Int] = {
    if (samples.length + toDiagnoseSamples.length >= 3) Option.empty
    else if (expertiseSum <= 4) Option(1)
    else if (expertiseSum <= 10) Option(2)
    else Option(3)
  }

  def sampleToAnalyze(): Option[Sample] = samples.find(s => !s.isAnalysed)

  def sampleToProduce(): Option[Sample] = samples.find(isCompleteSample)

  def sampleToTrash(): Option[Sample] = {
    samples.find(s => !s.canBeCompleted && (!s.opCanBeCompleted || toDiagnoseSamples.exists(_.health > s.health)))
      .orElse(
        if (samples.count(!_.canBeCompleted) >= 2)
          samples.filter(s => !s.canBeCompleted).minByOption(_.health)
        else Option.empty
      )
  }


  def opponentIsInLaboratory(): Boolean = world.opponent.target == Locations.LABORATORY

  def opponentIsInMolecules(): Boolean = world.opponent.target == Locations.MOLECULES && world.opponent.eta == 0

  def opponentCanCompleteSample(): Boolean = world.opponent.samples.exists(_.opCanBeCompleted)

  def canCompleteSample: Boolean = samples.exists(if (world.opponent.equals(this)) _.opCanBeCompleted else _.canBeCompleted)

  def canCompleteSampleWithOpponents: Boolean = samples.filter(s => !s.canBeCompleted && !isCompleteSample(s))
    .foldLeft(Map[Char, Int]())((collected, sample) => if (collected.isEmpty) sample.missingMols else sample.missingMols.map { case (mol, cost) => (mol, if (collected(mol) > 0) collected(mol) + cost else 0) })
    .exists(s => s._2 > 0 && world.availableMolecules(s._1) < s._2 && world.opponent.storage(s._1) > 0)

  def diagnosedSamples(): List[Sample] = world.samples.filter(s => s.canBeCompleted && s.neededForProject)

  def hasCompleteSample: Boolean = samples.exists(isCompleteSample)

  def isStrictlyCompleteSample(sample: Sample): Boolean = {
    val missingSum = if (world.opponent.equals(this)) sample.opStrictlyMissingSum
    else sample.strictlyMissingSum
    sample.isAnalysed && missingSum == 0
  }

  def isCompleteSample(sample: Sample): Boolean = {
    val missingSum = if (world.opponent.equals(this)) sample.opMissingSum
    else sample.missingSum
    sample.isAnalysed && missingSum == 0
  }

  def getActionBeforeGameEnd: Option[String] = {
    Option(target match {
      case Locations.SAMPLES if hasCompleteSample => (3 + samples.count(isCompleteSample), "GOTO LABORATORY")
      case Locations.SAMPLES if canCompleteSample => (3 + 3 + samples.filter(_.canBeCompleted).head.missingSum + 1, "GOTO MOLECULES")
      case Locations.SAMPLES if samples.count(s => !s.isAnalysed) > 0 => (3 + 3 + 3 + samples.filter(s => !s.isAnalysed).minByOption(_.rank).map(s => s.rank match {
        case 1 => Math.min(5 - expertiseSum, 0)
        case 2 => Math.min(8 - expertiseSum, 0)
        case 3 => Math.min(10 - expertiseSum, 0)
      }).get + 2, "GOTO DIAGNOSIS")
      case Locations.SAMPLES if toChooseRank.isDefined => (1 + 3 + 3 + 3 + (toChooseRank.get match {
        case 1 => Math.min(5 - expertiseSum, 0)
        case 2 => Math.min(8 - expertiseSum, 0)
        case 3 => Math.min(10 - expertiseSum, 0)
      }) + 2, "CONNECT " + toChooseRank.get)
      case Locations.DIAGNOSIS if hasCompleteSample => (4 + samples.count(isCompleteSample), "GOTO LABORATORY")
      case Locations.DIAGNOSIS if canCompleteSample => (3 + 3 + samples.filter(_.canBeCompleted).head.missingSum + 1, "GOTO MOLECULES")
      case Locations.DIAGNOSIS if samples.count(s => !s.isAnalysed) > 0 => (3 + 3 + samples.filter(s => !s.isAnalysed).minByOption(_.rank).map(s => s.rank match {
        case 1 => Math.min(5 - expertiseSum, 0)
        case 2 => Math.min(8 - expertiseSum, 0)
        case 3 => Math.min(10 - expertiseSum, 0)
      }).get + 2, "CONNECT " + samples.filter(s => !s.isAnalysed).minByOption(_.rank).get.id)
      case Locations.MOLECULES if hasCompleteSample => (3 + samples.count(isCompleteSample), "GOTO LABORATORY")
      case Locations.MOLECULES if canCompleteSample => (3 + samples.filter(_.canBeCompleted).head.missingSum + 1,
        "CONNECT " + samples.find(s => s.canBeCompleted).head.missingMols.maxByOption(_._2).map(_._1).get)
      case Locations.LABORATORY if hasCompleteSample => (samples.count(isCompleteSample), "CONNECT " + samples.filter(isCompleteSample).head.id)
      case _ => null
    }).flatMap(pair => if (200 - world.turns < pair._1) Option(pair._2) else Option.empty)
  }

  def canCompleteSample(sample: Sample): Boolean = {
    if (!sample.isAnalysed) false
    else {
      val missingMols = if (world.opponent.equals(this)) sample.opStrictlyMissingMols else sample.missingMols
      val missingSum = if (world.opponent.equals(this)) sample.opStrictlyMissingSum else sample.missingSum

      sample.isAnalysed && (missingMols
        .forall({ case (typ: Char, count: Int) =>
          count <= world.availableMolecules(typ)
        }) && missingSum + storageSum <= 10)
    }
  }

  def canStrictlyCompleteSample(sample: Sample): Boolean = sample.isAnalysed && ((if (world.opponent.equals(this)) sample.opStrictlyMissingMols else sample.strictlyMissingMols)
    .forall({ case (typ: Char, count: Int) =>
      count <= world.availableMolecules(typ)
    }) && (if (world.opponent.equals(this)) sample.opStrictlyMissingSum else sample.strictlyMissingSum) + storageSum <= 10)

  def neededForProject(sample: Sample): Boolean = sample.isAnalysed && world.projects.exists(_ (sample.gain) - expertise(sample.gain) > 0)

  def strictlyMissingStorage(sample: Sample): Map[Char, Int] = {
    if (!sample.isAnalysed) createMoleculesMap("0", "0", "0", "0", "0")
    else sample.cost.map { case (mol, cost) =>
      val missing = cost - expertise(mol) - storage(mol)
      (mol, if (missing >= 0) missing else 0)
    }
  }

  def gainedExpertise(sample: Sample): List[Char] = {
    if (!sample.isAnalysed) List()
    else {
      val samples = if (this.samples.contains(sample)) this.samples
      else if (this.world.samples.contains(sample)) this.samples ::: this.world.samples
      else this.samples ::: this.world.samples ::: (if (world.opponent.equals(this)) this.world.robot.samples else this.world.opponent.samples)

      samples.takeWhile(_.id != sample.id).filter(if (world.opponent.equals(this)) _.opCanBeCompleted else _.canBeCompleted).map(_.gain)
    }
  }

  def requiredStorage(sample: Sample): Map[Char, Int] = {
    if (!sample.isAnalysed) createMoleculesMap("0", "0", "0", "0", "0")
    else {
      sample.cost.map { case (mol, cost) =>
        val gainedExpertise = if (world.opponent.equals(this)) sample.opGainedExpertise else sample.gainedExpertise
        val missing = cost - expertise(mol) - gainedExpertise.count(_ == mol)
        (mol, if (missing >= 0) missing else 0)
      }
    }
  }

  def usedStorage(sample: Sample): Map[Char, Int] = {
    if (!sample.isAnalysed) createMoleculesMap("0", "0", "0", "0", "0")
    else {
      val samples = if (this.samples.contains(sample)) this.samples
      else if (this.world.samples.contains(sample)) this.samples ::: this.world.samples
      else this.samples ::: this.world.samples ::: (if (world.opponent.equals(this)) this.world.robot.samples else this.world.opponent.samples)

      samples.takeWhile(_.id != sample.id)
        .foldLeft(createMoleculesMap("0", "0", "0", "0", "0"))((usedStorage, sample) => {
          val requiredStorage = if (world.opponent.equals(this)) sample.opRequiredStorage else sample.requiredStorage
          if (if (world.opponent.equals(this)) sample.opCanBeCompleted else sample.canBeCompleted)
            usedStorage.map { case (mol, req) => (mol, req + requiredStorage.getOrElse(mol, 0)) } else usedStorage
        })
    }
  }

  def missingStorage(sample: Sample): Map[Char, Int] = {
    if (sample.isAnalysed) {
      sample.cost.map { case (mol, cost) =>
        val availableStorage = storage(mol) - (if (world.opponent.equals(this)) sample.opUsedStorage(mol) else sample.usedStorage(mol))
        val gainedExperience = if (world.opponent.equals(this)) sample.opGainedExpertise else sample.gainedExpertise
        val missing = if (availableStorage >= 0) cost - expertise(mol) - availableStorage - gainedExperience.count(_ == mol)
        else -availableStorage + Math.max(cost - expertise(mol) - gainedExperience.count(_ == mol), 0)
        (mol, Math.max(missing, 0))
      }
    } else {
      createMoleculesMap("0", "0", "0", "0", "0")
    }
  }

  private def sortSamples(s1: Sample, s2: Sample) = {
    //    val missingDiagnosedStorage = world.samples.headOption.map(_.missingMols).getOrElse(createMoleculesMap("0", "0", "0", "0", "0"))

    val onlyFirstIsAnalyzed = s1.isAnalysed && !s2.isAnalysed
    val onlySecondIsAnalyzed = s2.isAnalysed && !s1.isAnalysed
    val onlyFirsIsCompleted = isCompleteSample(s1) && !isCompleteSample(s2)
    val onlySecondIsCompleted = isCompleteSample(s2) && !isCompleteSample(s1)
    val onlyFirstCanBeCompleted = s1.canBeCompleted && !s2.canBeCompleted
    val onlySecondCanBeCompleted = s2.canBeCompleted && !s1.canBeCompleted
    val onlyFirstCanStrictlyBeCompleted = s1.canStrictlyBeCompleted && !s2.canStrictlyBeCompleted
    val onlySecondCanStrictlyBeCompleted = s2.canStrictlyBeCompleted && !s1.canStrictlyBeCompleted
    val onlyFirsIsStrictlyCompleted = isStrictlyCompleteSample(s1) && !isStrictlyCompleteSample(s2)
    val onlySecondIsStrictlyCompleted = isStrictlyCompleteSample(s2) && !isStrictlyCompleteSample(s1)
    val onlyFirstHelpsCompletingSecond = s1.isAnalysed && s2.isAnalysed && s2.strictlyMissingMols(s1.gain) > 0 && s1.strictlyMissingMols(s2.gain) == 0
    val onlySecondHelpsCompletingFirst = s1.isAnalysed && s2.isAnalysed && s1.strictlyMissingMols(s2.gain) > 0 && s2.strictlyMissingMols(s1.gain) == 0
    val firstHasLessMissingMols = s1.strictlyMissingSum < s2.strictlyMissingSum
    val secondHasLessMissingMols = s2.strictlyMissingSum < s1.strictlyMissingSum
    val firstHasMoreHealth = s1.health > s2.health
    val secondHasMoreHealth = s2.health > s1.health
    val firstIsMoreRank = s1.rank > s2.rank
    val secondIsMoreRank = s2.rank > s1.rank
    val firstIsMoreRecent = s1.id < s2.id
    val secondIsMoreRecent = s2.id < s1.id

    onlyFirstIsAnalyzed ||
      (!onlySecondIsAnalyzed && onlyFirsIsCompleted) ||
      (!onlySecondIsAnalyzed && !onlySecondIsCompleted && onlyFirstCanBeCompleted) ||
      (!onlySecondIsAnalyzed && !onlySecondIsCompleted && !onlySecondCanBeCompleted && onlyFirsIsStrictlyCompleted) ||
      (!onlySecondIsAnalyzed && !onlySecondIsCompleted && !onlySecondCanBeCompleted && !onlySecondIsStrictlyCompleted && onlyFirstCanStrictlyBeCompleted) ||
      (!onlySecondIsAnalyzed && !onlySecondIsCompleted && !onlySecondCanBeCompleted && !onlySecondIsStrictlyCompleted && !onlySecondCanStrictlyBeCompleted && onlyFirstHelpsCompletingSecond) ||
      (!onlySecondIsAnalyzed && !onlySecondIsCompleted && !onlySecondCanBeCompleted && !onlySecondIsStrictlyCompleted && !onlySecondCanStrictlyBeCompleted && !onlySecondHelpsCompletingFirst && firstHasMoreHealth) ||
      (!onlySecondIsAnalyzed && !onlySecondIsCompleted && !onlySecondCanBeCompleted && !onlySecondIsStrictlyCompleted && !onlySecondCanStrictlyBeCompleted && !onlySecondHelpsCompletingFirst && !secondHasMoreHealth && firstHasLessMissingMols) ||
      (!onlySecondIsAnalyzed && !onlySecondIsCompleted && !onlySecondCanBeCompleted && !onlySecondIsStrictlyCompleted && !onlySecondCanStrictlyBeCompleted && !onlySecondHelpsCompletingFirst && !secondHasMoreHealth && !secondHasLessMissingMols && firstIsMoreRank) ||
      (!onlySecondIsAnalyzed && !onlySecondIsCompleted && !onlySecondCanBeCompleted && !onlySecondIsStrictlyCompleted && !onlySecondCanStrictlyBeCompleted && !onlySecondHelpsCompletingFirst && !secondHasMoreHealth && !secondHasLessMissingMols && !secondIsMoreRank && firstIsMoreRecent) ||
      (!onlySecondIsAnalyzed && !onlySecondIsCompleted && !onlySecondCanBeCompleted && !onlySecondIsStrictlyCompleted && !onlySecondCanStrictlyBeCompleted && !onlySecondHelpsCompletingFirst && !secondHasMoreHealth && !secondHasLessMissingMols && !secondIsMoreRank && !secondIsMoreRecent)
  }

  def chooseMolecule(): Option[Char] = {
    if (storageSum < 10) {
      if (world.opponent.equals(this)) {
        def moleculeToCompleteSample(): Option[Char] = samples.filter(s => s.opCanBeCompleted && !isStrictlyCompleteSample(s) && s.health >= 20)
          .foldLeft(Map[Char, Int]())((collected, sample) => if (collected.isEmpty) sample.opMissingMols else sample.opMissingMols.map { case (mol, cost) => (mol, if (collected(mol) > 0) collected(mol) + cost else 0) })
          .filter(s => s._2 > 0 && world.availableMolecules(s._1) > 0).maxByOption(_._2).map(_._1)

        def moleculeToCompleteDiagnosed(): Option[Char] = diagnosedSamples().filter(s => s.health >= 20)
          .foldLeft(Map[Char, Int]())((collected, sample) => if (collected.isEmpty) sample.opMissingMols else sample.opMissingMols.map { case (mol, cost) => (mol, if (collected(mol) > 0) collected(mol) + cost else 0) })
          .filter(s => s._2 > 0 && world.availableMolecules(s._1) > 0).maxByOption(_._2).map(_._1)

        var chosenMol = moleculeToCompleteSample()
        if (chosenMol.isEmpty)
          chosenMol = moleculeToCompleteDiagnosed()
        chosenMol
      }
      else {
        def moleculeToCompleteSample(): Option[Char] = samples.filter(s => s.canBeCompleted && !isCompleteSample(s))
          .foldLeft(Map[Char, Int]())((collected, sample) => if (collected.isEmpty) sample.missingMols else sample.missingMols.map { case (mol, cost) => (mol, if (collected(mol) > 0) collected(mol) + cost else 0) })
          .filter(s => s._2 > 0 && world.availableMolecules(s._1) > 0).maxByOption(_._2).map(_._1)

        def moleculeToCompleteDiagnosed(): Option[Char] = toDiagnoseSamples
          .foldLeft(Map[Char, Int]())((collected, sample) => if (collected.isEmpty) sample.missingMols else sample.missingMols.map { case (mol, cost) => (mol, if (collected(mol) > 0) collected(mol) + cost else 0) })
          .filter(s => s._2 > 0 && world.availableMolecules(s._1) > 0).maxByOption(_._2).map(_._1)

        // def moleculeForCarriedSample(): Option[Char] = samples.filter(s => !s.canBeCompleted && !isCompleteSample(s))
        //   .foldLeft(Map[Char, Int]())((collected, sample) => if (collected.isEmpty) sample.missingMols else sample.missingMols.map { case (mol, cost) => (mol, if (collected(mol) > 0) collected(mol) + cost else 0) })
        //   .filter(s => s._2 > 0 && world.availableMolecules(s._1) > 0).maxByOption(_._2).map(_._1)

        def moleculeToStealFromOpponent() = world.opponent.chooseMolecule()

        var chosenMol = moleculeToStealFromOpponent()
        if (chosenMol.isEmpty)
          chosenMol = moleculeToCompleteSample()
        if (chosenMol.isEmpty)
          chosenMol = moleculeToCompleteDiagnosed()
        // if (chosenMol.isEmpty && samples.filter(_.isAnalysed).length > 0)
        // chosenMol = moleculeToStealFromOpponent()
        // if (chosenMol.isEmpty)
        //   chosenMol = moleculeForCarriedSample()
        chosenMol
      }
    }
    else {
      Option.empty
    }
  }

  def completeAndSortSamples(samples: List[Sample]): List[Sample] = {
    timeId += 1
    var sorted = timed("updating " + samples.length, () => samples.map(s => {
      s.strictlyMissingMols = timed("strictlyMissingStorage", () => strictlyMissingStorage(s))
      // s.opStrictlyMissingMols = timed("opstrictlyMissingStorage", world.opponent.strictlyMissingStorage(s))
      s.strictlyMissingSum = timed("strSum", () => s.strictlyMissingMols.values.sum)
      // s.opStrictlyMissingSum = timed("opstrSum", () => s.opStrictlyMissingMols.values.sum)
      s.canStrictlyBeCompleted = timed("canStrictlyCompleteSample", () => canStrictlyCompleteSample(s))
      // s.opCanStrictlyBeCompleted = timed("opcanStrictlyCompleteSample", () => world.opponent.canStrictlyCompleteSample(s))
      s.gainedExpertise = timed("gainedExpertise", () => gainedExpertise(s))
      // s.opGainedExpertise = timed("opgainedExpertise", () => world.opponent.gainedExpertise(s))
      s.requiredStorage = timed("requiredStorage", () => requiredStorage(s))
      // s.opRequiredStorage = timed("oprequiredStorage", () => world.opponent.requiredStorage(s))
      s.usedStorage = timed("usedStorage", () => usedStorage(s))
      // s.opUsedStorage = timed("opusedStorage", () => world.opponent.usedStorage(s))
      s.missingMols = timed("missingStorage", () => missingStorage(s))
      // s.opMissingMols = timed("opmissingStorage", () => world.opponent.missingStorage(s))
      s.missingSum = timed("missSum", () => s.missingMols.values.sum)
      // s.opMissingSum = timed("opmissSum", () => s.opMissingMols.values.sum)
      s.canBeCompleted = timed("canCompleteSample", () => canCompleteSample(s))
      // s.opCanBeCompleted = timed("opcanCompleteSample", () => world.opponent.canCompleteSample(s))
      s.neededForProject = timed("canCompleteSample", () => neededForProject(s))
      s
    }))
    sorted = timed("sorting " + samples.length, () => sorted.sortWith(sortSamples))

    sorted = timed("updating " + samples.length, () => sorted.map(s => {
      s.strictlyMissingMols = timed("strictlyMissingStorage", () => strictlyMissingStorage(s))
      s.opStrictlyMissingMols = timed("opstrictlyMissingStorage", () => world.opponent.strictlyMissingStorage(s))
      s.strictlyMissingSum = timed("strSum", () => s.strictlyMissingMols.values.sum)
      s.opStrictlyMissingSum = timed("opstrSum", () => s.opStrictlyMissingMols.values.sum)
      s.canStrictlyBeCompleted = timed("canStrictlyCompleteSample", () => canStrictlyCompleteSample(s))
      s.opCanStrictlyBeCompleted = timed("opcanStrictlyCompleteSample", () => world.opponent.canStrictlyCompleteSample(s))
      s.gainedExpertise = timed("gainedExpertise", () => gainedExpertise(s))
      s.opGainedExpertise = timed("opgainedExpertise", () => world.opponent.gainedExpertise(s))
      s.requiredStorage = timed("requiredStorage", () => requiredStorage(s))
      s.opRequiredStorage = timed("oprequiredStorage", () => world.opponent.requiredStorage(s))
      s.usedStorage = timed("usedStorage", () => usedStorage(s))
      s.opUsedStorage = timed("opusedStorage", () => world.opponent.usedStorage(s))
      s.missingMols = timed("missingStorage", () => missingStorage(s))
      s.opMissingMols = timed("opmissingStorage", () => world.opponent.missingStorage(s))
      s.missingSum = timed("missSum", () => s.missingMols.values.sum)
      s.opMissingSum = timed("opmissSum", () => s.opMissingMols.values.sum)
      s.canBeCompleted = timed("canCompleteSample", () => canCompleteSample(s))
      s.opCanBeCompleted = timed("opcanCompleteSample", () => world.opponent.canCompleteSample(s))
      s.neededForProject = timed("canCompleteSample", () => neededForProject(s))
      s
    }))
    sorted
  }

  def updateRobot(robotUpdate: String): Unit = {
    val Array(_target, _eta, _score, _storageA, _storageB, _storageC, _storageD, _storageE, _expertiseA, _expertiseB, _expertiseC, _expertiseD, _expertiseE) = robotUpdate.split(" ").filter(!_.equals(""))
    target = Locations.withName(_target)
    eta = _eta.toInt
    score = _score.toInt
    storage = createMoleculesMap(_storageA, _storageB, _storageC, _storageD, _storageE)
    storageSum = storage.values.sum
    expertise = createMoleculesMap(_expertiseA, _expertiseB, _expertiseC, _expertiseD, _expertiseE)
    expertiseSum = expertise.values.sum
  }
}

case class Sample(
                   id: Int,
                   carrierId: Int,
                   rank: Int,
                   gain: Char,
                   health: Int,
                   cost: Map[Char, Int],
                 ) {
  var gainedExpertise: List[Char] = _
  var neededForProject: Boolean = _
  var canBeCompleted: Boolean = _
  var canStrictlyBeCompleted: Boolean = _
  var strictlyMissingMols: Map[Char, Int] = _
  var strictlyMissingSum: Int = _
  var missingMols: Map[Char, Int] = _
  var missingSum: Int = _
  var requiredStorage: Map[Char, Int] = _
  var usedStorage: Map[Char, Int] = _
  var opGainedExpertise: List[Char] = _
  var opCanBeCompleted: Boolean = _
  var opCanStrictlyBeCompleted: Boolean = _
  var opStrictlyMissingMols: Map[Char, Int] = _
  var opStrictlyMissingSum: Int = _
  var opMissingMols: Map[Char, Int] = _
  var opMissingSum: Int = _
  var opRequiredStorage: Map[Char, Int] = _
  var opUsedStorage: Map[Char, Int] = _

  def isAnalysed: Boolean = health != -1
}

class World() {
  var robot: Robot = new Robot(this)
  var opponent: Robot = new Robot(this)
  var samples: List[Sample] = List()
  var availableMolecules: Map[Char, Int] = createMoleculesMap("0", "0", "0", "0", "0")
  var projects: List[Map[Char, Int]] = List()
  var turns: Integer = 0

  def initializeProjects(projectsStr: List[String]): Unit = {
    projects = (projectsStr.map(p => {
      p.split(" ").filter(_ != "").map(_.toInt)
        .zipWithIndex.map({ case (v, i) => (('A' + i).toChar, v) }).toMap
    }) ::: List(createMoleculesMap("1", "1", "1", "1", "1")))
      .sortBy(_.values.sum)
  }

  def updateSamples(sampleLines: List[String]): Unit = {
    val parsedSamples = sampleLines.map(p => {
      val Array(_sampleId, _carriedBy, _rank, expertiseGain, _health, _costA, _costB, _costC, _costD, _costE) = p.split(" ").filter(!_.equals(""))
      Sample(
        _sampleId.toInt,
        _carriedBy.toInt,
        _rank.toInt,
        expertiseGain.head,
        _health.toInt,
        createMoleculesMap(_costA, _costB, _costC, _costD, _costE)
      )
    })
    samples = parsedSamples.filter(_.carrierId == -1)
    robot.samples = parsedSamples.filter(_.carrierId == 0)
    opponent.samples = parsedSamples.filter(_.carrierId == 1)
  }

  def updateAll(inputStr: List[List[String]], turns: Int = 0): Unit = {
    initializeProjects(inputStr.head)
    robot.updateRobot(inputStr(1).head)
    opponent.updateRobot(inputStr(1)(1))
    availableMolecules = (inputStr(1)(2) split " ").filter(_ != "").map(_.toInt)
      .zipWithIndex.map({ case (v, i) => (('A' + i).toChar, v) }).toMap
    updateSamples(inputStr(2))
    robot.robotBrain.state = robot.target match {
      case Locations.SAMPLES => Collect()
      case Locations.DIAGNOSIS => Analyze()
      case Locations.MOLECULES => Gather()
      case Locations.LABORATORY => Produce()
      case _ => Collect()
    }

    this.turns = if (turns == 0) this.turns + 1 else turns
  }

  def simulateTurn(sample: Sample = null): Unit = {
    val goto = """GOTO (.*)""".r
    val connect = """CONNECT (.*)""".r
    val action = robot.nextAction()
    (robot.target, action) match {
      case (_, goto(location)) =>
        robot.target = Locations.withName(location)
        turns += 3
      case (Locations.SAMPLES, _) =>
        robot.samples = sample :: robot.samples
        turns += 1
      case (Locations.DIAGNOSIS, connect(sampleId)) =>
        if (sample != null) {
          robot.samples = sample :: robot.samples.filter(_.id != sample.id)
        }
        else if (robot.world.samples.exists(_.id == sampleId.toInt)) {
          robot.samples = robot.world.samples.filter(_.id == sampleId.toInt) ::: robot.samples
          robot.world.samples = robot.world.samples.filter(_.id != sampleId.toInt)
        } else {
          robot.world.samples = robot.samples.filter(_.id == sampleId.toInt) ::: robot.world.samples
          robot.samples = robot.samples.filter(_.id != sampleId.toInt)
        }
        turns += 1
      case (Locations.MOLECULES, connect(molecule)) =>
        robot.storage = robot.storage.updatedWith(molecule.charAt(0))(v => v.map(_ + 1))
        turns += 1
      case (Locations.LABORATORY, connect(sampleId)) =>
        val sample = robot.samples.find(_.id == sampleId.toInt).get
        robot.storage = robot.requiredStorage(sample).foldLeft(robot.storage)((mol, req) => mol.updatedWith(req._1)(_.map(_ - req._2)))
        robot.samples = robot.samples.filter(_.id != sampleId.toInt)
        robot.expertise = robot.expertise.updatedWith(sample.gain)(v => v.map(_ + 1))
        turns += 1
      case (_, _) =>
    }
  }
}

object Player extends App {
  val projectCount = readLine.toInt
  val projectLines = (0 until projectCount).map(_ => readLine).toList

  val world: World = new World()
  world.initializeProjects(projectLines)

  while (true) {
    val robotUpdate = readLine
    val opponentUpdate = readLine
    val availMolecules = readLine
    val sampleCount = readLine.toInt
    val sampleLines = (0 until sampleCount).map(_ => readLine).toList
    val inputStr = List(projectLines, List(robotUpdate, opponentUpdate, availMolecules), sampleLines)

    world.updateAll(inputStr)

    val action = world.robot.nextAction()

    Console.err.println(turnString(inputStr, action))

    println(action)
  }
}
