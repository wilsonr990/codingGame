package puzzle.marsLanderEpisode1
import scala.io.StdIn._

object Player extends App {
  val surfaceN      = readLine.toInt // the number of points used to draw the surface of Mars.
  var prev          = 0
  var prevXLanding  = 0
  var freeFallLimit = 0
  var landingMinX   = 0
  var landingMaxX   = 0

  for (i <- 0 until surfaceN) {
    val Array(landX, landY) = (readLine split " ").filter(_ != "").map(_.toInt)
    if (landY == prev) {
      freeFallLimit = prev
      landingMinX = prevXLanding
      landingMaxX = landX
    }
    prev = landY
    prevXLanding = landX
  }

  // game loop
  while (true) {
    val Array(x, y, hSpeed, vSpeed, fuel, rotate, power) = (readLine split " ").filter(_ != "").map(_.toInt)

    var powerOutput  = 0
    var rotateOutput = 0
    if (vSpeed < -35 || Math.abs(hSpeed) > 20) {
      powerOutput = 4
    }

    if (hSpeed > 20) rotateOutput = 15
    else if (hSpeed < -20) rotateOutput = -15
    else if (x >= landingMinX && x <= landingMaxX) {
      if (rotate > 0) rotateOutput = rotate - 15
      else if (rotate < 0) rotateOutput = rotate + 15
    } else {
      if (x < landingMinX) rotateOutput = -15
      else if (x > landingMaxX) rotateOutput = 15
    }

    println(rotateOutput + " " + powerOutput)
  }
}
