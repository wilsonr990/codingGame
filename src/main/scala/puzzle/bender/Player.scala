package puzzle.bender

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.io.StdIn._

object Utils {
  var timeId = 0;
  var debug  = false;

  def createMoleculesMap(strings: String*): Map[Char, Int] = {
    strings.map(_.toInt).zipWithIndex.map({ case (v, i) => (('A' + i).toChar, v) }).toMap
  }

  def timed[T](n: String, f: () => T): T = {
    if (debug) {
      val start = System.nanoTime()
      val ret   = f()
      val end   = System.nanoTime()
      val us    = (end - start) / 1000
      if (us != 0) Console.err.println(s"${timeId} Time $n: $us us")
      ret
    } else
      f()
  }

  def turnString(inputStr: List[List[String]]): String = {
    def listToString(list: List[Any]): String = if (list.isEmpty) "List()" else list.mkString("List(\"", "\", \"", "\")")

    def listOfListsToString(list: List[List[Any]]): String = if (list.isEmpty) "List()" else list.map(listToString).mkString("List(", ", ", ")")

    "testGameSituation(" + listOfListsToString(inputStr) + ", \"" + "\")"
  }

  def turnString(inputStr: List[List[String]], action: String): String = {
    def listToString(list: List[Any]): String = if (list.isEmpty) "List()" else list.mkString("List(\"", "\", \"", "\")")

    def listOfListsToString(list: List[List[Any]]): String = if (list.isEmpty) "List()" else list.map(listToString).mkString("List(", ", ", ")")

    "testGameSituation(" + listOfListsToString(inputStr) + ", \"" + action + "\")"
  }
}

// Exceptions
sealed class CustomException(message: String = "")          extends Exception(message)
final case class InvalidDataException(message: String = "") extends CustomException(message)
case class Bounds(left: Int, top: Int, right: Int, bottom: Int)
case class Dimension(fieldId: String,
                     garbageId: String,
                     fields: Map[Location, Field] = Map(),
                     garbage: mutable.Map[Location, Garbage] = mutable.Map())
case class DimensionalLocation(location: Location, dimensionId: String)
case class Location(r: Int, c: Int) {
  def right(offset: Int = 1)(implicit bounds: Bounds): Option[Location] = {
    if (c > bounds.right) None
    else {
      val newC = c + offset
      Some(Location(r, if (newC > bounds.right) bounds.right else newC))
    }
  }
  def left(offset: Int = 1)(implicit bounds: Bounds): Option[Location] = {
    if (c < bounds.left) None
    else {
      val newC = c - offset
      Some(Location(r, if (newC < bounds.left) bounds.left else newC))
    }
  }
  def top(offset: Int = 1)(implicit bounds: Bounds): Option[Location] = {
    if (r < bounds.top) None
    else {
      val newR = r - offset
      Some(Location(if (newR < bounds.top) bounds.top else newR, c))
    }
  }
  def bottom(offset: Int = 1)(implicit bounds: Bounds): Option[Location] = {
    if (r > bounds.bottom) None
    else {
      val newR = r + offset
      Some(Location(if (newR > bounds.bottom) bounds.bottom else newR, c))
    }
  }

  def moveTo(direction: Char)(implicit bounds: Bounds): Option[Location] = {
    direction match {
      case 'U' => top()
      case 'D' => bottom()
      case 'L' => left()
      case 'R' => right()
    }
  }
  def directionTo(newL: Location): String = {
    if (newL.c > c) "R"
    else if (newL.r > r) "D"
    else if (newL.c < c) "L"
    else if (newL.r < r) "U"
    else throw InvalidDataException("Already in Location")
  }

  def distanceTo(o: Location): Int = {
    Math.abs(c - o.c) + Math.abs(r - o.r)
  }
}
case class Bender(world: World) {
  var location: Location = _
  def updateLocation(location: Location): Unit = {
    this.location = location
  }
}
case class Fry(world: World) {
  var location: Location = _
  def updateLocation(location: Location): Unit = {
    this.location = location
  }
}
case class PathNode(dimensionalLocation: DimensionalLocation, parent: DimensionalLocation, rootDistance: Int, direction: String, path: List[Location])
sealed class MazeCell(val location: Location, val direction: String, val path: List[Location])
case class NormalCell(override val location: Location, override val direction: String, override val path: List[Location])
    extends MazeCell(location, direction, path)
case class GarbageCell(garbageLastKnownLocation: Location,
                       override val location: Location,
                       override val direction: String,
                       override val path: List[Location])
    extends MazeCell(location, direction, path)
case class SwitchCell(switch: Switch, override val direction: String, override val path: List[Location])
    extends MazeCell(switch.location, direction, path)
case class FieldCell(field: Field, override val direction: String, override val path: List[Location])
    extends MazeCell(field.location, direction, path)
case class Garbage(originalLocation: Location, location: Location)
case class Switch(location: Location, field: Field)
case class Field(location: Location, switchLocation: Location, active: Boolean)

case class Maze(world: World) {
  var bounds: Bounds                                       = _
  var originalMazeLines: Array[String]                     = Array()
  var mazeLines: Array[String]                             = Array()
  val mazeNeighbors: mutable.Map[Location, List[MazeCell]] = mutable.Map()
  private var initialDimensionId: String                   = ""
  private var hasVisitedDestination: Boolean               = false
  private var bestDestinationPath: PathNode                = _
  private var destination: Location                        = _

  private val dimensions: mutable.Map[String, Dimension]               = mutable.Map()
  private val visitedPaths: mutable.Map[DimensionalLocation, PathNode] = mutable.Map()
  private val toVisit: mutable.PriorityQueue[PathNode] = mutable.PriorityQueue.empty {
    Ordering.by(
      p =>
        (-p.rootDistance - p.dimensionalLocation.location.distanceTo(destination)) *
          (dimensions(p.dimensionalLocation.dimensionId).garbageId.count(_ == ';') + 1) / 2)
  }

  def updateMaze(bounds: Bounds, mazeLines: Array[String]): Unit = {
    this.bounds = bounds
    this.originalMazeLines = mazeLines.clone()
    this.mazeLines = mazeLines

    precalculateNeighbors()

    val fieldId = world.fields.values.map(f => if (f.active) "1" else "0").mkString("")
    initialDimensionId = s"$fieldId-"
    this.dimensions(initialDimensionId) = Dimension(fieldId, "", world.fields.toMap, world.garbage)
  }
  private def precalculateNeighbors(): Unit = {
    case class Node(location: Location, parent: Option[Node])
    val toVisit: mutable.Queue[Node] = mutable.Queue(Node(world.bender.location, None))
    var visited: List[Location]      = List()

    var listedParent: Boolean = false
    while (toVisit.nonEmpty) {
      val node = toVisit.dequeue()
      listedParent = false

      val expectedNeighbors = getExpectedNeighbors(node.location)
      val neighbors         = getNeighbors(node.location, save = false)
      expectedNeighbors
        .collect {
          case uselessN if !neighbors.exists(_.path(1) == uselessN.path(1)) && !uselessN.isInstanceOf[GarbageCell] =>
            val (prev, post) = mazeLines(uselessN.location.r).splitAt(uselessN.location.c)
            mazeLines(uselessN.location.r) = prev + '#' + post.tail
            listedParent = true
            node.parent.foreach(toVisit.enqueue)
        }

      world.garbage.collect {
        case (location, _) if !visited.contains(location) =>
          val neighbors = getNeighbors(location, save = false)
          if (neighbors.length == 1 || (neighbors.length == 2 &&
              ((neighbors.head.direction.head == 'R' && neighbors(1).direction.head != 'L') ||
              (neighbors.head.direction.head == 'L' && neighbors(1).direction.head != 'R') ||
              (neighbors.head.direction.head == 'U' && neighbors(1).direction.head != 'D') ||
              (neighbors.head.direction.head == 'D' && neighbors(1).direction.head != 'U')))) {
            val (prev, post) = mazeLines(location.r).splitAt(location.c)
            mazeLines(location.r) = prev + '#' + post.tail
            visited ::= location
            world.garbage.remove(location)
            listedParent = true
            node.parent.foreach(toVisit.enqueue)
          }
      }
      if (!listedParent) {
        visited ::= node.location
        toVisit.enqueueAll(neighbors.collect {
          case c if !visited.contains(c.location) =>
            Node(c.location, Some(node))
        })
      }
    }
    val l = ""
  }
  def getPath(origin: Location, destination: Location): List[PathNode] = {
    this.destination = destination
    calculatePathToDestination(origin)
  }
  private def calculatePathToDestination(currentLocation: Location): List[PathNode] = {
    val initialLocation = DimensionalLocation(currentLocation, initialDimensionId)
    val initialNode     = PathNode(initialLocation, initialLocation, 0, "", List())
    visitedPaths(initialLocation) = initialNode
    toVisit.enqueue(initialNode)

    while (toVisit.nonEmpty && (!hasVisitedDestination)) {
      val path = toVisit.dequeue()
      if (path.dimensionalLocation.location == destination) {
        hasVisitedDestination = true
        bestDestinationPath = path
      } else {
        getNeighbors(path.dimensionalLocation.location)
          .foreach(n => {
            filterWithGarbage(path.dimensionalLocation, n)
              .map(n => updatePaths(path, n))
          })
      }
    }
    calculatePath(bestDestinationPath)
  }
  private def filterWithGarbage(dimensionalLocation: DimensionalLocation, n: MazeCell) = {
    val currentDimension = dimensions(dimensionalLocation.dimensionId)
    val garbageIndex     = n.path.indexWhere(currentDimension.garbage.contains)

    if (world.useGarbage && garbageIndex != -1) {
      var garbageLocation: Location   = null
      var garbagePath: List[Location] = List()
      var garbageDirection            = ""

      val (directionTillGarbage, additional) = n.direction.splitAt(garbageIndex)
      if (directionTillGarbage.nonEmpty) {
        val additionalDraftSteps     = additional.takeWhile(_ == directionTillGarbage.last)
        val garbageLastKnownLocation = n.path.drop(garbageIndex).head
        garbageDirection = directionTillGarbage + additionalDraftSteps.dropRight(1)
        garbagePath = n.path.take(garbageDirection.length + 1)
        garbageLocation = garbagePath.last

        implicit val bounds: Bounds = world.maze.bounds
        val garbageNextLocation     = garbageLocation.moveTo(garbageDirection.last)
        val canBeMoved = (!world.fields.contains(garbageLocation) || !currentDimension.fields(garbageLocation).active) &&
          garbageNextLocation.nonEmpty && garbageNextLocation.get != world.fry.location &&
          originalMazeLines(garbageNextLocation.get.r)(garbageNextLocation.get.c) == '.'
        if (canBeMoved) {
          Some(GarbageCell(garbageLastKnownLocation, garbageLocation, garbageDirection, garbagePath))
        } else {
          None
        }
      } else None
    } else {
      Some(n)
    }
  }
  private def getNeighbors(location: Location, save: Boolean = true): List[MazeCell] = {
    if (mazeNeighbors.contains(location)) {
      mazeNeighbors(location)
    } else {
      val toVisit: mutable.Queue[MazeCell] = mutable.Queue.empty
      val neighbors: ArrayBuffer[MazeCell] = mutable.ArrayBuffer()

      getExpectedNeighbors(location)
        .foreach(n => toVisit.enqueue(n))

      while (toVisit.nonEmpty) {
        val neighbor  = toVisit.dequeue
        val direction = neighbor.path.tail.zipWithIndex.map { case (c, i) => neighbor.path(i).directionTo(c) }.mkString("")

        neighbor match {
          case _: SwitchCell =>
            val switch = world.switches(neighbor.location)

            val fieldNeighbors = getExpectedNeighbors(switch.field.location)
            if (fieldNeighbors.length > 1)
              neighbors.addOne(SwitchCell(switch, direction, neighbor.path))
            else {
              neighbors.addOne(NormalCell(neighbor.location, direction, neighbor.path))
            }
          case _: NormalCell if world.bender.location == neighbor.location || world.fry.location == neighbor.location =>
            neighbors.addOne(NormalCell(neighbor.location, direction, neighbor.path))
          case _: FieldCell =>
            val newLoc = getExpectedNeighbors(neighbor.location)
              .filter(n => !neighbor.path.exists(n.path.tail.contains(_)))
            if (newLoc.nonEmpty) {
              val field = world.fields(neighbor.location)
              neighbors.addOne(FieldCell(field, direction, neighbor.path))
            }
          case _: GarbageCell =>
            implicit val bounds: Bounds = world.maze.bounds
            val garbageNextLocation     = neighbor.location.moveTo(neighbor.direction.last)
            val canBeMoved = garbageNextLocation.nonEmpty && garbageNextLocation.get != world.fry.location &&
              originalMazeLines(garbageNextLocation.get.r)(garbageNextLocation.get.c) == '.'

            if (canBeMoved) {
              val newLoc = getExpectedNeighbors(neighbor.location)
                .filter(n => !neighbor.path.exists(n.path.tail.contains(_)))
              if (newLoc.length > 1 || world.switches.contains(neighbor.location))
                neighbors.addOne(NormalCell(neighbor.location, direction, neighbor.path))
              else if (newLoc.nonEmpty) {
                toVisit.enqueue(
                  GarbageCell(
                    neighbor.asInstanceOf[GarbageCell].garbageLastKnownLocation,
                    newLoc.head.location,
                    neighbor.direction + newLoc.head.direction,
                    neighbor.path ::: newLoc.head.path.tail
                  ))
              }
            } else {
              val newLoc = getExpectedNeighbors(neighbor.location)
                .filter(n => !neighbor.path.contains(n.path(1)) && !n.isInstanceOf[GarbageCell])
              if (newLoc.length == 2 &&
                  ((newLoc.head.direction.head == 'R' && newLoc(1).direction.head == 'L') ||
                  (newLoc.head.direction.head == 'L' && newLoc(1).direction.head == 'R') ||
                  (newLoc.head.direction.head == 'U' && newLoc(1).direction.head == 'D') ||
                  (newLoc.head.direction.head == 'D' && newLoc(1).direction.head == 'U')))
                neighbors.addOne(NormalCell(neighbor.location, direction, neighbor.path))
              else {
                if (world.switches.contains(neighbor.location)) {
                  neighbors.addOne(NormalCell(neighbor.location, direction, neighbor.path))
                } else {
                  val d = ""
                }
              }
            }
          case _: NormalCell =>
            val newLoc = getExpectedNeighbors(neighbor.location)
              .filter(n => !neighbor.path.exists(n.path.tail.contains(_)))

            if (newLoc.length > 1)
              neighbors.addOne(NormalCell(neighbor.location, direction, neighbor.path))
            else if (newLoc.nonEmpty) {
              newLoc.head match {
                case n: SwitchCell =>
                  toVisit.enqueue(SwitchCell(n.switch, neighbor.direction + n.direction, neighbor.path ::: n.path.tail))
                case n: FieldCell =>
                  toVisit.enqueue(FieldCell(n.field, neighbor.direction + n.direction, neighbor.path ::: n.path.tail))
                case n: NormalCell =>
                  toVisit.enqueue(NormalCell(n.location, neighbor.direction + n.direction, neighbor.path ::: n.path.tail))
                case n: GarbageCell =>
                  toVisit.enqueue(GarbageCell(n.location, n.location, neighbor.direction + n.direction, neighbor.path ::: n.path.tail))
                case _ =>
                  throw InvalidDataException("Unknown type of neighbors received")
              }
            }
          case _ =>
            throw InvalidDataException("Unknown type of neighbors received")
        }
      }
      val result = neighbors.toList
      if (save)
        mazeNeighbors(location) = result
      result
    }
  }
  def getExpectedNeighbors(cellLocation: Location): List[MazeCell] = {
    if (mazeNeighbors.contains(cellLocation)) {
      mazeNeighbors(cellLocation)
    } else {
      implicit val bounds: Bounds = world.maze.bounds
      List(
        cellLocation.left(),
        cellLocation.bottom(),
        cellLocation.right(),
        cellLocation.top()
      ).collect {
        case Some(location) if mazeLines(location.r)(location.c) == '.' && world.fields.contains(location) =>
          FieldCell(world.fields(location), cellLocation.directionTo(location), List(cellLocation, location))
        case Some(location) if mazeLines(location.r)(location.c) == '.' && world.switches.contains(location) =>
          SwitchCell(world.switches(location), cellLocation.directionTo(location), List(cellLocation, location))
        case Some(location) if mazeLines(location.r)(location.c) == '.' =>
          NormalCell(location, cellLocation.directionTo(location), List(cellLocation, location))
        case Some(location) if world.useGarbage && mazeLines(location.r)(location.c) == '+' =>
          if (!world.garbage.contains(location)) {
            world.garbage(location) = Garbage(location, location)
            dimensions.foreach(d => d._2.garbage(location) = Garbage(location, location))
          }
          GarbageCell(location, location, cellLocation.directionTo(location), List(cellLocation, location))
      }
    }
  }
  private def moveGarbage(dimensionalLocation: DimensionalLocation, neighbor: GarbageCell): String = {
    val currentDimension = dimensions(dimensionalLocation.dimensionId)

    implicit val bounds: Bounds = world.maze.bounds
    val newGarbageLocation = neighbor.direction.last match {
      case 'L' => neighbor.location.left().get
      case 'R' => neighbor.location.right().get
      case 'U' => neighbor.location.top().get
      case 'D' => neighbor.location.bottom().get
    }
    val updatedGarbage = currentDimension.garbage
      .clone()
      .addOne(
        newGarbageLocation,
        currentDimension.garbage(neighbor.garbageLastKnownLocation).copy(location = newGarbageLocation)
      )
    updatedGarbage.remove(neighbor.garbageLastKnownLocation)

    val updatedFields: Map[Location, Field] = if (world.switches.contains(newGarbageLocation)) {
      val fieldLocation = world.switches(newGarbageLocation).field.location
      currentDimension.fields
        .updated(fieldLocation, currentDimension.fields(fieldLocation).copy(active = !currentDimension.fields(fieldLocation).active))
    } else if (world.switches.contains(neighbor.garbageLastKnownLocation)) {
      val fieldLocation = world.switches(neighbor.garbageLastKnownLocation).field.location
      currentDimension.fields
        .updated(fieldLocation, currentDimension.fields(fieldLocation).copy(active = !currentDimension.fields(fieldLocation).active))
    } else {
      currentDimension.fields
    }

    val newFieldId = updatedFields.map(f => if (f._2.active) "1" else "0").mkString("")

    val newGarbageId = updatedGarbage
      .collect {
        case (location, garbage) if location != garbage.originalLocation =>
          s"${garbage.originalLocation.r}.${garbage.originalLocation.c},${garbage.location.r}.${garbage.location.c}"
      }
      .toList
      .sorted
      .mkString(";")
    val newDimensionId = s"$newFieldId-$newGarbageId"

    if (!dimensions.contains(newDimensionId)) {
      dimensions(newDimensionId) = Dimension(newFieldId, newGarbageId, updatedFields, updatedGarbage)
    }
    newDimensionId
  }
  private def toggleField(dimensionId: String, location: Location): String = {
    val currentDimension = dimensions(dimensionId)
    val updatedFields = currentDimension.fields
      .updated(location, currentDimension.fields(location).copy(active = !currentDimension.fields(location).active))

    val newFieldId = updatedFields.map(f => if (f._2.active) "1" else "0").mkString("")

    val newDimensionId = s"$newFieldId-${currentDimension.garbageId}"
    if (!dimensions.contains(newDimensionId)) {
      dimensions(newDimensionId) = Dimension(newFieldId, currentDimension.garbageId, updatedFields, currentDimension.garbage)
    }
    newDimensionId
  }
  private def updatePaths(path: PathNode, neighborCell: MazeCell): Unit = {
    neighborCell match {
      case neighbor: GarbageCell if !path.dimensionalLocation.dimensionId.contains(';') =>
        val newLevel = moveGarbage(path.dimensionalLocation, neighbor)

        val neighborLeveledLocation = DimensionalLocation(neighbor.location, newLevel)
        if (path.parent != neighborLeveledLocation) {
          val pathRootDistance = path.rootDistance + neighbor.direction.length
          val neighborPath =
            PathNode(
              neighborLeveledLocation,
              path.dimensionalLocation,
              pathRootDistance,
              neighbor.direction,
              neighbor.path
            )
          if (!visitedPaths.contains(neighborLeveledLocation) ||
              visitedPaths(neighborLeveledLocation).rootDistance + visitedPaths(neighborLeveledLocation).dimensionalLocation.location.distanceTo(
                destination) > neighborPath.rootDistance + neighborPath.dimensionalLocation.location.distanceTo(destination)) {
            visitedPaths(neighborLeveledLocation) = neighborPath
            toVisit.enqueue(neighborPath)
          }
        }
      case neighbor: SwitchCell =>
        val newLevel = toggleField(path.dimensionalLocation.dimensionId, neighbor.switch.field.location)

        val neighborLeveledLocation = DimensionalLocation(neighbor.location, newLevel)
        if (path.parent != neighborLeveledLocation) {
          val pathRootDistance = path.rootDistance + neighbor.direction.length
          val neighborPath =
            PathNode(
              neighborLeveledLocation,
              path.dimensionalLocation,
              pathRootDistance,
              neighbor.direction,
              neighbor.path
            )
          if (!visitedPaths.contains(neighborLeveledLocation) ||
              visitedPaths(neighborLeveledLocation).rootDistance + visitedPaths(neighborLeveledLocation).dimensionalLocation.location.distanceTo(
                destination) > neighborPath.rootDistance + neighborPath.dimensionalLocation.location.distanceTo(destination)) {
            visitedPaths(neighborLeveledLocation) = neighborPath
            toVisit.enqueue(neighborPath)
          }
        }
      case neighbor: FieldCell =>
        val neighborLeveledLocation = DimensionalLocation(neighbor.location, path.dimensionalLocation.dimensionId)
        if (path.parent != neighborLeveledLocation) {
          val pathRootDistance = path.rootDistance + neighbor.direction.length
          val neighborPath =
            PathNode(
              neighborLeveledLocation,
              path.dimensionalLocation,
              pathRootDistance,
              neighbor.direction,
              neighbor.path
            )
          if ((!visitedPaths.contains(neighborLeveledLocation) ||
              visitedPaths(neighborLeveledLocation).rootDistance + visitedPaths(neighborLeveledLocation).dimensionalLocation.location.distanceTo(
                destination) > neighborPath.rootDistance + neighborPath.dimensionalLocation.location.distanceTo(destination)) &&
              !dimensions(neighborLeveledLocation.dimensionId).fields(neighbor.location).active) {
            visitedPaths(neighborLeveledLocation) = neighborPath
            toVisit.enqueue(neighborPath)
          }
        }
      case neighbor: NormalCell =>
        val neighborLeveledLocation = DimensionalLocation(neighbor.location, path.dimensionalLocation.dimensionId)
        if (path.parent != neighborLeveledLocation) {
          val pathRootDistance = path.rootDistance + neighbor.direction.length
          val neighborPath =
            PathNode(
              neighborLeveledLocation,
              path.dimensionalLocation,
              pathRootDistance,
              neighbor.direction,
              neighbor.path
            )
          if (!visitedPaths.contains(neighborLeveledLocation) ||
              visitedPaths(neighborLeveledLocation).rootDistance + visitedPaths(neighborLeveledLocation).dimensionalLocation.location.distanceTo(
                destination) > neighborPath.rootDistance + neighborPath.dimensionalLocation.location.distanceTo(destination)) {
            visitedPaths(neighborLeveledLocation) = neighborPath
            toVisit.enqueue(neighborPath)
          }
        }
      case _ =>
    }
  }
  private def calculatePath(node: PathNode): List[PathNode] = {
    var currentNode          = node
    var path: List[PathNode] = List(node)

    while (currentNode.rootDistance > 1) {
      currentNode = visitedPaths(currentNode.parent)
      path = currentNode :: path
    }
    path
  }
}
class World() {
  val bender: Bender                          = Bender(this)
  val fry: Fry                                = Fry(this)
  val maze: Maze                              = Maze(this)
  val garbage: mutable.Map[Location, Garbage] = mutable.Map()
  val switches: mutable.Map[Location, Switch] = mutable.Map()
  val fields: mutable.Map[Location, Field]    = mutable.Map()
  var comprise: Boolean                       = false
  var useGarbage: Boolean                     = true

  def initialize(width: Int,
                 height: Int,
                 boardLines: Array[String],
                 startX: Int,
                 startY: Int,
                 targetX: Int,
                 targetY: Int,
                 switchCount: Int,
                 switchLines: Array[String]): Unit = {
    bender.updateLocation(Location(startY, startX))
    fry.updateLocation(Location(targetY, targetX))

    switchLines
      .foreach(s => {
        val Array(switchX, switchY, fieldX, fieldY, initialState) = s
          .split(" ")

        val switchLocation = Location(switchY.toInt, switchX.toInt)
        val fieldLocation  = Location(fieldY.toInt, fieldX.toInt)

        val field = Field(fieldLocation, switchLocation, initialState == "1")

        fields(field.location) = field
        switches(switchLocation) = Switch(switchLocation, field)
      })

    maze.updateMaze(Bounds(0, 0, width, height), boardLines)
  }

  def calculatePath(): String = {
    val path = maze.getPath(bender.location, fry.location)

    var pathString = pathToString(path)

    if (comprise) {
      pathString = comprisePath(pathString)
    } else {
      pathString = pathString.replaceAll("\\[[RLUD]*\\]", "")
    }
    pathString
  }

  private def pathToString(path: List[PathNode]): String = {
    maze.mazeNeighbors.clear()
    maze.mazeLines = maze.originalMazeLines
    path
      .map(node => {
        if (node.path.nonEmpty) {
          node.path.tail.zipWithIndex
            .map {
              case (l, i) =>
                val direction          = node.path(i).directionTo(l)
                val optionalDirections = List("U", "D", "R", "L").filter(d => !maze.getExpectedNeighbors(node.path(i)).exists(_.direction == d))
                if (optionalDirections.nonEmpty)
                  s"${optionalDirections.mkString("[", "", "]")}$direction"
                else
                  direction
            }
            .mkString("")
        } else {
          ""
        }
      })
      .mkString("")
  }

  def comprisePath(optionsPath: String, useOptions: Boolean = true): String = {
    var path      = optionsPath
    val cleanPath = optionsPath.replaceAll("\\[[RLUD]*\\]", "")
    if (!useOptions) {
      path = cleanPath
    }
    var comprisedPaths: Seq[(String, String)] = Seq((path, cleanPath))

    var keepCompressing = true
    while (keepCompressing) {
      keepCompressing = false
      comprisedPaths = comprisedPaths
        .flatMap {
          case currentPath @ (comprisedPath, cleanPath) =>
            val appliedF = comprisedPath.count(_ == ';')
            if (appliedF < 9) {
              var maxLimit        = cleanPath.length / 2
              var reachedMaxLimit = false
              val paths = (1 until maxLimit)
                .flatMap(size => {
                  if (!reachedMaxLimit) {
                    val paths = cleanPath
                      .sliding(size)
                      .toSeq
                      .groupBy(identity)
                      .collect {
                        case (function, count) if count.length > 1 =>
                          var str            = comprisedPath
                          val functionString = (appliedF + 1).toString
                          (0 to function.length).foreach(s => {
                            val parts = function.splitAt(s)
                            if (parts._1.nonEmpty && parts._2.nonEmpty) {
                              val regex1 = s"(?<!\\[)${parts._1.mkString("(\\[[RLUD]*\\])?")}(?<!\\])(?<g>\\[[RLUD]*?${parts._2}[RLUD]*?\\])"
                              val regex2 = s"(?<g>\\[[RLUD]*?${parts._1}[RLUD]*?\\])(?<!\\[)${parts._2.mkString("(\\[[RLUD]*\\])?")}(?<!\\])"
                              str = str
                                .replaceAll(regex1, s"$functionString$${g}")
                                .replaceAll(regex2, s"$${g}$functionString")
                            } else if (parts._1.nonEmpty) {
                              val regex = s"(?<!\\[)${parts._1.mkString("(\\[[RLUD]*\\])?")}(?<!\\])"
                              str = str
                                .replaceAll(regex, s"$functionString")
                            } else if (parts._2.nonEmpty) {
                              val regex = s"(?<!\\[)${parts._2.mkString("(\\[[RLUD]*\\])?")}(?<!\\])"
                              str = str
                                .replaceAll(regex, s"$functionString")
                            }
                          })
                          var cleanStr    = str.replaceAll("\\[[RLUD]*]", "")
                          val repetitions = cleanStr.split(functionString, -1).length - 1
                          if (repetitions > 2 && cleanStr.endsWith(functionString.repeat(repetitions))) {
                            str = s"${str.split(functionString, 2).head}$functionString;$function$functionString"
                            cleanStr = s"${cleanStr.split(functionString, 2).head}$functionString;$function$functionString"
                          } else {
                            str = s"$str;$function"
                            cleanStr = s"$cleanStr;$function"
                          }
                          if (cleanStr.length < cleanPath.length)
                            (str, cleanStr)
                          else
                            currentPath
                      }
                    if (paths.isEmpty) {
                      reachedMaxLimit = true
                      if (size < maxLimit)
                        maxLimit = size
                    }
                    paths
                  } else Iterator.empty[(String, String)]
                })
                .toSet
              if (paths.size == 1) {
                paths
              } else {
                keepCompressing = true
                paths.removedAll(Seq((comprisedPath, cleanPath)))
              }
            } else {
              Iterator((comprisedPath, cleanPath))
            }
        }
        .sortBy(_._2.length)
        .take(10)
    }

    comprisedPaths
      .minBy(_._2.length)
      ._2
  }
}

object Player extends App {
  private val Array(width, height)    = readLine split " "
  private val boardLines              = (0 until height.toInt).map(_ => readLine).toArray
  private val Array(startX, startY)   = readLine split " "
  private val Array(targetX, targetY) = readLine split " "
  private val switchCount             = readLine
  private val switchLines             = (0 until switchCount.toInt).map(_ => readLine).toArray

  val world: World = new World()
  world.comprise = true
  world.useGarbage = true
  world.initialize(width.toInt, height.toInt, boardLines, startX.toInt, startY.toInt, targetX.toInt, targetY.toInt, switchCount.toInt, switchLines)

  val inputStr =
    List(List(width, height), boardLines.toList, List(startX, startY), List(targetX, targetY), List(switchCount), switchLines.toList)

  val path = world.calculatePath()

  Console.err.println(Utils.turnString(inputStr, path))

  println(path) // Rick's next move (UP DOWN LEFT or RIGHT).
}
