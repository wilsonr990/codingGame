package puzzle.theLabyrinth

import scala.collection.immutable.Queue
import scala.collection.mutable
import scala.io.StdIn._

object Utils {
  var timeId = 0;
  val debug  = false;

  def createMoleculesMap(strings: String*): Map[Char, Int] = {
    strings.map(_.toInt).zipWithIndex.map({ case (v, i) => (('A' + i).toChar, v) }).toMap
  }

  def timed[T](n: String, f: () => T): T = {
    if (debug) {
      val start = System.nanoTime()
      val ret   = f()
      val end   = System.nanoTime()
      val us    = (end - start) / 1000
      if (us != 0) Console.err.println(s"${timeId} Time $n: $us us")
      ret
    } else
      f()
  }

  def turnString(inputStr: List[List[String]]): String = {
    def listToString(list: List[Any]): String = if (list.isEmpty) "List()" else list.mkString("List(\"", "\", \"", "\")")

    def listOfListsToString(list: List[List[Any]]): String = if (list.isEmpty) "List()" else list.map(listToString).mkString("List(", ", ", ")")

    "testGameSituation(" + listOfListsToString(inputStr) + ", \"" + "\")"
  }

  def turnString(inputStr: List[List[String]], action: String): String = {
    def listToString(list: List[Any]): String = if (list.isEmpty) "List()" else list.mkString("List(\"", "\", \"", "\")")

    def listOfListsToString(list: List[List[Any]]): String = if (list.isEmpty) "List()" else list.map(listToString).mkString("List(", ", ", ")")

    "testGameSituation(" + listOfListsToString(inputStr) + ", \"" + action + "\")"
  }
}

// Exceptions
sealed class CustomException(message: String = "")          extends Exception(message)
final case class InvalidDataException(message: String = "") extends CustomException(message)

case class Bounds(left: Int, top: Int, right: Int, bottom: Int)
case class Location(r: Int, c: Int) {
  def right(offset: Int = 1)(implicit bounds: Bounds): Location = {
    val newC = c + offset
    Location(r, if (newC > bounds.right) bounds.right else newC)
  }
  def left(offset: Int = 1)(implicit bounds: Bounds): Location = {
    val newC = c - offset
    Location(r, if (newC < bounds.left) bounds.left else newC)
  }
  def top(offset: Int = 1)(implicit bounds: Bounds): Location = {
    val newR = r - offset
    Location(if (newR < bounds.top) bounds.top else newR, c)
  }
  def bottom(offset: Int = 1)(implicit bounds: Bounds): Location = {
    val newR = r + offset
    Location(if (newR > bounds.bottom) bounds.bottom else newR, c)
  }

  def directionTo(newL: Location): String = {
    if (newL.c > c) "RIGHT"
    else if (newL.r > r) "DOWN"
    else if (newL.c < c) "LEFT"
    else if (newL.r < r) "UP"
    else throw InvalidDataException("Already in Location")
  }
}

object Destination extends Enumeration {
  val ControlRoom, ClosestUnknown, StartCell = Value
}

case class Rick(world: World) {
  var location: Location             = _
  var destination: Destination.Value = Destination.ControlRoom
  def updateLocation(location: Location, mazeLines: List[String]): Unit = {
    this.location = location
    if (mazeLines(location.r)(location.c) == 'C')
      destination = Destination.StartCell
  }

  def nextAction(): String = {
    location.directionTo(world.maze.desiredPath(1))
  }
}

sealed class MazeCell(val walkable: Boolean) {
  var neighbors: List[Location] = List()
  var rootDistance: Int         = 0
}
case class WallCell()    extends MazeCell(false)
case class StartCell()   extends MazeCell(true)
case class NormalCell()  extends MazeCell(true)
case class ControlCell() extends MazeCell(true)
case class UnknownCell() extends MazeCell(false)

case class Maze(world: World) {
  var cells: Map[Location, MazeCell]     = Map()
  var destination: Location              = _
  var startLocation: Location            = _
  var controlLocation: Location          = _
  var destinationType: Destination.Value = Destination.ControlRoom
  var desiredPath: List[Location]        = List()

  def reset(): Unit = {
    destination = null
    startLocation = null
    controlLocation = null
    desiredPath = List()
    cells = Map()
  }

  def calculateDestinationType(mazeLines: List[String]): Destination.Value = {
    if (world.rick.destination == Destination.ControlRoom) {
      if (mazeLines.exists(s => s.contains('C')))
        Destination.ControlRoom
      else
        Destination.ClosestUnknown
    } else if (world.rick.destination == Destination.StartCell) {
      Destination.StartCell
    } else
      Destination.ClosestUnknown
  }
  def updateMaze(mazeLines: List[String]): Unit = {
    reset()

    cells = cells.updated(world.rick.location, NormalCell())

    destinationType = calculateDestinationType(mazeLines)

    calculateDistancesToDestination(world.rick.location, mazeLines)

    desiredPath = calculatePath(destination)
  }

  def reachedFixedDestination(): Boolean =
    destination != null &&
      ((destinationType == Destination.StartCell && cells(destination).isInstanceOf[StartCell]) ||
        (destinationType == Destination.ControlRoom && cells(destination).isInstanceOf[ControlCell]))

  private def calculateDistancesToDestination(cellLocation: Location, mazeLines: List[String]): Unit = {
    val toVisit: mutable.Queue[Location] = mutable.Queue.empty
    toVisit.enqueue(cellLocation)

    while (toVisit.nonEmpty) {
      val currentLocation: Location = toVisit.dequeue()
      implicit val bounds: Bounds   = world.bounds
      updateNeighbor(currentLocation, mazeLines, Location(currentLocation.r, currentLocation.left().c), toVisit)
      updateNeighbor(currentLocation, mazeLines, Location(currentLocation.bottom().r, currentLocation.c), toVisit)
      updateNeighbor(currentLocation, mazeLines, Location(currentLocation.r, currentLocation.right().c), toVisit)
      updateNeighbor(currentLocation, mazeLines, Location(currentLocation.top().r, currentLocation.c), toVisit)
    }
  }

  private def updateNeighbor(currentLocation: Location, mazeLines: List[String], neighbor: Location, toVisit: mutable.Queue[Location]): Unit = {
    if (!cells(currentLocation).neighbors.contains(neighbor))
      cells = cells.updatedWith(currentLocation) { cellOpt =>
        cellOpt.map((cell: MazeCell) => {
          cell.neighbors ::= neighbor
          cell
        })
      }
    if (!cells.contains(neighbor)) {
      if (mazeLines(neighbor.r)(neighbor.c) == 'C') {
        val cell = ControlCell()
        cell.rootDistance = cells(currentLocation).rootDistance + 1
        cells = cells.updated(neighbor, cell)
        controlLocation = neighbor
        toVisit.enqueue(neighbor)
        if (destinationType == Destination.ControlRoom && startLocation != null && (cell.rootDistance + cells(startLocation).rootDistance) <= world.alarm_rounds) {
          destination = neighbor
        }
      } else if (mazeLines(neighbor.r)(neighbor.c) == 'T') {
        val cell = StartCell()
        cell.rootDistance = cells(currentLocation).rootDistance + 1
        startLocation = neighbor
        cells = cells.updated(neighbor, cell)
        toVisit.enqueue(neighbor)
        if (destinationType == Destination.StartCell) {
          destination = neighbor
        } else if (destinationType == Destination.ControlRoom && controlLocation != null && (cell.rootDistance + cells(controlLocation).rootDistance) <= world.alarm_rounds) {
          destination = controlLocation
        }
      } else if (mazeLines(neighbor.r)(neighbor.c) == '?') {
        val cell = UnknownCell()
        cells = cells.updated(neighbor, cell)
        if (!reachedFixedDestination() && (destination == null || cells(currentLocation).rootDistance < cells(destination).rootDistance))
          destination = currentLocation
      } else if (mazeLines(neighbor.r)(neighbor.c) == '#') {
        val cell = WallCell()
        cells = cells.updated(neighbor, cell)
      } else {
        val cell = NormalCell()
        toVisit.enqueue(neighbor)
        cell.rootDistance = cells(currentLocation).rootDistance + 1
        cells = cells.updated(neighbor, cell)
      }
    }
  }

  private def calculatePath(location: Location): List[Location] = {
    val currentCell = cells(location)
    if (currentCell.rootDistance != 0) {
      calculatePath(
        currentCell.neighbors
          .filter(l => cells(l).walkable)
          .filter(l => cells(l).rootDistance == currentCell.rootDistance - 1)
          .head) ::: List(location)
    } else List(location)
  }
}

class World() {
  var bounds: Bounds    = _
  var alarm_rounds: Int = _
  val rick: Rick        = Rick(this)
  val maze: Maze        = Maze(this)

  def initialize(rows: Int, cols: Int, alarm_rounds: Int): Unit = {
    this.bounds = Bounds(0, 0, cols, rows)
    this.alarm_rounds = alarm_rounds
  }
  def update(location: Location, mazeList: List[String]): Unit = {
    rick.updateLocation(location, mazeList)
    if (rick.destination == Destination.StartCell)
      alarm_rounds -= 1
    maze.updateMaze(mazeList)
  }
}

object Player extends App {
  val Array(rows, cols, alarm_rounds) = (readLine split " ").filter(_ != "")

  val world: World = new World()
  world.initialize(rows.toInt, cols.toInt, alarm_rounds.toInt)

  // game loop
  while (true) {
    // Rick location r,c
    val Array(r, c) = (readLine split " ").filter(_ != "")
    // Maze lines
    val mazeLines = (0 until rows.toInt).map(_ => readLine).toList
    val inputStr  = List(List(rows, cols, alarm_rounds), List(r, c), mazeLines)

    world.update(Location(r.toInt, c.toInt), mazeLines)

    val action = world.rick.nextAction()

    Console.err.println(Utils.turnString(inputStr), action)

    println(action) // Rick's next move (UP DOWN LEFT or RIGHT).
  }
}
