import unittest
import numpy as np
from python.bot_programming.SummerChallenge2024withFiverrOlymbits.main import HurdleRace, Archery, RollerSpeedSkating, \
    Diving, Game


class Test(unittest.TestCase):

    def test_comparison(self):
        game1 = HurdleRace(".....", 0, np.array([0, 1, 2]), np.array([0, 0, 0]))
        game2 = HurdleRace(".....", 0, np.array([0, 1, 2]), np.array([0, 0, 0]))
        assert (game1 == game2)

        game1 = Archery("12345", 0, np.array([[0, 3], [0, 1], [0, 1]]))
        game2 = Archery("12345", 0, np.array([[0, 3], [0, 1], [0, 1]]))
        assert (game1 == game2)

        game1 = RollerSpeedSkating("UDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]), 3)
        game2 = RollerSpeedSkating("UDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]), 3)
        assert (game1 == game2)

        game1 = Diving("UDDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]))
        game2 = Diving("UDDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]))
        assert (game1 == game2)

        minigame1 = Diving("UDDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]))
        minigame2 = RollerSpeedSkating("UDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]), 3)
        game1 = Game(0, 0, [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]], [minigame1, minigame2])

        minigame1 = Diving("UDDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]))
        minigame2 = RollerSpeedSkating("UDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]), 3)
        game2 = Game(0, 0, [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]], [minigame1, minigame2])
        assert (game1 == game2)

    def test_best_paths_len(self):
        best_path = HurdleRace(".#", 0, np.array([0, 1, 2]), np.array([0, 0, 0])).best_path()
        assert best_path == "U"
        best_path_len = HurdleRace(".#", 0, np.array([0, 1, 2]), np.array([0, 0, 0])).best_path_len()
        assert best_path_len == 1
        best_path_len = HurdleRace("...", 0, np.array([0, 1, 2]), np.array([0, 0, 0])).best_path_len()
        assert best_path_len == 1
        best_path_len = HurdleRace("....", 0, np.array([0, 1, 2]), np.array([0, 0, 0])).best_path_len()
        assert best_path_len == 2
        best_path_len = HurdleRace(".......#......#.#...#...", 0, np.array([0, 0, 0]),
                                   np.array([0, 0, 0])).best_path_len()
        assert best_path_len == 10
        best_path_len = HurdleRace(".......#......#.#...#...", 0, np.array([0, 0, 0]),
                                   np.array([3, 0, 0])).best_path_len()
        assert best_path_len == 13

        best_path_len = Archery("12345", 0, np.array([[0, 3], [0, 1], [0, 1]])).best_path_len()
        assert best_path_len == 5

        best_path_len = RollerSpeedSkating("UDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]), 3).best_path_len()
        assert best_path_len == 3

        best_path_len = RollerSpeedSkating("UDLR", 0, np.array([0, 3, 3]), np.array([-2, 0, 0]), 3).best_path_len()
        assert best_path_len == 3

        best_path_len = Diving("UDDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0])).best_path_len()
        assert best_path_len == 5

        minigame1 = HurdleRace(".....", 0, np.array([0, 1, 2]), np.array([0, 0, 0]))
        minigame2 = Archery("12345", 0, np.array([[0, 3], [0, 1], [0, 1]]))
        minigame3 = RollerSpeedSkating("UDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]), 3)
        minigame4 = Diving("UDDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]))
        best_path_len = Game(0, 0, [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                             [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                             [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                             [minigame1, minigame2, minigame3, minigame4]).best_path_len()
        assert best_path_len == 5

    def test_weights(self):
        weights = HurdleRace("GAME_OVER", 0, np.array([0, 0, 0]), np.array([0, 0, 0])).weights(True)
        assert weights == {'U': 1, 'D': 1, 'L': 1, 'R': 1}
        weights = HurdleRace(".#...", 0, np.array([0, 0, 0]), np.array([0, 0, 0])).weights(True)
        assert weights == {'U': 1, 'D': 0, 'L': 0, 'R': 0}
        weights = HurdleRace(".#...", 0, np.array([0, 0, 0]), np.array([0, 3, 3])).weights(True)
        assert weights == {'U': 1, 'D': 0, 'L': 0, 'R': 0}
        weights = HurdleRace(".#...", 0, np.array([0, 0, 0]), np.array([0, 0, 3])).weights(True)
        assert weights == {'U': 1, 'D': 0, 'L': 0, 'R': 0}
        weights = HurdleRace("...", 0, np.array([0, 1, 2]), np.array([0, 0, 0])).weights(True)
        assert weights == {'U': 0, 'D': 0, 'L': 0, 'R': 1}
        weights = HurdleRace("....", 0, np.array([0, 1, 2]), np.array([0, 0, 0])).weights(True)
        assert weights == {'U': 0, 'D': 0, 'L': 0, 'R': 1}
        weights = HurdleRace(".....", 0, np.array([0, 1, 2]), np.array([0, 0, 0])).weights(True)
        assert weights == {'U': 0, 'D': 0, 'L': 0, 'R': 1}
        weights = HurdleRace("......", 0, np.array([0, 1, 2]), np.array([0, 0, 0])).weights(True)
        assert weights == {'U': 0, 'D': 0, 'L': 0, 'R': 1}
        weights = HurdleRace("......", 0, np.array([1, 0, 0]), np.array([0, 0, 0])).weights(True)
        assert weights == {'U': 0, 'D': 0, 'L': 0, 'R': 1}
        weights = HurdleRace("......", 0, np.array([3, 0, 0]), np.array([0, 0, 0])).weights(True)
        assert weights == {'U': 1, 'D': 1, 'L': 1, 'R': 1}

        weights = Archery("GAME_OVER", 0, np.array([[0, 3], [0, 1], [0, 1]])).weights(True)
        assert weights == {'U': 1, 'D': 1, 'L': 1, 'R': 1}
        weights = Archery("12345", 0, np.array([[0, 1], [0, 1], [0, 1]])).weights(True)
        assert weights == {'U': 0.25, 'D': 0.25, 'L': 0.25, 'R': 0.25}
        weights = Archery("12345", 0, np.array([[1, 2], [0, 1], [0, 1]])).weights(True)
        assert weights == {'U': 0.5, 'D': 0, 'L': 0.5, 'R': 0}
        weights = Archery("12345", 0, np.array([[1, 1], [1, 1], [1, 1]])).weights(True)
        assert weights == {'U': 0.25, 'D': 0.25, 'L': 0.25, 'R': 0.25}
        weights = Archery("1", 0, np.array([[0, 2], [0, 0], [0, 0]])).weights(True)
        assert weights == {'U': 1, 'D': 0, 'L': 0, 'R': 0}
        weights = Archery("131122113739313", 0, np.array([[0, 2], [0, 0], [0, 0]])).weights(True)
        assert weights == {'U': 1, 'D': 0, 'L': 0, 'R': 0}

        weights = RollerSpeedSkating("GAME_OVER", 0, np.array([0, 0, 0]), np.array([0, 0, 0]), 3).weights(True)
        assert weights == {'U': 1, 'D': 1, 'L': 1, 'R': 1}
        weights = RollerSpeedSkating("UDLR", 0, np.array([0, 0, 0]), np.array([0, 0, 0]), 3).weights(True)
        assert weights == {'D': 0, 'L': 0, 'R': 1, 'U': 0}
        weights = RollerSpeedSkating("UDLR", 0, np.array([0, 1, 0]), np.array([0, 0, 0]), 3).weights(True)
        assert weights == {'D': 0, 'L': 0, 'R': 1, 'U': 0}
        weights = RollerSpeedSkating("UDLR", 0, np.array([0, 0, 0]), np.array([3, 0, 0]), 3).weights(True)
        assert weights == {'D': 0, 'L': 0, 'R': 0, 'U': 1}
        weights = RollerSpeedSkating("UDLR", 0, np.array([1, 0, 0]), np.array([3, 0, 0]), 3).weights(True)
        assert weights == {'D': 0, 'L': 0, 'R': 0, 'U': 1}

        weights = Diving("GAME_OVER", 0, np.array([0, 0, 0]), np.array([0, 0, 0])).weights(True)
        assert weights == {'U': 1, 'D': 1, 'L': 1, 'R': 1}
        weights = Diving("UDDLR", 0, np.array([0, 0, 0]), np.array([0, 0, 0])).weights(True)
        assert weights == {'U': 1, 'D': 0, 'L': 0, 'R': 0}
        weights = Diving("UDDLR", 0, np.array([5, 0, 0]), np.array([0, 0, 0])).weights(True)
        assert weights == {'U': 1, 'D': 1, 'L': 1, 'R': 1}
        weights = Diving("UDDLR", 0, np.array([5, 0, 0]), np.array([0, 1, 0])).weights(True)
        assert weights == {'U': 1, 'D': 0, 'L': 0, 'R': 0}
        weights = Diving("DRRUDLRUDRDULU", 0, np.array([0, 0, 0]), np.array([0, 0, 0])).weights(True)
        assert weights == {'U': 0, 'D': 1, 'L': 0, 'R': 0}

        minigame1 = HurdleRace(".#...", 0, np.array([0, 0, 0]), np.array([0, 0, 0]))
        minigame2 = Archery("12345", 0, np.array([[0, 3], [0, 1], [0, 1]]))
        minigame3 = RollerSpeedSkating("UDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]), 3)
        minigame4 = Diving("UDDLR", 0, np.array([5, 0, 0]), np.array([0, 1, 0]))
        weights = Game(0, 0, [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                       [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                       [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                       [minigame1, minigame2, minigame3, minigame4]).weights()
        assert weights == {'D': 0.0, 'L': 0.0, 'R': 0.8019999999999998, 'U': 1.2059999999999993}

    def test_next_actions_score(self):
        score = HurdleRace(".#..#.....", 0, np.array([0, 0, 0]), np.array([0, 0, 0])).next_actions_score("ULURL")
        assert score == 1.0
        score = HurdleRace(".#..#.....", 0, np.array([0, 0, 0]), np.array([0, 0, 0])).next_actions_score("ULULR")
        assert score == 1.0
        score = HurdleRace(".#..#.....", 0, np.array([0, 0, 0]), np.array([0, 0, 0])).next_actions_score("ULURR")
        assert score == 1.0
        score = HurdleRace(".#..#.....", 0, np.array([0, 0, 0]), np.array([0, 0, 0])).next_actions_score("ULURU")
        assert score == 1.0
        score = HurdleRace(".#..#.....", 0, np.array([0, 0, 0]), np.array([0, 0, 0])).next_actions_score("ULUUR")
        assert score == 1.0
        score = HurdleRace("..#..#.....", 0, np.array([0, 0, 0]), np.array([0, 0, 0])).next_actions_score("LULURL")
        assert score == 1.0
        score = HurdleRace("...#..#.....", 0, np.array([0, 0, 0]), np.array([0, 0, 0])).next_actions_score("DULURL")
        assert score == 1.0
        score = HurdleRace("...#..#.....", 0, np.array([7, 0, 0]), np.array([0, 0, 0])).next_actions_score("RL")
        assert score == 1.0
        score = HurdleRace("...#..#.....", 0, np.array([7, 0, 0]), np.array([0, 0, 0])).next_actions_score("DD")
        assert score == 1.0
        score = HurdleRace("...#..#.....", 0, np.array([7, 0, 0]), np.array([0, 0, 0])).next_actions_score("DLL")
        assert score == 1.0
        score = HurdleRace("...#..#.....", 0, np.array([7, 0, 0]), np.array([0, 0, 0])).next_actions_score("LLLL")
        assert score == 1.0
        score = HurdleRace("...#..#.....", 0, np.array([7, 5, 0]), np.array([0, 0, 0])).next_actions_score("RL")
        assert score == 1.0
        score = HurdleRace("...#..#.....", 0, np.array([7, 5, 0]), np.array([0, 0, 0])).next_actions_score("DD")
        assert score == 1.0
        score = HurdleRace("...#..#.....", 0, np.array([7, 5, 0]), np.array([0, 0, 0])).next_actions_score("DLL")
        assert score == 1.0
        score = HurdleRace("...#..#.....", 0, np.array([7, 5, 0]), np.array([0, 0, 0])).next_actions_score("LLLL")
        assert score == 0.5

        score = Archery("12345", 0, np.array([[0, 3], [0, 1], [0, 1]])).next_actions_score("ULURL")
        assert score == 0.7515480025000234

        score = RollerSpeedSkating("UDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]), 3).next_actions_score("LLLL")
        assert score == 1

        score = Diving("UDDLR", 0, np.array([0, 0, 0]), np.array([0, 0, 0])).next_actions_score("UDDLR")
        assert score == 1

        score = Diving("UDDLR", 0, np.array([5, 0, 0]), np.array([0, 0, 0])).next_actions_score("LDDLR")
        assert score == 1

        score = Diving("UDDLR", 0, np.array([0, 0, 0]), np.array([2, 0, 0])).next_actions_score("UDDLU")
        assert score == 1

        score = Diving("UDDLR", 0, np.array([0, 0, 0]), np.array([0, 0, 0])).next_actions_score("LDDLR")
        assert score == 0

        minigame1 = HurdleRace(".....", 0, np.array([0, 0, 0]), np.array([0, 0, 0]))
        minigame2 = Archery("12345", 0, np.array([[0, 0], [0, 0], [0, 0]]))
        minigame3 = RollerSpeedSkating("UDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]), 3)
        minigame4 = Diving("UDDLR", 0, np.array([0, 0, 0]), np.array([0, 0, 0]))
        score = Game(0, 0, [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [minigame1, minigame2, minigame3, minigame4]).next_actions_score("UDDLR")
        assert score == 0.6690145570160375

        minigame1 = HurdleRace(".....", 0, np.array([0, 0, 0]), np.array([0, 0, 0]))
        minigame2 = Archery("12345", 0, np.array([[0, 0], [0, 0], [0, 0]]))
        minigame3 = RollerSpeedSkating("UDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]), 3)
        minigame4 = Diving("UDDLR", 0, np.array([0, 0, 0]), np.array([0, 0, 0]))
        score = Game(0, 0, [[1, 1, 1, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [minigame1, minigame2, minigame3, minigame4]).next_actions_score("UDDLR")
        assert score == 0.9426505965037334

        minigame1 = HurdleRace(".....", 0, np.array([0, 0, 0]), np.array([0, 0, 0]))
        minigame2 = Archery("12345", 0, np.array([[0, 0], [0, 0], [0, 0]]))
        minigame3 = RollerSpeedSkating("UDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]), 3)
        minigame4 = Diving("UDDLR", 0, np.array([0, 0, 0]), np.array([0, 0, 0]))
        score = Game(0, 0, [[1, 0, 1, 1], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [minigame1, minigame2, minigame3, minigame4]).next_actions_score("UDDLR")
        assert score == 0.6748377806348267

    def test_minigame_current_players_score(self):
        game1 = HurdleRace(".....", 0, np.array([0, 1, 2]), np.array([0, 0, 0])).get_scores()[0]
        game2 = HurdleRace(".....", 0, np.array([2, 1, 3]), np.array([0, 0, 0])).get_scores()[0]
        game3 = HurdleRace(".....", 0, np.array([3, 1, 3]), np.array([0, 0, 0])).get_scores()[0]
        game4 = HurdleRace(".....", 0, np.array([3, 1, 4]), np.array([0, 0, 0]), finished=True).get_scores()[0]
        game5 = HurdleRace(".....", 0, np.array([4, 1, 4]), np.array([0, 0, 0]), finished=True).get_scores()[0]
        assert 0 <= game1 < game2 < game3 < game4 < game5 <= 3

        game1 = Archery("12345", 0, np.array([[0, 3], [0, 1], [0, 1]])).get_scores()[0]
        game2 = Archery("12345", 0, np.array([[0, 2], [0, 1], [0, 2]])).get_scores()[0]
        game3 = Archery("12345", 0, np.array([[0, 0], [0, 1], [0, 2]])).get_scores()[0]
        game4 = Archery("", 0, np.array([[0, 2], [0, 1], [0, 2]]), finished=True).get_scores()[0]
        game5 = Archery("", 0, np.array([[0, 0], [0, 1], [0, 2]]), finished=True).get_scores()[0]
        assert 0 <= game1 < game2 < game3 < game4 < game5 <= 3

        game1 = RollerSpeedSkating("UDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]), 3).get_scores()[0]
        game2 = RollerSpeedSkating("UDLR", 0, np.array([1, 0, 3]), np.array([0, 0, 0]), 3).get_scores()[0]
        game3 = RollerSpeedSkating("UDLR", 0, np.array([3, 0, 0]), np.array([0, 0, 0]), 3).get_scores()[0]
        game4 = RollerSpeedSkating("UDLR", 0, np.array([1, 0, 3]), np.array([0, 0, 0]), 0, finished=True).get_scores()[
            0]
        game5 = RollerSpeedSkating("UDLR", 0, np.array([3, 0, 0]), np.array([0, 0, 0]), 0, finished=True).get_scores()[
            0]
        assert 0 <= game1 < game2 < game3 < game4 < game5 <= 3

        game1 = Diving("UDDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0])).get_scores()[0]
        game2 = Diving("UDDLR", 0, np.array([1, 0, 3]), np.array([0, 0, 0])).get_scores()[0]
        game3 = Diving("UDDLR", 0, np.array([3, 0, 0]), np.array([0, 0, 0])).get_scores()[0]
        game4 = Diving("", 0, np.array([1, 0, 3]), np.array([0, 0, 0]), finished=True).get_scores()[0]
        game5 = Diving("", 0, np.array([3, 0, 0]), np.array([0, 0, 0]), finished=True).get_scores()[0]
        assert 0 <= game1 < game2 < game3 < game4 < game5 <= 3

        minigame1 = HurdleRace(".....", 0, np.array([0, 1, 2]), np.array([0, 0, 0]))
        minigame2 = Archery("12345", 0, np.array([[0, 3], [0, 1], [0, 1]]))
        minigame3 = Diving("UDDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]))
        minigame4 = RollerSpeedSkating("UDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]), 3)
        game1 = Game(0, 0, [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [minigame1, minigame2, minigame3, minigame4]).get_scores()[0]

        minigame1 = HurdleRace(".....", 0, np.array([0, 1, 2]), np.array([0, 0, 0]))
        minigame2 = Archery("12345", 0, np.array([[0, 3], [0, 1], [0, 1]]))
        minigame3 = Diving("UDDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]))
        minigame4 = RollerSpeedSkating("UDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]), 3)
        game2 = Game(0, 0, [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[1, 1, 1, 5], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [minigame1, minigame2, minigame3, minigame4]).get_scores()[0]

        minigame1 = HurdleRace(".....", 0, np.array([0, 1, 2]), np.array([0, 0, 0]))
        minigame2 = Archery("12345", 0, np.array([[0, 3], [0, 1], [0, 1]]))
        minigame3 = Diving("UDDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]))
        minigame4 = RollerSpeedSkating("UDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]), 3)
        game3 = Game(0, 0, [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[2, 2, 2, 2], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [minigame1, minigame2, minigame3, minigame4]).get_scores()[0]

        minigame1 = HurdleRace(".....", 0, np.array([0, 1, 2]), np.array([0, 0, 0]))
        minigame2 = Archery("12345", 0, np.array([[0, 3], [0, 1], [0, 1]]))
        minigame3 = Diving("UDDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]))
        minigame4 = RollerSpeedSkating("UDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]), 3)
        game4 = Game(0, 0, [[1, 1, 1, 1], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [minigame1, minigame2, minigame3, minigame4]).get_scores()[0]

        minigame1 = HurdleRace(".....", 0, np.array([0, 1, 2]), np.array([0, 0, 0]))
        minigame2 = Archery("12345", 0, np.array([[0, 3], [0, 1], [0, 1]]))
        minigame3 = Diving("UDDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]))
        minigame4 = RollerSpeedSkating("UDLR", 0, np.array([0, 3, 3]), np.array([0, 0, 0]), 3)
        game5 = Game(0, 0, [[2, 2, 2, 2], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [minigame1, minigame2, minigame3, minigame4]).get_scores()[0]
        assert 0 <= game1 < game2 < game3 < game4 < game5

    def test_race_simulation(self):
        game: HurdleRace = (HurdleRace("....#..", 0, np.array([0, 1, 2]), np.array([0, 0, 0]))
                            .simulate(np.array(["U", "D", "R"])))
        assert np.all(game.positions == [2, 3, 4])
        assert np.all(game.stuns == [0, 0, 2])
        assert np.all(game.get_scores() == [0, 0.1, 0.3])

        game: HurdleRace = (game.simulate(np.array(["L", "L", "R"])))
        assert np.all(game.positions == [3, 4, 4])
        assert np.all(game.stuns == [0, 2, 1])
        assert np.all(game.get_scores() == [0, 0.3, 0.3])

        game: HurdleRace = (game.simulate(np.array(["U", "R", "R"])))
        assert np.all(game.positions == [5, 4, 4])
        assert np.all(game.stuns == [0, 1, 0])
        assert np.all(game.get_scores() == [0.3, 0.1, 0.1])

        game: HurdleRace = (game.simulate(np.array(["R", "D", "U"])))
        assert np.all(game.positions == [6, 4, 6])
        assert np.all(game.stuns == [0, 0, 0])
        assert np.all(game.get_scores() == [3, 0, 3])
        assert game.finished

    def test_archery_simulation(self):
        game: Archery = (Archery("12345", 0, np.array([[3, 3], [3, 3], [3, 3]]))
                         .simulate(np.array(["U", "D", "R"])))
        assert np.all(game.positions == [[3, 2], [3, 4], [4, 3]])
        assert game.wind == "2345"
        assert np.all(game.get_scores() == [0.3, 0.1, 0.1])

        game: Archery = (game.simulate(np.array(["U", "L", "U"])))
        assert np.all(game.positions == [[3, 0], [1, 4], [4, 1]])
        assert game.wind == "345"
        assert np.all(game.get_scores() == [0.3, 0.1, 0.1])

        game: Archery = (game.simulate(np.array(["R", "U", "L"])))
        assert np.all(game.positions == [[6, 0], [1, 1], [1, 1]])
        assert game.wind == "45"
        assert np.all(game.get_scores() == [0, 0.3, 0.3])

        game: Archery = (game.simulate(np.array(["L", "U", "U"])))
        assert np.all(game.positions == [[2, 0], [1, -3], [1, -3]])
        assert game.wind == "5"
        assert np.all(game.get_scores() == [0.3, 0.1, 0.1])

        game: Archery = (game.simulate(np.array(["L", "D", "D"])))
        assert np.all(game.positions == [[-3, 0], [1, 2], [1, 2]])
        assert game.wind == ""
        assert np.all(game.get_scores() == [0, 3, 3])
        assert game.finished

    def test_roller_simulation(self):
        game = (RollerSpeedSkating("UDLR", 0, np.array([0, 0, 0]), np.array([0, 0, 0]), 4)
                .simulate(np.array(["U", "D", "R"])))
        assert np.all(game.spaces == [1, 2, 3])
        assert np.all(game.risks == [0, 0, 2])
        assert game.turns_left == 3
        assert np.all(game.get_scores() == [0, 0.1, 0.3])

        game.risk_order = "LRUD"
        game = (game.simulate(np.array(["D", "U", "D"])))
        assert np.all(game.spaces == [4, 4, 6])
        assert np.all(game.risks == [4, 4, 4])
        assert game.turns_left == 2
        assert np.all(game.get_scores() == [0.1, 0.1, 0.3])

        game.risk_order = "UDRL"
        game = (game.simulate(np.array(["D", "L", "U"])))
        assert np.all(game.spaces == [6, 7, 7])
        assert np.all(game.risks == [4, -2, -2])
        assert game.turns_left == 1
        assert np.all(game.get_scores() == [0, 0.3, 0.3])

        game.risk_order = "UDRL"
        game = (game.simulate(np.array(["L", "L", "U"])))
        assert np.all(game.spaces == [9, 7, 7])
        assert np.all(game.risks == [-2, -1, -1])
        assert game.turns_left == 0
        assert game.finished
        assert np.all(game.get_scores() == [3, 1, 1])

    def test_diving_simulation(self):
        game = (Diving("UDDLR", 0, np.array([0, 0, 0]), np.array([0, 0, 0]))
                .simulate(np.array(["U", "D", "R"])))
        assert game.diving_goal == "DDLR"
        assert np.all(game.points == [1, 0, 0])
        assert np.all(game.combos == [1, 0, 0])
        assert np.all(game.get_scores() == [0.3, 0.1, 0.1])

        game = (game.simulate(np.array(["D", "D", "R"])))
        assert game.diving_goal == "DLR"
        assert np.all(game.points == [3, 1, 0])
        assert np.all(game.combos == [2, 1, 0])
        assert np.all(game.get_scores() == [0.3, 0.1, 0])

        game = (game.simulate(np.array(["U", "D", "D"])))
        assert game.diving_goal == "LR"
        assert np.all(game.points == [4, 3, 1])
        assert np.all(game.combos == [1, 2, 1])
        assert np.all(game.get_scores() == [0.3, 0.1, 0])

        game = (game.simulate(np.array(["L", "L", "L"])))
        assert game.diving_goal == "R"
        assert np.all(game.points == [6, 6, 3])
        assert np.all(game.combos == [2, 3, 2])
        assert np.all(game.get_scores() == [0.3, 0.3, 0])

        game = (game.simulate(np.array(["L", "R", "L"])))
        assert game.diving_goal == ""
        assert np.all(game.points == [7, 10, 4])
        assert np.all(game.combos == [1, 4, 1])
        assert game.finished
        assert np.all(game.get_scores() == [1, 3, 0])

    def test_full_game_simulation(self):
        minigame1 = HurdleRace("....#..", 0, np.array([0, 1, 2]), np.array([0, 0, 0]))
        minigame2 = Archery("12345", 0, np.array([[3, 3], [3, 3], [3, 3]]))
        minigame3 = RollerSpeedSkating("UDLR", 0, np.array([0, 0, 0]), np.array([0, 0, 0]), 2)
        minigame4 = Diving("UDDUR", 0, np.array([0, 0, 0]), np.array([0, 0, 0]))
        game = (Game(0, 0, [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [[1, 1, 1, 1], [1, 1, 1, 1], [1, 1, 1, 1]],
                     [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                     [minigame1, minigame2, minigame3, minigame4])
                .simulate(np.array(["L", "R", "L"])))

        minigame = game.mini_games[0]
        assert np.all(minigame.positions == [1, 4, 3])
        assert np.all(minigame.stuns == [0, 2, 0])
        assert np.all(minigame.get_scores() == [0, 0.3, 0.1])

        minigame = game.mini_games[1]
        assert np.all(minigame.positions == [[2, 3], [4, 3], [2, 3]])
        assert minigame.wind == "2345"
        assert np.all(minigame.get_scores() == [0.3, 0, 0.3])

        minigame = game.mini_games[2]
        assert np.all(minigame.spaces == [2, 3, 2])
        assert np.all(minigame.risks == [4, 2, 4])
        assert minigame.turns_left == 1
        assert np.all(minigame.get_scores() == [0.1, 0.3, 0.1])

        minigame = game.mini_games[3]
        assert minigame.diving_goal == "DDUR"
        assert np.all(minigame.points == [0, 0, 0])
        assert np.all(minigame.combos == [0, 0, 0])
        assert np.all(minigame.get_scores() == [0.3, 0.3, 0.3])

        assert game.turn == 1
        assert np.all(game.golds == [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])
        assert np.all(abs(game.get_scores() - [1.859, 2.197, 2.0449]) < 0.001)

        game.mini_games[2].risk_order = "LRUD"
        game = (game.simulate(np.array(["L", "R", "L"])))

        minigame = game.mini_games[0]
        assert np.all(minigame.positions == [2, 4, 4])
        assert np.all(minigame.stuns == [0, 1, 2])
        assert np.all(minigame.get_scores() == [0, 0.3, 0.3])

        minigame = game.mini_games[1]
        assert np.all(minigame.positions == [[0, 3], [6, 3], [0, 3]])
        assert minigame.wind == "345"
        assert np.all(minigame.get_scores() == [0.3, 0, 0.3])

        minigame = game.mini_games[2]
        assert np.all(minigame.spaces == [3, 5, 3])
        assert np.all(minigame.risks == [-2, 2, -2])
        assert minigame.turns_left == 0
        assert minigame.finished
        assert np.all(minigame.get_scores() == [1, 3, 1])

        minigame = game.mini_games[3]
        assert minigame.diving_goal == "DUR"
        assert np.all(minigame.points == [0, 0, 0])
        assert np.all(minigame.combos == [0, 0, 0])
        assert np.all(minigame.get_scores() == [0.3, 0.3, 0.3])

        assert game.turn == 2
        assert np.all(game.golds == [[0, 0, 0, 0], [0, 0, 1, 0], [0, 0, 0, 0]])
        assert np.all(abs(game.get_scores() - [3.38, 6.76, 4.394]) < 0.001)

        game.mini_games[2].risk_order = "LRUD"
        game = (game.simulate(np.array(["L", "L", "L"])))

        minigame = game.mini_games[0]
        assert np.all(minigame.positions == [3, 4, 4])
        assert np.all(minigame.stuns == [0, 0, 1])
        assert np.all(minigame.get_scores() == [0, 0.3, 0.3])

        minigame = game.mini_games[1]
        assert np.all(minigame.positions == [[-3, 3], [3, 3], [-3, 3]])
        assert minigame.wind == "45"
        assert np.all(minigame.get_scores() == [0.3, 0.3, 0.3])

        minigame = game.mini_games[2]
        assert np.all(minigame.spaces == [3, 5, 3])
        assert np.all(minigame.risks == [-2, 2, -2])
        assert minigame.turns_left == 0
        assert minigame.finished
        assert np.all(minigame.get_scores() == [1, 3, 1])

        minigame = game.mini_games[3]
        assert minigame.diving_goal == "UR"
        assert np.all(minigame.points == [0, 0, 0])
        assert np.all(minigame.combos == [0, 0, 0])
        assert np.all(minigame.get_scores() == [0.3, 0.3, 0.3])

        assert game.turn == 3
        assert np.all(game.golds == [[0, 0, 0, 0], [0, 0, 1, 0], [0, 0, 0, 0]])
        assert np.all(abs(game.get_scores() - [3.38, 8.788, 4.394]) < 0.001)

        game.mini_games[2].risk_order = "RDLU"
        game = (game.simulate(np.array(["U", "R", "R"])))

        minigame = game.mini_games[0]
        assert np.all(minigame.positions == [5, 6, 4])
        assert np.all(minigame.stuns == [0, 0, 0])
        assert minigame.finished
        assert np.all(minigame.get_scores() == [1, 3, 0])

        minigame = game.mini_games[1]
        assert np.all(minigame.positions == [[-3, -1], [7, 3], [1, 3]])
        assert minigame.wind == "5"
        assert np.all(minigame.get_scores() == [0.3, 0, 0.3])

        minigame = game.mini_games[2]
        assert np.all(minigame.spaces == [3, 5, 3])
        assert np.all(minigame.risks == [-2, 2, -2])
        assert minigame.turns_left == 0
        assert minigame.finished
        assert np.all(minigame.get_scores() == [1, 3, 1])

        minigame = game.mini_games[3]
        assert minigame.diving_goal == "R"
        assert np.all(minigame.points == [1, 0, 0])
        assert np.all(minigame.combos == [1, 0, 0])
        assert np.all(minigame.get_scores() == [0.3, 0.1, 0.1])

        assert game.turn == 4
        assert np.all(game.golds == [[0, 0, 0, 0], [1, 0, 1, 0], [0, 0, 0, 0]])
        assert np.all(abs(game.get_scores() - [6.76, 17.6, 2.86]) < 0.001)

        game.mini_games[2].risk_order = "RDLU"
        game = (game.simulate(np.array(["R", "L", "U"])))

        minigame = game.mini_games[0]
        assert np.all(minigame.positions == [5, 6, 4])
        assert np.all(minigame.stuns == [0, 0, 0])
        assert np.all(minigame.get_scores() == [1, 3, 0])

        minigame = game.mini_games[1]
        assert np.all(minigame.positions == [[2, -1], [2, 3], [1, -2]])
        assert minigame.wind == ""
        assert minigame.finished
        assert np.all(minigame.get_scores() == [3, 0, 3])

        minigame = game.mini_games[2]
        assert np.all(minigame.spaces == [3, 5, 3])
        assert np.all(minigame.risks == [-2, 2, -2])
        assert minigame.turns_left == 0
        assert minigame.finished
        assert np.all(minigame.get_scores() == [1, 3, 1])

        minigame = game.mini_games[3]
        assert minigame.diving_goal == ""
        assert np.all(minigame.points == [3, 0, 0])
        assert np.all(minigame.combos == [2, 0, 0])
        assert minigame.finished
        assert np.all(minigame.get_scores() == [3, 1, 1])

        assert np.all(game.golds == [[0, 1, 0, 1], [1, 0, 1, 0], [0, 1, 0, 0]])
        assert np.all(abs(game.get_scores() - [64, 32, 16]) < 0.001)
