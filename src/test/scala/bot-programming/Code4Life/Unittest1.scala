package botProgramming.Code4Life

import scala.language.implicitConversions

object Unittest1 extends App {
  var world: World = new World()
  world.opponent.storage = Utils.createMoleculesMap("1", "0", "0", "0", "0")
  world.opponent.samples = List(Sample(2, 1, 1, 'B', 10, Map('A' -> 1, 'B' -> 0, 'C' -> 0)))
  world.availableMolecules = Utils.createMoleculesMap("5", "5", "5", "0", "0")
  var robot = world.robot
  var turns = 0

  // TEST GAME seed=766532801

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("START_POS 0 0 0 0 0 0 0 0 0 0 0 0", "START_POS 0 0 0 0 0 0 0 0 0 0 0 0", "5 5 5 5 5"), List()), "GOTO SAMPLES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 1 0 0 0 0 0 0 0 0 0 0 0", "SAMPLES 1 0 0 0 0 0 0 0 0 0 0 0", "5 5 5 5 5"), List()), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 0 0 0 0 0 0 0 0 0 0 0", "SAMPLES 0 0 0 0 0 0 0 0 0 0 0 0", "5 5 5 5 5"), List()), "CONNECT 1")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 0 0 0 0 0 0 0 0 0 0 0", "SAMPLES 0 0 0 0 0 0 0 0 0 0 0 0", "5 5 5 5 5"), List("0 0 1 0 -1 -1 -1 -1 -1 -1", "1 1 1 0 -1 -1 -1 -1 -1 -1")), "CONNECT 1")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 0 0 0 0 0 0 0 0 0 0 0", "SAMPLES 0 0 0 0 0 0 0 0 0 0 0 0", "5 5 5 5 5"), List("0 0 1 0 -1 -1 -1 -1 -1 -1", "2 0 1 0 -1 -1 -1 -1 -1 -1", "1 1 1 0 -1 -1 -1 -1 -1 -1", "3 1 1 0 -1 -1 -1 -1 -1 -1")), "CONNECT 1")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 0 0 0 0 0 0 0 0 0 0 0", "SAMPLES 0 0 0 0 0 0 0 0 0 0 0 0", "5 5 5 5 5"), List("0 0 1 0 -1 -1 -1 -1 -1 -1", "2 0 1 0 -1 -1 -1 -1 -1 -1", "4 0 1 0 -1 -1 -1 -1 -1 -1", "1 1 1 0 -1 -1 -1 -1 -1 -1", "3 1 1 0 -1 -1 -1 -1 -1 -1", "5 1 1 0 -1 -1 -1 -1 -1 -1")), "GOTO DIAGNOSIS")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 2 0 0 0 0 0 0 0 0 0 0 0", "DIAGNOSIS 2 0 0 0 0 0 0 0 0 0 0 0", "5 5 5 5 5"), List("0 0 1 0 -1 -1 -1 -1 -1 -1", "2 0 1 0 -1 -1 -1 -1 -1 -1", "4 0 1 0 -1 -1 -1 -1 -1 -1", "1 1 1 0 -1 -1 -1 -1 -1 -1", "3 1 1 0 -1 -1 -1 -1 -1 -1", "5 1 1 0 -1 -1 -1 -1 -1 -1")), "COMING")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 1 0 0 0 0 0 0 0 0 0 0 0", "DIAGNOSIS 1 0 0 0 0 0 0 0 0 0 0 0", "5 5 5 5 5"), List("0 0 1 0 -1 -1 -1 -1 -1 -1", "2 0 1 0 -1 -1 -1 -1 -1 -1", "4 0 1 0 -1 -1 -1 -1 -1 -1", "1 1 1 0 -1 -1 -1 -1 -1 -1", "3 1 1 0 -1 -1 -1 -1 -1 -1", "5 1 1 0 -1 -1 -1 -1 -1 -1")), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 0 0 0 0 0 0 0 0 0 0 0", "DIAGNOSIS 0 0 0 0 0 0 0 0 0 0 0 0", "5 5 5 5 5"), List("0 0 1 0 -1 -1 -1 -1 -1 -1", "2 0 1 0 -1 -1 -1 -1 -1 -1", "4 0 1 0 -1 -1 -1 -1 -1 -1", "1 1 1 0 -1 -1 -1 -1 -1 -1", "3 1 1 0 -1 -1 -1 -1 -1 -1", "5 1 1 0 -1 -1 -1 -1 -1 -1")), "CONNECT 0")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 0 0 0 0 0 0 0 0 0 0 0", "DIAGNOSIS 0 0 0 0 0 0 0 0 0 0 0 0", "5 5 5 5 5"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 0 -1 -1 -1 -1 -1 -1", "4 0 1 0 -1 -1 -1 -1 -1 -1", "1 1 1 B 1 1 0 0 0 2", "3 1 1 0 -1 -1 -1 -1 -1 -1", "5 1 1 0 -1 -1 -1 -1 -1 -1")), "CONNECT 2")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 0 0 0 0 0 0 0 0 0 0 0", "DIAGNOSIS 0 0 0 0 0 0 0 0 0 0 0 0", "5 5 5 5 5"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 A 1 0 3 0 0 0", "4 0 1 0 -1 -1 -1 -1 -1 -1", "1 1 1 B 1 1 0 0 0 2", "3 1 1 C 10 0 0 0 0 4", "5 1 1 0 -1 -1 -1 -1 -1 -1")), "CONNECT 4")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 0 0 0 0 0 0 0 0 0 0 0", "DIAGNOSIS 0 0 0 0 0 0 0 0 0 0 0 0", "5 5 5 5 5"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 A 1 0 3 0 0 0", "4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2", "3 1 1 C 10 0 0 0 0 4", "5 1 1 A 1 0 0 0 2 1")), "GOTO MOLECULES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 2 0 0 0 0 0 0 0 0 0 0 0", "MOLECULES 2 0 0 0 0 0 0 0 0 0 0 0", "5 5 5 5 5"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 A 1 0 3 0 0 0", "4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2", "3 1 1 C 10 0 0 0 0 4", "5 1 1 A 1 0 0 0 2 1")), "COMING")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 1 0 0 0 0 0 0 0 0 0 0 0", "MOLECULES 1 0 0 0 0 0 0 0 0 0 0 0", "5 5 5 5 5"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 A 1 0 3 0 0 0", "4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2", "3 1 1 C 10 0 0 0 0 4", "5 1 1 A 1 0 0 0 2 1")), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 0 0 0 0 0 0 0 0 0 0 0", "MOLECULES 0 0 0 0 0 0 0 0 0 0 0 0", "5 5 5 5 5"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 A 1 0 3 0 0 0", "4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2", "3 1 1 C 10 0 0 0 0 4", "5 1 1 A 1 0 0 0 2 1")), "CONNECT C")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 0 0 0 1 0 0 0 0 0 0 0", "MOLECULES 0 0 0 0 0 0 1 0 0 0 0 0", "5 5 4 5 4"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 A 1 0 3 0 0 0", "4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2", "3 1 1 C 10 0 0 0 0 4", "5 1 1 A 1 0 0 0 2 1")), "CONNECT B")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 0 0 1 1 0 0 0 0 0 0 0", "MOLECULES 0 0 0 0 0 0 2 0 0 0 0 0", "5 4 4 5 3"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 A 1 0 3 0 0 0", "4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2", "3 1 1 C 10 0 0 0 0 4", "5 1 1 A 1 0 0 0 2 1")), "CONNECT C")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 0 0 1 2 0 0 0 0 0 0 0", "MOLECULES 0 0 0 0 0 0 3 0 0 0 0 0", "5 4 3 5 2"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 A 1 0 3 0 0 0", "4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2", "3 1 1 C 10 0 0 0 0 4", "5 1 1 A 1 0 0 0 2 1")), "CONNECT D")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 0 0 1 2 1 0 0 0 0 0 0", "MOLECULES 0 0 0 0 0 0 4 0 0 0 0 0", "5 4 3 4 1"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 A 1 0 3 0 0 0", "4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2", "3 1 1 C 10 0 0 0 0 4", "5 1 1 A 1 0 0 0 2 1")), "CONNECT C")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 0 0 1 3 1 0 0 0 0 0 0", "MOLECULES 0 0 0 0 0 0 5 0 0 0 0 0", "5 4 2 4 0"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 A 1 0 3 0 0 0", "4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2", "3 1 1 C 10 0 0 0 0 4", "5 1 1 A 1 0 0 0 2 1")), "CONNECT B")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 0 0 2 3 1 0 0 0 0 0 0", "MOLECULES 0 0 0 0 0 1 5 0 0 0 0 0", "5 3 2 3 0"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 A 1 0 3 0 0 0", "4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2", "3 1 1 C 10 0 0 0 0 4", "5 1 1 A 1 0 0 0 2 1")), "CONNECT B")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 0 0 3 3 1 0 0 0 0 0 0", "MOLECULES 0 0 0 0 0 2 5 0 0 0 0 0", "5 2 2 2 0"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 A 1 0 3 0 0 0", "4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2", "3 1 1 C 10 0 0 0 0 4", "5 1 1 A 1 0 0 0 2 1")), "CONNECT D")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 0 0 3 3 2 0 0 0 0 0 0", "LABORATORY 2 0 0 0 0 2 5 0 0 0 0 0", "5 2 2 1 0"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 A 1 0 3 0 0 0", "4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2", "3 1 1 C 10 0 0 0 0 4", "5 1 1 A 1 0 0 0 2 1")), "CONNECT A")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 0 1 3 3 2 0 0 0 0 0 0", "LABORATORY 1 0 0 0 0 2 5 0 0 0 0 0", "4 2 2 1 0"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 A 1 0 3 0 0 0", "4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2", "3 1 1 C 10 0 0 0 0 4", "5 1 1 A 1 0 0 0 2 1")), "CONNECT D")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 0 1 3 3 3 0 0 0 0 0 0", "LABORATORY 0 0 0 0 0 2 5 0 0 0 0 0", "4 2 2 0 0"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 A 1 0 3 0 0 0", "4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2", "3 1 1 C 10 0 0 0 0 4", "5 1 1 A 1 0 0 0 2 1")), "GOTO LABORATORY")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 2 0 1 3 3 3 0 0 0 0 0 0", "LABORATORY 0 10 0 0 0 2 1 0 0 1 0 0", "4 2 2 0 4"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 A 1 0 3 0 0 0", "4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2", "5 1 1 A 1 0 0 0 2 1")), "COMING")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 1 0 1 3 3 3 0 0 0 0 0 0", "LABORATORY 0 11 0 0 0 0 0 1 0 1 0 0", "4 2 2 2 5"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 A 1 0 3 0 0 0", "4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2")), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 0 1 3 3 3 0 0 0 0 0 0", "SAMPLES 2 11 0 0 0 0 0 1 0 1 0 0", "4 2 2 2 5"), List("0 0 1 B 1 0 1 3 1 0", "2 0 1 A 1 0 3 0 0 0", "4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2")), "CONNECT 0")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 1 1 2 0 2 0 0 1 0 0 0", "SAMPLES 1 11 0 0 0 0 0 1 0 1 0 0", "4 3 5 3 5"), List("2 0 1 A 1 0 3 0 0 0", "4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2")), "CONNECT 2")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 2 1 0 0 2 0 1 1 0 0 0", "SAMPLES 0 11 0 0 0 0 0 1 0 1 0 0", "4 5 5 3 5"), List("4 0 1 D 1 2 0 0 2 0", "1 1 1 B 1 1 0 0 0 2")), "CONNECT 4")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 3 0 0 0 0 0 1 1 0 1 0", "SAMPLES 0 11 0 0 0 0 0 1 0 1 0 0", "5 5 5 5 5"), List("1 1 1 B 1 1 0 0 0 2", "6 1 1 0 -1 -1 -1 -1 -1 -1")), "GOTO SAMPLES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 2 3 0 0 0 0 0 1 1 0 1 0", "SAMPLES 0 11 0 0 0 0 0 1 0 1 0 0", "5 5 5 5 5"), List("1 1 1 B 1 1 0 0 0 2", "6 1 1 0 -1 -1 -1 -1 -1 -1", "7 1 1 0 -1 -1 -1 -1 -1 -1")), "COMING")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 1 3 0 0 0 0 0 1 1 0 1 0", "DIAGNOSIS 2 11 0 0 0 0 0 1 0 1 0 0", "5 5 5 5 5"), List("1 1 1 B 1 1 0 0 0 2", "6 1 1 0 -1 -1 -1 -1 -1 -1", "7 1 1 0 -1 -1 -1 -1 -1 -1")), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 3 0 0 0 0 0 1 1 0 1 0", "DIAGNOSIS 1 11 0 0 0 0 0 1 0 1 0 0", "5 5 5 5 5"), List("1 1 1 B 1 1 0 0 0 2", "6 1 1 0 -1 -1 -1 -1 -1 -1", "7 1 1 0 -1 -1 -1 -1 -1 -1")), "CONNECT 1")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 3 0 0 0 0 0 1 1 0 1 0", "DIAGNOSIS 0 11 0 0 0 0 0 1 0 1 0 0", "5 5 5 5 5"), List("8 0 1 0 -1 -1 -1 -1 -1 -1", "1 1 1 B 1 1 0 0 0 2", "6 1 1 0 -1 -1 -1 -1 -1 -1", "7 1 1 0 -1 -1 -1 -1 -1 -1")), "CONNECT 1")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 3 0 0 0 0 0 1 1 0 1 0", "DIAGNOSIS 0 11 0 0 0 0 0 1 0 1 0 0", "5 5 5 5 5"), List("8 0 1 0 -1 -1 -1 -1 -1 -1", "9 0 1 0 -1 -1 -1 -1 -1 -1", "1 1 1 B 1 1 0 0 0 2", "6 1 1 E 1 0 0 2 1 0", "7 1 1 0 -1 -1 -1 -1 -1 -1")), "CONNECT 1")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 3 0 0 0 0 0 1 1 0 1 0", "DIAGNOSIS 0 11 0 0 0 0 0 1 0 1 0 0", "5 5 5 5 5"), List("8 0 1 0 -1 -1 -1 -1 -1 -1", "9 0 1 0 -1 -1 -1 -1 -1 -1", "10 0 1 0 -1 -1 -1 -1 -1 -1", "1 1 1 B 1 1 0 0 0 2", "6 1 1 E 1 0 0 2 1 0", "7 1 1 A 1 0 1 1 1 1")), "GOTO DIAGNOSIS")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 2 3 0 0 0 0 0 1 1 0 1 0", "MOLECULES 2 11 0 0 0 0 0 1 0 1 0 0", "5 5 5 5 5"), List("8 0 1 0 -1 -1 -1 -1 -1 -1", "9 0 1 0 -1 -1 -1 -1 -1 -1", "10 0 1 0 -1 -1 -1 -1 -1 -1", "1 1 1 B 1 1 0 0 0 2", "6 1 1 E 1 0 0 2 1 0", "7 1 1 A 1 0 1 1 1 1")), "COMING")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 1 3 0 0 0 0 0 1 1 0 1 0", "MOLECULES 1 11 0 0 0 0 0 1 0 1 0 0", "5 5 5 5 5"), List("8 0 1 0 -1 -1 -1 -1 -1 -1", "9 0 1 0 -1 -1 -1 -1 -1 -1", "10 0 1 0 -1 -1 -1 -1 -1 -1", "1 1 1 B 1 1 0 0 0 2", "6 1 1 E 1 0 0 2 1 0", "7 1 1 A 1 0 1 1 1 1")), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 3 0 0 0 0 0 1 1 0 1 0", "MOLECULES 0 11 0 0 0 0 0 1 0 1 0 0", "5 5 5 5 5"), List("8 0 1 0 -1 -1 -1 -1 -1 -1", "9 0 1 0 -1 -1 -1 -1 -1 -1", "10 0 1 0 -1 -1 -1 -1 -1 -1", "1 1 1 B 1 1 0 0 0 2", "6 1 1 E 1 0 0 2 1 0", "7 1 1 A 1 0 1 1 1 1")), "CONNECT 8")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 3 0 0 0 0 0 1 1 0 1 0", "MOLECULES 0 11 0 0 0 0 1 1 0 1 0 0", "5 5 5 5 4"), List("8 0 1 E 1 1 1 1 1 0", "9 0 1 0 -1 -1 -1 -1 -1 -1", "10 0 1 0 -1 -1 -1 -1 -1 -1", "1 1 1 B 1 1 0 0 0 2", "6 1 1 E 1 0 0 2 1 0", "7 1 1 A 1 0 1 1 1 1")), "CONNECT 9")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 3 0 0 0 0 0 1 1 0 1 0", "MOLECULES 0 11 0 0 0 0 2 1 0 1 0 0", "5 5 5 5 3"), List("8 0 1 E 1 1 1 1 1 0", "9 0 1 A 10 0 0 4 0 0", "10 0 1 0 -1 -1 -1 -1 -1 -1", "1 1 1 B 1 1 0 0 0 2", "6 1 1 E 1 0 0 2 1 0", "7 1 1 A 1 0 1 1 1 1")), "CONNECT 10")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 3 0 0 0 0 0 1 1 0 1 0", "MOLECULES 0 11 0 0 1 0 2 1 0 1 0 0", "5 5 4 5 3"), List("8 0 1 E 1 1 1 1 1 0", "9 0 1 A 10 0 0 4 0 0", "10 0 1 D 1 0 2 1 0 0", "1 1 1 B 1 1 0 0 0 2", "6 1 1 E 1 0 0 2 1 0", "7 1 1 A 1 0 1 1 1 1")), "GOTO MOLECULES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 2 3 0 0 0 0 0 1 1 0 1 0", "MOLECULES 0 11 0 0 2 0 2 1 0 1 0 0", "5 5 3 5 3"), List("8 0 1 E 1 1 1 1 1 0", "9 0 1 A 10 0 0 4 0 0", "10 0 1 D 1 0 2 1 0 0", "1 1 1 B 1 1 0 0 0 2", "6 1 1 E 1 0 0 2 1 0", "7 1 1 A 1 0 1 1 1 1")), "COMING")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 1 3 0 0 0 0 0 1 1 0 1 0", "MOLECULES 0 11 0 0 3 0 2 1 0 1 0 0", "5 5 2 5 3"), List("8 0 1 E 1 1 1 1 1 0", "9 0 1 A 10 0 0 4 0 0", "10 0 1 D 1 0 2 1 0 0", "1 1 1 B 1 1 0 0 0 2", "6 1 1 E 1 0 0 2 1 0", "7 1 1 A 1 0 1 1 1 1")), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 3 0 0 0 0 0 1 1 0 1 0", "MOLECULES 0 11 0 0 3 1 2 1 0 1 0 0", "5 5 2 4 3"), List("8 0 1 E 1 1 1 1 1 0", "9 0 1 A 10 0 0 4 0 0", "10 0 1 D 1 0 2 1 0 0", "1 1 1 B 1 1 0 0 0 2", "6 1 1 E 1 0 0 2 1 0", "7 1 1 A 1 0 1 1 1 1")), "CONNECT C")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 3 0 0 1 0 0 1 1 0 1 0", "MOLECULES 0 11 0 0 3 2 2 1 0 1 0 0", "5 5 1 3 3"), List("8 0 1 E 1 1 1 1 1 0", "9 0 1 A 10 0 0 4 0 0", "10 0 1 D 1 0 2 1 0 0", "1 1 1 B 1 1 0 0 0 2", "6 1 1 E 1 0 0 2 1 0", "7 1 1 A 1 0 1 1 1 1")), "CONNECT B")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 3 0 1 1 0 0 1 1 0 1 0", "MOLECULES 0 11 0 1 3 2 2 1 0 1 0 0", "5 3 1 3 3"), List("8 0 1 E 1 1 1 1 1 0", "9 0 1 A 10 0 0 4 0 0", "10 0 1 D 1 0 2 1 0 0", "1 1 1 B 1 1 0 0 0 2", "6 1 1 E 1 0 0 2 1 0", "7 1 1 A 1 0 1 1 1 1")), "CONNECT C")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 3 0 1 2 0 0 1 1 0 1 0", "LABORATORY 2 11 0 1 3 2 2 1 0 1 0 0", "5 3 0 3 3"), List("8 0 1 E 1 1 1 1 1 0", "9 0 1 A 10 0 0 4 0 0", "10 0 1 D 1 0 2 1 0 0", "1 1 1 B 1 1 0 0 0 2", "6 1 1 E 1 0 0 2 1 0", "7 1 1 A 1 0 1 1 1 1")), "GOTO LABORATORY")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 2 3 0 1 2 0 0 1 1 0 1 0", "LABORATORY 1 11 0 1 3 2 2 1 0 1 0 0", "5 3 0 3 3"), List("8 0 1 E 1 1 1 1 1 0", "9 0 1 A 10 0 0 4 0 0", "10 0 1 D 1 0 2 1 0 0", "1 1 1 B 1 1 0 0 0 2", "6 1 1 E 1 0 0 2 1 0", "7 1 1 A 1 0 1 1 1 1")), "COMING")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 1 3 0 1 2 0 0 1 1 0 1 0", "LABORATORY 0 11 0 1 3 2 2 1 0 1 0 0", "5 3 0 3 3"), List("8 0 1 E 1 1 1 1 1 0", "9 0 1 A 10 0 0 4 0 0", "10 0 1 D 1 0 2 1 0 0", "1 1 1 B 1 1 0 0 0 2", "6 1 1 E 1 0 0 2 1 0", "7 1 1 A 1 0 1 1 1 1")), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 3 0 1 2 0 0 1 1 0 1 0", "LABORATORY 0 12 0 1 2 1 2 1 0 1 0 1", "5 3 1 4 3"), List("8 0 1 E 1 1 1 1 1 0", "9 0 1 A 10 0 0 4 0 0", "10 0 1 D 1 0 2 1 0 0", "1 1 1 B 1 1 0 0 0 2", "7 1 1 A 1 0 1 1 1 1")), "CONNECT 8")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 4 0 1 1 0 0 1 1 0 1 1", "LABORATORY 0 13 0 0 2 0 2 2 0 1 0 1", "5 4 2 5 3"), List("9 0 1 A 10 0 0 4 0 0", "10 0 1 D 1 0 2 1 0 0", "1 1 1 B 1 1 0 0 0 2")), "CONNECT 10")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 5 0 0 0 0 0 1 1 0 2 1", "LABORATORY 0 14 0 0 2 0 1 2 1 1 0 1", "5 5 3 5 4"), List("9 0 1 A 10 0 0 4 0 0")), "GOTO SAMPLES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 2 5 0 0 0 0 0 1 1 0 2 1", "SAMPLES 2 14 0 0 2 0 1 2 1 1 0 1", "5 5 3 5 4"), List("9 0 1 A 10 0 0 4 0 0")), "COMING")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 1 5 0 0 0 0 0 1 1 0 2 1", "SAMPLES 1 14 0 0 2 0 1 2 1 1 0 1", "5 5 3 5 4"), List("9 0 1 A 10 0 0 4 0 0")), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 5 0 0 0 0 0 1 1 0 2 1", "SAMPLES 0 14 0 0 2 0 1 2 1 1 0 1", "5 5 3 5 4"), List("9 0 1 A 10 0 0 4 0 0")), "CONNECT 2")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 5 0 0 0 0 0 1 1 0 2 1", "SAMPLES 0 14 0 0 2 0 1 2 1 1 0 1", "5 5 3 5 4"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 0 -1 -1 -1 -1 -1 -1", "12 1 1 0 -1 -1 -1 -1 -1 -1")), "CONNECT 2")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 5 0 0 0 0 0 1 1 0 2 1", "SAMPLES 0 14 0 0 2 0 1 2 1 1 0 1", "5 5 3 5 4"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 0 -1 -1 -1 -1 -1 -1", "13 0 2 0 -1 -1 -1 -1 -1 -1", "12 1 1 0 -1 -1 -1 -1 -1 -1", "14 1 1 0 -1 -1 -1 -1 -1 -1")), "GOTO DIAGNOSIS")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 2 5 0 0 0 0 0 1 1 0 2 1", "SAMPLES 0 14 0 0 2 0 1 2 1 1 0 1", "5 5 3 5 4"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 0 -1 -1 -1 -1 -1 -1", "13 0 2 0 -1 -1 -1 -1 -1 -1", "12 1 1 0 -1 -1 -1 -1 -1 -1", "14 1 1 0 -1 -1 -1 -1 -1 -1", "15 1 1 0 -1 -1 -1 -1 -1 -1")), "COMING")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 1 5 0 0 0 0 0 1 1 0 2 1", "DIAGNOSIS 2 14 0 0 2 0 1 2 1 1 0 1", "5 5 3 5 4"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 0 -1 -1 -1 -1 -1 -1", "13 0 2 0 -1 -1 -1 -1 -1 -1", "12 1 1 0 -1 -1 -1 -1 -1 -1", "14 1 1 0 -1 -1 -1 -1 -1 -1", "15 1 1 0 -1 -1 -1 -1 -1 -1")), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 5 0 0 0 0 0 1 1 0 2 1", "DIAGNOSIS 1 14 0 0 2 0 1 2 1 1 0 1", "5 5 3 5 4"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 0 -1 -1 -1 -1 -1 -1", "13 0 2 0 -1 -1 -1 -1 -1 -1", "12 1 1 0 -1 -1 -1 -1 -1 -1", "14 1 1 0 -1 -1 -1 -1 -1 -1", "15 1 1 0 -1 -1 -1 -1 -1 -1")), "CONNECT 11")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 5 0 0 0 0 0 1 1 0 2 1", "DIAGNOSIS 0 14 0 0 2 0 1 2 1 1 0 1", "5 5 3 5 4"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 D 30 0 0 0 6 0", "13 0 2 0 -1 -1 -1 -1 -1 -1", "12 1 1 0 -1 -1 -1 -1 -1 -1", "14 1 1 0 -1 -1 -1 -1 -1 -1", "15 1 1 0 -1 -1 -1 -1 -1 -1")), "CONNECT 13")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 5 0 0 0 0 0 1 1 0 2 1", "DIAGNOSIS 0 14 0 0 2 0 1 2 1 1 0 1", "5 5 3 5 4"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 D 30 0 0 0 6 0", "13 0 2 A 10 2 3 0 3 0", "12 1 1 B 1 1 0 1 2 1", "14 1 1 0 -1 -1 -1 -1 -1 -1", "15 1 1 0 -1 -1 -1 -1 -1 -1")), "GOTO MOLECULES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 2 5 0 0 0 0 0 1 1 0 2 1", "DIAGNOSIS 0 14 0 0 2 0 1 2 1 1 0 1", "5 5 3 5 4"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 D 30 0 0 0 6 0", "13 0 2 A 10 2 3 0 3 0", "12 1 1 B 1 1 0 1 2 1", "14 1 1 D 1 1 0 0 1 3", "15 1 1 0 -1 -1 -1 -1 -1 -1")), "COMING")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 1 5 0 0 0 0 0 1 1 0 2 1", "DIAGNOSIS 0 14 0 0 2 0 1 2 1 1 0 1", "5 5 3 5 4"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 D 30 0 0 0 6 0", "13 0 2 A 10 2 3 0 3 0", "12 1 1 B 1 1 0 1 2 1", "14 1 1 D 1 1 0 0 1 3", "15 1 1 A 1 0 2 2 0 1")), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 5 0 0 0 0 0 1 1 0 2 1", "MOLECULES 2 14 0 0 2 0 1 2 1 1 0 1", "5 5 3 5 4"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 D 30 0 0 0 6 0", "13 0 2 A 10 2 3 0 3 0", "12 1 1 B 1 1 0 1 2 1", "14 1 1 D 1 1 0 0 1 3", "15 1 1 A 1 0 2 2 0 1")), "CONNECT D")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 5 0 0 0 1 0 1 1 0 2 1", "MOLECULES 1 14 0 0 2 0 1 2 1 1 0 1", "5 5 3 4 4"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 D 30 0 0 0 6 0", "13 0 2 A 10 2 3 0 3 0", "12 1 1 B 1 1 0 1 2 1", "14 1 1 D 1 1 0 0 1 3", "15 1 1 A 1 0 2 2 0 1")), "CONNECT D")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 5 0 0 0 2 0 1 1 0 2 1", "MOLECULES 0 14 0 0 2 0 1 2 1 1 0 1", "5 5 3 3 4"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 D 30 0 0 0 6 0", "13 0 2 A 10 2 3 0 3 0", "12 1 1 B 1 1 0 1 2 1", "14 1 1 D 1 1 0 0 1 3", "15 1 1 A 1 0 2 2 0 1")), "CONNECT D")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 5 0 0 0 3 0 1 1 0 2 1", "MOLECULES 0 14 0 0 3 0 1 2 1 1 0 1", "5 5 2 2 4"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 D 30 0 0 0 6 0", "13 0 2 A 10 2 3 0 3 0", "12 1 1 B 1 1 0 1 2 1", "14 1 1 D 1 1 0 0 1 3", "15 1 1 A 1 0 2 2 0 1")), "CONNECT D")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 5 0 0 0 4 0 1 1 0 2 1", "MOLECULES 0 14 0 1 3 0 1 2 1 1 0 1", "5 4 2 1 4"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 D 30 0 0 0 6 0", "13 0 2 A 10 2 3 0 3 0", "12 1 1 B 1 1 0 1 2 1", "14 1 1 D 1 1 0 0 1 3", "15 1 1 A 1 0 2 2 0 1")), "CONNECT B")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 5 0 1 0 4 0 1 1 0 2 1", "MOLECULES 0 14 0 1 3 1 1 2 1 1 0 1", "5 3 2 0 4"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 D 30 0 0 0 6 0", "13 0 2 A 10 2 3 0 3 0", "12 1 1 B 1 1 0 1 2 1", "14 1 1 D 1 1 0 0 1 3", "15 1 1 A 1 0 2 2 0 1")), "CONNECT A")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 5 1 1 0 4 0 1 1 0 2 1", "MOLECULES 0 14 0 1 3 1 2 2 1 1 0 1", "4 3 2 0 3"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 D 30 0 0 0 6 0", "13 0 2 A 10 2 3 0 3 0", "12 1 1 B 1 1 0 1 2 1", "14 1 1 D 1 1 0 0 1 3", "15 1 1 A 1 0 2 2 0 1")), "CONNECT B")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 5 1 2 0 4 0 1 1 0 2 1", "LABORATORY 2 14 0 1 3 1 2 2 1 1 0 1", "4 2 2 0 3"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 D 30 0 0 0 6 0", "13 0 2 A 10 2 3 0 3 0", "12 1 1 B 1 1 0 1 2 1", "14 1 1 D 1 1 0 0 1 3", "15 1 1 A 1 0 2 2 0 1")), "GOTO LABORATORY")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 2 5 1 2 0 4 0 1 1 0 2 1", "LABORATORY 1 14 0 1 3 1 2 2 1 1 0 1", "4 2 2 0 3"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 D 30 0 0 0 6 0", "13 0 2 A 10 2 3 0 3 0", "12 1 1 B 1 1 0 1 2 1", "14 1 1 D 1 1 0 0 1 3", "15 1 1 A 1 0 2 2 0 1")), "COMING")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 1 5 1 2 0 4 0 1 1 0 2 1", "LABORATORY 0 14 0 1 3 1 2 2 1 1 0 1", "4 2 2 0 3"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 D 30 0 0 0 6 0", "13 0 2 A 10 2 3 0 3 0", "12 1 1 B 1 1 0 1 2 1", "14 1 1 D 1 1 0 0 1 3", "15 1 1 A 1 0 2 2 0 1")), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 5 1 2 0 4 0 1 1 0 2 1", "LABORATORY 0 15 0 0 2 1 2 3 1 1 0 1", "4 3 3 0 3"), List("9 0 1 A 10 0 0 4 0 0", "11 0 2 D 30 0 0 0 6 0", "13 0 2 A 10 2 3 0 3 0", "12 1 1 B 1 1 0 1 2 1", "14 1 1 D 1 1 0 0 1 3")), "CONNECT 11")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 35 1 2 0 0 0 1 1 0 3 1", "LABORATORY 0 16 0 0 2 0 0 3 1 1 1 1", "4 3 3 5 5"), List("9 0 1 A 10 0 0 4 0 0", "13 0 2 A 10 2 3 0 3 0", "12 1 1 B 1 1 0 1 2 1")), "CONNECT 13")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 45 0 0 0 0 0 2 1 0 3 1", "SAMPLES 2 16 0 0 2 0 0 3 1 1 1 1", "5 5 3 5 5"), List("9 0 1 A 10 0 0 4 0 0", "12 1 1 B 1 1 0 1 2 1")), "GOTO SAMPLES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 2 45 0 0 0 0 0 2 1 0 3 1", "SAMPLES 1 16 0 0 2 0 0 3 1 1 1 1", "5 5 3 5 5"), List("9 0 1 A 10 0 0 4 0 0", "12 1 1 B 1 1 0 1 2 1")), "CATCH ME!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 1 45 0 0 0 0 0 2 1 0 3 1", "SAMPLES 0 16 0 0 2 0 0 3 1 1 1 1", "5 5 3 5 5"), List("9 0 1 A 10 0 0 4 0 0", "12 1 1 B 1 1 0 1 2 1")), "IF YOU CAN!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 45 0 0 0 0 0 2 1 0 3 1", "SAMPLES 0 16 0 0 2 0 0 3 1 1 1 1", "5 5 3 5 5"), List("9 0 1 A 10 0 0 4 0 0", "12 1 1 B 1 1 0 1 2 1", "16 1 1 0 -1 -1 -1 -1 -1 -1")), "CONNECT 2")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 45 0 0 0 0 0 2 1 0 3 1", "SAMPLES 0 16 0 0 2 0 0 3 1 1 1 1", "5 5 3 5 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 0 -1 -1 -1 -1 -1 -1", "12 1 1 B 1 1 0 1 2 1", "16 1 1 0 -1 -1 -1 -1 -1 -1", "18 1 1 0 -1 -1 -1 -1 -1 -1")), "CONNECT 2")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 45 0 0 0 0 0 2 1 0 3 1", "DIAGNOSIS 2 16 0 0 2 0 0 3 1 1 1 1", "5 5 3 5 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 0 -1 -1 -1 -1 -1 -1", "19 0 2 0 -1 -1 -1 -1 -1 -1", "12 1 1 B 1 1 0 1 2 1", "16 1 1 0 -1 -1 -1 -1 -1 -1", "18 1 1 0 -1 -1 -1 -1 -1 -1")), "GOTO DIAGNOSIS")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 2 45 0 0 0 0 0 2 1 0 3 1", "DIAGNOSIS 1 16 0 0 2 0 0 3 1 1 1 1", "5 5 3 5 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 0 -1 -1 -1 -1 -1 -1", "19 0 2 0 -1 -1 -1 -1 -1 -1", "12 1 1 B 1 1 0 1 2 1", "16 1 1 0 -1 -1 -1 -1 -1 -1", "18 1 1 0 -1 -1 -1 -1 -1 -1")), "CATCH ME!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 1 45 0 0 0 0 0 2 1 0 3 1", "DIAGNOSIS 0 16 0 0 2 0 0 3 1 1 1 1", "5 5 3 5 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 0 -1 -1 -1 -1 -1 -1", "19 0 2 0 -1 -1 -1 -1 -1 -1", "12 1 1 B 1 1 0 1 2 1", "16 1 1 0 -1 -1 -1 -1 -1 -1", "18 1 1 0 -1 -1 -1 -1 -1 -1")), "IF YOU CAN!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 45 0 0 0 0 0 2 1 0 3 1", "DIAGNOSIS 0 16 0 0 2 0 0 3 1 1 1 1", "5 5 3 5 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 0 -1 -1 -1 -1 -1 -1", "19 0 2 0 -1 -1 -1 -1 -1 -1", "12 1 1 B 1 1 0 1 2 1", "16 1 1 B 1 1 0 2 2 0", "18 1 1 0 -1 -1 -1 -1 -1 -1")), "CONNECT 17")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 45 0 0 0 0 0 2 1 0 3 1", "DIAGNOSIS 0 16 0 0 2 0 0 3 1 1 1 1", "5 5 3 5 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 B 20 5 3 0 0 0", "19 0 2 0 -1 -1 -1 -1 -1 -1", "12 1 1 B 1 1 0 1 2 1", "16 1 1 B 1 1 0 2 2 0", "18 1 1 B 1 1 0 1 1 1")), "CONNECT 19")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 45 0 0 0 0 0 2 1 0 3 1", "MOLECULES 2 16 0 0 2 0 0 3 1 1 1 1", "5 5 3 5 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "16 1 1 B 1 1 0 2 2 0", "18 1 1 B 1 1 0 1 1 1")), "GOTO MOLECULES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 2 45 0 0 0 0 0 2 1 0 3 1", "MOLECULES 1 16 0 0 2 0 0 3 1 1 1 1", "5 5 3 5 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "16 1 1 B 1 1 0 2 2 0", "18 1 1 B 1 1 0 1 1 1")), "CATCH ME!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 1 45 0 0 0 0 0 2 1 0 3 1", "MOLECULES 0 16 0 0 2 0 0 3 1 1 1 1", "5 5 3 5 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "16 1 1 B 1 1 0 2 2 0", "18 1 1 B 1 1 0 1 1 1")), "IF YOU CAN!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 45 0 0 0 0 0 2 1 0 3 1", "MOLECULES 0 16 1 0 2 0 0 3 1 1 1 1", "4 5 3 5 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "16 1 1 B 1 1 0 2 2 0", "18 1 1 B 1 1 0 1 1 1")), "CONNECT A")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 45 1 0 0 0 0 2 1 0 3 1", "MOLECULES 0 16 2 0 2 0 0 3 1 1 1 1", "2 5 3 5 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "16 1 1 B 1 1 0 2 2 0", "18 1 1 B 1 1 0 1 1 1")), "CONNECT A")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 45 2 0 0 0 0 2 1 0 3 1", "MOLECULES 0 16 3 0 2 0 0 3 1 1 1 1", "0 5 3 5 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "16 1 1 B 1 1 0 2 2 0", "18 1 1 B 1 1 0 1 1 1")), "CONNECT C")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 45 2 0 1 0 0 2 1 0 3 1", "MOLECULES 0 16 3 0 2 1 0 3 1 1 1 1", "0 5 2 4 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "16 1 1 B 1 1 0 2 2 0", "18 1 1 B 1 1 0 1 1 1")), "CONNECT C")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 45 2 0 2 0 0 2 1 0 3 1", "MOLECULES 0 16 3 0 3 1 0 3 1 1 1 1", "0 5 0 4 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "16 1 1 B 1 1 0 2 2 0", "18 1 1 B 1 1 0 1 1 1")), "GOTO DIAGNOSIS")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 2 45 2 0 2 0 0 2 1 0 3 1", "LABORATORY 2 16 3 0 3 1 0 3 1 1 1 1", "0 5 0 4 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "16 1 1 B 1 1 0 2 2 0", "18 1 1 B 1 1 0 1 1 1")), "CATCH ME!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 1 45 2 0 2 0 0 2 1 0 3 1", "LABORATORY 1 16 3 0 3 1 0 3 1 1 1 1", "0 5 0 4 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "16 1 1 B 1 1 0 2 2 0", "18 1 1 B 1 1 0 1 1 1")), "IF YOU CAN!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 45 2 0 2 0 0 2 1 0 3 1", "LABORATORY 0 16 3 0 3 1 0 3 1 1 1 1", "0 5 0 4 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "16 1 1 B 1 1 0 2 2 0", "18 1 1 B 1 1 0 1 1 1")), "WAIT")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 45 2 0 2 0 0 2 1 0 3 1", "LABORATORY 0 17 3 0 3 1 0 3 2 1 1 1", "0 5 0 4 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "16 1 1 B 1 1 0 2 2 0")), "WAIT")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 45 2 0 2 0 0 2 1 0 3 1", "LABORATORY 0 18 3 0 2 0 0 3 3 1 1 1", "0 5 1 5 5"), List("9 0 1 A 10 0 0 4 0 0", "17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1")), "CONNECT 9")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 45 2 0 2 0 0 2 1 0 3 1", "SAMPLES 2 18 3 0 2 0 0 3 3 1 1 1", "0 5 1 5 5"), List("17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "9 -1 1 A 10 0 0 4 0 0")), "GOTO MOLECULES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 2 45 2 0 2 0 0 2 1 0 3 1", "SAMPLES 1 18 3 0 2 0 0 3 3 1 1 1", "0 5 1 5 5"), List("17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "9 -1 1 A 10 0 0 4 0 0")), "CATCH ME!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 1 45 2 0 2 0 0 2 1 0 3 1", "SAMPLES 0 18 3 0 2 0 0 3 3 1 1 1", "0 5 1 5 5"), List("17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "9 -1 1 A 10 0 0 4 0 0")), "IF YOU CAN!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 45 2 0 2 0 0 2 1 0 3 1", "SAMPLES 0 18 3 0 2 0 0 3 3 1 1 1", "0 5 1 5 5"), List("17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "20 1 1 0 -1 -1 -1 -1 -1 -1", "9 -1 1 A 10 0 0 4 0 0")), "CONNECT E")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 45 2 0 2 0 1 2 1 0 3 1", "DIAGNOSIS 2 18 3 0 2 0 0 3 3 1 1 1", "0 5 1 5 4"), List("17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "20 1 1 0 -1 -1 -1 -1 -1 -1", "9 -1 1 A 10 0 0 4 0 0")), "CONNECT C")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 45 2 0 3 0 1 2 1 0 3 1", "DIAGNOSIS 1 18 3 0 2 0 0 3 3 1 1 1", "0 5 0 5 4"), List("17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "20 1 1 0 -1 -1 -1 -1 -1 -1", "9 -1 1 A 10 0 0 4 0 0")), "GOTO LABORATORY")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 2 45 2 0 3 0 1 2 1 0 3 1", "DIAGNOSIS 0 18 3 0 2 0 0 3 3 1 1 1", "0 5 0 5 4"), List("17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "20 1 1 0 -1 -1 -1 -1 -1 -1", "9 -1 1 A 10 0 0 4 0 0")), "CATCH ME!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 1 45 2 0 3 0 1 2 1 0 3 1", "DIAGNOSIS 0 18 3 0 2 0 0 3 3 1 1 1", "0 5 0 5 4"), List("17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "20 1 1 E 1 0 0 3 0 0", "9 -1 1 A 10 0 0 4 0 0")), "IF YOU CAN!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 45 2 0 3 0 1 2 1 0 3 1", "MOLECULES 2 18 3 0 2 0 0 3 3 1 1 1", "0 5 0 5 4"), List("17 0 2 B 20 5 3 0 0 0", "19 0 2 E 10 3 0 3 0 2", "12 1 1 B 1 1 0 1 2 1", "20 1 1 E 1 0 0 3 0 0", "9 -1 1 A 10 0 0 4 0 0")), "CONNECT 19")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 55 1 0 0 0 0 2 1 0 3 2", "MOLECULES 1 18 3 0 2 0 0 3 3 1 1 1", "1 5 3 5 5"), List("17 0 2 B 20 5 3 0 0 0", "12 1 1 B 1 1 0 1 2 1", "20 1 1 E 1 0 0 3 0 0", "9 -1 1 A 10 0 0 4 0 0")), "GOTO SAMPLES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 2 55 1 0 0 0 0 2 1 0 3 2", "MOLECULES 0 18 3 0 2 0 0 3 3 1 1 1", "1 5 3 5 5"), List("17 0 2 B 20 5 3 0 0 0", "12 1 1 B 1 1 0 1 2 1", "20 1 1 E 1 0 0 3 0 0", "9 -1 1 A 10 0 0 4 0 0")), "CATCH ME!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 1 55 1 0 0 0 0 2 1 0 3 2", "MOLECULES 0 18 3 0 2 1 0 3 3 1 1 1", "1 5 3 4 5"), List("17 0 2 B 20 5 3 0 0 0", "12 1 1 B 1 1 0 1 2 1", "20 1 1 E 1 0 0 3 0 0", "9 -1 1 A 10 0 0 4 0 0")), "IF YOU CAN!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 55 1 0 0 0 0 2 1 0 3 2", "LABORATORY 2 18 3 0 2 1 0 3 3 1 1 1", "1 5 3 4 5"), List("17 0 2 B 20 5 3 0 0 0", "12 1 1 B 1 1 0 1 2 1", "20 1 1 E 1 0 0 3 0 0", "9 -1 1 A 10 0 0 4 0 0")), "CONNECT 2")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 55 1 0 0 0 0 2 1 0 3 2", "LABORATORY 1 18 3 0 2 1 0 3 3 1 1 1", "1 5 3 4 5"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 0 -1 -1 -1 -1 -1 -1", "12 1 1 B 1 1 0 1 2 1", "20 1 1 E 1 0 0 3 0 0", "9 -1 1 A 10 0 0 4 0 0")), "CONNECT 2")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 55 1 0 0 0 0 2 1 0 3 2", "LABORATORY 0 18 3 0 2 1 0 3 3 1 1 1", "1 5 3 4 5"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 0 -1 -1 -1 -1 -1 -1", "22 0 2 0 -1 -1 -1 -1 -1 -1", "12 1 1 B 1 1 0 1 2 1", "20 1 1 E 1 0 0 3 0 0", "9 -1 1 A 10 0 0 4 0 0")), "GOTO DIAGNOSIS")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 2 55 1 0 0 0 0 2 1 0 3 2", "LABORATORY 0 19 3 0 0 1 0 3 3 1 1 2", "1 5 5 4 5"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 0 -1 -1 -1 -1 -1 -1", "22 0 2 0 -1 -1 -1 -1 -1 -1", "12 1 1 B 1 1 0 1 2 1", "9 -1 1 A 10 0 0 4 0 0")), "CATCH ME!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 1 55 1 0 0 0 0 2 1 0 3 2", "LABORATORY 0 20 3 0 0 0 0 3 4 1 1 2", "1 5 5 5 5"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 0 -1 -1 -1 -1 -1 -1", "22 0 2 0 -1 -1 -1 -1 -1 -1", "9 -1 1 A 10 0 0 4 0 0")), "IF YOU CAN!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 55 1 0 0 0 0 2 1 0 3 2", "SAMPLES 2 20 3 0 0 0 0 3 4 1 1 2", "1 5 5 5 5"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 0 -1 -1 -1 -1 -1 -1", "22 0 2 0 -1 -1 -1 -1 -1 -1", "9 -1 1 A 10 0 0 4 0 0")), "CONNECT 21")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 55 1 0 0 0 0 2 1 0 3 2", "SAMPLES 1 20 3 0 0 0 0 3 4 1 1 2", "1 5 5 5 5"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 B 10 0 2 2 3 0", "22 0 2 0 -1 -1 -1 -1 -1 -1", "9 -1 1 A 10 0 0 4 0 0")), "CONNECT 22")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 55 1 0 0 0 0 2 1 0 3 2", "SAMPLES 0 20 3 0 0 0 0 3 4 1 1 2", "1 5 5 5 5"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 B 10 0 2 2 3 0", "22 0 2 A 10 0 0 3 2 2", "9 -1 1 A 10 0 0 4 0 0")), "GOTO MOLECULES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 2 55 1 0 0 0 0 2 1 0 3 2", "SAMPLES 0 20 3 0 0 0 0 3 4 1 1 2", "1 5 5 5 5"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 B 10 0 2 2 3 0", "22 0 2 A 10 0 0 3 2 2", "23 1 2 0 -1 -1 -1 -1 -1 -1", "9 -1 1 A 10 0 0 4 0 0")), "CATCH ME!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 1 55 1 0 0 0 0 2 1 0 3 2", "SAMPLES 0 20 3 0 0 0 0 3 4 1 1 2", "1 5 5 5 5"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 B 10 0 2 2 3 0", "22 0 2 A 10 0 0 3 2 2", "23 1 2 0 -1 -1 -1 -1 -1 -1", "24 1 2 0 -1 -1 -1 -1 -1 -1", "9 -1 1 A 10 0 0 4 0 0")), "IF YOU CAN!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 55 1 0 0 0 0 2 1 0 3 2", "DIAGNOSIS 2 20 3 0 0 0 0 3 4 1 1 2", "1 5 5 5 5"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 B 10 0 2 2 3 0", "22 0 2 A 10 0 0 3 2 2", "23 1 2 0 -1 -1 -1 -1 -1 -1", "24 1 2 0 -1 -1 -1 -1 -1 -1", "9 -1 1 A 10 0 0 4 0 0")), "CONNECT C")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 55 1 0 1 0 0 2 1 0 3 2", "DIAGNOSIS 1 20 3 0 0 0 0 3 4 1 1 2", "1 5 4 5 5"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 B 10 0 2 2 3 0", "22 0 2 A 10 0 0 3 2 2", "23 1 2 0 -1 -1 -1 -1 -1 -1", "24 1 2 0 -1 -1 -1 -1 -1 -1", "9 -1 1 A 10 0 0 4 0 0")), "CONNECT C")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 55 1 0 2 0 0 2 1 0 3 2", "DIAGNOSIS 0 20 3 0 0 0 0 3 4 1 1 2", "1 5 3 5 5"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 B 10 0 2 2 3 0", "22 0 2 A 10 0 0 3 2 2", "23 1 2 0 -1 -1 -1 -1 -1 -1", "24 1 2 0 -1 -1 -1 -1 -1 -1", "9 -1 1 A 10 0 0 4 0 0")), "CONNECT B")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 55 1 1 2 0 0 2 1 0 3 2", "DIAGNOSIS 0 20 3 0 0 0 0 3 4 1 1 2", "1 4 3 5 5"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 B 10 0 2 2 3 0", "22 0 2 A 10 0 0 3 2 2", "23 1 2 0 -1 -1 -1 -1 -1 -1", "24 1 2 0 -1 -1 -1 -1 -1 -1", "9 1 1 A 10 0 0 4 0 0")), "CONNECT C")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 55 1 1 3 0 0 2 1 0 3 2", "DIAGNOSIS 0 20 3 0 0 0 0 3 4 1 1 2", "1 4 2 5 5"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 B 10 0 2 2 3 0", "22 0 2 A 10 0 0 3 2 2", "23 1 2 A 30 6 0 0 0 0", "24 1 2 0 -1 -1 -1 -1 -1 -1", "9 1 1 A 10 0 0 4 0 0")), "CONNECT C")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 55 1 1 4 0 0 2 1 0 3 2", "DIAGNOSIS 0 20 3 0 0 0 0 3 4 1 1 2", "1 4 1 5 5"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 B 10 0 2 2 3 0", "22 0 2 A 10 0 0 3 2 2", "23 1 2 A 30 6 0 0 0 0", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0")), "CONNECT E")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 55 1 1 4 0 1 2 1 0 3 2", "MOLECULES 2 20 3 0 0 0 0 3 4 1 1 2", "1 4 1 5 4"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 B 10 0 2 2 3 0", "22 0 2 A 10 0 0 3 2 2", "23 1 2 A 30 6 0 0 0 0", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0")), "CONNECT E")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 55 1 1 4 0 2 2 1 0 3 2", "MOLECULES 1 20 3 0 0 0 0 3 4 1 1 2", "1 4 1 5 3"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 B 10 0 2 2 3 0", "22 0 2 A 10 0 0 3 2 2", "23 1 2 A 30 6 0 0 0 0", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0")), "CONNECT E")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 55 1 1 4 0 3 2 1 0 3 2", "MOLECULES 0 20 3 0 0 0 0 3 4 1 1 2", "1 4 1 5 2"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 B 10 0 2 2 3 0", "22 0 2 A 10 0 0 3 2 2", "23 1 2 A 30 6 0 0 0 0", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0")), "CONNECT E")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 55 1 1 4 0 4 2 1 0 3 2", "MOLECULES 0 20 3 0 0 0 1 3 4 1 1 2", "1 4 1 5 0"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 B 10 0 2 2 3 0", "22 0 2 A 10 0 0 3 2 2", "23 1 2 A 30 6 0 0 0 0", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0")), "GOTO LABORATORY")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 2 55 1 1 4 0 4 2 1 0 3 2", "LABORATORY 2 20 3 0 0 0 1 3 4 1 1 2", "1 4 1 5 0"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 B 10 0 2 2 3 0", "22 0 2 A 10 0 0 3 2 2", "23 1 2 A 30 6 0 0 0 0", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0")), "CATCH ME!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 1 55 1 1 4 0 4 2 1 0 3 2", "LABORATORY 1 20 3 0 0 0 1 3 4 1 1 2", "1 4 1 5 0"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 B 10 0 2 2 3 0", "22 0 2 A 10 0 0 3 2 2", "23 1 2 A 30 6 0 0 0 0", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0")), "IF YOU CAN!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 55 1 1 4 0 4 2 1 0 3 2", "LABORATORY 0 20 3 0 0 0 1 3 4 1 1 2", "1 4 1 5 0"), List("17 0 2 B 20 5 3 0 0 0", "21 0 2 B 10 0 2 2 3 0", "22 0 2 A 10 0 0 3 2 2", "23 1 2 A 30 6 0 0 0 0", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0")), "CONNECT 21")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 65 1 0 2 0 4 2 2 0 3 2", "LABORATORY 0 50 0 0 0 0 1 4 4 1 1 2", "4 5 3 5 0"), List("17 0 2 B 20 5 3 0 0 0", "22 0 2 A 10 0 0 3 2 2", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0")), "GOTO MOLECULES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 2 65 1 0 2 0 4 2 2 0 3 2", "SAMPLES 2 50 0 0 0 0 1 4 4 1 1 2", "4 5 3 5 0"), List("17 0 2 B 20 5 3 0 0 0", "22 0 2 A 10 0 0 3 2 2", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0")), "CATCH ME!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 1 65 1 0 2 0 4 2 2 0 3 2", "SAMPLES 1 50 0 0 0 0 1 4 4 1 1 2", "4 5 3 5 0"), List("17 0 2 B 20 5 3 0 0 0", "22 0 2 A 10 0 0 3 2 2", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0")), "IF YOU CAN!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 65 1 0 2 0 4 2 2 0 3 2", "SAMPLES 0 50 0 0 0 0 1 4 4 1 1 2", "4 5 3 5 0"), List("17 0 2 B 20 5 3 0 0 0", "22 0 2 A 10 0 0 3 2 2", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0")), "CONNECT A")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 65 2 0 2 0 4 2 2 0 3 2", "SAMPLES 0 50 0 0 0 0 1 4 4 1 1 2", "3 5 3 5 0"), List("17 0 2 B 20 5 3 0 0 0", "22 0 2 A 10 0 0 3 2 2", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0", "25 1 2 0 -1 -1 -1 -1 -1 -1")), "CONNECT A")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 65 3 0 2 0 4 2 2 0 3 2", "DIAGNOSIS 2 50 0 0 0 0 1 4 4 1 1 2", "2 5 3 5 0"), List("17 0 2 B 20 5 3 0 0 0", "22 0 2 A 10 0 0 3 2 2", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0", "25 1 2 0 -1 -1 -1 -1 -1 -1")), "CONNECT B")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 65 3 1 2 0 4 2 2 0 3 2", "DIAGNOSIS 1 50 0 0 0 0 1 4 4 1 1 2", "2 4 3 5 0"), List("17 0 2 B 20 5 3 0 0 0", "22 0 2 A 10 0 0 3 2 2", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0", "25 1 2 0 -1 -1 -1 -1 -1 -1")), "GOTO LABORATORY")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 2 65 3 1 2 0 4 2 2 0 3 2", "DIAGNOSIS 0 50 0 0 0 0 1 4 4 1 1 2", "2 4 3 5 0"), List("17 0 2 B 20 5 3 0 0 0", "22 0 2 A 10 0 0 3 2 2", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0", "25 1 2 0 -1 -1 -1 -1 -1 -1")), "CATCH ME!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 1 65 3 1 2 0 4 2 2 0 3 2", "DIAGNOSIS 0 50 0 0 0 0 1 4 4 1 1 2", "2 4 3 5 0"), List("17 0 2 B 20 5 3 0 0 0", "22 0 2 A 10 0 0 3 2 2", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0", "25 1 2 C 10 2 3 0 0 2")), "IF YOU CAN!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 65 3 1 2 0 4 2 2 0 3 2", "MOLECULES 2 50 0 0 0 0 1 4 4 1 1 2", "2 4 3 5 0"), List("17 0 2 B 20 5 3 0 0 0", "22 0 2 A 10 0 0 3 2 2", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0", "25 1 2 C 10 2 3 0 0 2")), "CONNECT 17")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 85 0 0 2 0 4 2 3 0 3 2", "MOLECULES 1 50 0 0 0 0 1 4 4 1 1 2", "5 5 3 5 0"), List("22 0 2 A 10 0 0 3 2 2", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0", "25 1 2 C 10 2 3 0 0 2")), "GOTO MOLECULES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 2 85 0 0 2 0 4 2 3 0 3 2", "MOLECULES 0 50 0 0 0 0 1 4 4 1 1 2", "5 5 3 5 0"), List("22 0 2 A 10 0 0 3 2 2", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0", "25 1 2 C 10 2 3 0 0 2")), "CATCH ME!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 1 85 0 0 2 0 4 2 3 0 3 2", "MOLECULES 0 50 0 0 1 0 1 4 4 1 1 2", "5 5 2 5 0"), List("22 0 2 A 10 0 0 3 2 2", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0", "25 1 2 C 10 2 3 0 0 2")), "IF YOU CAN!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 85 0 0 2 0 4 2 3 0 3 2", "MOLECULES 0 50 0 0 2 0 1 4 4 1 1 2", "5 5 1 5 0"), List("22 0 2 A 10 0 0 3 2 2", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0", "25 1 2 C 10 2 3 0 0 2")), "CONNECT C")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 85 0 0 3 0 4 2 3 0 3 2", "LABORATORY 2 50 0 0 2 0 1 4 4 1 1 2", "5 5 0 5 0"), List("22 0 2 A 10 0 0 3 2 2", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0", "25 1 2 C 10 2 3 0 0 2")), "GOTO LABORATORY")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 2 85 0 0 3 0 4 2 3 0 3 2", "LABORATORY 1 50 0 0 2 0 1 4 4 1 1 2", "5 5 0 5 0"), List("22 0 2 A 10 0 0 3 2 2", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0", "25 1 2 C 10 2 3 0 0 2")), "CATCH ME!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 1 85 0 0 3 0 4 2 3 0 3 2", "LABORATORY 0 50 0 0 2 0 1 4 4 1 1 2", "5 5 0 5 0"), List("22 0 2 A 10 0 0 3 2 2", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0", "25 1 2 C 10 2 3 0 0 2")), "IF YOU CAN!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 85 0 0 3 0 4 2 3 0 3 2", "LABORATORY 0 60 0 0 2 0 1 4 4 2 1 2", "5 5 0 5 0"), List("22 0 2 A 10 0 0 3 2 2", "24 1 2 B 20 2 0 0 1 4", "9 1 1 A 10 0 0 4 0 0")), "CONNECT 22")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 95 0 0 0 0 4 3 3 0 3 2", "LABORATORY 0 70 0 0 0 0 1 5 4 2 1 2", "5 5 5 5 0"), List("24 1 2 B 20 2 0 0 1 4")), "GOTO SAMPLES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 2 95 0 0 0 0 4 3 3 0 3 2", "SAMPLES 2 70 0 0 0 0 1 5 4 2 1 2", "5 5 5 5 0"), List("24 1 2 B 20 2 0 0 1 4")), "CATCH ME!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 1 95 0 0 0 0 4 3 3 0 3 2", "SAMPLES 1 70 0 0 0 0 1 5 4 2 1 2", "5 5 5 5 0"), List("24 1 2 B 20 2 0 0 1 4")), "IF YOU CAN!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 95 0 0 0 0 4 3 3 0 3 2", "SAMPLES 0 70 0 0 0 0 1 5 4 2 1 2", "5 5 5 5 0"), List("24 1 2 B 20 2 0 0 1 4")), "CONNECT 3")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 95 0 0 0 0 4 3 3 0 3 2", "SAMPLES 0 70 0 0 0 0 1 5 4 2 1 2", "5 5 5 5 0"), List("26 0 3 0 -1 -1 -1 -1 -1 -1", "24 1 2 B 20 2 0 0 1 4", "27 1 2 0 -1 -1 -1 -1 -1 -1")), "CONNECT 3")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 95 0 0 0 0 4 3 3 0 3 2", "SAMPLES 0 70 0 0 0 0 1 5 4 2 1 2", "5 5 5 5 0"), List("26 0 3 0 -1 -1 -1 -1 -1 -1", "28 0 3 0 -1 -1 -1 -1 -1 -1", "24 1 2 B 20 2 0 0 1 4", "27 1 2 0 -1 -1 -1 -1 -1 -1", "29 1 2 0 -1 -1 -1 -1 -1 -1")), "CONNECT 3")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 95 0 0 0 0 4 3 3 0 3 2", "DIAGNOSIS 2 70 0 0 0 0 1 5 4 2 1 2", "5 5 5 5 0"), List("26 0 3 0 -1 -1 -1 -1 -1 -1", "28 0 3 0 -1 -1 -1 -1 -1 -1", "30 0 3 0 -1 -1 -1 -1 -1 -1", "24 1 2 B 20 2 0 0 1 4", "27 1 2 0 -1 -1 -1 -1 -1 -1", "29 1 2 0 -1 -1 -1 -1 -1 -1")), "GOTO DIAGNOSIS")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 2 95 0 0 0 0 4 3 3 0 3 2", "DIAGNOSIS 1 70 0 0 0 0 1 5 4 2 1 2", "5 5 5 5 0"), List("26 0 3 0 -1 -1 -1 -1 -1 -1", "28 0 3 0 -1 -1 -1 -1 -1 -1", "30 0 3 0 -1 -1 -1 -1 -1 -1", "24 1 2 B 20 2 0 0 1 4", "27 1 2 0 -1 -1 -1 -1 -1 -1", "29 1 2 0 -1 -1 -1 -1 -1 -1")), "CATCH ME!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 1 95 0 0 0 0 4 3 3 0 3 2", "DIAGNOSIS 0 70 0 0 0 0 1 5 4 2 1 2", "5 5 5 5 0"), List("26 0 3 0 -1 -1 -1 -1 -1 -1", "28 0 3 0 -1 -1 -1 -1 -1 -1", "30 0 3 0 -1 -1 -1 -1 -1 -1", "24 1 2 B 20 2 0 0 1 4", "27 1 2 0 -1 -1 -1 -1 -1 -1", "29 1 2 0 -1 -1 -1 -1 -1 -1")), "IF YOU CAN!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 95 0 0 0 0 4 3 3 0 3 2", "DIAGNOSIS 0 70 0 0 0 0 1 5 4 2 1 2", "5 5 5 5 0"), List("26 0 3 0 -1 -1 -1 -1 -1 -1", "28 0 3 0 -1 -1 -1 -1 -1 -1", "30 0 3 0 -1 -1 -1 -1 -1 -1", "24 1 2 B 20 2 0 0 1 4", "27 1 2 E 30 0 0 0 0 6", "29 1 2 0 -1 -1 -1 -1 -1 -1")), "CONNECT 26")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 95 0 0 0 0 4 3 3 0 3 2", "DIAGNOSIS 0 70 0 0 0 0 1 5 4 2 1 2", "5 5 5 5 0"), List("26 0 3 D 50 0 0 7 3 0", "28 0 3 0 -1 -1 -1 -1 -1 -1", "30 0 3 0 -1 -1 -1 -1 -1 -1", "24 1 2 B 20 2 0 0 1 4", "27 1 2 E 30 0 0 0 0 6", "29 1 2 E 10 3 2 2 0 0")), "CONNECT 28")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 95 0 0 0 0 4 3 3 0 3 2", "LABORATORY 3 70 0 0 0 0 1 5 4 2 1 2", "5 5 5 5 0"), List("26 0 3 D 50 0 0 7 3 0", "28 0 3 E 40 0 0 0 7 0", "30 0 3 0 -1 -1 -1 -1 -1 -1", "24 1 2 B 20 2 0 0 1 4", "27 1 2 E 30 0 0 0 0 6", "29 1 2 E 10 3 2 2 0 0")), "CONNECT 30")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 95 0 0 0 0 4 3 3 0 3 2", "LABORATORY 2 70 0 0 0 0 1 5 4 2 1 2", "5 5 5 5 0"), List("26 0 3 D 50 0 0 7 3 0", "28 0 3 E 40 0 0 0 7 0", "30 0 3 D 40 0 0 7 0 0", "24 1 2 B 20 2 0 0 1 4", "27 1 2 E 30 0 0 0 0 6", "29 1 2 E 10 3 2 2 0 0")), "CONNECT 30")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 95 0 0 0 0 4 3 3 0 3 2", "LABORATORY 1 70 0 0 0 0 1 5 4 2 1 2", "5 5 5 5 0"), List("26 0 3 D 50 0 0 7 3 0", "28 0 3 E 40 0 0 0 7 0", "24 1 2 B 20 2 0 0 1 4", "27 1 2 E 30 0 0 0 0 6", "29 1 2 E 10 3 2 2 0 0", "30 -1 3 D 40 0 0 7 0 0")), "GOTO MOLECULES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 2 95 0 0 0 0 4 3 3 0 3 2", "LABORATORY 0 70 0 0 0 0 1 5 4 2 1 2", "5 5 5 5 0"), List("26 0 3 D 50 0 0 7 3 0", "28 0 3 E 40 0 0 0 7 0", "24 1 2 B 20 2 0 0 1 4", "27 1 2 E 30 0 0 0 0 6", "29 1 2 E 10 3 2 2 0 0", "30 -1 3 D 40 0 0 7 0 0")), "CATCH ME!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 1 95 0 0 0 0 4 3 3 0 3 2", "LABORATORY 0 130 0 0 0 0 1 5 4 2 1 3", "5 5 5 5 0"), List("26 0 3 D 50 0 0 7 3 0", "28 0 3 E 40 0 0 0 7 0", "24 1 2 B 20 2 0 0 1 4", "27 1 2 E 30 0 0 0 0 6", "30 -1 3 D 40 0 0 7 0 0")), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 95 0 0 0 0 4 3 3 0 3 2", "LABORATORY 0 150 0 0 0 0 0 5 5 2 1 3", "5 5 5 5 1"), List("26 0 3 D 50 0 0 7 3 0", "28 0 3 E 40 0 0 0 7 0", "27 1 2 E 30 0 0 0 0 6", "30 -1 3 D 40 0 0 7 0 0")), "CONNECT D")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 95 0 0 0 1 4 3 3 0 3 2", "SAMPLES 2 150 0 0 0 0 0 5 5 2 1 3", "5 5 5 4 1"), List("26 0 3 D 50 0 0 7 3 0", "28 0 3 E 40 0 0 0 7 0", "27 1 2 E 30 0 0 0 0 6", "30 -1 3 D 40 0 0 7 0 0")), "CONNECT D")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 95 0 0 0 2 4 3 3 0 3 2", "SAMPLES 1 150 0 0 0 0 0 5 5 2 1 3", "5 5 5 3 1"), List("26 0 3 D 50 0 0 7 3 0", "28 0 3 E 40 0 0 0 7 0", "27 1 2 E 30 0 0 0 0 6", "30 -1 3 D 40 0 0 7 0 0")), "CONNECT D")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 95 0 0 0 3 4 3 3 0 3 2", "SAMPLES 0 150 0 0 0 0 0 5 5 2 1 3", "5 5 5 2 1"), List("26 0 3 D 50 0 0 7 3 0", "28 0 3 E 40 0 0 0 7 0", "27 1 2 E 30 0 0 0 0 6", "30 -1 3 D 40 0 0 7 0 0")), "CONNECT D")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 95 0 0 0 4 4 3 3 0 3 2", "SAMPLES 0 150 0 0 0 0 0 5 5 2 1 3", "5 5 5 1 1"), List("26 0 3 D 50 0 0 7 3 0", "28 0 3 E 40 0 0 0 7 0", "27 1 2 E 30 0 0 0 0 6", "31 1 2 0 -1 -1 -1 -1 -1 -1", "30 -1 3 D 40 0 0 7 0 0")), "GOTO LABORATORY")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 2 95 0 0 0 4 4 3 3 0 3 2", "DIAGNOSIS 2 150 0 0 0 0 0 5 5 2 1 3", "5 5 5 1 1"), List("26 0 3 D 50 0 0 7 3 0", "28 0 3 E 40 0 0 0 7 0", "27 1 2 E 30 0 0 0 0 6", "31 1 2 0 -1 -1 -1 -1 -1 -1", "30 -1 3 D 40 0 0 7 0 0")), "COMING")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 1 95 0 0 0 4 4 3 3 0 3 2", "DIAGNOSIS 1 150 0 0 0 0 0 5 5 2 1 3", "5 5 5 1 1"), List("26 0 3 D 50 0 0 7 3 0", "28 0 3 E 40 0 0 0 7 0", "27 1 2 E 30 0 0 0 0 6", "31 1 2 0 -1 -1 -1 -1 -1 -1", "30 -1 3 D 40 0 0 7 0 0")), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 95 0 0 0 4 4 3 3 0 3 2", "DIAGNOSIS 0 150 0 0 0 0 0 5 5 2 1 3", "5 5 5 1 1"), List("26 0 3 D 50 0 0 7 3 0", "28 0 3 E 40 0 0 0 7 0", "27 1 2 E 30 0 0 0 0 6", "31 1 2 0 -1 -1 -1 -1 -1 -1", "30 -1 3 D 40 0 0 7 0 0")), "CONNECT 28")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 135 0 0 0 0 4 3 3 0 3 3", "DIAGNOSIS 0 150 0 0 0 0 0 5 5 2 1 3", "5 5 5 5 1"), List("26 0 3 D 50 0 0 7 3 0", "27 1 2 E 30 0 0 0 0 6", "31 1 2 0 -1 -1 -1 -1 -1 -1", "30 1 3 D 40 0 0 7 0 0")), "GOTO MOLECULES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 2 135 0 0 0 0 4 3 3 0 3 3", "DIAGNOSIS 0 150 0 0 0 0 0 5 5 2 1 3", "5 5 5 5 1"), List("26 0 3 D 50 0 0 7 3 0", "27 1 2 E 30 0 0 0 0 6", "31 1 2 E 20 0 0 0 0 5", "30 1 3 D 40 0 0 7 0 0")), "COMING")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 1 135 0 0 0 0 4 3 3 0 3 3", "MOLECULES 2 150 0 0 0 0 0 5 5 2 1 3", "5 5 5 5 1"), List("26 0 3 D 50 0 0 7 3 0", "27 1 2 E 30 0 0 0 0 6", "31 1 2 E 20 0 0 0 0 5", "30 1 3 D 40 0 0 7 0 0")), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 135 0 0 0 0 4 3 3 0 3 3", "MOLECULES 1 150 0 0 0 0 0 5 5 2 1 3", "5 5 5 5 1"), List("26 0 3 D 50 0 0 7 3 0", "27 1 2 E 30 0 0 0 0 6", "31 1 2 E 20 0 0 0 0 5", "30 1 3 D 40 0 0 7 0 0")), "CONNECT C")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 135 0 0 1 0 4 3 3 0 3 3", "MOLECULES 0 150 0 0 0 0 0 5 5 2 1 3", "5 5 4 5 1"), List("26 0 3 D 50 0 0 7 3 0", "27 1 2 E 30 0 0 0 0 6", "31 1 2 E 20 0 0 0 0 5", "30 1 3 D 40 0 0 7 0 0")), "GOTO SAMPLES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 2 135 0 0 1 0 4 3 3 0 3 3", "DIAGNOSIS 2 150 0 0 0 0 0 5 5 2 1 3", "5 5 4 5 1"), List("26 0 3 D 50 0 0 7 3 0", "27 1 2 E 30 0 0 0 0 6", "31 1 2 E 20 0 0 0 0 5", "30 1 3 D 40 0 0 7 0 0")), "COMING")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 1 135 0 0 1 0 4 3 3 0 3 3", "DIAGNOSIS 1 150 0 0 0 0 0 5 5 2 1 3", "5 5 4 5 1"), List("26 0 3 D 50 0 0 7 3 0", "27 1 2 E 30 0 0 0 0 6", "31 1 2 E 20 0 0 0 0 5", "30 1 3 D 40 0 0 7 0 0")), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 135 0 0 1 0 4 3 3 0 3 3", "DIAGNOSIS 0 150 0 0 0 0 0 5 5 2 1 3", "5 5 4 5 1"), List("26 0 3 D 50 0 0 7 3 0", "27 1 2 E 30 0 0 0 0 6", "31 1 2 E 20 0 0 0 0 5", "30 1 3 D 40 0 0 7 0 0")), "CONNECT 3")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 0 135 0 0 1 0 4 3 3 0 3 3", "DIAGNOSIS 0 150 0 0 0 0 0 5 5 2 1 3", "5 5 4 5 1"), List("26 0 3 D 50 0 0 7 3 0", "32 0 3 0 -1 -1 -1 -1 -1 -1", "27 1 2 E 30 0 0 0 0 6", "30 1 3 D 40 0 0 7 0 0", "31 -1 2 E 20 0 0 0 0 5")), "GOTO DIAGNOSIS")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 2 135 0 0 1 0 4 3 3 0 3 3", "DIAGNOSIS 0 150 0 0 0 0 0 5 5 2 1 3", "5 5 4 5 1"), List("26 0 3 D 50 0 0 7 3 0", "32 0 3 0 -1 -1 -1 -1 -1 -1", "30 1 3 D 40 0 0 7 0 0", "31 -1 2 E 20 0 0 0 0 5", "27 -1 2 E 30 0 0 0 0 6")), "COMING")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 1 135 0 0 1 0 4 3 3 0 3 3", "DIAGNOSIS 0 150 0 0 0 0 0 5 5 2 1 3", "5 5 4 5 1"), List("26 0 3 D 50 0 0 7 3 0", "32 0 3 0 -1 -1 -1 -1 -1 -1", "31 -1 2 E 20 0 0 0 0 5", "27 -1 2 E 30 0 0 0 0 6", "30 -1 3 D 40 0 0 7 0 0")), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 135 0 0 1 0 4 3 3 0 3 3", "SAMPLES 2 150 0 0 0 0 0 5 5 2 1 3", "5 5 4 5 1"), List("26 0 3 D 50 0 0 7 3 0", "32 0 3 0 -1 -1 -1 -1 -1 -1", "31 -1 2 E 20 0 0 0 0 5", "27 -1 2 E 30 0 0 0 0 6", "30 -1 3 D 40 0 0 7 0 0")), "CONNECT 27")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("DIAGNOSIS 0 135 0 0 1 0 4 3 3 0 3 3", "SAMPLES 1 150 0 0 0 0 0 5 5 2 1 3", "5 5 4 5 1"), List("26 0 3 D 50 0 0 7 3 0", "32 0 3 0 -1 -1 -1 -1 -1 -1", "27 0 2 E 30 0 0 0 0 6", "31 -1 2 E 20 0 0 0 0 5", "30 -1 3 D 40 0 0 7 0 0")), "GOTO MOLECULES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 2 135 0 0 1 0 4 3 3 0 3 3", "SAMPLES 0 150 0 0 0 0 0 5 5 2 1 3", "5 5 4 5 1"), List("26 0 3 D 50 0 0 7 3 0", "32 0 3 0 -1 -1 -1 -1 -1 -1", "27 0 2 E 30 0 0 0 0 6", "31 -1 2 E 20 0 0 0 0 5", "30 -1 3 D 40 0 0 7 0 0")), "COMING")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 1 135 0 0 1 0 4 3 3 0 3 3", "SAMPLES 0 150 0 0 0 0 0 5 5 2 1 3", "5 5 4 5 1"), List("26 0 3 D 50 0 0 7 3 0", "32 0 3 0 -1 -1 -1 -1 -1 -1", "27 0 2 E 30 0 0 0 0 6", "33 1 2 0 -1 -1 -1 -1 -1 -1", "31 -1 2 E 20 0 0 0 0 5", "30 -1 3 D 40 0 0 7 0 0")), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 135 0 0 1 0 4 3 3 0 3 3", "SAMPLES 0 150 0 0 0 0 0 5 5 2 1 3", "5 5 4 5 1"), List("26 0 3 D 50 0 0 7 3 0", "32 0 3 0 -1 -1 -1 -1 -1 -1", "27 0 2 E 30 0 0 0 0 6", "33 1 2 0 -1 -1 -1 -1 -1 -1", "34 1 2 0 -1 -1 -1 -1 -1 -1", "31 -1 2 E 20 0 0 0 0 5", "30 -1 3 D 40 0 0 7 0 0")), "CONNECT E")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("MOLECULES 0 135 0 0 1 0 5 3 3 0 3 3", "SAMPLES 0 150 0 0 0 0 0 5 5 2 1 3", "5 5 4 5 0"), List("26 0 3 D 50 0 0 7 3 0", "32 0 3 0 -1 -1 -1 -1 -1 -1", "27 0 2 E 30 0 0 0 0 6", "33 1 2 0 -1 -1 -1 -1 -1 -1", "34 1 2 0 -1 -1 -1 -1 -1 -1", "35 1 2 0 -1 -1 -1 -1 -1 -1", "31 -1 2 E 20 0 0 0 0 5", "30 -1 3 D 40 0 0 7 0 0")), "GOTO LABORATORY")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 2 135 0 0 1 0 5 3 3 0 3 3", "DIAGNOSIS 2 150 0 0 0 0 0 5 5 2 1 3", "5 5 4 5 0"), List("26 0 3 D 50 0 0 7 3 0", "32 0 3 0 -1 -1 -1 -1 -1 -1", "27 0 2 E 30 0 0 0 0 6", "33 1 2 0 -1 -1 -1 -1 -1 -1", "34 1 2 0 -1 -1 -1 -1 -1 -1", "35 1 2 0 -1 -1 -1 -1 -1 -1", "31 -1 2 E 20 0 0 0 0 5", "30 -1 3 D 40 0 0 7 0 0")), "COMING")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 1 135 0 0 1 0 5 3 3 0 3 3", "DIAGNOSIS 1 150 0 0 0 0 0 5 5 2 1 3", "5 5 4 5 0"), List("26 0 3 D 50 0 0 7 3 0", "32 0 3 0 -1 -1 -1 -1 -1 -1", "27 0 2 E 30 0 0 0 0 6", "33 1 2 0 -1 -1 -1 -1 -1 -1", "34 1 2 0 -1 -1 -1 -1 -1 -1", "35 1 2 0 -1 -1 -1 -1 -1 -1", "31 -1 2 E 20 0 0 0 0 5", "30 -1 3 D 40 0 0 7 0 0")), "FOR YOU!")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 135 0 0 1 0 5 3 3 0 3 3", "DIAGNOSIS 0 150 0 0 0 0 0 5 5 2 1 3", "5 5 4 5 0"), List("26 0 3 D 50 0 0 7 3 0", "32 0 3 0 -1 -1 -1 -1 -1 -1", "27 0 2 E 30 0 0 0 0 6", "33 1 2 0 -1 -1 -1 -1 -1 -1", "34 1 2 0 -1 -1 -1 -1 -1 -1", "35 1 2 0 -1 -1 -1 -1 -1 -1", "31 -1 2 E 20 0 0 0 0 5", "30 -1 3 D 40 0 0 7 0 0")), "CONNECT 27")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("LABORATORY 0 165 0 0 1 0 2 3 3 0 3 4", "DIAGNOSIS 0 150 0 0 0 0 0 5 5 2 1 3", "5 5 4 5 3"), List("26 0 3 D 50 0 0 7 3 0", "32 0 3 0 -1 -1 -1 -1 -1 -1", "33 1 2 D 20 5 0 0 0 0", "34 1 2 0 -1 -1 -1 -1 -1 -1", "35 1 2 0 -1 -1 -1 -1 -1 -1", "31 -1 2 E 20 0 0 0 0 5", "30 -1 3 D 40 0 0 7 0 0")), "GOTO SAMPLES")

  testGameSituation(List(List("3 3 0 0 3", "0 4 4 0 0", "4 0 0 0 4"), List("SAMPLES 2 165 0 0 1 0 2 3 3 0 3 4", "LABORATORY 3 150 0 0 0 0 0 5 5 2 1 3", "5 5 4 5 3"), List("26 0 3 D 50 0 0 7 3 0", "32 0 3 0 -1 -1 -1 -1 -1 -1", "33 1 2 D 20 5 0 0 0 0", "34 1 2 0 -1 -1 -1 -1 -1 -1", "35 1 2 0 -1 -1 -1 -1 -1 -1", "31 -1 2 E 20 0 0 0 0 5", "30 -1 3 D 40 0 0 7 0 0")), "CATCH ME!")

  private def testGameSituation(testStr: List[List[String]], expectedAction: String): Unit = {
    world = new World()
    robot = world.robot

    world.updateAll(testStr, turns)
    turns += 1

    val nextAction = robot.nextAction()

    if (nextAction != robot.nextAction() || !nextAction.equals(expectedAction)) throw new Exception(nextAction)
    world.simulateTurn()
  }
}