//package botProgramming.CodersStrikeBack
//
//import java.io.ByteArrayInputStream
//import org.scalatest.FunSuite
//
//class ModelSpec extends FunSuite {
//  test("Documentation") {
//
//    /**
//      * The pods have a circular force-field around their center, with a radius of 400 units, which activates in case of collisions with other pods.
//      */
//    info("a pod can be created")
//    val pod1 = a(Pod().withRadius(10))
//
//    info("a pod cannot be created without radius")
//    assertThrows[Pod.NotReadyToBuildData](a(Pod()))
//
//    info("a team can be created with two pods")
//    val pod2  = a(Pod().withRadius(10))
//    val team1 = a(Team().withPod(pod1).withPod(pod2))
//
//    info("a team cannot be created with less than two pods\"")
//    val pod3 = a(Pod().withRadius(10))
//    assertThrows[Team.NotEnoughPodsException](a(Team()))
//    assertThrows[Team.NotEnoughPodsException](a(Team().withPod(pod3)))
//
//    info("a team cannot be created with a pod that has already a team")
//    assertThrows[Pod.AlreadyHasATeam](a(Team().withPod(pod1).withPod(pod3)))
//
//    info("a team cannot be created with more than two pods")
//    val pod4 = a(Pod().withRadius(10))
//    val pod5 = a(Pod().withRadius(10))
//    assertThrows[Team.AlreadyHaveTwoBotsException](a(Team().withPod(pod3).withPod(pod4).withPod(pod5)))
//
//    /** The circuit of the race is made up of checkpoints.*/
//    info("a checkpoint can be created")
//    val checkpoint1 = a(Checkpoint().withPosition(botProgramming.CodersStrikeBack.Vector(100, 100)).withRadius(20))
//
//    info("a checkpoint cannot be created without position or radius")
//    assertThrows[Checkpoint.NotReadyToBuildData](a(Checkpoint()))
//    assertThrows[Checkpoint.NotReadyToBuildData](a(Checkpoint().withPosition(botProgramming.CodersStrikeBack.Vector(0, 0))))
//    assertThrows[Checkpoint.NotReadyToBuildData](a(Checkpoint().withRadius(20)))
//
//    info("a circuit can be created with two checkpoints or more")
//    val checkpoint2 = a(Checkpoint().withPosition(botProgramming.CodersStrikeBack.Vector(200, 100)).withRadius(20))
//    val circuit1    = a(Circuit().withCheckpoint(checkpoint1).withCheckpoint(checkpoint2))
//
//    info("a circuit can be created with checkpoint used for other circuit")
//    val checkpoint3 = a(Checkpoint().withPosition(botProgramming.CodersStrikeBack.Vector(300, 100)).withRadius(20))
//    val circuit2    = a(Circuit().withCheckpoint(checkpoint1).withCheckpoint(checkpoint2).withCheckpoint(checkpoint3))
//
//    info("a circuit cannot be created with less than two checkpoints")
//    assertThrows[Circuit.NotEnoughCheckpointsException](a(Circuit()))
//    assertThrows[Circuit.NotEnoughCheckpointsException](a(Circuit().withCheckpoint(checkpoint1)))
//
//    info("a circuit cannot be created with two touching checkpoints")
//    assertThrows[Circuit.CheckpointsShouldNotBeTouching](
//      a(Circuit().withCheckpoint(checkpoint1).withCheckpoint(a(Checkpoint().withPosition(botProgramming.CodersStrikeBack.Vector(100, 139)).withRadius(20))))
//    )
//
//    info("a race can be created with one circuit and a number of laps")
//    val race1 = a(Race().withCircuit(circuit1).withLaps(2))
//
//    info("a race can be created with circuit used for other race")
//    val race2 = a(Race().withCircuit(circuit1).withLaps(3))
//
//    info("a race can be created reading input")
//    val race3 = Console.withIn(new ByteArrayInputStream("1\n2\n0 0\n0 800".getBytes)) {
//      val race = a(Race().fromInput)
//      assert(race.laps == 1)
//      assert(race.circuit.checkpoints.size == 2)
//      assert(race.circuit.checkpoints(0) == a(Checkpoint().withRadius(400).withPosition(botProgramming.CodersStrikeBack.Vector(0, 0))))
//      assert(race.circuit.checkpoints(1) == a(Checkpoint().withRadius(400).withPosition(botProgramming.CodersStrikeBack.Vector(0, 800))))
//      race
//    }
//
//    info("a race cannot be created without circuit or number of laps")
//    assertThrows[Race.NotReadyToBuildData](a(Race()))
//    assertThrows[Race.NotReadyToBuildData](a(Race().withCircuit(circuit1)))
//    assertThrows[Race.NotReadyToBuildData](a(Race().withLaps(3)))
//
//    info("a race cannot be created with zero or negative number of laps")
//    assertThrows[Race.NumberOfLapsShouldBeGreaterThanZero](a(Race().withCircuit(circuit1).withLaps(-1)))
//    assertThrows[Race.NumberOfLapsShouldBeGreaterThanZero](a(Race().withCircuit(circuit1).withLaps(0)))
//
//    info("a player can be created")
//    val player1 = a(APlayer())
//
//    /** The players each control a team of two pods during a race.*/
//    info("a player can control a team")
//    player1.controls(team1)
//
//    info("a player cannot control more than one team")
//    val team2 = a(Team().withPod(pod3).withPod(pod4))
//    assertThrows[APlayer.NotReadyToControlTeam](player1.controls(team2))
//
//    info("a team cannot be controlled by two players")
//    val player2 = a(APlayer())
//    assertThrows[Team.AlreadyControlledByAPlayer](player2.controls(team1))
//
//    info("a player can participate in a race")
//    player1.playInRace(race1)
//
//    info("two players can participate in the same race")
//    player2.controls(team2)
//    player2.playInRace(race1)
//
//    info("a player cannot be participate in a race with already two players")
//    val pod6    = a(Pod().withRadius(10))
//    val team3   = a(Team().withPod(pod5).withPod(pod6))
//    val player3 = a(APlayer()).controls(team3)
//    assertThrows[Race.NotWaitingForParticipant](player3.playInRace(race1))
//
//    info("a player cannot be participating in more than one race")
//    assertThrows[APlayer.NotReadyToPlayInRace](player1.playInRace(race2))
//
//    info("a player cannot participate in a race without controlling a team")
//    val player4 = a(APlayer())
//    assertThrows[APlayer.NotReadyToPlayInRace](player4.playInRace(race2))
//
//    info("a pod starts in a position")
//    pod1.startsIn(botProgramming.CodersStrikeBack.Vector(0, 0))
//
//    info("a pod starts with a given angle")
//    pod1.startsWithAngle(Angle(0))
//
//    info("a pod starts with a given speed")
//    pod1.startsWithInitialSpeed(botProgramming.CodersStrikeBack.Vector(0, 0))
//
//    info("a pod can start a race in the same position than other bot that is out of the race")
//    player3.playInRace(race2)
//    pod5.startsIn(botProgramming.CodersStrikeBack.Vector(0, 0))
//
//    info("a pod cannot start touching other pod in the same race")
//    assertThrows[Pod.CantStartIn](pod2.startsIn(botProgramming.CodersStrikeBack.Vector(0, 0)))
//    assertThrows[Pod.CantStartIn](pod3.startsIn(botProgramming.CodersStrikeBack.Vector(0, 19)))
//    assertThrows[Pod.CantStartIn](pod4.startsIn(botProgramming.CodersStrikeBack.Vector(19, 0)))
//
//    info("a pod cannot start if not in a race")
//    val pod7 = a(Pod().withRadius(10))
//    assertThrows[Pod.NotReadyForARace](pod7.startsIn(botProgramming.CodersStrikeBack.Vector(0, 0)))
//
//    info("pod lap starts in 0")
//    pod2.startsIn(botProgramming.CodersStrikeBack.Vector(500, 20)).startsWithAngle(Angle(0)).startsWithInitialSpeed(botProgramming.CodersStrikeBack.Vector(0, 0))
//    pod3.startsWithInitialSpeed(botProgramming.CodersStrikeBack.Vector(0, 0)).startsIn(botProgramming.CodersStrikeBack.Vector(500, 40)).startsWithAngle(Angle(0))
//    pod4.startsWithAngle(Angle(0)).startsWithInitialSpeed(botProgramming.CodersStrikeBack.Vector(0, 0)).startsIn(botProgramming.CodersStrikeBack.Vector(0, 100))
//    assert(race1.lapsOf(pod1) == 0)
//    assert(race1.lapsOf(pod2) == 0)
//    assert(race1.lapsOf(pod3) == 0)
//    assert(race1.lapsOf(pod4) == 0)
//
//    info("pod lap cannot be obtained if race has not been started")
//    assertThrows[Race.RaceNotInProgress](assert(race2.lapsOf(pod2) == 0))
//
//    /**
//      * Expert Rules
//      * On each turn the pods movements are computed this way:
//      * Rotation: the pod rotates to face the target point, with a maximum of 18 degrees (except for the 1rst round).
//      * Acceleration: the pod's facing vector is multiplied by the given thrust value. The result is added to the current speed vector.
//      * Movement: The speed vector is added to the position of the pod. If a collision would occur at this point, the pods rebound off each other.
//      * Friction: the current speed vector of each pod is multiplied by 0.85
//      */
//    info("A pod can receive an action when is in race")
//    pod1.nextAction(botProgramming.CodersStrikeBack.Vector(0, -100), 100)
//
//    info("pod properties doesn't change before turn is updated")
//    assert(pod1.getMovementProps.angle ~== Angle(0))
//    assert(pod1.getMovementProps.location ~== botProgramming.CodersStrikeBack.Vector(0, 0))
//    assert(pod1.getMovementProps.speed ~== botProgramming.CodersStrikeBack.Vector(0, 0))
//
//    info("A pod can change his Angle and Speed by more than 18 grades in his first turn")
//    pod2.nextAction(botProgramming.CodersStrikeBack.Vector(500, 100), 0)
//    race1.updateTurn()
//    assert(pod2.getMovementProps.angle == Angle.PI / 2)
//    assert(pod2.getMovementProps.location ~== botProgramming.CodersStrikeBack.Vector(500, 20))
//    assert(pod2.getMovementProps.speed ~== botProgramming.CodersStrikeBack.Vector(0, 0))
//
//    info("pod properties change after turn is updated.")
//    assert(pod1.getMovementProps.angle == Angle(-Math.PI / 2))
//    assert(pod1.getMovementProps.location ~== botProgramming.CodersStrikeBack.Vector(0, -100))
//    assert(pod1.getMovementProps.speed ~== botProgramming.CodersStrikeBack.Vector(0, -85))
//
//    info("A race increment turn every time it is updated")
//    assert(race1.turn() == 1)
//    race1.updateTurn()
//    assert(race1.turn() == 2)
//
//    /** The pod will pivot to face the destination point by a maximum of 18 degrees per turn and will then accelerate in that direction. **/
//    info("A pod cannot rotate more than 18 grades after first turn")
//    pod2.nextAction(botProgramming.CodersStrikeBack.Vector(500, 0), 0)
//    pod3.nextAction(botProgramming.CodersStrikeBack.Vector(500, 80), 0)
//    race1.updateTurn()
//    assert(pod2.getMovementProps.angle == Angle.PI / 2 - Angle.PI / 10)
//    assert(pod2.getMovementProps.location ~== botProgramming.CodersStrikeBack.Vector(500, 20))
//    assert(pod2.getMovementProps.speed ~== botProgramming.CodersStrikeBack.Vector(0, 0))
//    assert(pod3.getMovementProps.angle == Angle.PI / 10)
//    assert(pod3.getMovementProps.location ~== botProgramming.CodersStrikeBack.Vector(500, 40))
//    assert(pod3.getMovementProps.speed ~== botProgramming.CodersStrikeBack.Vector(0, 0))
//
//    /** The thrust value of a pod is its acceleration and must be between 0 and 100. **/
//    info("A pod cannot receive action with a trust lower tha 0 or higher than 100")
//    assertThrows[Pod.InvalidThrust](pod1.nextAction(botProgramming.CodersStrikeBack.Vector(100, 120), 120))
//    assertThrows[Pod.InvalidThrust](pod1.nextAction(botProgramming.CodersStrikeBack.Vector(100, 120), -1))
//
//    /** * Collisions are elastic. The minimum impulse of a collision is 120. **/
//    // rotate to each other pod
//    (0 to 5).foreach { _ =>
//      pod2.nextAction(botProgramming.CodersStrikeBack.Vector(500, 40), 0)
//      pod3.nextAction(botProgramming.CodersStrikeBack.Vector(500, 20), 0)
//      race1.updateTurn()
//    }
//    assert(pod2.getMovementProps.angle == Angle.PI / 2)
//    assert(pod2.getMovementProps.location ~== botProgramming.CodersStrikeBack.Vector(500, 20))
//    assert(pod2.getMovementProps.speed ~== botProgramming.CodersStrikeBack.Vector(0, 0))
//    assert(pod3.getMovementProps.angle == Angle(-Math.PI / 2))
//    assert(pod3.getMovementProps.location ~== botProgramming.CodersStrikeBack.Vector(500, 40))
//    assert(pod3.getMovementProps.speed ~== botProgramming.CodersStrikeBack.Vector(0, 0))
//
//    info("crash  with minimum impulse 120")
//    // accelerate to each other pod and crash
//    pod2.nextAction(botProgramming.CodersStrikeBack.Vector(500, 40), 100)
//    pod3.nextAction(botProgramming.CodersStrikeBack.Vector(500, 20), 100)
//    race1.updateTurn()
//    assert(pod2.getMovementProps.angle == Angle.PI / 2)
//    assert(pod2.getMovementProps.location ~== botProgramming.CodersStrikeBack.Vector(500, -100))
//    assert(pod2.getMovementProps.speed ~== botProgramming.CodersStrikeBack.Vector(0, -102))
//    assert(pod3.getMovementProps.angle == Angle(-Math.PI / 2))
//    assert(pod3.getMovementProps.location ~== botProgramming.CodersStrikeBack.Vector(500, 160))
//    assert(pod3.getMovementProps.speed ~== botProgramming.CodersStrikeBack.Vector(0, 102))
//
//    // rotate to see each pod again
//    (0 to 10).foreach { _ =>
//      pod2.nextAction(botProgramming.CodersStrikeBack.Vector(500, 40), 0)
//      pod3.nextAction(botProgramming.CodersStrikeBack.Vector(500, 20), 0)
//      race1.updateTurn()
//    }
//
//    info("crash  with impulse greater than 120")
//    // accelerate to each other pod
//    (0 to 2).foreach { _ =>
//      pod2.nextAction(botProgramming.CodersStrikeBack.Vector(500, 40), 90)
//      pod3.nextAction(botProgramming.CodersStrikeBack.Vector(500, 20), 90)
//      race1.updateTurn()
//    }
//    // ensure collision at time 0
//    pod2.nextAction(botProgramming.CodersStrikeBack.Vector(500, 40), 60)
//    pod3.nextAction(botProgramming.CodersStrikeBack.Vector(500, 20), 60)
//    race1.updateTurn()
//    assert(pod2.getMovementProps.angle == Angle.PI / 2)
//    assert(pod2.getMovementProps.location ~== botProgramming.CodersStrikeBack.Vector(500, 20))
//    assert(pod2.getMovementProps.speed ~== botProgramming.CodersStrikeBack.Vector(0, 209))
//    assert(pod3.getMovementProps.angle == Angle(-Math.PI / 2))
//    assert(pod3.getMovementProps.location ~== botProgramming.CodersStrikeBack.Vector(500, 40))
//    assert(pod3.getMovementProps.speed ~== botProgramming.CodersStrikeBack.Vector(0, -209))
//    // crash
//    race1.updateTurn()
//    assert(pod2.getMovementProps.angle == Angle.PI / 2)
//    assert(pod2.getMovementProps.location ~== botProgramming.CodersStrikeBack.Vector(500, -189))
//    assert(pod2.getMovementProps.speed ~== botProgramming.CodersStrikeBack.Vector(0, -178))
//    assert(pod3.getMovementProps.angle == Angle(-Math.PI / 2))
//    assert(pod3.getMovementProps.location ~== botProgramming.CodersStrikeBack.Vector(500, 249))
//    assert(pod3.getMovementProps.speed ~== botProgramming.CodersStrikeBack.Vector(0, 178))
//
//    /** To pass a checkpoint, the center of a pod must be inside the radius of the checkpoint.*/
//    info("a pod initial checkpoint is 0")
//    assert(pod4.getRaceProps.nextCheck == 0)
//
//    info("a pod can pass through a checkpoint")
//    // accelerate and touch first checkpoint
//    pod4.nextAction(botProgramming.CodersStrikeBack.Vector(100, 100), 100)
//    race1.updateTurn()
//    assert(pod4.getMovementProps.angle == Angle(0))
//    assert(pod4.getMovementProps.location ~== botProgramming.CodersStrikeBack.Vector(100, 100))
//    assert(pod4.getMovementProps.speed ~== botProgramming.CodersStrikeBack.Vector(85, 0))
//    assert(pod4.getRaceProps.nextCheck == 1)
//
//    /** To complete one lap, your vehicle (pod) must pass through each one in order and back through the start.*/
//    info("a pod completes a lap.")
//    assert(race1.lapsOf(pod1) == 0)
//    // move until touch second checkpoint
//    pod4.nextAction(botProgramming.CodersStrikeBack.Vector(200, 100), 0)
//    race1.updateTurn()
//    assert(pod4.getRaceProps.nextCheck == 0)
//    assert(race1.lapsOf(pod4) == 0)
//    // rotate to first checkpoint
//    (0 to 10).foreach { _ =>
//      pod4.nextAction(botProgramming.CodersStrikeBack.Vector(100, 100), 0)
//      race1.updateTurn()
//    }
//    // accelerate to first checkpoint
//    pod4.nextAction(botProgramming.CodersStrikeBack.Vector(100, 100), 100)
//    race1.updateTurn()
//    // move until touch first checkpoint
//    (0 to 7).foreach { _ =>
//      pod4.nextAction(botProgramming.CodersStrikeBack.Vector(100, 100), 0)
//      race1.updateTurn()
//    }
//    assert(pod4.getRaceProps.nextCheck == 1)
//    assert(race1.lapsOf(pod4) == 1)
//
//    /** The first player to reach the start on the final lap wins.*/
//    info("a pod can complete a race.")
//    // rotate to second checkpoint
//    (0 to 10).foreach { _ =>
//      pod4.nextAction(botProgramming.CodersStrikeBack.Vector(200, 100), 0)
//      race1.updateTurn()
//    }
//    assert(pod4.getRaceProps.nextCheck == 1)
//    assert(race1.lapsOf(pod4) == 1)
//    // accelerate to second checkpoint
//    (0 to 1).foreach { _ =>
//      pod4.nextAction(botProgramming.CodersStrikeBack.Vector(200, 100), 100)
//      race1.updateTurn()
//    }
//    // move until touch second checkpoint
//    (0 to 5).foreach { _ =>
//      pod4.nextAction(botProgramming.CodersStrikeBack.Vector(200, 100), 0)
//      race1.updateTurn()
//    }
//    assert(pod4.getRaceProps.nextCheck == 0)
//    assert(race1.lapsOf(pod4) == 1)
//    // rotate to first checkpoint
//    (0 to 10).foreach { _ =>
//      pod4.nextAction(botProgramming.CodersStrikeBack.Vector(100, 100), 0)
//      race1.updateTurn()
//    }
//    // accelerate to first checkpoint
//    (0 to 1).foreach { _ =>
//      pod4.nextAction(botProgramming.CodersStrikeBack.Vector(100, 100), 100)
//      race1.updateTurn()
//    }
//    // move until touch first checkpoint
//    (0 to 13).foreach { _ =>
//      pod4.nextAction(botProgramming.CodersStrikeBack.Vector(100, 100), 0)
//      race1.updateTurn()
//    }
//    assert(race1.ended)
//
//    /** As soon as a pod completes the race, that pod's team is declared the winner.*/
//    info("a race can have a winner")
//    assert(race1.winner == team2)
//
//    info("a pod cannot be updated if race is ended")
//    assertThrows[Pod.NotInRace](pod1.nextAction(botProgramming.CodersStrikeBack.Vector(100, 100), 0))
//    assertThrows[Pod.NotInRace](pod2.nextAction(botProgramming.CodersStrikeBack.Vector(100, 100), 0))
//    assertThrows[Pod.NotInRace](pod3.nextAction(botProgramming.CodersStrikeBack.Vector(100, 100), 0))
//    assertThrows[Pod.NotInRace](pod4.nextAction(botProgramming.CodersStrikeBack.Vector(100, 100), 0))
//    assertThrows[Race.RaceNotInProgress](race1.updateTurn())
//
//    info("a race have no winner if has not ended")
//    assertThrows[Race.RaceNotCompleted](race2.winner)
//    val pod8  = a(Pod().withRadius(10))
//    val team4 = a(Team().withPod(pod7).withPod(pod8))
//    player4.controls(team4)
//    player4.playInRace(race2)
//    assertThrows[Race.RaceNotCompleted](race2.winner)
//
//    /** The game is played on a map 16000 units wide and 9000 units high. The coordinate X=0, Y=0 is the top left pixel.*/
//    info("a map can be created with dimension")
//    val map1 = a(aMap().withDimension(Dimension(16000, 9000)))
//
//    info("a game can be created with map")
//    val game1 = a(Game().withMap(map1)).asInstanceOf[CodersStrikeBackGame]
//
//    info("a game cannot be created without map")
//    assertThrows[Game.NoMapException](a(Game()))
//
//    info("a game can have two players")
//    val player5 = a(APlayer())
//    val player6 = a(APlayer())
//    game1.setPlayer(player5).setPlayer(player6)
//
//    info("a game cannot have more than two players")
//    val player7 = a(APlayer())
//    assertThrows[CodersStrikeBackGame.NotWaitingForPlayers](game1.setPlayer(player5).setPlayer(player7))
//
//    info("a game can have a race")
//    game1.setRace(race3)
//
//    info("a game cannot have a race if it doesnt have players")
//    val game2 = a(Game().withMap(map1)).asInstanceOf[CodersStrikeBackGame]
//    val race4 = a(Race().withCircuit(circuit1).withLaps(3))
//    assertThrows[CodersStrikeBackGame.NotWaitingForRace](game2.setRace(race4))
//
//    info("a game can initialize race with input ")
//    Console.withIn(new ByteArrayInputStream("0 0 0 0 0 0\n1000 0 0 0 0 0\n2000 0 0 0 0 0\n3000 0 0 0 0 0".getBytes)) {
//      game1.startFromInput()
//    }
//
//    /** The goal
//      * Win the race.
//      * The disposition of the checkpoints is selected randomly for each race.*/
//    /**
//    * The pods work as follows:
//    * To move a pod, you must print a target destination point followed by a thrust value. Details of the protocol can be found further down.
//    * You can use 1 acceleration boost in the race, you only need to replace the thrust value by the BOOST keyword.
//    * You may activate a pod's shields with the SHIELD command instead of accelerating. This will give the pod much more weight if it collides with another. However, the pod will not be able to accelerate for the next 3 turns.
//    * The pods may move normally outside the game area.
//    * If none of your pods make it to their next checkpoint in under 100 turns, you are eliminated and lose the game. Only one pod need to complete the race.
//    *
//    * Note: You may activate debug mode in the settings panel () to view additional game data.
//    *
//    * Victory Conditions
//    * Be the first to complete all the laps of the circuit with one pod.
//    *
//    * Lose Conditions
//    * Your program provides incorrect output.
//    * Your program times out.
//    * None of your pods reach their next checkpoint in time.
//    * Somebody else wins.
//    * The speed's values are truncated and the position's values are rounded to the nearest integer.
//    * A boost is in fact an acceleration of 650. The number of boost available is common between pods. If no boost is available, the maximum thrust is used.
//    * A shield multiplies the Pod mass by 10.
//    * The provided angle is absolute. 0° means facing EAST while 90° means facing SOUTH.
//    * Note
//    * The program must first read the initialization data from standard input. Then, within an infinite loop, read the contextual data from the standard input and provide to the standard output the desired instructions.
//    * Game Input
//    * Initialization input
//    * Line 1: laps : the number of laps to complete the race.
//    * Line 2: checkpointCount : the number of checkpoints in the circuit.
//    * Next checkpointCount lines : 2 integers checkpointX , checkpointY for the coordinates of checkpoint.
//    * Input for one game turn
//    * First 2 lines: Your two pods.
//    * Next 2 lines: The opponent's pods.
//    * Each pod is represented by: 6 integers, x & y for the position. vx & vy for the speed vector. angle for the rotation angle in degrees. nextCheckPointId for the number of the next checkpoint the pod must go through.
//    * Output for one game turn
//    * Two lines: 2 integers for the target coordinates of your pod followed by thrust , the acceleration to give your pod, or by SHIELD to activate the shields, or by BOOST for an acceleration burst. One line per pod.
//    * Constraints
//    * 0 ≤ thrust ≤ 100
//    * 2 ≤ checkpointCount ≤ 8
//    * Response time first turn ≤ 1000ms
//    * Response time per turn ≤ 75ms
//    */
//  }
//}
