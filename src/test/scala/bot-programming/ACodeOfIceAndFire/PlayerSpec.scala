//package botProgramming.ACodeOfIceAndFire
//
//class PlayerSpec extends FlatSpec with Matchers with Assertions {
//  val dCoordinate: Coordinate    = Coordinate(0, 0)
//  implicit val validate: Boolean = true
//
//  /**
//    * The map
//    *
//    * The map is a grid of size 12x12, where the top-left corner is the cell (0,0). The map is randomly generated at the start of each game.
//    * Both players start with headquarters (HQ) from opposite edges of the map ((0,0) and (11,11)).
//    *
//    * A map cell can be either:
//    * void (#): not a playable cell.
//    * neutral (.): doesn't belong to any player.
//    * captured (O or X): belongs to a player.
//    * inactive (o or x): belongs to a player but inactive.
//    */
//  "Map" should "accomplish above description" in {
//
//    val map = GameMap(Set(VoidCell(dCoordinate)), Set.empty, Set.empty)
//
//    assert(map.dimension.x == 12)
//    assert(map.dimension.y == 12)
//
//    assert(Cell('#')(dCoordinate) == VoidCell(dCoordinate))
//    assert(Cell('.')(dCoordinate) == NeutralCell(dCoordinate))
//    assert(Cell('O')(dCoordinate) == ActiveCell(Faction.o, dCoordinate))
//    assert(Cell('o')(dCoordinate) == InactiveCell(Faction.o, dCoordinate))
//    assert(Cell('X')(dCoordinate) == ActiveCell(Faction.x, dCoordinate))
//    assert(Cell('x')(dCoordinate) == InactiveCell(Faction.x, dCoordinate))
//
//    // try to add Cells out of dimension
//    assertThrows[InvalidCellException](
//      GameMap(Set(VoidCell(Coordinate(13, 13))), Set.empty, Set.empty)
//    )
//
//    // try to add duplicated Cells out of dimension. Just using Set in GameMap fixes this problem
//    Assertions.assertDoesNotCompile("GameMap(List(VoidCell(Coordinate(1, 1))))")
//  }
//
//  /**
//    * Territory ownership
//    *
//    * Throughout the game, each player will capture cells to enlarge their territory. A player territory is composed of
//    * all the cells owned by the player that are active.
//    * A cell is said to be active if and only if the cell is connected to the headquarters. That is, there exists a path
//    * of owned cells from the headquarters to this cell.
//    */
//  "GamePlayer" should "start with correct status" in {
//    val map = GameMap(
//      Set(
//        VoidCell(Coordinate(1, 0)),
//        ActiveCell(Faction.o, Coordinate(2, 0)),
//        VoidCell(Coordinate(3, 0)),
//        ActiveCell(Faction.x, Coordinate(3, 1)),
//        ActiveCell(Faction.o, Coordinate(3, 0)),
//        InactiveCell(Faction.o, Coordinate(4, 0)),
//        InactiveCell(Faction.o, Coordinate(5, 0)),
//        VoidCell(Coordinate(8, 0))
//      ),
//      Set.empty,
//      Set.empty
//    )
//
//    map.territoryOf(Faction.o) should have size 2
//    map.territoryOf(Faction.x) should have size 1
//
//    // validate there are not invalid Active cells
//    assertThrows[InvalidTerritory](
//      GameMap(
//        Set(
//          ActiveCell(Faction.o, Coordinate(2, 0)),
//          ActiveCell(Faction.x, Coordinate(4, 0)),
//          ActiveCell(Faction.o, Coordinate(5, 0))
//        ),
//        Set.empty,
//        Set.empty
//      )
//    )
//  }
//
//  /**
//    * Income
//    *
//    * At the beginning of each turn, a player gains or loses gold based on their income. A player has +1 income for each active cell owned.
//    * Every turn, army units cost some income (upkeep).
//    *
//    * Level 1 units reduce income by 1 per unit.
//    * Level 2 units reduce income by 4 per unit.
//    * Level 3 units reduce income by 20 per unit.
//    * If a player has negative income and cannot pay their upkeep using their gold, all of the player's units die and
//    * the player's gold is reset to 0.
//    */
//  "GamePlayer" should "get recalculate gold correctly" in {
//    val map = GameMap(
//      Set(
//        VoidCell(Coordinate(1, 0)),
//        VoidCell(Coordinate(3, 0)),
//        VoidCell(Coordinate(8, 0)),
//        InactiveCell(Faction.o, Coordinate(4, 0)),
//        InactiveCell(Faction.o, Coordinate(5, 0)),
//        ActiveCell(Faction.o, Coordinate(2, 0)),
//        ActiveCell(Faction.o, Coordinate(3, 0)),
//        ActiveCell(Faction.x, Coordinate(3, 1))
//      ),
//      Set(
//        Headquarter(Faction.o, Coordinate(0, 0)),
//        Headquarter(Faction.x, Coordinate(11, 11))
//      ),
//      Set.empty
//    )
//
//    val players = List(GamePlayer(Faction.o), GamePlayer(Faction.x))
//
//    val initialStatus = GameStatus(map, players)
//
//    val actions = List(Wait())
////    val newStatus = Turn(initialStatus).run(actions)
////
////    newStatus.goldOf(Faction.o) should be
////    10
//  }
//}
