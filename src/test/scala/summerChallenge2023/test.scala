package summerChallenge2023

import puzzle.bender.Utils

object test extends App {

  def bestRemainingMutant(mutantScores: Map[String, Double], threshold: Int): String = {
    // Write your code here
    mutantScores.maxBy {
      case (_, score) if score <= threshold => score
      case _                                => 0
    }._1
  }

  def mergeFiles(fileContents: List[String]): String = {
    // Write your code here
    fileContents
      .flatMap(
        file =>
          file
            .split('\n')
            .map(
              _.split(';')
                .map(entry => {
                  val Array(key, value) = entry.split("=", 2)
                  (key, value)
                })
                .toMap)
      )
      .groupBy(_("Name"))
      .collect {
        case (_, list) =>
          val map             = list.flatten.toMap
          val name            = map("Name")
          val otherParameters = map.removed("Name").toList.sortBy(_._1).map(v => v._1 + "=" + v._2).mkString(";")
          s"Name=$name${if (otherParameters.nonEmpty) ";" + otherParameters else ""}"
      }
      .toList
      .sorted
      .mkString("\n")
  }

  val f = mergeFiles(
    List(
      "Name=John;Age=15;Power=Telepathy\nName=Mary;Age=16;Power=Glows in the dark;Team=Basketball",
      "Name=Adam;Age=17;Score=133;Power=Can turn anything into candy\nName=John;Score=283.5;City=NYC"
    ))

  def findCorrectPath(instructions: List[String], target: List[Int]): String = {
    // Write your code here
    import scala.collection.mutable

    var positionX = 0
    var positionY = 0
    var direction = 'R'

    var firstMovedUp: (Int, String)    = null
    var firstMovedDown: (Int, String)  = null
    var firstMovedRight: (Int, String) = null
    var firstMovedLeft: (Int, String)  = null

    case class Rotation(index: Int, direction: Char, turnDirection: Char)

    val rotationAlternatives: mutable.Map[Rotation, (Int, Int)] = mutable.Map()
    instructions.zipWithIndex.foreach {
      case (instruction, index) =>
        instruction match {
          case "FORWARD" =>
            rotationAlternatives(Rotation(index + 1, direction, 'F')) = (positionX, positionY)
            direction match {
              case 'R' =>
                if (firstMovedRight == null) firstMovedRight = (index + 1, "FORWARD")
                positionX += 1
              case 'L' =>
                if (firstMovedLeft == null) firstMovedLeft = (index + 1, "FORWARD")
                positionX -= 1
              case 'U' =>
                if (firstMovedUp == null) firstMovedUp = (index + 1, "FORWARD")
                positionY += 1
              case 'D' =>
                if (firstMovedDown == null) firstMovedDown = (index + 1, "FORWARD")
                positionY -= 1
            }
          case "BACK" =>
            rotationAlternatives(Rotation(index + 1, direction, 'B')) = (positionX, positionY)
            direction match {
              case 'R' =>
                if (firstMovedLeft == null) firstMovedLeft = (index + 1, "BACK")
                positionX -= 1
              case 'L' =>
                if (firstMovedRight == null) firstMovedRight = (index + 1, "BACK")
                positionX += 1
              case 'U' =>
                if (firstMovedDown == null) firstMovedDown = (index + 1, "BACK")
                positionY -= 1
              case 'D' =>
                if (firstMovedUp == null) firstMovedUp = (index + 1, "BACK")
                positionY += 1
            }
          case "TURN LEFT" =>
            rotationAlternatives(Rotation(index + 1, direction, 'L')) = (positionX, positionY)
            direction match {
              case 'R' => direction = 'U'
              case 'L' => direction = 'D'
              case 'U' => direction = 'L'
              case 'D' => direction = 'R'
            }
          case "TURN RIGHT" =>
            rotationAlternatives(Rotation(index + 1, direction, 'R')) = (positionX, positionY)
            direction match {
              case 'R' => direction = 'D'
              case 'L' => direction = 'U'
              case 'U' => direction = 'R'
              case 'D' => direction = 'L'
            }
        }
    }

    val List(targetX, targetY) = target

    if (positionY == targetY + 2 && positionX == targetX) {
      // moved up once more than needed. replace by down movement
      "Replace instruction " + firstMovedUp._1 + " with " + (if (firstMovedUp._2 == "FORWARD") "BACK" else "FORWARD")
    } else if (positionY == targetY - 2 && positionX == targetX) {
      // moved down once more than needed. replace by up movement
      "Replace instruction " + firstMovedDown._1 + " with " + (if (firstMovedDown._2 == "FORWARD") "BACK" else "FORWARD")
    } else if (positionX == targetX + 2 && positionY == targetY) {
      // moved right once more than needed. replace by left movement
      "Replace instruction " + firstMovedRight._1 + " with " + (if (firstMovedRight._2 == "FORWARD") "BACK" else "FORWARD")
    } else if (positionX == targetX - 2 && positionY == targetY) {
      // moved left once more than needed. replace by right movement
      "Replace instruction " + firstMovedLeft._1 + " with " + (if (firstMovedLeft._2 == "FORWARD") "BACK" else "FORWARD")
    } else {
      rotationAlternatives.collectFirst {
        case (Rotation(index, 'R', 'L'), (x, y)) if x + (positionY - y) == targetX - 1 && y - (positionX - x) == targetY =>
          "Replace instruction " + index + " with FORWARD"
        case (Rotation(index, 'L', 'L'), (x, y)) if x + (positionY - y) == targetX - 1 && y - (positionX - x) == targetY =>
          "Replace instruction " + index + " with BACK"
        case (Rotation(index, 'R', 'R'), (x, y)) if x - (positionY - y) == targetX - 1 && y + (positionX - x) == targetY =>
          "Replace instruction " + index + " with FORWARD"
        case (Rotation(index, 'L', 'R'), (x, y)) if x - (positionY - y) == targetX - 1 && y + (positionX - x) == targetY =>
          "Replace instruction " + index + " with BACK"

        case (Rotation(index, 'R', 'L'), (x, y)) if x + (positionY - y) == targetX + 1 && y - (positionX - x) == targetY =>
          "Replace instruction " + index + " with BACK"
        case (Rotation(index, 'L', 'L'), (x, y)) if x + (positionY - y) == targetX + 1 && y - (positionX - x) == targetY =>
          "Replace instruction " + index + " with FORWARD"
        case (Rotation(index, 'R', 'R'), (x, y)) if x - (positionY - y) == targetX + 1 && y + (positionX - x) == targetY =>
          "Replace instruction " + index + " with BACK"
        case (Rotation(index, 'L', 'R'), (x, y)) if x - (positionY - y) == targetX + 1 && y + (positionX - x) == targetY =>
          "Replace instruction " + index + " with FORWARD"

        case (Rotation(index, 'U', 'L'), (x, y)) if x + (positionY - y) == targetX && y - (positionX - x) == targetY - 1 =>
          "Replace instruction " + index + " with FORWARD"
        case (Rotation(index, 'D', 'L'), (x, y)) if x + (positionY - y) == targetX && y - (positionX - x) == targetY - 1 =>
          "Replace instruction " + index + " with BACK"
        case (Rotation(index, 'U', 'R'), (x, y)) if x - (positionY - y) == targetX && y + (positionX - x) == targetY - 1 =>
          "Replace instruction " + index + " with FORWARD"
        case (Rotation(index, 'D', 'R'), (x, y)) if x - (positionY - y) == targetX && y + (positionX - x) == targetY - 1 =>
          "Replace instruction " + index + " with BACK"

        case (Rotation(index, 'U', 'L'), (x, y)) if x + (positionY - y) == targetX && y - (positionX - x) == targetY + 1 =>
          "Replace instruction " + index + " with BACK"
        case (Rotation(index, 'D', 'L'), (x, y)) if x + (positionY - y) == targetX && y - (positionX - x) == targetY + 1 =>
          "Replace instruction " + index + " with FORWARD"
        case (Rotation(index, 'U', 'R'), (x, y)) if x - (positionY - y) == targetX && y + (positionX - x) == targetY + 1 =>
          "Replace instruction " + index + " with BACK"
        case (Rotation(index, 'D', 'R'), (x, y)) if x - (positionY - y) == targetX && y + (positionX - x) == targetY + 1 =>
          "Replace instruction " + index + " with FORWARD"

        case (Rotation(index, _, 'L'), (x, y)) if x - (positionX - x) == targetX && y - (positionY - y) == targetY =>
          "Replace instruction " + index + " with TURN RIGHT"
        case (Rotation(index, _, 'R'), (x, y)) if x - (positionX - x) == targetX && y - (positionY - y) == targetY =>
          "Replace instruction " + index + " with TURN LEFT"

        case (Rotation(index, 'R', 'B'), (x, y)) if x + (positionY - y) == targetX && y - (positionX - x) == targetY + 1 =>
          "Replace instruction " + index + " with TURN RIGHT"
        case (Rotation(index, 'L', 'F'), (x, y)) if x + (positionY - y) == targetX && y - (positionX - x) == targetY + 1 =>
          "Replace instruction " + index + " with TURN RIGHT"
        case (Rotation(index, 'R', 'B'), (x, y)) if x - (positionY - y) == targetX && y + (positionX - x) == targetY + 1 =>
          "Replace instruction " + index + " with TURN LEFT"
        case (Rotation(index, 'L', 'F'), (x, y)) if x - (positionY - y) == targetX && y + (positionX - x) == targetY + 1 =>
          "Replace instruction " + index + " with TURN LEFT"

        case (Rotation(index, 'R', 'F'), (x, y)) if x + (positionY - y) == targetX && y - (positionX - x) == targetY - 1 =>
          "Replace instruction " + index + " with TURN RIGHT"
        case (Rotation(index, 'L', 'B'), (x, y)) if x + (positionY - y) == targetX && y - (positionX - x) == targetY - 1 =>
          "Replace instruction " + index + " with TURN RIGHT"
        case (Rotation(index, 'R', 'F'), (x, y)) if x - (positionY - y) == targetX && y + (positionX - x) == targetY - 1 =>
          "Replace instruction " + index + " with TURN LEFT"
        case (Rotation(index, 'L', 'B'), (x, y)) if x - (positionY - y) == targetX && y + (positionX - x) == targetY - 1 =>
          "Replace instruction " + index + " with TURN LEFT"

        case (Rotation(index, 'U', 'B'), (x, y)) if x + (positionY - y) == targetX - 1 && y - (positionX - x) == targetY =>
          "Replace instruction " + index + " with TURN RIGHT"
        case (Rotation(index, 'D', 'F'), (x, y)) if x + (positionY - y) == targetX - 1 && y - (positionX - x) == targetY =>
          "Replace instruction " + index + " with TURN RIGHT"
        case (Rotation(index, 'U', 'B'), (x, y)) if x - (positionY - y) == targetX - 1 && y + (positionX - x) == targetY =>
          "Replace instruction " + index + " with TURN LEFT"
        case (Rotation(index, 'D', 'F'), (x, y)) if x - (positionY - y) == targetX - 1 && y + (positionX - x) == targetY =>
          "Replace instruction " + index + " with TURN LEFT"

        case (Rotation(index, 'U', 'F'), (x, y)) if x + (positionY - y) == targetX + 1 && y - (positionX - x) == targetY =>
          "Replace instruction " + index + " with TURN RIGHT"
        case (Rotation(index, 'D', 'B'), (x, y)) if x + (positionY - y) == targetX + 1 && y - (positionX - x) == targetY =>
          "Replace instruction " + index + " with TURN RIGHT"
        case (Rotation(index, 'U', 'F'), (x, y)) if x - (positionY - y) == targetX + 1 && y + (positionX - x) == targetY =>
          "Replace instruction " + index + " with TURN LEFT"
        case (Rotation(index, 'D', 'B'), (x, y)) if x - (positionY - y) == targetX + 1 && y + (positionX - x) == targetY =>
          "Replace instruction " + index + " with TURN LEFT"
      }.get
    }
  }

  var r = findCorrectPath(List("FORWARD", "FORWARD", "FORWARD"), List(1, 0))
  // 3,0
  assert(r == "Replace instruction 1 with BACK")
  r = findCorrectPath(List("FORWARD", "FORWARD", "TURN LEFT", "FORWARD"), List(4, 0))
  // 2,1
  assert(r == "Replace instruction 3 with FORWARD")
  r = findCorrectPath(List("FORWARD", "FORWARD", "TURN RIGHT", "BACK"), List(2, 0))
  // 2,1
  assert(r == "Replace instruction 3 with FORWARD")
  r = findCorrectPath(List("TURN LEFT", "FORWARD", "FORWARD", "FORWARD"), List(4, 0))
  // 0,3
  assert(r == "Replace instruction 1 with FORWARD")
  r = findCorrectPath(List("TURN LEFT", "FORWARD", "TURN RIGHT", "FORWARD", "TURN RIGHT", "BACK", "TURN LEFT", "TURN LEFT", "BACK"), List(0, 1))
  // 1,1
  assert(r == "Replace instruction 7 with FORWARD")
  r = findCorrectPath(List("FORWARD", "TURN LEFT", "FORWARD", "FORWARD", "FORWARD"), List(1, -3))
  // 1,3
  assert(r == "Replace instruction 2 with TURN RIGHT")
  r = findCorrectPath(List("BACK", "TURN RIGHT", "FORWARD", "TURN RIGHT", "FORWARD", "FORWARD", "FORWARD", "FORWARD"), List(-1, 4))
  // -5, -1
  assert(r == "Replace instruction 3 with TURN RIGHT")
  r = findCorrectPath(List("FORWARD", "TURN RIGHT", "FORWARD", "TURN RIGHT", "FORWARD", "FORWARD", "FORWARD", "FORWARD"), List(-1, 4))
  // -3, -1
  assert(r == "Replace instruction 1 with TURN RIGHT")

  def gearBalance(nGears: Int, connections: List[List[Int]]): List[Int] = {
    // Write your code here
    import scala.collection.mutable

    var clockwise: List[Int]        = List(0)
    var counterClockwise: List[Int] = List()

    var isValid = true

    val toVisit: mutable.Queue[List[Int]] = mutable.Queue()
    toVisit.enqueueAll(connections)

    while (toVisit.nonEmpty && isValid) {
      val visiting = toVisit.dequeue()

      val List(first, second) = visiting
      if (clockwise.contains(first)) {
        if (clockwise.contains(second))
          isValid = false
        else if (!counterClockwise.contains(second))
          counterClockwise ::= second
      } else if (clockwise.contains(second)) {
        if (clockwise.contains(first))
          isValid = false
        else if (!counterClockwise.contains(first))
          counterClockwise ::= first
      } else if (counterClockwise.contains(first)) {
        if (counterClockwise.contains(second))
          isValid = false
        else if (!clockwise.contains(second))
          clockwise ::= second
      } else if (counterClockwise.contains(second)) {
        if (counterClockwise.contains(first))
          isValid = false
        else if (!clockwise.contains(first))
          clockwise ::= first
      } else {
        toVisit.enqueue(visiting)
      }
    }
    if (isValid)
      List(clockwise.length, counterClockwise.length)
    else
      List(-1, -1)
  }

  val s = gearBalance(2, List(List(0, 1)))
  assert(s == List(1, 1))

  def mixWishes(wishA: String, wishB: String): String = {
    // Write your code here
    var results: List[(String, String)] = List((wishB, ""))
    var currentBest                     = wishB + wishA
    wishA.sliding(1).zipWithIndex.map(s => wishA.splitAt(s._2)._2).foreach {
      case s if s.nonEmpty =>
        val c             = s.head
        val remainingBase = s.tail
        results = results.flatMap {
          case (currentString, result) if currentString.nonEmpty =>
            if (currentString.contains(c)) {
              val Array(part1, part2) = currentString.split(c.toString, 2)
              val updatedResult       = s"$result$part1$c"

              if (updatedResult.length + part2.length + remainingBase.length < currentBest.length) {
                val small = if (part2.length > remainingBase.length) part2 else remainingBase
                val big   = if (part2 == small) remainingBase else part2
                val matched = (1 to small.length)
                  .flatMap(size => {
                    small
                      .sliding(size)
                      .filter(big.contains(_))
                  })
                  .maxByOption(_.length)

                val newVal = if (matched.nonEmpty) {
                  s"$updatedResult${part2.replaceFirst(matched.get, "")}$remainingBase"
                } else {
                  s"$updatedResult$part2$remainingBase"
                }
                currentBest = newVal
              }

              if (updatedResult.length + remainingBase.length > currentBest.length || updatedResult.length + part2.length > currentBest.length) {
                if (result.length + remainingBase.length < currentBest.length)
                  List((currentString, s"$result$c"))
                else
                  List()
              } else if (result.length + remainingBase.length < currentBest.length && part2.nonEmpty) {
                List((part2, updatedResult), (currentString, s"$result$c"))
              } else if (result.length + remainingBase.length < currentBest.length) {
                List((currentString, s"$result$c"))
              } else {
                List((part2, updatedResult))
              }
            } else {
              if (result.length + remainingBase.length > currentBest.length || result.length + currentString.length > currentBest.length) {
                List()
              } else {
                List((currentString, s"$result$c"))
              }
            }
          case _ => List()
        }
    }

    currentBest
  }

  r = mixWishes("first test", "second tested")
  assert(r == "firsecondedt test")
  r = mixWishes("I wish for laser eyes please", "I wish for telepathy please")
  assert(r == "I wish for telepaser ethyes please")
  Utils.debug = true
  Utils.timed("mixWishes ", () => {
    r = mixWishes("my first wish is to have infinite wishes", "oh no it is not allowed i wasted my wish")
  })
  assert(r == "myoh fno irst wish is noto havllowed infini wasted my wishes")

  def findCorrectPath(instructions: String, target: List[Int], obstacles: List[List[Int]]): String = {

    // Write your code here
    import scala.collection.mutable

    var positionX = 0
    var positionY = 0
    var direction = 'R'

//    val fObstacles: List[List[Int]] = obstacles.filter {
//      case List(x, y)
//        if obstacles.contains(List(x + 1, y)) && obstacles.contains(List(x - 1, y)) && obstacles.contains(List(x, y + 1)) && obstacles.contains(
//          List(x, y - 1)) =>
//        false
//      case List(x, y) => true
//    }
    case class Rotation(index: Int, direction: Char, turnDirection: Char, position: (Int, Int))

    val rotationAlternatives: mutable.Map[Rotation, List[Char]] = mutable.Map()

    instructions.zipWithIndex.foreach {
      case (instruction, index) =>
        instruction match {
          case 'F' =>
            rotationAlternatives(Rotation(index + 1, direction, 'F', (positionX, positionY))) = List('L', 'R', 'B')
            direction match {
              case 'R' =>
                positionX += 1
              case 'L' =>
                positionX -= 1
              case 'U' =>
                positionY += 1
              case 'D' =>
                positionY -= 1
            }

          case 'B' =>
            rotationAlternatives(Rotation(index + 1, direction, 'B', (positionX, positionY))) = List('L', 'R', 'F')
            direction match {
              case 'R' =>
                positionX -= 1
              case 'L' =>
                positionX += 1
              case 'U' =>
                positionY -= 1
              case 'D' =>
                positionY += 1
            }
          case 'L' =>
            rotationAlternatives(Rotation(index + 1, direction, 'L', (positionX, positionY))) = List('R', 'F', 'B')
            direction match {
              case 'R' => direction = 'U'
              case 'L' => direction = 'D'
              case 'U' => direction = 'L'
              case 'D' => direction = 'R'
            }
          case 'R' =>
            rotationAlternatives(Rotation(index + 1, direction, 'R', (positionX, positionY))) = List('L', 'F', 'B')
            direction match {
              case 'R' => direction = 'D'
              case 'L' => direction = 'U'
              case 'U' => direction = 'R'
              case 'D' => direction = 'L'
            }
        }
        rotationAlternatives.foreach {
          case (k @ Rotation(index, _, _, (x, y)), l) if l.isEmpty || obstacles.contains(List(x, y)) =>
            rotationAlternatives.remove(k)
          case (k @ Rotation(index, 'U', 'F', (x, y)), l) =>
            if (l.contains('B') && obstacles.contains(List(positionX, positionY - 2)))
              rotationAlternatives(k) = l.filter(_ != 'B')
            if (l.contains('R') && obstacles.contains(List(x + (positionY - y) - 1, y - (positionX - x))))
              rotationAlternatives(k) = l.filter(_ != 'R')
            if (l.contains('L') && obstacles.contains(List(x - (positionY - y) - 1, y + (positionX - x))))
              rotationAlternatives(k) = l.filter(_ != 'L')
          case (k @ Rotation(index, 'D', 'B', (x, y)), l) =>
            if (l.contains('F') && obstacles.contains(List(positionX, positionY - 2)))
              rotationAlternatives(k) = l.filter(_ != 'F')
            if (l.contains('R') && obstacles.contains(List(x + (positionY - y) - 1, y - (positionX - x))))
              rotationAlternatives(k) = l.filter(_ != 'R')
            if (l.contains('L') && obstacles.contains(List(x - (positionY - y) - 1, y + (positionX - x))))
              rotationAlternatives(k) = l.filter(_ != 'L')
          case (k @ Rotation(index, 'D', 'F', (x, y)), l) =>
            if (l.contains('B') && obstacles.contains(List(positionX, positionY + 2)))
              rotationAlternatives(k) = l.filter(_ != 'B')
            if (l.contains('R') && obstacles.contains(List(x + (positionY - y) + 1, y - (positionX - x))))
              rotationAlternatives(k) = l.filter(_ != 'R')
            if (l.contains('L') && obstacles.contains(List(x - (positionY - y) + 1, y + (positionX - x))))
              rotationAlternatives(k) = l.filter(_ != 'L')
          case (k @ Rotation(index, 'U', 'B', (x, y)), l) =>
            if (l.contains('F') && obstacles.contains(List(positionX, positionY + 2)))
              rotationAlternatives(k) = l.filter(_ != 'F')
            if (l.contains('R') && obstacles.contains(List(x + (positionY - y) + 1, y - (positionX - x))))
              rotationAlternatives(k) = l.filter(_ != 'R')
            if (l.contains('L') && obstacles.contains(List(x - (positionY - y) + 1, y + (positionX - x))))
              rotationAlternatives(k) = l.filter(_ != 'L')
          case (k @ Rotation(index, 'R', 'F', (x, y)), l) =>
            if (l.contains('B') && obstacles.contains(List(positionX - 2, positionY)))
              rotationAlternatives(k) = l.filter(_ != 'B')
            if (l.contains('R') && obstacles.contains(List(x + (positionY - y), y - (positionX - x) + 1)))
              rotationAlternatives(k) = l.filter(_ != 'R')
            if (l.contains('L') && obstacles.contains(List(x - (positionY - y), y + (positionX - x) + 1)))
              rotationAlternatives(k) = l.filter(_ != 'L')
          case (k @ Rotation(index, 'L', 'B', (x, y)), l) =>
            if (l.contains('F') && obstacles.contains(List(positionX - 2, positionY)))
              rotationAlternatives(k) = l.filter(_ != 'F')
            if (l.contains('R') && obstacles.contains(List(x + (positionY - y), y - (positionX - x) + 1)))
              rotationAlternatives(k) = l.filter(_ != 'R')
            if (l.contains('L') && obstacles.contains(List(x - (positionY - y), y + (positionX - x) + 1)))
              rotationAlternatives(k) = l.filter(_ != 'L')
          case (k @ Rotation(index, 'L', 'F', (x, y)), l) =>
            if (l.contains('B') && obstacles.contains(List(positionX + 2, positionY)))
              rotationAlternatives(k) = l.filter(_ != 'B')
            if (l.contains('R') && obstacles.contains(List(x + (positionY - y), y - (positionX - x) - 1)))
              rotationAlternatives(k) = l.filter(_ != 'R')
            if (l.contains('L') && obstacles.contains(List(x - (positionY - y), y + (positionX - x) - 1)))
              rotationAlternatives(k) = l.filter(_ != 'L')
          case (k @ Rotation(index, 'R', 'B', (x, y)), l) =>
            if (l.contains('F') && obstacles.contains(List(positionX + 2, positionY)))
              rotationAlternatives(k) = l.filter(_ != 'F')
            if (l.contains('R') && obstacles.contains(List(x + (positionY - y), y - (positionX - x) - 1)))
              rotationAlternatives(k) = l.filter(_ != 'R')
            if (l.contains('L') && obstacles.contains(List(x - (positionY - y), y + (positionX - x) - 1)))
              rotationAlternatives(k) = l.filter(_ != 'L')
          case (k @ Rotation(index, 'R', 'L', (x, y)), l) =>
            if (l.contains('F') && obstacles
                  .exists { case List(targetX, targetY) => x + (targetY - y) == targetX - 1 && y - (targetX - x) == targetY })
              rotationAlternatives(k) = l.filter(_ != 'F')
            if (l.contains('B') && obstacles.contains(List(x + (positionY - y) - 1, y - (positionX - x))))
              rotationAlternatives(k) = l.filter(_ != 'B')
            if (l.contains('R') && obstacles.contains(List(x - (positionX - x), y - (positionY - y))))
              rotationAlternatives(k) = l.filter(_ != 'R')
          case (k @ Rotation(index, 'L', 'L', (x, y)), l) =>
            if (l.contains('B') && obstacles
                  .exists { case List(targetX, targetY) => x + (targetY - y) == targetX - 1 && y - (targetX - x) == targetY })
              rotationAlternatives(k) = l.filter(_ != 'B')
            if (l.contains('F') && obstacles.contains(List(x + (positionY - y) - 1, y - (positionX - x))))
              rotationAlternatives(k) = l.filter(_ != 'F')
            if (l.contains('R') && obstacles.contains(List(x - (positionX - x), y - (positionY - y))))
              rotationAlternatives(k) = l.filter(_ != 'R')
          case (k @ Rotation(index, 'R', 'R', (x, y)), l) =>
            if (l.contains('F') && obstacles.contains(List(x - (positionY - y) + 1, y + (positionX - x))))
              rotationAlternatives(k) = l.filter(_ != 'F')
            if (l.contains('B') && obstacles.contains(List(x - (positionY - y) - 1, y + (positionX - x))))
              rotationAlternatives(k) = l.filter(_ != 'B')
            if (l.contains('L') && obstacles.contains(List(x - (positionX - x), y - (positionY - y))))
              rotationAlternatives(k) = l.filter(_ != 'L')
          case (k @ Rotation(index, 'L', 'R', (x, y)), l) =>
            if (l.contains('B') && obstacles.contains(List(x - (positionY - y) + 1, y + (positionX - x))))
              rotationAlternatives(k) = l.filter(_ != 'B')
            if (l.contains('F') && obstacles.contains(List(x - (positionY - y) - 1, y + (positionX - x))))
              rotationAlternatives(k) = l.filter(_ != 'F')
            if (l.contains('L') && obstacles.contains(List(x - (positionX - x), y - (positionY - y))))
              rotationAlternatives(k) = l.filter(_ != 'L')
          case (k @ Rotation(index, 'U', 'L', (x, y)), l) =>
            if (l.contains('F') && obstacles.contains(List(x + (positionY - y), y - (positionX - x) + 1)))
              rotationAlternatives(k) = l.filter(_ != 'F')
            if (l.contains('B') && obstacles.contains(List(x + (positionY - y), y - (positionX - x) - 1)))
              rotationAlternatives(k) = l.filter(_ != 'B')
            if (l.contains('R') && obstacles.contains(List(x - (positionX - x), y - (positionY - y))))
              rotationAlternatives(k) = l.filter(_ != 'R')
          case (k @ Rotation(index, 'D', 'L', (x, y)), l) =>
            if (l.contains('B') && obstacles.contains(List(x + (positionY - y), y - (positionX - x) + 1)))
              rotationAlternatives(k) = l.filter(_ != 'B')
            if (l.contains('F') && obstacles.contains(List(x + (positionY - y), y - (positionX - x) - 1)))
              rotationAlternatives(k) = l.filter(_ != 'F')
            if (l.contains('R') && obstacles.contains(List(x - (positionX - x), y - (positionY - y))))
              rotationAlternatives(k) = l.filter(_ != 'R')
          case (k @ Rotation(index, 'U', 'R', (x, y)), l) =>
            if (l.contains('F') && obstacles.contains(List(x - (positionY - y), y + (positionX - x) + 1)))
              rotationAlternatives(k) = l.filter(_ != 'F')
            if (l.contains('B') && obstacles.contains(List(x - (positionY - y), y + (positionX - x) - 1)))
              rotationAlternatives(k) = l.filter(_ != 'B')
            if (l.contains('L') && obstacles.contains(List(x - (positionX - x), y - (positionY - y))))
              rotationAlternatives(k) = l.filter(_ != 'L')
          case (k @ Rotation(index, 'D', 'R', (x, y)), l) =>
            if (l.contains('B') && obstacles.contains(List(x - (positionY - y), y + (positionX - x) + 1)))
              rotationAlternatives(k) = l.filter(_ != 'B')
            if (l.contains('F') && obstacles.contains(List(x - (positionY - y), y + (positionX - x) - 1)))
              rotationAlternatives(k) = l.filter(_ != 'F')
            if (l.contains('L') && obstacles.contains(List(x - (positionX - x), y - (positionY - y))))
              rotationAlternatives(k) = l.filter(_ != 'L')
        }
    }

    val List(targetX, targetY) = target

    rotationAlternatives.collectFirst {
      case (Rotation(index, 'U', 'F', (x, y)), l) if l.contains('B') && positionY == targetY + 2 && positionX == targetX =>
        "Replace instruction " + index + " with BACK"
      case (Rotation(index, 'D', 'B', (x, y)), l) if l.contains('F') && positionY == targetY + 2 && positionX == targetX =>
        "Replace instruction " + index + " with FORWARD"
      case (Rotation(index, 'D', 'F', (x, y)), l) if l.contains('B') && positionY == targetY - 2 && positionX == targetX =>
        "Replace instruction " + index + " with BACK"
      case (Rotation(index, 'U', 'B', (x, y)), l) if l.contains('F') && positionY == targetY - 2 && positionX == targetX =>
        "Replace instruction " + index + " with FORWARD"

      case (Rotation(index, 'R', 'F', (x, y)), l) if l.contains('B') && positionX == targetX + 2 && positionY == targetY =>
        "Replace instruction " + index + " with BACK"
      case (Rotation(index, 'L', 'B', (x, y)), l) if l.contains('F') && positionX == targetX + 2 && positionY == targetY =>
        "Replace instruction " + index + " with FORWARD"
      case (Rotation(index, 'L', 'F', (x, y)), l) if l.contains('B') && positionX == targetX - 2 && positionY == targetY =>
        "Replace instruction " + index + " with BACK"
      case (Rotation(index, 'R', 'B', (x, y)), l) if l.contains('F') && positionX == targetX - 2 && positionY == targetY =>
        "Replace instruction " + index + " with FORWARD"

      case (Rotation(index, 'R', 'L', (x, y)), l) if l.contains('F') && x + (positionY - y) == targetX - 1 && y - (positionX - x) == targetY =>
        "Replace instruction " + index + " with FORWARD"
      case (Rotation(index, 'L', 'L', (x, y)), l) if l.contains('B') && x + (positionY - y) == targetX - 1 && y - (positionX - x) == targetY =>
        "Replace instruction " + index + " with BACK"
      case (Rotation(index, 'R', 'R', (x, y)), l) if l.contains('F') && x - (positionY - y) == targetX - 1 && y + (positionX - x) == targetY =>
        "Replace instruction " + index + " with FORWARD"
      case (Rotation(index, 'L', 'R', (x, y)), l) if l.contains('B') && x - (positionY - y) == targetX - 1 && y + (positionX - x) == targetY =>
        "Replace instruction " + index + " with BACK"

      case (Rotation(index, 'R', 'L', (x, y)), l) if l.contains('B') && x + (positionY - y) == targetX + 1 && y - (positionX - x) == targetY =>
        "Replace instruction " + index + " with BACK"
      case (Rotation(index, 'L', 'L', (x, y)), l) if l.contains('F') && x + (positionY - y) == targetX + 1 && y - (positionX - x) == targetY =>
        "Replace instruction " + index + " with FORWARD"
      case (Rotation(index, 'R', 'R', (x, y)), l) if l.contains('B') && x - (positionY - y) == targetX + 1 && y + (positionX - x) == targetY =>
        "Replace instruction " + index + " with BACK"
      case (Rotation(index, 'L', 'R', (x, y)), l) if l.contains('F') && x - (positionY - y) == targetX + 1 && y + (positionX - x) == targetY =>
        "Replace instruction " + index + " with FORWARD"

      case (Rotation(index, 'U', 'L', (x, y)), l) if l.contains('F') && x + (positionY - y) == targetX && y - (positionX - x) == targetY - 1 =>
        "Replace instruction " + index + " with FORWARD"
      case (Rotation(index, 'D', 'L', (x, y)), l) if l.contains('B') && x + (positionY - y) == targetX && y - (positionX - x) == targetY - 1 =>
        "Replace instruction " + index + " with BACK"
      case (Rotation(index, 'U', 'R', (x, y)), l) if l.contains('F') && x - (positionY - y) == targetX && y + (positionX - x) == targetY - 1 =>
        "Replace instruction " + index + " with FORWARD"
      case (Rotation(index, 'D', 'R', (x, y)), l) if l.contains('B') && x - (positionY - y) == targetX && y + (positionX - x) == targetY - 1 =>
        "Replace instruction " + index + " with BACK"

      case (Rotation(index, 'U', 'L', (x, y)), l) if l.contains('B') && x + (positionY - y) == targetX && y - (positionX - x) == targetY + 1 =>
        "Replace instruction " + index + " with BACK"
      case (Rotation(index, 'D', 'L', (x, y)), l) if l.contains('F') && x + (positionY - y) == targetX && y - (positionX - x) == targetY + 1 =>
        "Replace instruction " + index + " with FORWARD"
      case (Rotation(index, 'U', 'R', (x, y)), l) if l.contains('B') && x - (positionY - y) == targetX && y + (positionX - x) == targetY + 1 =>
        "Replace instruction " + index + " with BACK"
      case (Rotation(index, 'D', 'R', (x, y)), l) if l.contains('F') && x - (positionY - y) == targetX && y + (positionX - x) == targetY + 1 =>
        "Replace instruction " + index + " with FORWARD"

      case (Rotation(index, _, 'L', (x, y)), l) if l.contains('R') && x - (positionX - x) == targetX && y - (positionY - y) == targetY =>
        "Replace instruction " + index + " with TURN RIGHT"
      case (Rotation(index, _, 'R', (x, y)), l) if l.contains('L') && x - (positionX - x) == targetX && y - (positionY - y) == targetY =>
        "Replace instruction " + index + " with TURN LEFT"

      case (Rotation(index, 'R', 'B', (x, y)), l) if l.contains('R') && x + (positionY - y) == targetX && y - (positionX - x) == targetY + 1 =>
        "Replace instruction " + index + " with TURN RIGHT"
      case (Rotation(index, 'L', 'F', (x, y)), l) if l.contains('R') && x + (positionY - y) == targetX && y - (positionX - x) == targetY + 1 =>
        "Replace instruction " + index + " with TURN RIGHT"
      case (Rotation(index, 'R', 'B', (x, y)), l) if l.contains('L') && x - (positionY - y) == targetX && y + (positionX - x) == targetY + 1 =>
        "Replace instruction " + index + " with TURN LEFT"
      case (Rotation(index, 'L', 'F', (x, y)), l) if l.contains('L') && x - (positionY - y) == targetX && y + (positionX - x) == targetY + 1 =>
        "Replace instruction " + index + " with TURN LEFT"

      case (Rotation(index, 'R', 'F', (x, y)), l) if l.contains('R') && x + (positionY - y) == targetX && y - (positionX - x) == targetY - 1 =>
        "Replace instruction " + index + " with TURN RIGHT"
      case (Rotation(index, 'L', 'B', (x, y)), l) if l.contains('R') && x + (positionY - y) == targetX && y - (positionX - x) == targetY - 1 =>
        "Replace instruction " + index + " with TURN RIGHT"
      case (Rotation(index, 'R', 'F', (x, y)), l) if l.contains('L') && x - (positionY - y) == targetX && y + (positionX - x) == targetY - 1 =>
        "Replace instruction " + index + " with TURN LEFT"
      case (Rotation(index, 'L', 'B', (x, y)), l) if l.contains('L') && x - (positionY - y) == targetX && y + (positionX - x) == targetY - 1 =>
        "Replace instruction " + index + " with TURN LEFT"

      case (Rotation(index, 'U', 'B', (x, y)), l) if l.contains('R') && x + (positionY - y) == targetX - 1 && y - (positionX - x) == targetY =>
        "Replace instruction " + index + " with TURN RIGHT"
      case (Rotation(index, 'D', 'F', (x, y)), l) if l.contains('R') && x + (positionY - y) == targetX - 1 && y - (positionX - x) == targetY =>
        "Replace instruction " + index + " with TURN RIGHT"
      case (Rotation(index, 'U', 'B', (x, y)), l) if l.contains('L') && x - (positionY - y) == targetX - 1 && y + (positionX - x) == targetY =>
        "Replace instruction " + index + " with TURN LEFT"
      case (Rotation(index, 'D', 'F', (x, y)), l) if l.contains('L') && x - (positionY - y) == targetX - 1 && y + (positionX - x) == targetY =>
        "Replace instruction " + index + " with TURN LEFT"

      case (Rotation(index, 'U', 'F', (x, y)), l) if l.contains('R') && x + (positionY - y) == targetX + 1 && y - (positionX - x) == targetY =>
        "Replace instruction " + index + " with TURN RIGHT"
      case (Rotation(index, 'D', 'B', (x, y)), l) if l.contains('R') && x + (positionY - y) == targetX + 1 && y - (positionX - x) == targetY =>
        "Replace instruction " + index + " with TURN RIGHT"
      case (Rotation(index, 'U', 'F', (x, y)), l) if l.contains('L') && x - (positionY - y) == targetX + 1 && y + (positionX - x) == targetY =>
        "Replace instruction " + index + " with TURN LEFT"
      case (Rotation(index, 'D', 'B', (x, y)), l) if l.contains('L') && x - (positionY - y) == targetX + 1 && y + (positionX - x) == targetY =>
        "Replace instruction " + index + " with TURN LEFT"
    }.get
  }

  r = findCorrectPath("FFF", List(1, 0), List(List(2, 0)))
  // 3,0
  assert(r == "Replace instruction 2 with BACK")
  r = findCorrectPath("FFLF", List(4, 0), List())
  // 2,1
  assert(r == "Replace instruction 3 with FORWARD")
  r = findCorrectPath("FFRB", List(2, 0), List())
  // 2,1
  assert(r == "Replace instruction 2 with TURN RIGHT")
  r = findCorrectPath("LFFF", List(4, 0), List())
  // 0,3
  assert(r == "Replace instruction 1 with FORWARD")
  r = findCorrectPath("LFRFRBLLB", List(0, 1), List())
  // 1,1
  assert(r == "Replace instruction 4 with TURN RIGHT") // check vs 7 with FORWARD
  r = findCorrectPath("FLFFF", List(1, -3), List())
  // 1,3
  assert(r == "Replace instruction 2 with TURN RIGHT")
  r = findCorrectPath("BRFRFFFF", List(-1, 4), List())
  // -5, -1
  assert(r == "Replace instruction 3 with TURN RIGHT")
  r = findCorrectPath("FRFRFFFF", List(-1, 4), List())
  // -3, -1
  assert(r == "Replace instruction 1 with TURN RIGHT")
}
