-- 2
SELECT agent.name, COUNT(1) as score FROM mutant
                                              JOIN agent ON agent.agentId = recruiterId
GROUP BY agent.name
ORDER BY score DESC
    LIMIT 10;

-- 6
SELECT student.NAME
FROM (
         SELECT scm.chatId, MAX(studentsWhoEnteredTheRoomRecently.TIMEINROOM) AS MAXTIMEINROOM
         FROM (
                  SELECT oc.CHATID
                  FROM ONLINECHAT oc
                           JOIN STUDENTSCHATSMAP sc ON oc.CHATID = sc.CHATID
                  WHERE createdAt > '3961-05-05 15:00:00'
                  GROUP BY oc.CHATID
                  HAVING COUNT(sc.STUDENTID)>=7) recent_chats_with_7_people
                  LEFT JOIN STUDENTSCHATSMAP scm ON scm.CHATID = recent_chats_with_7_people.CHATID
                  JOIN (
             SELECT * FROM STUDENT
             WHERE avgGrade > (SELECT MEDIAN(AVGGRADE) FROM STUDENT)
         ) studentsWithGoodGrades ON studentsWithGoodGrades.STUDENTID = scm.STUDENTID
                  LEFT JOIN (
             SELECT rah.STUDENTID, enteredAt-exitedAt as TIMEINROOM
             FROM (
                      SELECT * FROM ROOM
                      WHERE ROOMNAME = 'Chemistry supply room') r
                      JOIN ROOMACCESSHISTORY rah ON r.ROOMID = rah.ROOMID
             WHERE enteredAt > '3961-05-05 15:00:00' -- entered the room recently
               AND exitedAt < '3961-05-12 15:00:00'
             ORDER BY enteredAt-exitedAt
                 LIMIT 10
         ) studentsWhoEnteredTheRoomRecently ON studentsWhoEnteredTheRoomRecently.STUDENTID = scm.STUDENTID
                  LEFT JOIN(
             SELECT c.CLASSID
             FROM CLASS c
                      LEFT JOIN (
                 SELECT CLASSID
                 FROM SCHEDULE
                 WHERE day = 'Friday'
                   AND hour = 15
             ) classesSchedThatTime ON classesSchedThatTime.CLASSID = c.CLASSID
             WHERE classesSchedThatTime.CLASSID IS NULL
         ) classesNotScheduledThatDay ON classesNotScheduledThatDay.CLASSID = studentsWithGoodGrades.CLASSID
                  LEFT JOIN (
             SELECT * FROM STUDENT
             WHERE AVGGRADE > (SELECT MEDIAN(AVGGRADE) FROM STUDENT)
               AND HEIGHT > (SELECT MEDIAN(HEIGHT) FROM STUDENT)
         ) tallestStudents ON tallestStudents.STUDENTID = scm.STUDENTID
                  LEFT JOIN (
             SELECT * FROM STUDENT
             WHERE avgGrade > (SELECT MEDIAN(AVGGRADE) FROM STUDENT)
               AND name ILIKE '%m%'
        AND name ILIKE '%w%'
         ) studentsWithMAndN ON studentsWithMAndN.STUDENTID = scm.STUDENTID
         GROUP BY scm.CHATID
         HAVING COUNT(distinct studentsWithGoodGrades.STUDENTID) = 7
            AND COUNT(studentsWhoEnteredTheRoomRecently.STUDENTID) > 0
            AND COUNT(classesNotScheduledThatDay.CLASSID) > 2
            AND COUNT(tallestStudents.STUDENTID) > 1
            AND COUNT(studentsWithMAndN.STUDENTID) > 0
            AND COUNT(distinct studentsWithGoodGrades.BEDROOMID) < 6
         ORDER BY MAXTIMEINROOM
             LIMIT 1
     ) culpritChat
         JOIN STUDENTSCHATSMAP scm ON scm.CHATID = culpritChat.CHATID
         JOIN STUDENT student ON student.STUDENTID = scm.STUDENTID;