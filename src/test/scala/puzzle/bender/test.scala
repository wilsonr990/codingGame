package puzzle.bender

import org.scalatest.funsuite.AnyFunSuite

class test extends AnyFunSuite {
  test("Maze initialized correctly") {
    val world = new World()

    world.initialize(
      10,
      10,
      Array("##########",
            "#...#....#",
            "###.##.#.#",
            "#.#..###.#",
            "#.##.#...#",
            "#..#.###.#",
            "#.##...#.#",
            "#..###.#.#",
            "#........#",
            "##########"),
      1,
      1,
      8,
      8,
      1,
      Array("2 3 7 3 1")
    )

    assert(world.maze.bounds.top == 0)
    assert(world.maze.bounds.left == 0)
    assert(world.maze.bounds.right == 10)
    assert(world.maze.bounds.bottom == 10)
    assert(world.bender.location == Location(1, 1))
    assert(world.fry.location == Location(8, 8))
    assert(world.switches.contains(Location(3, 2)))
    assert(world.switches(Location(3, 2)).location == Location(3, 2))
    assert(world.switches(Location(3, 2)).field.location == Location(3, 7))
    assert(world.switches(Location(3, 2)).field.active)
  }

  test("Maze Path Calculated correctly") {
    val world = new World()

    world.initialize(
      10,
      10,
      Array("##########",
            "#...#....#",
            "###.##.#.#",
            "#.#..###.#",
            "#.##.#...#",
            "#..#.###.#",
            "#.##...#.#",
            "#..###.#.#",
            "#........#",
            "##########"),
      1,
      1,
      8,
      8,
      0,
      Array()
    )

    var path = world.calculatePath()
    assert(path.length == 14)
    assert(path == "RRDDRDDDRRDDRR")

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 5)
    assert(path == "1;DR1")
  }

  test("Maze Path Calculated correctly with a switch") {
    val world = new World()

    world.initialize(
      10,
      10,
      Array("##########",
            "#........#",
            "#.######.#",
            "#........#",
            "########.#",
            "#..#.###.#",
            "#.##...#.#",
            "#..###.#.#",
            "#........#",
            "##########"),
      3,
      3,
      8,
      3,
      1,
      Array("2 3 7 3 1")
    )

    var path = world.calculatePath()
    assert(path.length == 7)
    assert(path == "LRRRRRR")

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 5)
    assert(path == "L1;R1")
  }

  test("Maze Path Calculated correctly with garbage") {
    val world = new World()

    world.initialize(
      10,
      10,
      Array("##########",
            "#........#",
            "#.##.#...#",
            "#...+....#",
            "####.###.#",
            "#.#....#.#",
            "#.#.####.#",
            "#.#.#..#.#",
            "#........#",
            "##########"),
      3,
      3,
      5,
      3,
      0,
      Array()
    )

    var path = world.calculatePath()
    assert(path.length == 10)
    assert(path == "LLUURRRDDR")

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 10)
    assert(path == "LLUURRRDDR")
  }

  test("Maze Path Calculated correctly with garbage 2") {
    val world = new World()

    world.initialize(
      10,
      10,
      Array("##########",
            "#........#",
            "#.##.#...#",
            "#...+....#",
            "####.###.#",
            "#.#.#..#.#",
            "#.#.####.#",
            "#.#.#..#.#",
            "#........#",
            "##########"),
      3,
      3,
      5,
      3,
      0,
      Array()
    )

    var path = world.calculatePath()
    assert(path.length == 10)
    assert(path == "LLUURRRDDR")

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 10)
    assert(path == "LLUURRRDDR")
  }

  test("Maze Path Calculated correctly with garbage 3") {
    val world = new World()

    world.initialize(
      10,
      10,
      Array("##########",
            "#........#",
            "#...+#...#",
            "#.##.#...#",
            "#..#.....#",
            "####.###.#",
            "#.#.#..#.#",
            "#.#.#..#.#",
            "#........#",
            "##########"),
      2,
      4,
      5,
      4,
      0,
      Array()
    )

    var path = world.calculatePath()
    assert(path.length == 11)
    assert(path == "LUURRURDDDR")

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 11)
    assert(path == "LUURRURDDDR")
  }

  test("Maze Path Calculated correctly with garbage 4") {
    val world = new World()

    world.initialize(
      10,
      10,
      Array("##########",
            "#...+....#",
            "#.######.#",
            "#........#",
            "#########",
            "####.###.#",
            "#.#.#..#.#",
            "#.#.#..#.#",
            "#........#",
            "##########"),
      1,
      2,
      8,
      2,
      0,
      Array()
    )

    var path = world.calculatePath()
    assert(path.length == 9)
    assert(path == "DRRRRRRRU")

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 9)
    assert(path == "DRRRRRRRU")
  }

  test("Maze Path Calculated correctly complex garbage") {
    val world = new World()

    world.initialize(
      10,
      10,
      Array("##########",
            "#....#...#",
            "#.##.#.#.#",
            "#...+..#.#",
            "#.##.###.#",
            "#.#..###.#",
            "#....###.#",
            "#.######.#",
            "#........#",
            "##########"),
      3,
      3,
      6,
      3,
      3,
      Array("4 6 4 4 0", "3 5 5 3 1", "1 6 1 4 1")
    )

    var path = world.calculatePath()
    assert(path.length == 23)
    assert(path == "LLUURRRDDDDLDLLUUURRRRR")

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 21)
    assert(path == "LL1DDDDLDLLU1RR;UURRR")
  }

  test("Maze Path Calculated correctly with a tricky garbage") {
    val world = new World()

    world.initialize(
      10,
      10,
      Array("##########",
            "#........#",
            "####+###.#",
            "#........#",
            "##########",
            "##########",
            "##########",
            "##########",
            "##########",
            "##########"),
      3,
      1,
      1,
      1,
      1,
      Array("1 3 2 1 1")
    )

    var path = world.calculatePath()
    assert(path.length == 22)
    assert(path == "RDURRRRDDLLLLLLRRUULLL")

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 20)
    assert(path == "RDURRRRDD11RRUU1;LLL")
  }

  test("Maze Path Calculated correctly with a tricky garbage 2") {
    val world = new World()

    world.initialize(
      10,
      10,
      Array("##########",
            "#........#",
            "####++##.#",
            "#........#",
            "##########",
            "##########",
            "##########",
            "##########",
            "##########",
            "##########"),
      3,
      1,
      1,
      1,
      1,
      Array("1 3 2 1 1")
    )

    var path = world.calculatePath()
    assert(path.length == 22)
    assert(path == "RDURRRRDDLLLLLLRRUULLL")

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 20)
    assert(path == "RDURRRRDD11RRUU1;LLL")
  }

  test("Maze Path Calculated correctly with garbage and switch") {
    val world = new World()

    world.initialize(
      10,
      10,
      Array("##########",
            "#.........#",
            "###.######",
            "#..+.....#",
            "#.#.####.#",
            "#......#.#",
            "#.#.####.#",
            "#.#.#..#.#",
            "#........#",
            "##########"),
      2,
      3,
      5,
      3,
      1,
      Array("3 1 4 3 1")
    )

    var path = world.calculatePath()
    assert(path.length == 11)
    assert(path == "LDDRRUUUDRR")

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 11)
    assert(path == "LDDRRUUUDRR")
  }

  test("Maze Path Calculated correctly with garbage impossible") {
    val world = new World()

    world.initialize(
      10,
      10,
      Array("##########",
            "#........#",
            "#.####...#",
            "#...+....#",
            "########.#",
            "#.#....#.#",
            "#.#.####.#",
            "#.#.#..#.#",
            "#........#",
            "##########"),
      3,
      3,
      5,
      3,
      0,
      Array()
    )

    var path = world.calculatePath()
    assert(path.length == 12)
    assert(path == "LLUURRRRRDDL")

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 12)
    assert(path == "LLUURRRRRDDL")
  }

  test("Maze Path Calculated correctly with a big map") {
    val world = new World()

    world.initialize(
      21,
      21,
      Array(
        "#####################",
        "#.#.....#.......#...#",
        "#.#+###.#.#####.#+..#",
        "#.+.....#.#.....#.#.#",
        "#.#####.#.#####.#.#.#",
        "#.#...#.#.#.......#.#",
        "#..##.#.#.#######.#.#",
        "#...#.......#.#.+.#.#",
        "###.#######.#.#.#.#.#",
        "#...#...#.....#.#.#.#",
        "###.###.#.###.#.#.#.#",
        "#.....+.........#...#",
        "####+.########..###.#",
        "#.#.+.......#.#.#...#",
        "#.#.#.###.#.#.#.###.#",
        "#...#.#.....#...#...#",
        "###.#.#####.###.###.#",
        "#.............#.....#",
        "####.##.###.#.+####.#",
        "#...................#",
        "#####################"
      ),
      11,
      16,
      15,
      3,
      9,
      Array("15 4 13 1 1", "18 1 1 9 1", "1 6 17 11 1", "10 1 19 2 1", "11 13 4 11 1", "19 12 7 2 1", "11 1 17 8 1", "5 17 9 19 1", "9 11 3 7 1")
    )

    var path = world.calculatePath()
//    assert(path.length == 93)
//    assert(path.length == 83)

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 58)
  }

  test("Maze Path Calculated correctly with a big map 2") {
    val world = new World()

    world.initialize(
      21,
      21,
      Array(
        "#####################",
        "#.#.......#.#.+...#.#",
        "#.#######.#.#.#.#.#.#",
        "#.#.+.#.#.........#.#",
        "#.#.+.#.#########.#.#",
        "#...#.#.#.#.#...#.#.#",
        "###.#.#.#.#..+#.#.#.#",
        "#.........#.....#...#",
        "#.#######..#+##.###.#",
        "#.#.#.#.#.......#.#.#",
        "#.#.#.#.######....#.#",
        "#.#...#...#...#.#.#.#",
        "#.###.###.##+.#.#.#.#",
        "#...........#.#.#.+.#",
        "#.#####.#...#.#.#.#.#",
        "#.....#.#.#.#.....#.#",
        "#####.#.#.#.#####.#.#",
        "#.#.#.#.....#.#.#.#.#",
        "#.#.#.#####.#.#.#.#.#",
        "#...................#",
        "#####################"
      ),
      17,
      3,
      8,
      1,
      11,
      Array(
        "19 12 13 3 1",
        "11 7 1 11 1",
        "1 12 11 17 1",
        "5 12 9 2 1",
        "1 5 19 8 0",
        "17 4 8 17 0",
        "1 19 13 19 1",
        "11 15 14 19 0",
        "12 11 4 15 1",
        "13 1 17 18 1",
        "15 18 7 7 1"
      )
    )

    var path = world.calculatePath()
    assert(path.length == 159)
//    assert(path.length == 143)

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 81)
  }

  test("Maze Path Calculated correctly with a big map 3") {
    val world = new World()

    world.initialize(
      21,
      21,
      Array(
        "#####################",
        "#...#.....#.........#",
        "##+.#####.+#####.##.#",
        "#.#.#...#.....#...#.#",
        "#.#..##.####+.###.#.#",
        "#.....#.#...#...+...#",
        "###+#.#.#.#.###.###.#",
        "#.#...#.#.#...#.#...#",
        "#.#.#.#.#.###.#.###.#",
        "#.....#.............#",
        "#####.###.#######.#.#",
        "#.#.....#.#.#.....+.#",
        "#.#####.#.#.##.##.#.#",
        "#...#.#...#.....#...#",
        "#.#.#.##..#####.##..#",
        "#.....#.#.#...#...+.#",
        "#.###.#.#.###.###.#.#",
        "#...#...#.....#.#...#",
        "###.#.#.+#.##.#.+##.#",
        "#...................#",
        "#####################"
      ),
      5,
      7,
      18,
      1,
      11,
      Array(
        "19 17 8 9 1",
        "2 17 17 4 0",
        "17 9 3 19 0",
        "13 17 7 17 1",
        "13 7 5 10 0",
        "6 3 15 8 1",
        "11 9 4 7 1",
        "8 1 19 9 1",
        "5 13 15 9 1",
        "4 19 15 14 1",
        "13 8 9 15 1"
      )
    )

    var path = world.calculatePath()
    assert(path.length == 123)

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 69)
  }

  test("Maze Path Calculated correctly with a big map 4") {
    val world = new World()

    world.initialize(
      21,
      21,
      Array(
        "#####################",
        "#.....#.#.#.+...#.#.#",
        "#####.#.#.#.###.#.#.#",
        "#.......#.#.........#",
        "##.#.##.#.##.######.#",
        "#.....#.......#.#.#.#",
        "#####.#######.#.#.#.#",
        "#.#.................#",
        "#..###++#+##+####.#.#",
        "#...#...#.#...#.....#",
        "#+#.###.#.###.#####.#",
        "#...#.....#.#.......#",
        "###.#####.#..+###.#.#",
        "#.#.#.#...#.....#.#.#",
        "#.#.#.#+#.#####.#.#.#",
        "#.........#.........#",
        "###.###.#.#+###.#.#.#",
        "#.#.#.....#.+...#...#",
        "#.#.#####.#.###.###.#",
        "#...................#",
        "#####################"
      ),
      9,
      4,
      5,
      11,
      11,
      Array(
        "16 9 18 3 0",
        "1 18 5 1 1",
        "19 12 5 14 1",
        "19 5 14 15 1",
        "5 3 13 15 0",
        "2 1 8 11 1",
        "11 9 1 7 1",
        "9 10 5 9 0",
        "7 9 8 19 1",
        "3 3 14 13 1",
        "9 16 15 5 0"
      )
    )

    var path = world.calculatePath()
    assert(path.length == 91)

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 58)
  }

  test("Maze Path Calculated correctly with a big map 13") {
    val world = new World()

    world.initialize(
      21,
      21,
      Array(
        "#####################",
        "#.#.#.#.........#.#.#",
        "#.#.#.#.###+###.#.#.#",
        "#.......#...#...#...#",
        "#####.#.#+#.#+#.###.#",
        "#.#.#.#...#.........#",
        "#.#.#.#.#.##..#####.#",
        "#.........#.#...#...#",
        "########..#.###.###.#",
        "#.....#.#.....#.#...#",
        "#.###.#.###.#.#.###.#",
        "#.#.#.....#.......#.#",
        "#.#.####+.#######...#",
        "#...#...#.#.#.#.+.#.#",
        "###.###.#.#.#.+...#.#",
        "#...#...#.+.#.#.#...#",
        "#+#.###.#.+.#.+.###.#",
        "#.#.#...#.#.#.#.#.#.#",
        "#.#.###.#.#.#.#.#.#.#",
        "#...................#",
        "#####################"
      ),
      15,
      17,
      1,
      18,
      11,
      Array(
        "15 5 8 19 1",
        "11 16 6 11 1",
        "9 11 7 14 1",
        "13 15 7 1 1",
        "5 13 5 7 1",
        "17 1 3 18 1",
        "15 9 2 3 1",
        "4 19 13 18 0",
        "19 18 9 3 0",
        "3 7 3 19 1",
        "15 11 15 4 0"
      )
    )

    var path = world.calculatePath()
    assert(path.length == 161)

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 82)
  }

  test("Maze Path Calculated correctly with a big map 14") {
    val world = new World()
    world.initialize(
      21,
      21,
      Array(
        "#####################",
        "#.....#.#.#.#.....#.#",
        "####....#.#.#####.#.#",
        "#...#.#...+...#.#.#.#",
        "#.#.+.###.###.#.#.#.#",
        "#...#.......#.......#",
        "###.#####.#.#+#####.#",
        "#.#...#...#.#...#...#",
        "#.#.#.###.#.###.##..#",
        "#.#.#.#...#.#.#.....#",
        "#.#.#.##..#.#.###.#.#",
        "#.#.#...#.#.#.#.....#",
        "#.#.###.#.#.+.#.#.#.#",
        "#.#.#.....#.#...+...#",
        "#.#.#.###.#.###.###.#",
        "#.....#...#.......#.#",
        "#####.+##.#######.#.#",
        "#.#.......#...#.....#",
        "#.+###+##..##.#####.#",
        "#...................#",
        "#####################"
      ),
      17,
      7,
      1,
      10,
      10,
      Array(
        "1 9 16 1 1",
        "18 5 3 11 0",
        "13 18 6 19 0",
        "11 3 19 4 1",
        "7 12 16 5 1",
        "5 2 13 4 1",
        "19 14 5 11 1",
        "19 15 11 17 0",
        "19 3 1 11 1",
        "3 3 16 19 1"
      )
    )

    var path = world.calculatePath()
    assert(path.length == 121)

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 59)
  }

  test("Maze Path Calculated correctly with a big map 16") {
    val world = new World()

    world.initialize(
      21,
      21,
      Array(
        "#####################",
        "#.#...........#.#.#.#",
        "#.#####.#####.#.#.#.#",
        "#...#.+.#.#.#.......#",
        "###.#.#.#.#.#######.#",
        "#.#...#...#.#.#...#.#",
        "#.##..###.#.#.###.#.#",
        "#.#.#...#...........#",
        "#.+.###.#+#.+######.#",
        "#.#.#.#...#.....#...#",
        "#.#.#.###.#+#.#.#.#.#",
        "#.#.#...#.....#.....#",
        "#.#.#.#.#####.#####.#",
        "#.#...#.............#",
        "#.###.#.##+####.###.#",
        "#.#.#...+.....#.#...#",
        "#.#.##..#####.#.#.#.#",
        "#.#...#.#.......#...#",
        "#.###.#.##.####.###.#",
        "#...................#",
        "#####################"
      ),
      17,
      11,
      7,
      3,
      10,
      Array("10 15 5 1 1",
            "13 1 1 19 1",
            "1 8 9 1 1",
            "7 1 7 4 1",
            "19 17 5 11 1",
            "14 17 18 7 0",
            "3 7 19 11 0",
            "3 16 13 2 1",
            "11 1 3 4 0",
            "7 7 1 12 1")
    )

    var path = world.calculatePath()
    assert(path.length == 104)

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 63)
  }

  test("Maze Path Calculated correctly with a big map 24") {
    val world = new World()

    world.initialize(
      21,
      21,
      Array(
        "#####################",
        "#.#.#.....+.....#.#.#",
        "#.#..##+#.#####.#.#.#",
        "#...........#.#...#.#",
        "###########.#.##+.#.#",
        "#.#.#...#.....#.#.#.#",
        "#.#...#.#.#+#.#.#.#.#",
        "#...#.....#...#...+.#",
        "##..#####.###.###.#.#",
        "#.#...........#.....#",
        "#.######.##.#.#.###.#",
        "#.....#.......#.....#",
        "###+#.##..##..#####.#",
        "#.#.........#...#.#.#",
        "#.#+#.#####.###.#.#.#",
        "#.....#.#.....#...#.#",
        "#####.#.#####.#.#.#.#",
        "#...#.#.......#.....#",
        "###.#.#######.#.###.#",
        "#...................#",
        "#####################"
      ),
      4,
      9,
      5,
      1,
      11,
      Array(
        "12 19 3 19 1",
        "13 7 5 17 1",
        "7 3 6 9 1",
        "17 17 9 15 1",
        "11 1 7 1 1",
        "8 13 16 11 0",
        "3 5 13 15 0",
        "9 17 13 1 1",
        "2 19 13 17 1",
        "9 19 11 7 0",
        "5 7 17 9 0"
      )
    )

    var path = world.calculatePath()
    assert(path.length == 27)

    world.comprise = true
    path = world.calculatePath()
    assert(path.length == 25)
  }

  test("Test Compresion only") {
    val world = new World()

    var comprised = world.comprisePath("RRRRRRRRRRRRR")
    assert(comprised == "1;R1")
    comprised = world.comprisePath("RRDDRDDDRRDDRR")
    assert(comprised == "RR1D1R1R;DDR")
    comprised = world.comprisePath("UUULLUUULLUUULRLLLLLLLLL")
    assert(comprised == "2L2L2R1;LLL1;UUUL")
    val string = "UUU[L]UUULLUUULLLLLLLL"
    comprised = world.comprisePath(string)
    assert(comprised == "11212222;UUU;LL")
    comprised = world.comprisePath(string, useOptions = false)
    assert(comprised == "22121111;LL;UUU")
  }
}
