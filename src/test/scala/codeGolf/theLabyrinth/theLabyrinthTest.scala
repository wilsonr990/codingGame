package codeGolf.theLabyrinth

import org.scalatest.funsuite.AnyFunSuite

class theLabyrinthTest extends AnyFunSuite {
  test("Maze initialized correctly") {
    val world = new M(10, 50, 2)

    assert(world.d.b == 0)
    assert(world.d.a == 0)
    assert(world.d.c == 50)
    assert(world.d.d == 10)
    assert(world.e == 2)
    assert(world.f.isEmpty)
  }

  test("Maze first update works correctly") {
    val world = new M(10, 30, 23)

    val inputLines = List(
      "??????????????????????????????",
      "????.....?????????????????????",
      "????#####?????????????????????",
      "????..T..?????????????????????",
      "????.....?????????????????????",
      "????#####?????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????"
    )

    world.p(B(3, 6), inputLines)

    assert(world.l == B(3, 6))
    assert(world.g == B(3, 4))
    assert(world.k.size == 3)
    assert(world.k(1) == B(3, 5))
    assert(world.f(B(3, 6)).a)
    assert(world.f(B(3, 6)).asInstanceOf[J].b.size == 4)
    assert(world.f(B(3, 6)).asInstanceOf[J].b.contains(B(2, 6)))
    assert(world.f(B(3, 6)).asInstanceOf[J].b.contains(B(3, 5)))
    assert(world.f(B(3, 6)).asInstanceOf[J].b.contains(B(3, 7)))
    assert(world.f(B(3, 6)).asInstanceOf[J].b.contains(B(4, 6)))

    assert(!world.f(B(2, 6)).a)
    assert(world.f(B(2, 6)).isInstanceOf[H])

    assert(world.f(B(3, 5)).a)
    assert(world.f(B(3, 5)).asInstanceOf[J].b.size == 4)
    assert(world.f(B(3, 5)).asInstanceOf[J].b.contains(B(2, 5)))
    assert(world.f(B(3, 5)).asInstanceOf[J].b.contains(B(3, 4)))
    assert(world.f(B(3, 5)).asInstanceOf[J].b.contains(B(3, 6)))
    assert(world.f(B(3, 5)).asInstanceOf[J].b.contains(B(4, 5)))

    assert(!world.f(B(2, 5)).a)
    assert(world.f(B(2, 5)).isInstanceOf[H])

    assert(world.f(B(3, 4)).a)
    assert(world.f(B(3, 4)).asInstanceOf[J].b.size == 4)
    assert(world.f(B(3, 4)).asInstanceOf[J].b.contains(B(2, 4)))
    assert(world.f(B(3, 4)).asInstanceOf[J].b.contains(B(3, 3)))
    assert(world.f(B(3, 4)).asInstanceOf[J].b.contains(B(3, 5)))
    assert(world.f(B(3, 4)).asInstanceOf[J].b.contains(B(4, 4)))

    assert(!world.f(B(2, 4)).a)
    assert(world.f(B(2, 4)).isInstanceOf[H])

    assert(!world.f(B(3, 3)).a)
    assert(world.f(B(3, 3)).isInstanceOf[L])

    assert(world.q() == "LEFT")
  }

  test("Maze consecutive update works correctly") {
    val world = new M(10, 30, 23)

    val inputLines = List(
      "??????????????????????????????",
      "????.....?????????????????????",
      "????#####?????????????????????",
      "????..T..?????????????????????",
      "????.....?????????????????????",
      "????#####?????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????"
    )

    world.p(B(3, 6), inputLines)

    assert(world.k(1) == B(3, 5))
    assert(world.q() == "LEFT")

    val inputLines2 = List(
      "??????????????????????????????",
      "????.....?????????????????????",
      "???.#####?????????????????????",
      "???#..T..?????????????????????",
      "???......?????????????????????",
      "???######?????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????"
    )
    world.p(B(3, 5), inputLines2)

    assert(world.l == B(3, 5))
    assert(world.g == B(4, 3))
    assert(world.k.size == 4)
    assert(world.k(1) == B(3, 4))
    assert(world.q() == "LEFT")
  }

  test("Maze find the control cell works correctly") {
    val world = new M(10, 30, 23)

    var inputLines = List(
      "??????????????????????????????",
      "????.....?????????????????????",
      "????#####?????????????????????",
      "????..T..?????????????????????",
      "????.....?????????????????????",
      "????#####?????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????"
    )

    world.p(B(3, 6), inputLines)

    assert(world.k(1) == B(3, 5))
    assert(world.q() == "LEFT")

    inputLines = List(
      "??????????????????????????????",
      "????.....?????????????????????",
      "???.#####?????????????????????",
      "???#..T..?????????????????????",
      "???......?????????????????????",
      "???######?????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????"
    )
    world.p(B(3, 5), inputLines)

    assert(world.k(1) == B(3, 4))
    assert(world.q() == "LEFT")

    inputLines = List(
      "??????????????????????????????",
      "????.....?????????????????????",
      "??#.#####?????????????????????",
      "??##..T..?????????????????????",
      "??#......?????????????????????",
      "??#######?????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????"
    )
    world.p(B(3, 4), inputLines)

    assert(world.k(1) == B(3, 5))
    assert(world.q() == "RIGHT")

    inputLines = List(
      "??????????????????????????????",
      "????.....?????????????????????",
      "??#.#####?????????????????????",
      "??##..T..?????????????????????",
      "??#......?????????????????????",
      "??#######?????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????"
    )
    world.p(B(3, 5), inputLines)

    assert(world.k(1) == B(3, 6))
    assert(world.q() == "RIGHT")

    inputLines = List(
      "??????????????????????????????",
      "????.....?????????????????????",
      "??#.#####?????????????????????",
      "??##..T..?????????????????????",
      "??#......?????????????????????",
      "??#######?????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????"
    )
    world.p(B(3, 6), inputLines)

    assert(world.k(1) == B(3, 7))
    assert(world.q() == "RIGHT")

    inputLines = List(
      "??????????????????????????????",
      "????.....?????????????????????",
      "??#.######????????????????????",
      "??##..T...????????????????????",
      "??#......C????????????????????",
      "??########????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????"
    )
    world.p(B(3, 7), inputLines)

    assert(world.l == B(3, 7))
    assert(world.g == B(4, 9))
    assert(world.k.length == 4)
    assert(world.k(1) == B(3, 8))
    assert(world.q() == "RIGHT")

    inputLines = List(
      "??????????????????????????????",
      "????.....?????????????????????",
      "??#.#######???????????????????",
      "??##..T....???????????????????",
      "??#......C.???????????????????",
      "??#########???????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????"
    )
    world.p(B(3, 8), inputLines)

    assert(world.l == B(3, 8))
    assert(world.g == B(4, 9))
    assert(world.k.length == 3)
    assert(world.k(1) == B(3, 9))
    assert(world.q() == "RIGHT")

    inputLines = List(
      "??????????????????????????????",
      "????.....?????????????????????",
      "??#.########??????????????????",
      "??##..T....#??????????????????",
      "??#......C.#??????????????????",
      "??##########??????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????"
    )
    world.p(B(3, 9), inputLines)

    assert(world.l == B(3, 9))
    assert(world.g == B(4, 9))
    assert(world.k.length == 2)
    assert(world.k(1) == B(4, 9))
    assert(world.q() == "DOWN")
  }

  test("Maze return to start cell works correctly") {
    val world = new M(10, 30, 23)

    val inputLines = List(
      "??????????????????????????????",
      "????.....?????????????????????",
      "??#.########??????????????????",
      "??##..T....#??????????????????",
      "??#......C.#??????????????????",
      "??##########??????????????????",
      "???????#####??????????????????",
      "??????????????????????????????",
      "??????????????????????????????",
      "??????????????????????????????"
    )
    world.p(B(4, 9), inputLines)

    assert(world.l == B(4, 9))
    assert(world.g == B(3, 6))
    assert(world.k.length == 5)
    assert(world.k(1) == B(3, 9))
    assert(world.q() == "UP")
  }
}
